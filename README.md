Introduction
============
The goal is to perform efficiant distance measurements among a swarm of 
UWB tag, so to manage UAV flight.
The current code is targetting the DWM1000-dev board, but should be easily
portable to other hardware with embedding a DW1000 chip.

It requires [Zephyr](https://www.zephyrproject.org/) at least version 3.4,
previous versions are not supported anymore due to migrating to the
new: GPIO API, device-tree format, timeout API, kernel work queue,
device-tree macro, zephyr-prefixed headers, pinctrl, spi_config structure.


Understanding the Hardware
==========================
* The DWM1001-dev is a development board, it means it is ready to use
  and include all the necessary hardware components: power supply
  (battery or USB), JTAG debugger (JLINK), a serial line mapped to USB,
  some LEDs and buttons, and of course the DWM1001C

* The DWM1001C is a module which include the following hardware,
  already wired together, DW1000 (the UWB chip), NRF528320 (an ARM controller),
  a ceremic antenna (for bluetooth capability provided by the NRF52832) and
  a PCB antenna (for the DW1000)

* The maximum SPI communication speed of the DW1000 is 20MHz.
  Listed below is the maximum compatible communication speed for 
  various micro controllers:
  
  | MCU        | Device   | SPI    |
  |------------|----------|--------|
  | STM32 F405 | Crazyfly | 20 MHz |
  | NRF 52820  | DWM1001  |  8 MHz |
  | NRF 52840  |          | 16 MHz |
  

Installation
============

Operating system dependencies
-----------------------------

### FreeBSD
Put in the `~/.zephrrc`
~~~sh
export ZEPHYR_TOOLCHAIN_VARIANT=gnuarmemb
export GNUARMEMB_TOOLCHAIN_PATH=/usr/local/gcc-arm-embedded
export HOSTCC=cc

# If not already specified in current environement
export PATH=/usr/local/bin:$PATH:$HOME/.local/bin
~~~
and install the following packages:
* sysutils/dtc
* devel/gcc-arm-embedded
* lang/rust (only for installing zephyr requirements with pip)

As another version of `dtc` is also part of the base system, 
it could be necessary to alter the `PATH` environment variable so that
`/usr/local/bin` is lookup before `/usr/bin` (ex: `PATH=/usr/local/bin:$PATH`).


### Linux
Put in the `~/.zephyrrc`
~~~sh
export ZEPHYR_TOOLCHAIN_VARIANT=gnuarmemb
export GNUARMEMB_TOOLCHAIN_PATH=/usr/
~~~
To install the necessary dependecies, refer to:
https://docs.zephyrproject.org/latest/getting_started/installation_linux.html
also install the following packages:
* gcc-arm-none-eabi
* openocd

It's also possible to install Zephyr Software Development Toolchain instead of the arm gcc embedded (you will need to change the `~/.zephyrrc`)
https://docs.zephyrproject.org/latest/getting_started/index.html#install-software-development-toolchain


West
----
~~~sh
PATH=${PATH}:${HOME}/.local/bin   # Ensure pip installed binaries are availables
pip install --user -U west        # Install west using pip (python 3)
~~~

Zephyr, dependencies, and modules
---------------------------------
Use a T3 (Forest topology) configuration to install Zephyr
( https://docs.zephyrproject.org/latest/guides/west/repo-tool.html )

1. Create and move to the root of your working projects
~~~sh
mkdir ZephyrProjects
cd ZephyrProjects
~~~

2. Manually initialise west for the T3 configuration
~~~sh
mkdir .west
cat > .west/config <<EOT
[manifest]
path = manifest-repo
[zephyr]
base = zephyr
EOT
~~~

3. Create the manifest
~~~sh
mkdir manifest-repo
cat > manifest-repo/west.yml <<EOT
manifest:
  remotes:
    - name: zephyrproject-rtos
      url-base: https://github.com/zephyrproject-rtos
    - name: inria-dalu
      url-base: git@gitlab.inria.fr:dalu
    - name: gitlab-sdalu
      url-base: https://gitlab.com/sdalu

  defaults:
    remote: inria-dalu

  projects:
    - name: zephyr
      remote: zephyrproject-rtos
      revision: v3.5.0
      import: true
    - name: zscilib
      remote: zephyrproject-rtos
    - name: zephyr-utils
      remote: gitlab-sdalu
      path: modules/utils
      revision: v0.2.4
    - name: decawave-drivers
      path: modules/decawave-drivers
      revision: v0.9.5
    - name: spank
      path: spank
      revision: v0.5.5
    - name: spank-extio
      path: spank-extio
      revision: v1.3.0
EOT
~~~

4. Import/Update all the repositories present in the manifest
~~~sh
west update
~~~

5. Ensure that you have all the python dependencies for zephyr
   (use of `TMPDIR` environment variable can be necessary
    if `/tmp` is short in space)
~~~sh
pip install --user -r zephyr/scripts/requirements.txt
~~~

### Redskin (this repository)

Clone this repository in the root of the zephyrproject
~~~sh
# Clone the zephyr-redskin repository
git clone --recurse-submodules git@gitlab.inria.fr:dalu/zephyr-redskin
~~~


Setting the build environment
=============================
You can use the provided helper script `helpers/zephyr-term.sh` to start a
terminal with the necessary environment, this script will also print a summary
of the commands to run as a friendly reminder:
~~~sh
sh helpers/zephyr-term.sh    # Open in the current terminal
sh helpers/zephyr-term.sh -x # Open in a new windows
~~~

Or go the long way:
~~~sh
# west need to be directly accessible without fullpath.
# So due to installation using pip, PATH need to be modified
# You can avoid typng this everytime by adding it to your shell rc-file
# or in the .zephyrrc
PATH=${PATH}:${HOME}/.local/bin

# Move to the root of the zephyr projects 
cd ZephyrProjects

# Source zephyr environment
source zephyr/zephyr-env.sh

# Change to the application directory
cd zephyr-redskin
~~~

If you have a preference for classical `Makefile`:
~~~sh
# Configure west to use Makefile (optional, once)
west config build.generator="Unix Makefiles"
~~~

Configuration
=============

You need to alter the `spank_neighbours_table.c` to reflect the
UAVs/Anchors configuration/topology using the macros
`SPANK_NODE_DEF_UAV` and/or `SPANK_NODE_DEF_ANCHOR` to respectively
define UAV and anchor. The macro take one argument, the MAC address of
the UWB interface.

~~~sh
# Update the file spank_neighbours_table.c to reflect 
# the UAVs/Anchors configuration
emacs redskin/src/spank_neighbours_table.c
~~~


The MAC address can be obtained once the firmware is running and you are
connected to the device terminal using `info spank` command and looking
at the field `MAC addr`.

> ~~~
> uart:~$ info spank
> Version    : v0.5.3
> Git commit : 6cbb3e6f043fa00a014ddc9a51cfbda9135bc806
> MAC addr   : d341:5073:7e94:8b48
> Ticks/sec  : 32768 (1ms ~ 33 ticks)
> Max ticks  : 0x7fffffffffffffff
> Behaviour  : REP- [Ranging|Electable|Pinned|Static]
> Size packed: <position:6>, <velocity:6>, <stddev_xyz:6>, <posture:24>
> Sniffer    : frame=off, packet=off
> ~~~
>
> So the MAC address for this device will be coded as: `0xd34150737e948b48`


If it is your first build, just use some dummy addresses the time necessary 
to retrieve the real ones.


Compiling
=========

~~~sh
# Build redskin application (with i2c and debug) on the DWM1001 board
west build -b decawave_dwm1001_dev redskin                          \
           -S i2c-slave -S debug                                    \
           --                                                       \
           -DDTS_ROOT=/path/to/zephyr-redskin                       \
		   -DDTC_OVERLAY_FILE="boards/decawave_dwm1001_dev.overlay

# Which can be simplified using the recommanded do-build script
./do-build -b decawave_dwm1001_dev -F i2c-slave,debug redskin
~~~


Flavors
-------
Using the `do-build` script, it is easy to customize the firmware:

| Features  | Description                          |
|-----------|--------------------------------------|
| i2c-slave | SPANK ExtIO on I2C                   |
| usb       | SPANK ExtIO on USB                   |
| sniffer   | Frame and packet sniffing            | 
| pin-reset | Use dedicated GPIO as hardware reset |
| debug     | Enable full debug <br/> (file `snippets/debug/debug.conf` may requires adjustment) |

Note, that it is not possible to build all flavors on all boards, for example
the `decawave_dwm1001_dev` board is not able to handle USB.

A build example for the `nrf52840_mdk` where SPANK ExtIO is available
on I2C and USB, with sniffer, pin reset and full debug support enabled:
~~~sh
./do-build -b nrf52840_mdk -F i2c-slave,sniffer,usb,pin-reset,debug redskin
~~~


Debug Support
-------------
The snippet `debug` will allow you to turn a lot of the debugging
features present in Zephyr and in SPANK. This will have a **HUGE** impact on
the code size as well as the stack usage.
The stack usage increase has *normally* been taken into account in this snippet,
but if you have custom threads/workqueues/... you will need to adjust for them.

When the debugger is connected it is possible to reboot the board by calling
the `sys_reboot` function, for example in `gdb`: `call sys_reboot(1)`


Flashing
========

~~~sh
# It is possible to flash using JLink, PyOCD, and OpenOCD
# On FreeBSD it is recommanded to flash application using OpenOCD
# which can require the master version for the NRF52 chip used by DWM1001
#  See: https://sourceforge.net/p/openocd/code/
west flash --runner openocd --openocd /opt/openocd/bin/openocd
~~~


Using the firmware
==================
* It is possible to control spank and accelerometer from the command line
  using `cu` or `minicom` line speed is defined to 115200 baud.
  (example on FreeBSD: `cu -s 115200 -l /dev/cua0`)
* it's possible to monitor the activity of spank (blue led) and 
  accelerometer (red led) thanks to the led blinking
* button 2 behaviour:
  * dblclick      : start spank
  * dblclick-long : stop spank
* if debugger is connected and active spank is not started automatically


Shell
-----
If the various support have been compiled in (`CONFIG_REDSKIN_SHELL=y`), the
following commands are available:
~~~sh
# Control SPANK behaviour
spank start|stop             # Start or stop the protocol
spank config                 # Show or set configuration parameters

# Navigation control
spank navctrl                # Allow sending navgiation control packets

# Sniffer packet mode (Need: CONFIG_SPANK_SNIFFER_PACKET=y)
spank sniffer packet         # Show/set filtering for dumping packets
spank sniffer packet <0x....|all|none|elect|navctrl|msg|
                             twr|twr-poll|twr-answer|twr-final|twr-report>

# Sniffer frame mode (Need: CONFIG_SPANK_SNIFFER_FRAME=y)
spank sniffer frame         # Show/set frame dumping status
spank sniffer frame <on|off>
spank sniffer frame output  # Show/set enabled output backend
spank sniffer frame output <0x....|all|none|console|extio-usb>

# Reporting internal data (Need: CONFIG_SPANK_REPORT=y)
spank report                 # Show/set filtering for reporting packets
spank report <0x....|all|none|twr-distance|state>

# Syslog (Need: CONFIG_SPANK_SYSLOG_RUNTIME=y)
spank syslog                 # Show/set log level for SPANK susbystem
spank syslog <none|fatal|error|warning|info|debug>

# UAV (Need: CONFIG_SPANK_UAV_PUPPET=y)
uav posture                  # Show/set UAV posture
uav posture <position> <stddev> <velocity> <stddev>

# Accelerometer (Need: CONFIG_REDSKIN_ACCELEROMETER)
accelerometer start|stop
accelerometer config <frequency> <scale>

# ExtIO (Need: CONFIG_REDSKIN_EXTIO=y)
extio i2c-reset              # Reset I2C bus used for ExtIO

# System operations
sys reboot                   # Perform cold reboot
~~~


USB interface
-------------

_Control transfer_ can be used to perform nearly the same operations as the 
ones available for I2C. Using _control transfer_ only, it will be necessary 
to actively pool for `DATA_READY`, and request data if present.

It is also possible to use _interrupt transfer_, in this case the device
initiate a transfer every time a new data (distance measurement) arrives,
consuming it. If the host is not ready to read the data, it will be lost.


I2C interface
-------------

The data-ready interrupt can be detected on the  **rising edge** (simpler)
or at **high level** (not necessary supported by all hardware) as the
data-ready line will be de-asserted only after a read of the I2C
`DATA_READY` register (meaning user has acknowledged the interrupt).

Note that the data-ready line is only asserted when a new data arrives
(meaning it won't be asserted again if some pending data remains, so
user is advised to read all the pending data).

The full description of the I2C is documented in the 
[spank-extio](https://gitlab.inria.fr/dalu/spank-extio) repository which also
provide a high-level API to control the board. Below a summary of the
available I2C registers:

|      | Register               | Description                               | Clock stretching |
|------|------------------------|-------------------------------------------|:----------------:|
| `RW` | `CONFIG_BEHAVIOUR`     | How to drop data (Not implemented)        | ✔                |
| `RW` | `CONFIG_REPORT`        | List of events reported with interruption |                  |
| `RO` | `DATA_READY`           | List of data ready events                 |                  |
| `RW` | `POSTURE`              | Posture                                   | ✔                |
| `RO` | `DISTANCE_MEASUREMENT` | Distance measurement                      | ✔                |
| `RO` | `VERSION`              | Firmware version                          |                  |
| `RO` | `SPANK_VERSION`        | Spank version                             |                  |

### DWM1001

* since Zephyr 3.2, a reset of the board will not correctly initialize 
  I2C device, it is necessary to power off / power on the card.

### Raspberry PI

If connecting to a Raspberry PI, keep in mind that:
* the IRQ detection from user space is slow, something between [25µs to 75µs](https://sites.google.com/site/hamlinhomeprojects/projects/raspberry-pi-isr)
* the Linux `BCM2835` driver (due to hardware?) is [not able to support more than one read message per I2C transfer](https://elixir.bootlin.com/linux/v5.7.8/source/drivers/i2c/busses/i2c-bcm2835.c#L350), so it won't be possible to resquest extra information after reading a register value (as it should have been possible with `DISTANCE_MEASUREMENT` register).
* there is a **bug** in the `BCM2835` hardware impacting the [I2C clock-stretching](https://elinux.org/BCM2835_datasheet_errata#p35_I2C_clock_stretching), this can result in failed I2C transfer, though our firmware will try it's best to recover from it and avoid bus outage. But it is more than likely than due to this bug you will need to switch to bit-banging (aka: i2c-gpio driver).

I2C configuration is done in `/boot/config.txt` 
(for Ubuntu it seems you need to use `/boot/firmware/config.txt` instead), 
you will need to **choose one of the fragment** below according to your
desired implementation:
~~~conf
# I2C native (at 100kHz) [ /dev/i2c-1 ]
dtparam=i2c_arm=on,i2c_arm_baudrate=100000
~~~

~~~conf
# I2C bit-banging (at arround ~100Khz) [ /dev/i2c-? ]
# Note: RPI has hard-wired pull-up resistor for it's I2C pin
dtparam=i2c_arm=off
gpio=2-3=np,dh
dtoverlay=i2c-gpio,i2c_gpio_sda=2,i2c_gpio_scl=3,i2c_gpio_delay_us=2
~~~

If you want to keep full compatibility with ioctl-based programs
when using `i2c-gpio`, you will need to create a symlink
from `i2c-?` (you need to check with the operating system the
name that has been assigned to the device) to `i2c-1`, 
it can be done with a udev rule,
for example in `/etc/udev/rules.d/99-i2c-bit-bang.rules`:
~~~
KERNEL=="i2c-?", SYMLINK+="i2c-1"
~~~

Notes
=====

* It is not advised to simultaneously use several interfaces (I2C, USB)
  at the same time, even if great care has been taken, some inconsistency
  can happen. For example the `DATA_READY`value can be wrong.

* Sending too many output to the console (spank syslog/report/sniffer)
  will likely break the SPANK protocol due to multiple timeout as
  not enough CPU time is left. 
  If possible try to limit yourself to: `spank report twr-distance`


Documentation
=============
* DWM1001-Dev: [datasheet][1], [schematic][2], [errata][3]
* DWM1001C: [datasheet][4], [schematic][5]
* DW1000: [user manual][6], [datasheet][7], [errata][8]
* NRF52832: [product specification][9], [errata][10]
* Zephyr: [documentation][11]


Development
===========

IDE support
-----------
Support for generating `compile_commands.json` has been added to
`CMakeLists.txt`, so it will be available once have build the
project (generally using `west`).

Support for Emacs is provided as `.dir-locals.el` that will be automatically
processed when editing files. Note that you are still require to have
`flycheck` enabled as well as the `flycheck-cmake` extension.
See: `.dir-locals.el` for more information.

Version information
-------------------
When building from git repositories, it is advised to install
[`version-export`](https://gitlab.com/sdalu/version-export) and have
it accessible from your `PATH`. It will allow the build process to
provide version information (otherwise version will be set to `<n/a>`). 
If `redskin` is compiled with shell support, this version information
can be displayed with the `info` command, and if ExtIO support is enable 
it is will also be available through I2C commands.


Debugging
---------

~~~sh
# Identify problem using r15/pc register
arm-none-eabi-addr2line -e build/zephyr/zephyr.elf __addr__
~~~

Misc notes on git
-----------------
~~~sh
git show-ref --tags		        # List tags index
~~~





[1]: https://www.decawave.com/wp-content/uploads/2019/01/DWM1001-DEV_Datasheet-1.2.pdf
[2]: https://www.decawave.com/wp-content/uploads/2018/08/dwm1001-dev_schematic.pdf
[3]: https://www.decawave.com/wp-content/uploads/2019/08/DWM1001-DEV_Errata.pdf
[4]: https://www.decawave.com/wp-content/uploads/2019/02/DWM1001_Datasheet.pdf
[5]: https://www.decawave.com/wp-content/uploads/2019/01/DWM1001_Module_Schematics_vD1.21.pdf
[6]: https://www.decawave.com/wp-content/uploads/2019/07/DW1000-User-Manual-1.pdf
[7]: https://www.decawave.com/wp-content/uploads/2020/04/DW1000_Datasheet.pdf
[8]: https://www.decawave.com/wp-content/uploads/2018/09/dw1000_errata_v1.1.pdf
[9]: https://infocenter.nordicsemi.com/pdf/nRF52832_PS_v1.4.pdf
[10]: https://infocenter.nordicsemi.com/pdf/nRF52832_Rev_2_Errata_v1.4.pdf
[11]: https://docs.zephyrproject.org/latest/
