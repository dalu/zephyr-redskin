#!/bin/sh
#
# Copyright (c) 2019,2024
# Stephane D'Alu, Inria Chroma / Inria Agora, INSA Lyon, CITI Lab.
#
# SPDX-License-Identifier: Apache-2.0
#

# Using west configuration to:
#
# * Set the board:
#   west config build.board decawave_dwm1001_dev
#
# * Use Makefile:
#   west config build.generator --global "Unix Makefiles"
#
# * Flash using an alternative version of OpenOCD
#   west config build.cmake-args -- "-DBOARD_FLASH_RUNNER=openocd -DOPENOCD=/opt/openocd/bin/openocd"
#


		
cat <<EOF
A small remainder of the commands to run:
(see: https://docs.zephyrproject.org/latest/guides/west/build-flash-debug.html)

* to build the firmware, prefer the provided "do-build" script 
  which correctly populate the DTC_OVERLAY_FILE and OVERLAY_CONFIG for "west"
./do-build -b decawave_dwm1001_dev -F i2c-slave,pin-reset,debug     redskin
./do-build -b nrf52840_mdk         -F usb,pin-reset,debug           redskin

* to perform extra kernel configuration:
west build -t menuconfig

* to flash the firmware:
west flash --runner openocd --openocd /opt/openocd/bin/openocd

* to connect to the console using either cu or screen:
  (note: speed is 230400 for the nrf52840_mdk)
cu -s 115200 -l /dev/cua?
screen /dev/cua? 115200

EOF

