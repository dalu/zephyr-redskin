#!/bin/sh
#
# Copyright (c) 2019,2022
# Stephane D'Alu, Inria Chroma / Inria Agora, INSA Lyon, CITI Lab.
#
# SPDX-License-Identifier: Apache-2.0
#

while getopts 'x' c
do
    case $c in
	x) xwin=yes ;;
    esac
done

shift $((OPTIND-1))


# If not specified, guess our base directory
if [ -z "${ZEPHYR_REDSKIN}" ]; then
    selfdir="$(cd -- "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P)"
    export ZEPHYR_REDSKIN=`dirname "${selfdir}"`
fi

# Ensure Zephyr environment is sourced or will be sourced
bash_source=true
if [ -z "${ZEPHYR_BASE}" ]; then
    zephyr_env="${ZEPHYR_REDSKIN}/../zephyr/zephyr-env.sh"
    if [ -e "${zephyr_env}" ]; then
	bash_source="source ${zephyr_env}"
    else
	cat <<EOF
\$ZEPHYR_BASE environment variable is undefined !
    
=> Ensure the Zephyr environment has been sourced (ie: source zephyr-env.sh)
   READ: https://docs.zephyrproject.org/latest/guides/env_vars.html

EOF
	exit 1
    fi
fi

# Check for west
if type west > /dev/null 2> /dev/null; then
    false # dummy
elif [ -e $HOME/.local/bin/west ]; then
    PATH=${PATH}:${HOME}/.local/bin
else
    cat <<EOF 
The 'west' script need to be accessible from the \$PATH environment variable!

=> Adjust your \$PATH variable before calling this script.

EOF
    exit 1
fi

# Build bash command
bash_help="sh ${ZEPHYR_REDSKIN}/helpers/cmds-help.sh"
bash_cmd="${bash_source} ; ${bash_help} ; exec bash"

# Check for terminal support
if [ "${xwin}" = "yes" ]; then
    if type gnome-terminal > /dev/null 2> /dev/null; then
	echo "Launching gnome terminal..."
	gnome-terminal --working-directory="${ZEPHYR_REDSKIN}" -- \
		       bash -c "${bash_cmd}"
    elif type xterm > /dev/null 2> /dev/null; then
	echo "Launching xterm..."
	( cd "${ZEPHYR_REDSKIN}" && xterm -T "Zephyr" -e bash -c "${bash_cmd}" ) &
    else
	( cd "${ZEPHYR_REDSKIN}" && bash -c "${bash_cmd}" )
	echo "Helper environment has been closed"
    fi
else
    ( cd "${ZEPHYR_REDSKIN}" && bash -c "${bash_cmd}" )
    echo "Helper environment has been closed"
fi
