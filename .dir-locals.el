;;; Directory Local Variables
;;; For more information see (info "(emacs) Directory Variables")
;;
;;; You can use M-x add-dir-local-variable to add more variables
;;;

;; Flycheck is required
;; If not already available or packaged by your distribution, you
;; you can use MELPA ( See: https://melpa.org/#/getting-started )
;; and do: M-x package-install RET flycheck RET
;;
;; The following modules are not available in MELPA and will need to be
;; installed manually if no packages are provided by the distribution
;; * https://github.com/xwl/flycheck-cmake
;; * https://github.com/xwl/cmake-compile-commands
;;
;; Checker is forced to `c/c++-cmake`.
;; The `irony` checker seems to have problems getting definition of
;; NULL, true, false even if includes are provided
;;
((c-mode
  (flycheck-checker . c/c++-cmake)
  (eval setq cmake-compile-commands-build-directories
	(list
	 (concat
	  (expand-file-name
	   (locate-dominating-file default-directory ".dir-locals.el"))
	  "build")))))


