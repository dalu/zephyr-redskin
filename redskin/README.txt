
NRF peripheric
I2C0 and SPI0 can't coexist
I2C1 and SPI1 can't coexist

Zephyr
Actually doesn't support I2C slave mode on nrf52832,
so it is necessary to directly use nordic sdk
https://github.com/zephyrproject-rtos/zephyr/issues/21445


VID / PID:
https://pid.codes/
http://wiki.openmoko.org/wiki/USB_Product_IDs
https://github.com/obdev/v-usb/blob/master/usbdrv/USB-IDs-for-free.txt
