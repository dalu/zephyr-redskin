#include <lvgl.h>

#include "gui/init.h"
#include "../font/font.h"

int
gui_init(void)
{

    lv_theme_t *theme = theme_dry_init(
            LV_THEME_DEFAULT_COLOR_PRIMARY,
	    LV_THEME_DEFAULT_COLOR_SECONDARY,
	    0,
	    LV_THEME_DEFAULT_FONT_SMALL,
	    LV_THEME_DEFAULT_FONT_NORMAL,
	    LV_THEME_DEFAULT_FONT_SUBTITLE,
	    LV_THEME_DEFAULT_FONT_TITLE);
    
    lv_theme_set_act(theme);

    return 0;
}
