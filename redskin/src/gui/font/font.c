#include <lvgl.h>
#include "font.h"

void *lv_theme_default_font_normal_custom_ptr   = &font_unscii_8;
void *lv_theme_default_font_title_custom_ptr    = &lv_font_montserrat_12;
void *lv_theme_default_font_subtitle_custom_ptr = &font_unscii_8;
void *lv_theme_default_font_small_custom_ptr    = &font_unscii_8;
