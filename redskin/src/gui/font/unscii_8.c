#include <lvgl.h>

/* See: https://github.com/littlevgl/lv_font_conv/blob/master/doc/font_spec.md
 */

#ifndef FONT_UNSCII_8
#define FONT_UNSCII_8 1
#endif

#if FONT_UNSCII_8

/* Store the image of the glyphs
 */
static LV_ATTRIBUTE_LARGE_CONST uint8_t glyph_bitmap[] = 
{
  /* Unicode: U+0020 ( ) [SPACE], Size: 8x8
   * Glyph (bounding: <x: 0, y: 0, width: 0, height: 0>):
   *   ........
   *   ........
   *   ........
   *   ........
   *   ........
   *   ........
   *   ........
   *   ........
   * Encoded data: offset=0, size=0
   */
  /* <no data> */

  /* Unicode: U+0021 (!) [EXCLAMATION MARK], Size: 8x8
   * Glyph (bounding: <x: 3, y: 0, width: 2, height: 7>):
   *   ...##...
   *   ...##...
   *   ...##...
   *   ...##...
   *   ...##...
   *   ........
   *   ...##...
   *   ........
   * Encoded data: offset=0, size=2
   */
  0xff, 0xcc,

  /* Unicode: U+0022 (") [QUOTATION MARK], Size: 8x8
   * Glyph (bounding: <x: 1, y: 0, width: 6, height: 3>):
   *   .##..##.
   *   .##..##.
   *   .##..##.
   *   ........
   *   ........
   *   ........
   *   ........
   *   ........
   * Encoded data: offset=2, size=3
   */
  0xcf, 0x3c, 0xc0,

  /* Unicode: U+0023 (#) [NUMBER SIGN], Size: 8x8
   * Glyph (bounding: <x: 0, y: 0, width: 7, height: 7>):
   *   .##.##..
   *   .##.##..
   *   #######.
   *   .##.##..
   *   #######.
   *   .##.##..
   *   .##.##..
   *   ........
   * Encoded data: offset=5, size=7
   */
  0x6c, 0xdb, 0xfb, 0x6f, 0xed, 0x9b, 0x00,

  /* Unicode: U+0024 ($) [DOLLAR SIGN], Size: 8x8
   * Glyph (bounding: <x: 1, y: 0, width: 6, height: 7>):
   *   ...##...
   *   ..#####.
   *   .##.....
   *   ..####..
   *   .....##.
   *   .#####..
   *   ...##...
   *   ........
   * Encoded data: offset=12, size=6
   */
  0x31, 0xfc, 0x1e, 0x0f, 0xe3, 0x00,

  /* Unicode: U+0025 (%) [PERCENT SIGN], Size: 8x8
   * Glyph (bounding: <x: 0, y: 1, width: 7, height: 6>):
   *   ........
   *   ##...##.
   *   ##..##..
   *   ...##...
   *   ..##....
   *   .##..##.
   *   ##...##.
   *   ........
   * Encoded data: offset=18, size=6
   */
  0xc7, 0x98, 0x61, 0x86, 0x78, 0xc0,

  /* Unicode: U+0026 (&) [AMPERSAND], Size: 8x8
   * Glyph (bounding: <x: 0, y: 0, width: 7, height: 7>):
   *   ..###...
   *   .##.##..
   *   ..###...
   *   .###.##.
   *   ##.###..
   *   ##..##..
   *   .###.##.
   *   ........
   * Encoded data: offset=24, size=7
   */
  0x38, 0xd8, 0xe3, 0xbd, 0xd9, 0x9d, 0x80,

  /* Unicode: U+0027 (') [APOSTROPHE], Size: 8x8
   * Glyph (bounding: <x: 2, y: 0, width: 3, height: 3>):
   *   ...##...
   *   ...##...
   *   ..##....
   *   ........
   *   ........
   *   ........
   *   ........
   *   ........
   * Encoded data: offset=31, size=2
   */
  0x6f, 0x00,

  /* Unicode: U+0028 (() [LEFT PARENTHESIS], Size: 8x8
   * Glyph (bounding: <x: 2, y: 0, width: 4, height: 7>):
   *   ....##..
   *   ...##...
   *   ..##....
   *   ..##....
   *   ..##....
   *   ...##...
   *   ....##..
   *   ........
   * Encoded data: offset=33, size=4
   */
  0x36, 0xcc, 0xc6, 0x30,

  /* Unicode: U+0029 ()) [RIGHT PARENTHESIS], Size: 8x8
   * Glyph (bounding: <x: 2, y: 0, width: 4, height: 7>):
   *   ..##....
   *   ...##...
   *   ....##..
   *   ....##..
   *   ....##..
   *   ...##...
   *   ..##....
   *   ........
   * Encoded data: offset=37, size=4
   */
  0xc6, 0x33, 0x36, 0xc0,

  /* Unicode: U+002A (*) [ASTERISK], Size: 8x8
   * Glyph (bounding: <x: 0, y: 1, width: 8, height: 5>):
   *   ........
   *   .##..##.
   *   ..####..
   *   ########
   *   ..####..
   *   .##..##.
   *   ........
   *   ........
   * Encoded data: offset=41, size=5
   */
  0x66, 0x3c, 0xff, 0x3c, 0x66,

  /* Unicode: U+002B (+) [PLUS SIGN], Size: 8x8
   * Glyph (bounding: <x: 1, y: 1, width: 6, height: 5>):
   *   ........
   *   ...##...
   *   ...##...
   *   .######.
   *   ...##...
   *   ...##...
   *   ........
   *   ........
   * Encoded data: offset=46, size=4
   */
  0x30, 0xcf, 0xcc, 0x30,

  /* Unicode: U+002C (,) [COMMA], Size: 8x8
   * Glyph (bounding: <x: 2, y: 5, width: 3, height: 3>):
   *   ........
   *   ........
   *   ........
   *   ........
   *   ........
   *   ...##...
   *   ...##...
   *   ..##....
   * Encoded data: offset=50, size=2
   */
  0x6f, 0x00,

  /* Unicode: U+002D (-) [HYPHEN-MINUS], Size: 8x8
   * Glyph (bounding: <x: 1, y: 3, width: 6, height: 1>):
   *   ........
   *   ........
   *   ........
   *   .######.
   *   ........
   *   ........
   *   ........
   *   ........
   * Encoded data: offset=52, size=1
   */
  0xfc,

  /* Unicode: U+002E (.) [FULL STOP], Size: 8x8
   * Glyph (bounding: <x: 3, y: 5, width: 2, height: 2>):
   *   ........
   *   ........
   *   ........
   *   ........
   *   ........
   *   ...##...
   *   ...##...
   *   ........
   * Encoded data: offset=53, size=1
   */
  0xf0,

  /* Unicode: U+002F (/) [SOLIDUS], Size: 8x8
   * Glyph (bounding: <x: 0, y: 0, width: 8, height: 7>):
   *   ......##
   *   .....##.
   *   ....##..
   *   ...##...
   *   ..##....
   *   .##.....
   *   ##......
   *   ........
   * Encoded data: offset=54, size=7
   */
  0x03, 0x06, 0x0c, 0x18, 0x30, 0x60, 0xc0,

  /* Unicode: U+0030 (0) [DIGIT ZERO], Size: 8x8
   * Glyph (bounding: <x: 1, y: 0, width: 6, height: 7>):
   *   ..####..
   *   .##..##.
   *   .##.###.
   *   .###.##.
   *   .##..##.
   *   .##..##.
   *   ..####..
   *   ........
   * Encoded data: offset=61, size=6
   */
  0x7b, 0x3d, 0xfb, 0xcf, 0x37, 0x80,

  /* Unicode: U+0031 (1) [DIGIT ONE], Size: 8x8
   * Glyph (bounding: <x: 1, y: 0, width: 6, height: 7>):
   *   ...##...
   *   ..###...
   *   ...##...
   *   ...##...
   *   ...##...
   *   ...##...
   *   .######.
   *   ........
   * Encoded data: offset=67, size=6
   */
  0x31, 0xc3, 0x0c, 0x30, 0xcf, 0xc0,

  /* Unicode: U+0032 (2) [DIGIT TWO], Size: 8x8
   * Glyph (bounding: <x: 1, y: 0, width: 6, height: 7>):
   *   ..####..
   *   .##..##.
   *   ....##..
   *   ...##...
   *   ..##....
   *   .##.....
   *   .######.
   *   ........
   * Encoded data: offset=73, size=6
   */
  0x7b, 0x31, 0x8c, 0x63, 0x0f, 0xc0,

  /* Unicode: U+0033 (3) [DIGIT THREE], Size: 8x8
   * Glyph (bounding: <x: 1, y: 0, width: 6, height: 7>):
   *   ..####..
   *   .##..##.
   *   .....##.
   *   ...###..
   *   .....##.
   *   .##..##.
   *   ..####..
   *   ........
   * Encoded data: offset=79, size=6
   */
  0x7b, 0x30, 0xce, 0x0f, 0x37, 0x80,

  /* Unicode: U+0034 (4) [DIGIT FOUR], Size: 8x8
   * Glyph (bounding: <x: 0, y: 0, width: 7, height: 7>):
   *   ...###..
   *   ..####..
   *   .##.##..
   *   ##..##..
   *   #######.
   *   ....##..
   *   ....##..
   *   ........
   * Encoded data: offset=85, size=7
   */
  0x1c, 0x79, 0xb6, 0x6f, 0xe1, 0x83, 0x00,

  /* Unicode: U+0035 (5) [DIGIT FIVE], Size: 8x8
   * Glyph (bounding: <x: 1, y: 0, width: 6, height: 7>):
   *   .######.
   *   .##.....
   *   .#####..
   *   .....##.
   *   .....##.
   *   .##..##.
   *   ..####..
   *   ........
   * Encoded data: offset=92, size=6
   */
  0xff, 0x0f, 0x83, 0x0f, 0x37, 0x80,

  /* Unicode: U+0036 (6) [DIGIT SIX], Size: 8x8
   * Glyph (bounding: <x: 1, y: 0, width: 6, height: 7>):
   *   ...###..
   *   ..##....
   *   .##.....
   *   .#####..
   *   .##..##.
   *   .##..##.
   *   ..####..
   *   ........
   * Encoded data: offset=98, size=6
   */
  0x39, 0x8c, 0x3e, 0xcf, 0x37, 0x80,

  /* Unicode: U+0037 (7) [DIGIT SEVEN], Size: 8x8
   * Glyph (bounding: <x: 1, y: 0, width: 6, height: 7>):
   *   .######.
   *   .....##.
   *   .....##.
   *   ....##..
   *   ...##...
   *   ...##...
   *   ...##...
   *   ........
   * Encoded data: offset=104, size=6
   */
  0xfc, 0x30, 0xc6, 0x30, 0xc3, 0x00,

  /* Unicode: U+0038 (8) [DIGIT EIGHT], Size: 8x8
   * Glyph (bounding: <x: 1, y: 0, width: 6, height: 7>):
   *   ..####..
   *   .##..##.
   *   .##..##.
   *   ..####..
   *   .##..##.
   *   .##..##.
   *   ..####..
   *   ........
   * Encoded data: offset=110, size=6
   */
  0x7b, 0x3c, 0xde, 0xcf, 0x37, 0x80,

  /* Unicode: U+0039 (9) [DIGIT NINE], Size: 8x8
   * Glyph (bounding: <x: 1, y: 0, width: 6, height: 7>):
   *   ..####..
   *   .##..##.
   *   .##..##.
   *   ..#####.
   *   .....##.
   *   ....##..
   *   ..###...
   *   ........
   * Encoded data: offset=116, size=6
   */
  0x7b, 0x3c, 0xdf, 0x0c, 0x67, 0x00,

  /* Unicode: U+003A (:) [COLON], Size: 8x8
   * Glyph (bounding: <x: 3, y: 1, width: 2, height: 6>):
   *   ........
   *   ...##...
   *   ...##...
   *   ........
   *   ........
   *   ...##...
   *   ...##...
   *   ........
   * Encoded data: offset=122, size=2
   */
  0xf0, 0xf0,

  /* Unicode: U+003B (;) [SEMICOLON], Size: 8x8
   * Glyph (bounding: <x: 2, y: 1, width: 3, height: 7>):
   *   ........
   *   ...##...
   *   ...##...
   *   ........
   *   ........
   *   ...##...
   *   ...##...
   *   ..##....
   * Encoded data: offset=124, size=3
   */
  0x6c, 0x06, 0xf0,

  /* Unicode: U+003C (<) [LESS-THAN SIGN], Size: 8x8
   * Glyph (bounding: <x: 1, y: 0, width: 5, height: 7>):
   *   ....##..
   *   ...##...
   *   ..##....
   *   .##.....
   *   ..##....
   *   ...##...
   *   ....##..
   *   ........
   * Encoded data: offset=127, size=5
   */
  0x19, 0x99, 0x86, 0x18, 0x60,

  /* Unicode: U+003D (=) [EQUALS SIGN], Size: 8x8
   * Glyph (bounding: <x: 1, y: 2, width: 6, height: 3>):
   *   ........
   *   ........
   *   .######.
   *   ........
   *   .######.
   *   ........
   *   ........
   *   ........
   * Encoded data: offset=132, size=3
   */
  0xfc, 0x0f, 0xc0,

  /* Unicode: U+003E (>) [GREATER-THAN SIGN], Size: 8x8
   * Glyph (bounding: <x: 1, y: 0, width: 5, height: 7>):
   *   .##.....
   *   ..##....
   *   ...##...
   *   ....##..
   *   ...##...
   *   ..##....
   *   .##.....
   *   ........
   * Encoded data: offset=135, size=5
   */
  0xc3, 0x0c, 0x33, 0x33, 0x00,

  /* Unicode: U+003F (?) [QUESTION MARK], Size: 8x8
   * Glyph (bounding: <x: 1, y: 0, width: 6, height: 7>):
   *   ..####..
   *   .##..##.
   *   .....##.
   *   ....##..
   *   ...##...
   *   ........
   *   ...##...
   *   ........
   * Encoded data: offset=140, size=6
   */
  0x7b, 0x30, 0xc6, 0x30, 0x03, 0x00,

  /* Unicode: U+0040 (@) [COMMERCIAL AT], Size: 8x8
   * Glyph (bounding: <x: 0, y: 0, width: 7, height: 7>):
   *   .#####..
   *   ##...##.
   *   ##.####.
   *   ##.####.
   *   ##.####.
   *   ##......
   *   .#####..
   *   ........
   * Encoded data: offset=146, size=7
   */
  0x7d, 0x8f, 0x7e, 0xfd, 0xf8, 0x1f, 0x00,

  /* Unicode: U+0041 (A) [LATIN CAPITAL LETTER A], Size: 8x8
   * Glyph (bounding: <x: 1, y: 0, width: 6, height: 7>):
   *   ...##...
   *   ..####..
   *   .##..##.
   *   .##..##.
   *   .######.
   *   .##..##.
   *   .##..##.
   *   ........
   * Encoded data: offset=153, size=6
   */
  0x31, 0xec, 0xf3, 0xff, 0x3c, 0xc0,

  /* Unicode: U+0042 (B) [LATIN CAPITAL LETTER B], Size: 8x8
   * Glyph (bounding: <x: 1, y: 0, width: 6, height: 7>):
   *   .#####..
   *   .##..##.
   *   .##..##.
   *   .#####..
   *   .##..##.
   *   .##..##.
   *   .#####..
   *   ........
   * Encoded data: offset=159, size=6
   */
  0xfb, 0x3c, 0xfe, 0xcf, 0x3f, 0x80,

  /* Unicode: U+0043 (C) [LATIN CAPITAL LETTER C], Size: 8x8
   * Glyph (bounding: <x: 1, y: 0, width: 6, height: 7>):
   *   ..####..
   *   .##..##.
   *   .##.....
   *   .##.....
   *   .##.....
   *   .##..##.
   *   ..####..
   *   ........
   * Encoded data: offset=165, size=6
   */
  0x7b, 0x3c, 0x30, 0xc3, 0x37, 0x80,

  /* Unicode: U+0044 (D) [LATIN CAPITAL LETTER D], Size: 8x8
   * Glyph (bounding: <x: 1, y: 0, width: 6, height: 7>):
   *   .####...
   *   .##.##..
   *   .##..##.
   *   .##..##.
   *   .##..##.
   *   .##.##..
   *   .####...
   *   ........
   * Encoded data: offset=171, size=6
   */
  0xf3, 0x6c, 0xf3, 0xcf, 0x6f, 0x00,

  /* Unicode: U+0045 (E) [LATIN CAPITAL LETTER E], Size: 8x8
   * Glyph (bounding: <x: 1, y: 0, width: 6, height: 7>):
   *   .######.
   *   .##.....
   *   .##.....
   *   .#####..
   *   .##.....
   *   .##.....
   *   .######.
   *   ........
   * Encoded data: offset=177, size=6
   */
  0xff, 0x0c, 0x3e, 0xc3, 0x0f, 0xc0,

  /* Unicode: U+0046 (F) [LATIN CAPITAL LETTER F], Size: 8x8
   * Glyph (bounding: <x: 1, y: 0, width: 6, height: 7>):
   *   .######.
   *   .##.....
   *   .##.....
   *   .#####..
   *   .##.....
   *   .##.....
   *   .##.....
   *   ........
   * Encoded data: offset=183, size=6
   */
  0xff, 0x0c, 0x3e, 0xc3, 0x0c, 0x00,

  /* Unicode: U+0047 (G) [LATIN CAPITAL LETTER G], Size: 8x8
   * Glyph (bounding: <x: 1, y: 0, width: 6, height: 7>):
   *   ..####..
   *   .##..##.
   *   .##.....
   *   .##.###.
   *   .##..##.
   *   .##..##.
   *   ..#####.
   *   ........
   * Encoded data: offset=189, size=6
   */
  0x7b, 0x3c, 0x37, 0xcf, 0x37, 0xc0,

  /* Unicode: U+0048 (H) [LATIN CAPITAL LETTER H], Size: 8x8
   * Glyph (bounding: <x: 1, y: 0, width: 6, height: 7>):
   *   .##..##.
   *   .##..##.
   *   .##..##.
   *   .######.
   *   .##..##.
   *   .##..##.
   *   .##..##.
   *   ........
   * Encoded data: offset=195, size=6
   */
  0xcf, 0x3c, 0xff, 0xcf, 0x3c, 0xc0,

  /* Unicode: U+0049 (I) [LATIN CAPITAL LETTER I], Size: 8x8
   * Glyph (bounding: <x: 1, y: 0, width: 6, height: 7>):
   *   .######.
   *   ...##...
   *   ...##...
   *   ...##...
   *   ...##...
   *   ...##...
   *   .######.
   *   ........
   * Encoded data: offset=201, size=6
   */
  0xfc, 0xc3, 0x0c, 0x30, 0xcf, 0xc0,

  /* Unicode: U+004A (J) [LATIN CAPITAL LETTER J], Size: 8x8
   * Glyph (bounding: <x: 1, y: 0, width: 6, height: 7>):
   *   .....##.
   *   .....##.
   *   .....##.
   *   .....##.
   *   .....##.
   *   .##..##.
   *   ..####..
   *   ........
   * Encoded data: offset=207, size=6
   */
  0x0c, 0x30, 0xc3, 0x0f, 0x37, 0x80,

  /* Unicode: U+004B (K) [LATIN CAPITAL LETTER K], Size: 8x8
   * Glyph (bounding: <x: 0, y: 0, width: 7, height: 7>):
   *   ##...##.
   *   ##..##..
   *   ##.##...
   *   ####....
   *   ##.##...
   *   ##..##..
   *   ##...##.
   *   ........
   * Encoded data: offset=213, size=7
   */
  0xc7, 0x9b, 0x67, 0x8d, 0x99, 0xb1, 0x80,

  /* Unicode: U+004C (L) [LATIN CAPITAL LETTER L], Size: 8x8
   * Glyph (bounding: <x: 1, y: 0, width: 6, height: 7>):
   *   .##.....
   *   .##.....
   *   .##.....
   *   .##.....
   *   .##.....
   *   .##.....
   *   .######.
   *   ........
   * Encoded data: offset=220, size=6
   */
  0xc3, 0x0c, 0x30, 0xc3, 0x0f, 0xc0,

  /* Unicode: U+004D (M) [LATIN CAPITAL LETTER M], Size: 8x8
   * Glyph (bounding: <x: 0, y: 0, width: 7, height: 7>):
   *   ##...##.
   *   ###.###.
   *   #######.
   *   ##.#.##.
   *   ##...##.
   *   ##...##.
   *   ##...##.
   *   ........
   * Encoded data: offset=226, size=7
   */
  0xc7, 0xdf, 0xfe, 0xbc, 0x78, 0xf1, 0x80,

  /* Unicode: U+004E (N) [LATIN CAPITAL LETTER N], Size: 8x8
   * Glyph (bounding: <x: 0, y: 0, width: 7, height: 7>):
   *   ##...##.
   *   ###..##.
   *   ####.##.
   *   ##.####.
   *   ##..###.
   *   ##...##.
   *   ##...##.
   *   ........
   * Encoded data: offset=233, size=7
   */
  0xc7, 0xcf, 0xde, 0xfc, 0xf8, 0xf1, 0x80,

  /* Unicode: U+004F (O) [LATIN CAPITAL LETTER O], Size: 8x8
   * Glyph (bounding: <x: 1, y: 0, width: 6, height: 7>):
   *   ..####..
   *   .##..##.
   *   .##..##.
   *   .##..##.
   *   .##..##.
   *   .##..##.
   *   ..####..
   *   ........
   * Encoded data: offset=240, size=6
   */
  0x7b, 0x3c, 0xf3, 0xcf, 0x37, 0x80,

  /* Unicode: U+0050 (P) [LATIN CAPITAL LETTER P], Size: 8x8
   * Glyph (bounding: <x: 1, y: 0, width: 6, height: 7>):
   *   .#####..
   *   .##..##.
   *   .##..##.
   *   .#####..
   *   .##.....
   *   .##.....
   *   .##.....
   *   ........
   * Encoded data: offset=246, size=6
   */
  0xfb, 0x3c, 0xfe, 0xc3, 0x0c, 0x00,

  /* Unicode: U+0051 (Q) [LATIN CAPITAL LETTER Q], Size: 8x8
   * Glyph (bounding: <x: 1, y: 0, width: 6, height: 7>):
   *   ..####..
   *   .##..##.
   *   .##..##.
   *   .##..##.
   *   .##..##.
   *   .##.##..
   *   ..##.##.
   *   ........
   * Encoded data: offset=252, size=6
   */
  0x7b, 0x3c, 0xf3, 0xcf, 0x66, 0xc0,

  /* Unicode: U+0052 (R) [LATIN CAPITAL LETTER R], Size: 8x8
   * Glyph (bounding: <x: 1, y: 0, width: 6, height: 7>):
   *   .#####..
   *   .##..##.
   *   .##..##.
   *   .#####..
   *   .##.##..
   *   .##..##.
   *   .##..##.
   *   ........
   * Encoded data: offset=258, size=6
   */
  0xfb, 0x3c, 0xfe, 0xdb, 0x3c, 0xc0,

  /* Unicode: U+0053 (S) [LATIN CAPITAL LETTER S], Size: 8x8
   * Glyph (bounding: <x: 1, y: 0, width: 6, height: 7>):
   *   ..####..
   *   .##..##.
   *   .##.....
   *   ..####..
   *   .....##.
   *   .##..##.
   *   ..####..
   *   ........
   * Encoded data: offset=264, size=6
   */
  0x7b, 0x3c, 0x1e, 0x0f, 0x37, 0x80,

  /* Unicode: U+0054 (T) [LATIN CAPITAL LETTER T], Size: 8x8
   * Glyph (bounding: <x: 1, y: 0, width: 6, height: 7>):
   *   .######.
   *   ...##...
   *   ...##...
   *   ...##...
   *   ...##...
   *   ...##...
   *   ...##...
   *   ........
   * Encoded data: offset=270, size=6
   */
  0xfc, 0xc3, 0x0c, 0x30, 0xc3, 0x00,

  /* Unicode: U+0055 (U) [LATIN CAPITAL LETTER U], Size: 8x8
   * Glyph (bounding: <x: 1, y: 0, width: 6, height: 7>):
   *   .##..##.
   *   .##..##.
   *   .##..##.
   *   .##..##.
   *   .##..##.
   *   .##..##.
   *   ..####..
   *   ........
   * Encoded data: offset=276, size=6
   */
  0xcf, 0x3c, 0xf3, 0xcf, 0x37, 0x80,

  /* Unicode: U+0056 (V) [LATIN CAPITAL LETTER V], Size: 8x8
   * Glyph (bounding: <x: 1, y: 0, width: 6, height: 7>):
   *   .##..##.
   *   .##..##.
   *   .##..##.
   *   .##..##.
   *   .##..##.
   *   ..####..
   *   ...##...
   *   ........
   * Encoded data: offset=282, size=6
   */
  0xcf, 0x3c, 0xf3, 0xcd, 0xe3, 0x00,

  /* Unicode: U+0057 (W) [LATIN CAPITAL LETTER W], Size: 8x8
   * Glyph (bounding: <x: 0, y: 0, width: 7, height: 7>):
   *   ##...##.
   *   ##...##.
   *   ##...##.
   *   ##.#.##.
   *   #######.
   *   ###.###.
   *   ##...##.
   *   ........
   * Encoded data: offset=288, size=7
   */
  0xc7, 0x8f, 0x1e, 0xbf, 0xfd, 0xf1, 0x80,

  /* Unicode: U+0058 (X) [LATIN CAPITAL LETTER X], Size: 8x8
   * Glyph (bounding: <x: 0, y: 0, width: 8, height: 7>):
   *   ##....##
   *   .##..##.
   *   ..####..
   *   ...##...
   *   ..####..
   *   .##..##.
   *   ##....##
   *   ........
   * Encoded data: offset=295, size=7
   */
  0xc3, 0x66, 0x3c, 0x18, 0x3c, 0x66, 0xc3,

  /* Unicode: U+0059 (Y) [LATIN CAPITAL LETTER Y], Size: 8x8
   * Glyph (bounding: <x: 0, y: 0, width: 8, height: 7>):
   *   ##....##
   *   .##..##.
   *   ..####..
   *   ...##...
   *   ...##...
   *   ...##...
   *   ...##...
   *   ........
   * Encoded data: offset=302, size=7
   */
  0xc3, 0x66, 0x3c, 0x18, 0x18, 0x18, 0x18,

  /* Unicode: U+005A (Z) [LATIN CAPITAL LETTER Z], Size: 8x8
   * Glyph (bounding: <x: 1, y: 0, width: 6, height: 7>):
   *   .######.
   *   .....##.
   *   ....##..
   *   ...##...
   *   ..##....
   *   .##.....
   *   .######.
   *   ........
   * Encoded data: offset=309, size=6
   */
  0xfc, 0x31, 0x8c, 0x63, 0x0f, 0xc0,

  /* Unicode: U+005B ([) [LEFT SQUARE BRACKET], Size: 8x8
   * Glyph (bounding: <x: 2, y: 0, width: 4, height: 7>):
   *   ..####..
   *   ..##....
   *   ..##....
   *   ..##....
   *   ..##....
   *   ..##....
   *   ..####..
   *   ........
   * Encoded data: offset=315, size=4
   */
  0xfc, 0xcc, 0xcc, 0xf0,

  /* Unicode: U+005C (\) [REVERSE SOLIDUS], Size: 8x8
   * Glyph (bounding: <x: 0, y: 0, width: 8, height: 7>):
   *   ##......
   *   .##.....
   *   ..##....
   *   ...##...
   *   ....##..
   *   .....##.
   *   ......##
   *   ........
   * Encoded data: offset=319, size=7
   */
  0xc0, 0x60, 0x30, 0x18, 0x0c, 0x06, 0x03,

  /* Unicode: U+005D (]) [RIGHT SQUARE BRACKET], Size: 8x8
   * Glyph (bounding: <x: 2, y: 0, width: 4, height: 7>):
   *   ..####..
   *   ....##..
   *   ....##..
   *   ....##..
   *   ....##..
   *   ....##..
   *   ..####..
   *   ........
   * Encoded data: offset=326, size=4
   */
  0xf3, 0x33, 0x33, 0xf0,

  /* Unicode: U+005E (^) [CIRCUMFLEX ACCENT], Size: 8x8
   * Glyph (bounding: <x: 0, y: 0, width: 7, height: 4>):
   *   ...#....
   *   ..###...
   *   .##.##..
   *   ##...##.
   *   ........
   *   ........
   *   ........
   *   ........
   * Encoded data: offset=330, size=4
   */
  0x10, 0x71, 0xb6, 0x30,

  /* Unicode: U+005F (_) [LOW LINE], Size: 8x8
   * Glyph (bounding: <x: 0, y: 7, width: 8, height: 1>):
   *   ........
   *   ........
   *   ........
   *   ........
   *   ........
   *   ........
   *   ........
   *   ########
   * Encoded data: offset=334, size=1
   */
  0xff,

  /* Unicode: U+0060 (`) [GRAVE ACCENT], Size: 8x8
   * Glyph (bounding: <x: 3, y: 0, width: 4, height: 3>):
   *   ...##...
   *   ....##..
   *   .....##.
   *   ........
   *   ........
   *   ........
   *   ........
   *   ........
   * Encoded data: offset=335, size=2
   */
  0xc6, 0x30,

  /* Unicode: U+0061 (a) [LATIN SMALL LETTER A], Size: 8x8
   * Glyph (bounding: <x: 1, y: 2, width: 6, height: 5>):
   *   ........
   *   ........
   *   ..####..
   *   .....##.
   *   ..#####.
   *   .##..##.
   *   ..#####.
   *   ........
   * Encoded data: offset=337, size=4
   */
  0x78, 0x37, 0xf3, 0x7c,

  /* Unicode: U+0062 (b) [LATIN SMALL LETTER B], Size: 8x8
   * Glyph (bounding: <x: 1, y: 0, width: 6, height: 7>):
   *   .##.....
   *   .##.....
   *   .#####..
   *   .##..##.
   *   .##..##.
   *   .##..##.
   *   .#####..
   *   ........
   * Encoded data: offset=341, size=6
   */
  0xc3, 0x0f, 0xb3, 0xcf, 0x3f, 0x80,

  /* Unicode: U+0063 (c) [LATIN SMALL LETTER C], Size: 8x8
   * Glyph (bounding: <x: 1, y: 2, width: 5, height: 5>):
   *   ........
   *   ........
   *   ..####..
   *   .##.....
   *   .##.....
   *   .##.....
   *   ..####..
   *   ........
   * Encoded data: offset=347, size=4
   */
  0x7e, 0x31, 0x87, 0x80,

  /* Unicode: U+0064 (d) [LATIN SMALL LETTER D], Size: 8x8
   * Glyph (bounding: <x: 1, y: 0, width: 6, height: 7>):
   *   .....##.
   *   .....##.
   *   ..#####.
   *   .##..##.
   *   .##..##.
   *   .##..##.
   *   ..#####.
   *   ........
   * Encoded data: offset=351, size=6
   */
  0x0c, 0x37, 0xf3, 0xcf, 0x37, 0xc0,

  /* Unicode: U+0065 (e) [LATIN SMALL LETTER E], Size: 8x8
   * Glyph (bounding: <x: 1, y: 2, width: 6, height: 5>):
   *   ........
   *   ........
   *   ..####..
   *   .##..##.
   *   .######.
   *   .##.....
   *   ..####..
   *   ........
   * Encoded data: offset=357, size=4
   */
  0x7b, 0x3f, 0xf0, 0x78,

  /* Unicode: U+0066 (f) [LATIN SMALL LETTER F], Size: 8x8
   * Glyph (bounding: <x: 1, y: 0, width: 5, height: 7>):
   *   ...###..
   *   ..##....
   *   .#####..
   *   ..##....
   *   ..##....
   *   ..##....
   *   ..##....
   *   ........
   * Encoded data: offset=361, size=5
   */
  0x3b, 0x3e, 0xc6, 0x31, 0x80,

  /* Unicode: U+0067 (g) [LATIN SMALL LETTER G], Size: 8x8
   * Glyph (bounding: <x: 1, y: 2, width: 6, height: 6>):
   *   ........
   *   ........
   *   ..#####.
   *   .##..##.
   *   .##..##.
   *   ..#####.
   *   .....##.
   *   .#####..
   * Encoded data: offset=366, size=5
   */
  0x7f, 0x3c, 0xdf, 0x0f, 0xe0,

  /* Unicode: U+0068 (h) [LATIN SMALL LETTER H], Size: 8x8
   * Glyph (bounding: <x: 1, y: 0, width: 6, height: 7>):
   *   .##.....
   *   .##.....
   *   .#####..
   *   .##..##.
   *   .##..##.
   *   .##..##.
   *   .##..##.
   *   ........
   * Encoded data: offset=371, size=6
   */
  0xc3, 0x0f, 0xb3, 0xcf, 0x3c, 0xc0,

  /* Unicode: U+0069 (i) [LATIN SMALL LETTER I], Size: 8x8
   * Glyph (bounding: <x: 2, y: 0, width: 5, height: 7>):
   *   ...##...
   *   ........
   *   ..###...
   *   ...##...
   *   ...##...
   *   ...##...
   *   ...####.
   *   ........
   * Encoded data: offset=377, size=5
   */
  0x60, 0x38, 0xc6, 0x31, 0xe0,

  /* Unicode: U+006A (j) [LATIN SMALL LETTER J], Size: 8x8
   * Glyph (bounding: <x: 1, y: 0, width: 5, height: 8>):
   *   ....##..
   *   ........
   *   ....##..
   *   ....##..
   *   ....##..
   *   ....##..
   *   ....##..
   *   .####...
   * Encoded data: offset=382, size=5
   */
  0x18, 0x06, 0x31, 0x8c, 0x7e,

  /* Unicode: U+006B (k) [LATIN SMALL LETTER K], Size: 8x8
   * Glyph (bounding: <x: 1, y: 0, width: 6, height: 7>):
   *   .##.....
   *   .##.....
   *   .##..##.
   *   .##.##..
   *   .####...
   *   .##.##..
   *   .##..##.
   *   ........
   * Encoded data: offset=387, size=6
   */
  0xc3, 0x0c, 0xf6, 0xf3, 0x6c, 0xc0,

  /* Unicode: U+006C (l) [LATIN SMALL LETTER L], Size: 8x8
   * Glyph (bounding: <x: 2, y: 0, width: 5, height: 7>):
   *   ..###...
   *   ...##...
   *   ...##...
   *   ...##...
   *   ...##...
   *   ...##...
   *   ...####.
   *   ........
   * Encoded data: offset=393, size=5
   */
  0xe3, 0x18, 0xc6, 0x31, 0xe0,

  /* Unicode: U+006D (m) [LATIN SMALL LETTER M], Size: 8x8
   * Glyph (bounding: <x: 0, y: 2, width: 7, height: 5>):
   *   ........
   *   ........
   *   ##..##..
   *   #######.
   *   ##.#.##.
   *   ##.#.##.
   *   ##...##.
   *   ........
   * Encoded data: offset=398, size=5
   */
  0xcd, 0xff, 0x5e, 0xbc, 0x60,

  /* Unicode: U+006E (n) [LATIN SMALL LETTER N], Size: 8x8
   * Glyph (bounding: <x: 1, y: 2, width: 6, height: 5>):
   *   ........
   *   ........
   *   .#####..
   *   .##..##.
   *   .##..##.
   *   .##..##.
   *   .##..##.
   *   ........
   * Encoded data: offset=403, size=4
   */
  0xfb, 0x3c, 0xf3, 0xcc,

  /* Unicode: U+006F (o) [LATIN SMALL LETTER O], Size: 8x8
   * Glyph (bounding: <x: 1, y: 2, width: 6, height: 5>):
   *   ........
   *   ........
   *   ..####..
   *   .##..##.
   *   .##..##.
   *   .##..##.
   *   ..####..
   *   ........
   * Encoded data: offset=407, size=4
   */
  0x7b, 0x3c, 0xf3, 0x78,

  /* Unicode: U+0070 (p) [LATIN SMALL LETTER P], Size: 8x8
   * Glyph (bounding: <x: 1, y: 2, width: 6, height: 6>):
   *   ........
   *   ........
   *   .#####..
   *   .##..##.
   *   .##..##.
   *   .#####..
   *   .##.....
   *   .##.....
   * Encoded data: offset=411, size=5
   */
  0xfb, 0x3c, 0xfe, 0xc3, 0x00,

  /* Unicode: U+0071 (q) [LATIN SMALL LETTER Q], Size: 8x8
   * Glyph (bounding: <x: 1, y: 2, width: 6, height: 6>):
   *   ........
   *   ........
   *   ..#####.
   *   .##..##.
   *   .##..##.
   *   ..#####.
   *   .....##.
   *   .....##.
   * Encoded data: offset=416, size=5
   */
  0x7f, 0x3c, 0xdf, 0x0c, 0x30,

  /* Unicode: U+0072 (r) [LATIN SMALL LETTER R], Size: 8x8
   * Glyph (bounding: <x: 1, y: 2, width: 6, height: 5>):
   *   ........
   *   ........
   *   .#####..
   *   .##..##.
   *   .##.....
   *   .##.....
   *   .##.....
   *   ........
   * Encoded data: offset=421, size=4
   */
  0xfb, 0x3c, 0x30, 0xc0,

  /* Unicode: U+0073 (s) [LATIN SMALL LETTER S], Size: 8x8
   * Glyph (bounding: <x: 1, y: 2, width: 6, height: 5>):
   *   ........
   *   ........
   *   ..#####.
   *   .##.....
   *   ..####..
   *   .....##.
   *   .#####..
   *   ........
   * Encoded data: offset=425, size=4
   */
  0x7f, 0x07, 0x83, 0xf8,

  /* Unicode: U+0074 (t) [LATIN SMALL LETTER T], Size: 8x8
   * Glyph (bounding: <x: 1, y: 0, width: 6, height: 7>):
   *   ..##....
   *   ..##....
   *   .######.
   *   ..##....
   *   ..##....
   *   ..##....
   *   ...####.
   *   ........
   * Encoded data: offset=429, size=6
   */
  0x61, 0x8f, 0xd8, 0x61, 0x83, 0xc0,

  /* Unicode: U+0075 (u) [LATIN SMALL LETTER U], Size: 8x8
   * Glyph (bounding: <x: 1, y: 2, width: 6, height: 5>):
   *   ........
   *   ........
   *   .##..##.
   *   .##..##.
   *   .##..##.
   *   .##..##.
   *   ..#####.
   *   ........
   * Encoded data: offset=435, size=4
   */
  0xcf, 0x3c, 0xf3, 0x7c,

  /* Unicode: U+0076 (v) [LATIN SMALL LETTER V], Size: 8x8
   * Glyph (bounding: <x: 1, y: 2, width: 6, height: 5>):
   *   ........
   *   ........
   *   .##..##.
   *   .##..##.
   *   .##..##.
   *   ..####..
   *   ...##...
   *   ........
   * Encoded data: offset=439, size=4
   */
  0xcf, 0x3c, 0xde, 0x30,

  /* Unicode: U+0077 (w) [LATIN SMALL LETTER W], Size: 8x8
   * Glyph (bounding: <x: 0, y: 2, width: 7, height: 5>):
   *   ........
   *   ........
   *   ##...##.
   *   ##...##.
   *   ##.#.##.
   *   .#####..
   *   .##.##..
   *   ........
   * Encoded data: offset=443, size=5
   */
  0xc7, 0x8f, 0x5b, 0xe6, 0xc0,

  /* Unicode: U+0078 (x) [LATIN SMALL LETTER X], Size: 8x8
   * Glyph (bounding: <x: 0, y: 2, width: 7, height: 5>):
   *   ........
   *   ........
   *   ##...##.
   *   .##.##..
   *   ..###...
   *   .##.##..
   *   ##...##.
   *   ........
   * Encoded data: offset=448, size=5
   */
  0xc6, 0xd8, 0xe3, 0x6c, 0x60,

  /* Unicode: U+0079 (y) [LATIN SMALL LETTER Y], Size: 8x8
   * Glyph (bounding: <x: 1, y: 2, width: 6, height: 6>):
   *   ........
   *   ........
   *   .##..##.
   *   .##..##.
   *   .##..##.
   *   ..#####.
   *   .....##.
   *   ..####..
   * Encoded data: offset=453, size=5
   */
  0xcf, 0x3c, 0xdf, 0x0d, 0xe0,

  /* Unicode: U+007A (z) [LATIN SMALL LETTER Z], Size: 8x8
   * Glyph (bounding: <x: 1, y: 2, width: 6, height: 5>):
   *   ........
   *   ........
   *   .######.
   *   ....##..
   *   ...##...
   *   ..##....
   *   .######.
   *   ........
   * Encoded data: offset=458, size=4
   */
  0xfc, 0x63, 0x18, 0xfc,

  /* Unicode: U+007B ({) [LEFT CURLY BRACKET], Size: 8x8
   * Glyph (bounding: <x: 1, y: 0, width: 6, height: 7>):
   *   ....###.
   *   ...##...
   *   ...##...
   *   .###....
   *   ...##...
   *   ...##...
   *   ....###.
   *   ........
   * Encoded data: offset=462, size=6
   */
  0x1c, 0xc3, 0x38, 0x30, 0xc1, 0xc0,

  /* Unicode: U+007C (|) [VERTICAL LINE], Size: 8x8
   * Glyph (bounding: <x: 3, y: 0, width: 2, height: 7>):
   *   ...##...
   *   ...##...
   *   ...##...
   *   ...##...
   *   ...##...
   *   ...##...
   *   ...##...
   *   ........
   * Encoded data: offset=468, size=2
   */
  0xff, 0xfc,

  /* Unicode: U+007D (}) [RIGHT CURLY BRACKET], Size: 8x8
   * Glyph (bounding: <x: 1, y: 0, width: 6, height: 7>):
   *   .###....
   *   ...##...
   *   ...##...
   *   ....###.
   *   ...##...
   *   ...##...
   *   .###....
   *   ........
   * Encoded data: offset=470, size=6
   */
  0xe0, 0xc3, 0x07, 0x30, 0xce, 0x00,

  /* Unicode: U+007E (~) [TILDE], Size: 8x8
   * Glyph (bounding: <x: 0, y: 0, width: 7, height: 2>):
   *   .###.##.
   *   ##.###..
   *   ........
   *   ........
   *   ........
   *   ........
   *   ........
   *   ........
   * Encoded data: offset=476, size=2
   */
  0x77, 0xb8,

  /* Unicode: U+00A0 ( ) [NO-BREAK SPACE], Size: 8x8
   * Glyph (bounding: <x: 0, y: 0, width: 0, height: 0>):
   *   ........
   *   ........
   *   ........
   *   ........
   *   ........
   *   ........
   *   ........
   *   ........
   * Encoded data: offset=478, size=0
   */
  /* <no data> */

  /* Unicode: U+00A1 (¡) [INVERTED EXCLAMATION MARK], Size: 8x8
   * Glyph (bounding: <x: 3, y: 0, width: 2, height: 7>):
   *   ...##...
   *   ........
   *   ...##...
   *   ...##...
   *   ...##...
   *   ...##...
   *   ...##...
   *   ........
   * Encoded data: offset=478, size=2
   */
  0xcf, 0xfc,

  /* Unicode: U+00A2 (¢) [CENT SIGN], Size: 8x8
   * Glyph (bounding: <x: 0, y: 1, width: 7, height: 6>):
   *   ........
   *   ...##...
   *   .######.
   *   ##.##...
   *   ##.##...
   *   .######.
   *   ...##...
   *   ........
   * Encoded data: offset=480, size=6
   */
  0x18, 0xff, 0x66, 0xc7, 0xe3, 0x00,

  /* Unicode: U+00A3 (£) [POUND SIGN], Size: 8x8
   * Glyph (bounding: <x: 0, y: 0, width: 7, height: 7>):
   *   ..###...
   *   .##.##..
   *   .##.....
   *   ####....
   *   .##.....
   *   .##..##.
   *   ######..
   *   ........
   * Encoded data: offset=486, size=7
   */
  0x38, 0xd9, 0x87, 0x86, 0x0c, 0xff, 0x00,

  /* Unicode: U+00A4 (¤) [CURRENCY SIGN], Size: 8x8
   * Glyph (bounding: <x: 1, y: 0, width: 6, height: 5>):
   *   .##..##.
   *   ..####..
   *   .##..##.
   *   ..####..
   *   .##..##.
   *   ........
   *   ........
   *   ........
   * Encoded data: offset=493, size=4
   */
  0xcd, 0xec, 0xde, 0xcc,

  /* Unicode: U+00A5 (¥) [YEN SIGN], Size: 8x8
   * Glyph (bounding: <x: 0, y: 0, width: 8, height: 7>):
   *   ##....##
   *   .##..##.
   *   ..####..
   *   ...##...
   *   ..####..
   *   ...##...
   *   ...##...
   *   ........
   * Encoded data: offset=497, size=7
   */
  0xc3, 0x66, 0x3c, 0x18, 0x3c, 0x18, 0x18,

  /* Unicode: U+00A6 (¦) [BROKEN BAR], Size: 8x8
   * Glyph (bounding: <x: 3, y: 0, width: 2, height: 7>):
   *   ...##...
   *   ...##...
   *   ...##...
   *   ........
   *   ...##...
   *   ...##...
   *   ...##...
   *   ........
   * Encoded data: offset=504, size=2
   */
  0xfc, 0xfc,

  /* Unicode: U+00A7 (§) [SECTION SIGN], Size: 8x8
   * Glyph (bounding: <x: 1, y: 0, width: 6, height: 8>):
   *   ..####..
   *   .##.....
   *   ..####..
   *   .##..##.
   *   .##..##.
   *   ..####..
   *   .....##.
   *   ..####..
   * Encoded data: offset=506, size=6
   */
  0x7b, 0x07, 0xb3, 0xcd, 0xe0, 0xde,

  /* Unicode: U+00A8 (¨) [DIAERESIS], Size: 8x8
   * Glyph (bounding: <x: 1, y: 0, width: 6, height: 1>):
   *   .##..##.
   *   ........
   *   ........
   *   ........
   *   ........
   *   ........
   *   ........
   *   ........
   * Encoded data: offset=512, size=1
   */
  0xcc,

  /* Unicode: U+00A9 (©) [COPYRIGHT SIGN], Size: 8x8
   * Glyph (bounding: <x: 0, y: 0, width: 8, height: 8>):
   *   .######.
   *   #......#
   *   #..###.#
   *   #.##...#
   *   #.##...#
   *   #..###.#
   *   #......#
   *   .######.
   * Encoded data: offset=513, size=8
   */
  0x7e, 0x81, 0x9d, 0xb1, 0xb1, 0x9d, 0x81, 0x7e,

  /* Unicode: U+00AA (ª) [FEMININE ORDINAL INDICATOR], Size: 8x8
   * Glyph (bounding: <x: 1, y: 0, width: 6, height: 6>):
   *   ..####..
   *   .##.##..
   *   .##.##..
   *   ..#####.
   *   ........
   *   .######.
   *   ........
   *   ........
   * Encoded data: offset=521, size=5
   */
  0x7b, 0x6d, 0x9f, 0x03, 0xf0,

  /* Unicode: U+00AB («) [LEFT-POINTING DOUBLE ANGLE QUOTATION MARK], Size: 8x8
   * Glyph (bounding: <x: 0, y: 1, width: 8, height: 5>):
   *   ........
   *   ..##..##
   *   .##..##.
   *   ##..##..
   *   .##..##.
   *   ..##..##
   *   ........
   *   ........
   * Encoded data: offset=526, size=5
   */
  0x33, 0x66, 0xcc, 0x66, 0x33,

  /* Unicode: U+00AC (¬) [NOT SIGN], Size: 8x8
   * Glyph (bounding: <x: 1, y: 1, width: 6, height: 3>):
   *   ........
   *   .######.
   *   .....##.
   *   .....##.
   *   ........
   *   ........
   *   ........
   *   ........
   * Encoded data: offset=531, size=3
   */
  0xfc, 0x30, 0xc0,

  /* Unicode: U+00AD (­) [SOFT HYPHEN], Size: 8x8
   * Glyph (bounding: <x: 2, y: 3, width: 4, height: 1>):
   *   ........
   *   ........
   *   ........
   *   ..####..
   *   ........
   *   ........
   *   ........
   *   ........
   * Encoded data: offset=534, size=1
   */
  0xf0,

  /* Unicode: U+00AE (®) [REGISTERED SIGN], Size: 8x8
   * Glyph (bounding: <x: 0, y: 0, width: 8, height: 8>):
   *   .######.
   *   #......#
   *   #.###..#
   *   #.#..#.#
   *   #.###..#
   *   #.#..#.#
   *   #......#
   *   .######.
   * Encoded data: offset=535, size=8
   */
  0x7e, 0x81, 0xb9, 0xa5, 0xb9, 0xa5, 0x81, 0x7e,

  /* Unicode: U+00AF (¯) [MACRON], Size: 8x8
   * Glyph (bounding: <x: 1, y: 0, width: 6, height: 1>):
   *   .######.
   *   ........
   *   ........
   *   ........
   *   ........
   *   ........
   *   ........
   *   ........
   * Encoded data: offset=543, size=1
   */
  0xfc,

  /* Unicode: U+00B0 (°) [DEGREE SIGN], Size: 8x8
   * Glyph (bounding: <x: 1, y: 0, width: 6, height: 3>):
   *   ..####..
   *   .##..##.
   *   ..####..
   *   ........
   *   ........
   *   ........
   *   ........
   *   ........
   * Encoded data: offset=544, size=3
   */
  0x7b, 0x37, 0x80,

  /* Unicode: U+00B1 (±) [PLUS-MINUS SIGN], Size: 8x8
   * Glyph (bounding: <x: 1, y: 0, width: 6, height: 7>):
   *   ...##...
   *   ...##...
   *   .######.
   *   ...##...
   *   ...##...
   *   ........
   *   .######.
   *   ........
   * Encoded data: offset=547, size=6
   */
  0x30, 0xcf, 0xcc, 0x30, 0x0f, 0xc0,

  /* Unicode: U+00B2 (²) [SUPERSCRIPT TWO], Size: 8x8
   * Glyph (bounding: <x: 1, y: 0, width: 4, height: 5>):
   *   .###....
   *   ...##...
   *   ..##....
   *   .##.....
   *   .####...
   *   ........
   *   ........
   *   ........
   * Encoded data: offset=553, size=3
   */
  0xe3, 0x6c, 0xf0,

  /* Unicode: U+00B3 (³) [SUPERSCRIPT THREE], Size: 8x8
   * Glyph (bounding: <x: 1, y: 0, width: 5, height: 5>):
   *   .####...
   *   ....##..
   *   ...##...
   *   ....##..
   *   .####...
   *   ........
   *   ........
   *   ........
   * Encoded data: offset=556, size=4
   */
  0xf0, 0xcc, 0x3f, 0x00,

  /* Unicode: U+00B4 (´) [ACUTE ACCENT], Size: 8x8
   * Glyph (bounding: <x: 2, y: 0, width: 4, height: 3>):
   *   ....##..
   *   ...##...
   *   ..##....
   *   ........
   *   ........
   *   ........
   *   ........
   *   ........
   * Encoded data: offset=560, size=2
   */
  0x36, 0xc0,

  /* Unicode: U+00B5 (µ) [MICRO SIGN], Size: 8x8
   * Glyph (bounding: <x: 0, y: 2, width: 7, height: 6>):
   *   ........
   *   ........
   *   .##..##.
   *   .##..##.
   *   .##..##.
   *   .#####..
   *   .##.....
   *   ##......
   * Encoded data: offset=562, size=6
   */
  0x66, 0xcd, 0x9b, 0xe6, 0x18, 0x00,

  /* Unicode: U+00B6 (¶) [PILCROW SIGN], Size: 8x8
   * Glyph (bounding: <x: 1, y: 0, width: 6, height: 7>):
   *   ..#####.
   *   .####.#.
   *   .####.#.
   *   ..###.#.
   *   ...##.#.
   *   ...##.#.
   *   ...##.#.
   *   ........
   * Encoded data: offset=568, size=6
   */
  0x7f, 0xdf, 0x5d, 0x34, 0xd3, 0x40,

  /* Unicode: U+00B7 (·) [MIDDLE DOT], Size: 8x8
   * Glyph (bounding: <x: 3, y: 3, width: 2, height: 1>):
   *   ........
   *   ........
   *   ........
   *   ...##...
   *   ........
   *   ........
   *   ........
   *   ........
   * Encoded data: offset=574, size=1
   */
  0xc0,

  /* Unicode: U+00B8 (¸) [CEDILLA], Size: 8x8
   * Glyph (bounding: <x: 3, y: 5, width: 2, height: 2>):
   *   ........
   *   ........
   *   ........
   *   ........
   *   ........
   *   ....#...
   *   ...##...
   *   ........
   * Encoded data: offset=575, size=1
   */
  0x70,

  /* Unicode: U+00B9 (¹) [SUPERSCRIPT ONE], Size: 8x8
   * Glyph (bounding: <x: 1, y: 0, width: 3, height: 5>):
   *   ..##....
   *   .###....
   *   ..##....
   *   ..##....
   *   ..##....
   *   ........
   *   ........
   *   ........
   * Encoded data: offset=576, size=2
   */
  0x7d, 0xb6,

  /* Unicode: U+00BA (º) [MASCULINE ORDINAL INDICATOR], Size: 8x8
   * Glyph (bounding: <x: 1, y: 0, width: 5, height: 6>):
   *   ..###...
   *   .##.##..
   *   .##.##..
   *   ..###...
   *   ........
   *   .#####..
   *   ........
   *   ........
   * Encoded data: offset=578, size=4
   */
  0x76, 0xf6, 0xe0, 0x7c,

  /* Unicode: U+00BB (») [RIGHT-POINTING DOUBLE ANGLE QUOTATION MARK], Size: 8x8
   * Glyph (bounding: <x: 0, y: 1, width: 8, height: 5>):
   *   ........
   *   ##..##..
   *   .##..##.
   *   ..##..##
   *   .##..##.
   *   ##..##..
   *   ........
   *   ........
   * Encoded data: offset=582, size=5
   */
  0xcc, 0x66, 0x33, 0x66, 0xcc,

  /* Unicode: U+00BC (¼) [VULGAR FRACTION ONE QUARTER], Size: 8x8
   * Glyph (bounding: <x: 0, y: 0, width: 8, height: 8>):
   *   .#....##
   *   ##...##.
   *   .#..##..
   *   .#.##.#.
   *   ..##.##.
   *   .##.#.#.
   *   ##..####
   *   ......#.
   * Encoded data: offset=587, size=8
   */
  0x43, 0xc6, 0x4c, 0x5a, 0x36, 0x6a, 0xcf, 0x02,

  /* Unicode: U+00BD (½) [VULGAR FRACTION ONE HALF], Size: 8x8
   * Glyph (bounding: <x: 0, y: 0, width: 8, height: 8>):
   *   .#......
   *   ##...##.
   *   .#..##..
   *   .#.####.
   *   ..##..##
   *   .##..##.
   *   ##..##..
   *   ....####
   * Encoded data: offset=595, size=8
   */
  0x40, 0xc6, 0x4c, 0x5e, 0x33, 0x66, 0xcc, 0x0f,

  /* Unicode: U+00BE (¾) [VULGAR FRACTION THREE QUARTERS], Size: 8x8
   * Glyph (bounding: <x: 0, y: 0, width: 8, height: 8>):
   *   ##......
   *   ..#...##
   *   .##..##.
   *   ..#.##.#
   *   ##.##.##
   *   ..##.#.#
   *   .##..###
   *   .......#
   * Encoded data: offset=603, size=8
   */
  0xc0, 0x23, 0x66, 0x2d, 0xdb, 0x35, 0x67, 0x01,

  /* Unicode: U+00BF (¿) [INVERTED QUESTION MARK], Size: 8x8
   * Glyph (bounding: <x: 1, y: 0, width: 6, height: 7>):
   *   ...##...
   *   ........
   *   ...##...
   *   ..##....
   *   .##.....
   *   .##..##.
   *   ..####..
   *   ........
   * Encoded data: offset=611, size=6
   */
  0x30, 0x03, 0x18, 0xc3, 0x37, 0x80,

  /* Unicode: U+00C0 (À) [LATIN CAPITAL LETTER A WITH GRAVE], Size: 8x8
   * Glyph (bounding: <x: 1, y: 0, width: 6, height: 7>):
   *   .###....
   *   ........
   *   ..####..
   *   .##..##.
   *   .######.
   *   .##..##.
   *   .##..##.
   *   ........
   * Encoded data: offset=617, size=6
   */
  0xe0, 0x07, 0xb3, 0xff, 0x3c, 0xc0,

  /* Unicode: U+00C1 (Á) [LATIN CAPITAL LETTER A WITH ACUTE], Size: 8x8
   * Glyph (bounding: <x: 1, y: 0, width: 6, height: 7>):
   *   ....###.
   *   ........
   *   ..####..
   *   .##..##.
   *   .######.
   *   .##..##.
   *   .##..##.
   *   ........
   * Encoded data: offset=623, size=6
   */
  0x1c, 0x07, 0xb3, 0xff, 0x3c, 0xc0,

  /* Unicode: U+00C2 (Â) [LATIN CAPITAL LETTER A WITH CIRCUMFLEX], Size: 8x8
   * Glyph (bounding: <x: 1, y: 0, width: 6, height: 7>):
   *   ...##...
   *   .##..##.
   *   ........
   *   ..####..
   *   .##..##.
   *   .######.
   *   .##..##.
   *   ........
   * Encoded data: offset=629, size=6
   */
  0x33, 0x30, 0x1e, 0xcf, 0xfc, 0xc0,

  /* Unicode: U+00C3 (Ã) [LATIN CAPITAL LETTER A WITH TILDE], Size: 8x8
   * Glyph (bounding: <x: 0, y: 0, width: 7, height: 7>):
   *   .###.##.
   *   ##.###..
   *   ........
   *   ..####..
   *   .##..##.
   *   .######.
   *   .##..##.
   *   ........
   * Encoded data: offset=635, size=7
   */
  0x77, 0xb8, 0x01, 0xe6, 0x6f, 0xd9, 0x80,

  /* Unicode: U+00C4 (Ä) [LATIN CAPITAL LETTER A WITH DIAERESIS], Size: 8x8
   * Glyph (bounding: <x: 1, y: 0, width: 6, height: 7>):
   *   .##..##.
   *   ........
   *   ..####..
   *   .##..##.
   *   .######.
   *   .##..##.
   *   .##..##.
   *   ........
   * Encoded data: offset=642, size=6
   */
  0xcc, 0x07, 0xb3, 0xff, 0x3c, 0xc0,

  /* Unicode: U+00C5 (Å) [LATIN CAPITAL LETTER A WITH RING ABOVE], Size: 8x8
   * Glyph (bounding: <x: 1, y: 0, width: 6, height: 7>):
   *   ...##...
   *   ...##...
   *   ........
   *   ..####..
   *   .##..##.
   *   .######.
   *   .##..##.
   *   ........
   * Encoded data: offset=648, size=6
   */
  0x30, 0xc0, 0x1e, 0xcf, 0xfc, 0xc0,

  /* Unicode: U+00C6 (Æ) [LATIN CAPITAL LETTER AE], Size: 8x8
   * Glyph (bounding: <x: 0, y: 0, width: 8, height: 7>):
   *   ..######
   *   .##.##..
   *   ##..##..
   *   #######.
   *   ##..##..
   *   ##..##..
   *   ##..####
   *   ........
   * Encoded data: offset=654, size=7
   */
  0x3f, 0x6c, 0xcc, 0xfe, 0xcc, 0xcc, 0xcf,

  /* Unicode: U+00C7 (Ç) [LATIN CAPITAL LETTER C WITH CEDILLA], Size: 8x8
   * Glyph (bounding: <x: 1, y: 0, width: 6, height: 8>):
   *   ..####..
   *   .##..##.
   *   .##.....
   *   .##.....
   *   .##.....
   *   .##..##.
   *   ..####..
   *   ...##...
   * Encoded data: offset=661, size=6
   */
  0x7b, 0x3c, 0x30, 0xc3, 0x37, 0x8c,

  /* Unicode: U+00C8 (È) [LATIN CAPITAL LETTER E WITH GRAVE], Size: 8x8
   * Glyph (bounding: <x: 1, y: 0, width: 6, height: 7>):
   *   .###....
   *   ........
   *   ..####..
   *   .##..##.
   *   .######.
   *   .##..##.
   *   .##..##.
   *   ........
   * Encoded data: offset=667, size=6
   */
  0xe0, 0x07, 0xb3, 0xff, 0x3c, 0xc0,

  /* Unicode: U+00C9 (É) [LATIN CAPITAL LETTER E WITH ACUTE], Size: 8x8
   * Glyph (bounding: <x: 1, y: 0, width: 6, height: 7>):
   *   ....###.
   *   ........
   *   ..####..
   *   .##..##.
   *   .######.
   *   .##..##.
   *   .##..##.
   *   ........
   * Encoded data: offset=673, size=6
   */
  0x1c, 0x07, 0xb3, 0xff, 0x3c, 0xc0,

  /* Unicode: U+00CA (Ê) [LATIN CAPITAL LETTER E WITH CIRCUMFLEX], Size: 8x8
   * Glyph (bounding: <x: 0, y: 0, width: 7, height: 7>):
   *   ...##...
   *   .##..##.
   *   ........
   *   #######.
   *   ####....
   *   ##......
   *   #######.
   *   ........
   * Encoded data: offset=679, size=7
   */
  0x18, 0xcc, 0x07, 0xff, 0x18, 0x3f, 0x80,

  /* Unicode: U+00CB (Ë) [LATIN CAPITAL LETTER E WITH DIAERESIS], Size: 8x8
   * Glyph (bounding: <x: 1, y: 0, width: 6, height: 7>):
   *   .##..##.
   *   ........
   *   ..####..
   *   .##..##.
   *   .######.
   *   .##..##.
   *   .##..##.
   *   ........
   * Encoded data: offset=686, size=6
   */
  0xcc, 0x07, 0xb3, 0xff, 0x3c, 0xc0,

  /* Unicode: U+00CC (Ì) [LATIN CAPITAL LETTER I WITH GRAVE], Size: 8x8
   * Glyph (bounding: <x: 1, y: 0, width: 6, height: 7>):
   *   .###....
   *   ........
   *   .######.
   *   ...##...
   *   ...##...
   *   ...##...
   *   .######.
   *   ........
   * Encoded data: offset=692, size=6
   */
  0xe0, 0x0f, 0xcc, 0x30, 0xcf, 0xc0,

  /* Unicode: U+00CD (Í) [LATIN CAPITAL LETTER I WITH ACUTE], Size: 8x8
   * Glyph (bounding: <x: 1, y: 0, width: 6, height: 7>):
   *   ....###.
   *   ........
   *   .######.
   *   ...##...
   *   ...##...
   *   ...##...
   *   .######.
   *   ........
   * Encoded data: offset=698, size=6
   */
  0x1c, 0x0f, 0xcc, 0x30, 0xcf, 0xc0,

  /* Unicode: U+00CE (Î) [LATIN CAPITAL LETTER I WITH CIRCUMFLEX], Size: 8x8
   * Glyph (bounding: <x: 1, y: 0, width: 6, height: 7>):
   *   ...##...
   *   .##..##.
   *   ........
   *   .######.
   *   ...##...
   *   ...##...
   *   .######.
   *   ........
   * Encoded data: offset=704, size=6
   */
  0x33, 0x30, 0x3f, 0x30, 0xcf, 0xc0,

  /* Unicode: U+00CF (Ï) [LATIN CAPITAL LETTER I WITH DIAERESIS], Size: 8x8
   * Glyph (bounding: <x: 1, y: 0, width: 6, height: 7>):
   *   .##..##.
   *   ........
   *   .######.
   *   ...##...
   *   ...##...
   *   ...##...
   *   .######.
   *   ........
   * Encoded data: offset=710, size=6
   */
  0xcc, 0x0f, 0xcc, 0x30, 0xcf, 0xc0,

  /* Unicode: U+00D0 (Ð) [LATIN CAPITAL LETTER ETH], Size: 8x8
   * Glyph (bounding: <x: 0, y: 0, width: 7, height: 7>):
   *   .####...
   *   .##.##..
   *   .##..##.
   *   ####.##.
   *   .##..##.
   *   .##.##..
   *   .####...
   *   ........
   * Encoded data: offset=716, size=7
   */
  0x78, 0xd9, 0x9f, 0xb6, 0x6d, 0x9e, 0x00,

  /* Unicode: U+00D1 (Ñ) [LATIN CAPITAL LETTER N WITH TILDE], Size: 8x8
   * Glyph (bounding: <x: 0, y: 0, width: 7, height: 7>):
   *   .###.##.
   *   ##.###..
   *   ........
   *   ##...##.
   *   ####.##.
   *   ##.####.
   *   ##...##.
   *   ........
   * Encoded data: offset=723, size=7
   */
  0x77, 0xb8, 0x06, 0x3f, 0x7b, 0xf1, 0x80,

  /* Unicode: U+00D2 (Ò) [LATIN CAPITAL LETTER O WITH GRAVE], Size: 8x8
   * Glyph (bounding: <x: 0, y: 0, width: 7, height: 7>):
   *   .###....
   *   ........
   *   .#####..
   *   ##...##.
   *   ##...##.
   *   ##...##.
   *   .#####..
   *   ........
   * Encoded data: offset=730, size=7
   */
  0x70, 0x01, 0xf6, 0x3c, 0x78, 0xdf, 0x00,

  /* Unicode: U+00D3 (Ó) [LATIN CAPITAL LETTER O WITH ACUTE], Size: 8x8
   * Glyph (bounding: <x: 0, y: 0, width: 7, height: 7>):
   *   ....###.
   *   ........
   *   .#####..
   *   ##...##.
   *   ##...##.
   *   ##...##.
   *   .#####..
   *   ........
   * Encoded data: offset=737, size=7
   */
  0x0e, 0x01, 0xf6, 0x3c, 0x78, 0xdf, 0x00,

  /* Unicode: U+00D4 (Ô) [LATIN CAPITAL LETTER O WITH CIRCUMFLEX], Size: 8x8
   * Glyph (bounding: <x: 0, y: 0, width: 7, height: 7>):
   *   ...##...
   *   .##..##.
   *   ........
   *   .#####..
   *   ##...##.
   *   ##...##.
   *   .#####..
   *   ........
   * Encoded data: offset=744, size=7
   */
  0x18, 0xcc, 0x03, 0xec, 0x78, 0xdf, 0x00,

  /* Unicode: U+00D5 (Õ) [LATIN CAPITAL LETTER O WITH TILDE], Size: 8x8
   * Glyph (bounding: <x: 0, y: 0, width: 7, height: 7>):
   *   .###.##.
   *   ##.###..
   *   ........
   *   .#####..
   *   ##...##.
   *   ##...##.
   *   .#####..
   *   ........
   * Encoded data: offset=751, size=7
   */
  0x77, 0xb8, 0x03, 0xec, 0x78, 0xdf, 0x00,

  /* Unicode: U+00D6 (Ö) [LATIN CAPITAL LETTER O WITH DIAERESIS], Size: 8x8
   * Glyph (bounding: <x: 0, y: 0, width: 7, height: 7>):
   *   .##..##.
   *   ........
   *   .#####..
   *   ##...##.
   *   ##...##.
   *   ##...##.
   *   .#####..
   *   ........
   * Encoded data: offset=758, size=7
   */
  0x66, 0x01, 0xf6, 0x3c, 0x78, 0xdf, 0x00,

  /* Unicode: U+00D7 (×) [MULTIPLICATION SIGN], Size: 8x8
   * Glyph (bounding: <x: 0, y: 1, width: 7, height: 5>):
   *   ........
   *   ##...##.
   *   .##.##..
   *   ..###...
   *   .##.##..
   *   ##...##.
   *   ........
   *   ........
   * Encoded data: offset=765, size=5
   */
  0xc6, 0xd8, 0xe3, 0x6c, 0x60,

  /* Unicode: U+00D8 (Ø) [LATIN CAPITAL LETTER O WITH STROKE], Size: 8x8
   * Glyph (bounding: <x: 1, y: 0, width: 6, height: 7>):
   *   ..#####.
   *   .##..##.
   *   .##.###.
   *   .######.
   *   .###.##.
   *   .##..##.
   *   .#####..
   *   ........
   * Encoded data: offset=770, size=6
   */
  0x7f, 0x3d, 0xff, 0xef, 0x3f, 0x80,

  /* Unicode: U+00D9 (Ù) [LATIN CAPITAL LETTER U WITH GRAVE], Size: 8x8
   * Glyph (bounding: <x: 0, y: 0, width: 7, height: 7>):
   *   .###....
   *   ........
   *   ##...##.
   *   ##...##.
   *   ##...##.
   *   ##...##.
   *   .#####..
   *   ........
   * Encoded data: offset=776, size=7
   */
  0x70, 0x03, 0x1e, 0x3c, 0x78, 0xdf, 0x00,

  /* Unicode: U+00DA (Ú) [LATIN CAPITAL LETTER U WITH ACUTE], Size: 8x8
   * Glyph (bounding: <x: 0, y: 0, width: 7, height: 7>):
   *   ....###.
   *   ........
   *   ##...##.
   *   ##...##.
   *   ##...##.
   *   ##...##.
   *   .#####..
   *   ........
   * Encoded data: offset=783, size=7
   */
  0x0e, 0x03, 0x1e, 0x3c, 0x78, 0xdf, 0x00,

  /* Unicode: U+00DB (Û) [LATIN CAPITAL LETTER U WITH CIRCUMFLEX], Size: 8x8
   * Glyph (bounding: <x: 0, y: 0, width: 7, height: 7>):
   *   ...##...
   *   .##..##.
   *   ........
   *   ##...##.
   *   ##...##.
   *   ##...##.
   *   .#####..
   *   ........
   * Encoded data: offset=790, size=7
   */
  0x18, 0xcc, 0x06, 0x3c, 0x78, 0xdf, 0x00,

  /* Unicode: U+00DC (Ü) [LATIN CAPITAL LETTER U WITH DIAERESIS], Size: 8x8
   * Glyph (bounding: <x: 0, y: 0, width: 7, height: 7>):
   *   .##..##.
   *   ........
   *   ##...##.
   *   ##...##.
   *   ##...##.
   *   ##...##.
   *   .#####..
   *   ........
   * Encoded data: offset=797, size=7
   */
  0x66, 0x03, 0x1e, 0x3c, 0x78, 0xdf, 0x00,

  /* Unicode: U+00DD (Ý) [LATIN CAPITAL LETTER Y WITH ACUTE], Size: 8x8
   * Glyph (bounding: <x: 1, y: 0, width: 6, height: 7>):
   *   ....###.
   *   ........
   *   .##..##.
   *   .##..##.
   *   ..####..
   *   ...##...
   *   ...##...
   *   ........
   * Encoded data: offset=804, size=6
   */
  0x1c, 0x0c, 0xf3, 0x78, 0xc3, 0x00,

  /* Unicode: U+00DE (Þ) [LATIN CAPITAL LETTER THORN], Size: 8x8
   * Glyph (bounding: <x: 0, y: 0, width: 7, height: 7>):
   *   ##......
   *   ##......
   *   ######..
   *   ##...##.
   *   ######..
   *   ##......
   *   ##......
   *   ........
   * Encoded data: offset=810, size=7
   */
  0xc1, 0x83, 0xf6, 0x3f, 0xd8, 0x30, 0x00,

  /* Unicode: U+00DF (ß) [LATIN SMALL LETTER SHARP S], Size: 8x8
   * Glyph (bounding: <x: 1, y: 0, width: 6, height: 7>):
   *   ..####..
   *   .##..##.
   *   .##..##.
   *   .##.##..
   *   .##..##.
   *   .##..##.
   *   .##.##..
   *   ........
   * Encoded data: offset=817, size=6
   */
  0x7b, 0x3c, 0xf6, 0xcf, 0x3d, 0x80,

  /* Unicode: U+00E0 (à) [LATIN SMALL LETTER A WITH GRAVE], Size: 8x8
   * Glyph (bounding: <x: 1, y: 0, width: 6, height: 7>):
   *   .###....
   *   ........
   *   ..####..
   *   .....##.
   *   ..#####.
   *   .##..##.
   *   ..#####.
   *   ........
   * Encoded data: offset=823, size=6
   */
  0xe0, 0x07, 0x83, 0x7f, 0x37, 0xc0,

  /* Unicode: U+00E1 (á) [LATIN SMALL LETTER A WITH ACUTE], Size: 8x8
   * Glyph (bounding: <x: 1, y: 0, width: 6, height: 7>):
   *   ....###.
   *   ........
   *   ..####..
   *   .....##.
   *   ..#####.
   *   .##..##.
   *   ..#####.
   *   ........
   * Encoded data: offset=829, size=6
   */
  0x1c, 0x07, 0x83, 0x7f, 0x37, 0xc0,

  /* Unicode: U+00E2 (â) [LATIN SMALL LETTER A WITH CIRCUMFLEX], Size: 8x8
   * Glyph (bounding: <x: 0, y: 0, width: 7, height: 7>):
   *   ...##...
   *   .##..##.
   *   ........
   *   ..#####.
   *   .##..##.
   *   ##...##.
   *   .######.
   *   ........
   * Encoded data: offset=835, size=7
   */
  0x18, 0xcc, 0x01, 0xf6, 0x78, 0xdf, 0x80,

  /* Unicode: U+00E3 (ã) [LATIN SMALL LETTER A WITH TILDE], Size: 8x8
   * Glyph (bounding: <x: 0, y: 0, width: 7, height: 7>):
   *   .###.##.
   *   ##.###..
   *   ........
   *   ..#####.
   *   .##..##.
   *   ##...##.
   *   .######.
   *   ........
   * Encoded data: offset=842, size=7
   */
  0x77, 0xb8, 0x01, 0xf6, 0x78, 0xdf, 0x80,

  /* Unicode: U+00E4 (ä) [LATIN SMALL LETTER A WITH DIAERESIS], Size: 8x8
   * Glyph (bounding: <x: 1, y: 0, width: 6, height: 7>):
   *   .##..##.
   *   ........
   *   ..####..
   *   .....##.
   *   ..#####.
   *   .##..##.
   *   ..#####.
   *   ........
   * Encoded data: offset=849, size=6
   */
  0xcc, 0x07, 0x83, 0x7f, 0x37, 0xc0,

  /* Unicode: U+00E5 (å) [LATIN SMALL LETTER A WITH RING ABOVE], Size: 8x8
   * Glyph (bounding: <x: 0, y: 0, width: 7, height: 7>):
   *   ...##...
   *   ...##...
   *   ........
   *   ..#####.
   *   .##..##.
   *   ##...##.
   *   .######.
   *   ........
   * Encoded data: offset=855, size=7
   */
  0x18, 0x30, 0x01, 0xf6, 0x78, 0xdf, 0x80,

  /* Unicode: U+00E6 (æ) [LATIN SMALL LETTER AE], Size: 8x8
   * Glyph (bounding: <x: 0, y: 2, width: 8, height: 5>):
   *   ........
   *   ........
   *   .######.
   *   ...##.##
   *   .#######
   *   ##.##...
   *   .###.###
   *   ........
   * Encoded data: offset=862, size=5
   */
  0x7e, 0x1b, 0x7f, 0xd8, 0x77,

  /* Unicode: U+00E7 (ç) [LATIN SMALL LETTER C WITH CEDILLA], Size: 8x8
   * Glyph (bounding: <x: 1, y: 2, width: 5, height: 6>):
   *   ........
   *   ........
   *   ..####..
   *   .##.....
   *   .##.....
   *   .##.....
   *   ..####..
   *   ...##...
   * Encoded data: offset=867, size=4
   */
  0x7e, 0x31, 0x87, 0x98,

  /* Unicode: U+00E8 (è) [LATIN SMALL LETTER E WITH GRAVE], Size: 8x8
   * Glyph (bounding: <x: 1, y: 0, width: 6, height: 7>):
   *   .###....
   *   ........
   *   ..####..
   *   .##..##.
   *   .######.
   *   .##.....
   *   ..####..
   *   ........
   * Encoded data: offset=871, size=6
   */
  0xe0, 0x07, 0xb3, 0xff, 0x07, 0x80,

  /* Unicode: U+00E9 (é) [LATIN SMALL LETTER E WITH ACUTE], Size: 8x8
   * Glyph (bounding: <x: 1, y: 0, width: 6, height: 7>):
   *   ....###.
   *   ........
   *   ..####..
   *   .##..##.
   *   .######.
   *   .##.....
   *   ..####..
   *   ........
   * Encoded data: offset=877, size=6
   */
  0x1c, 0x07, 0xb3, 0xff, 0x07, 0x80,

  /* Unicode: U+00EA (ê) [LATIN SMALL LETTER E WITH CIRCUMFLEX], Size: 8x8
   * Glyph (bounding: <x: 1, y: 0, width: 6, height: 7>):
   *   ...##...
   *   .##..##.
   *   ........
   *   ..####..
   *   .######.
   *   .##.....
   *   ..####..
   *   ........
   * Encoded data: offset=883, size=6
   */
  0x33, 0x30, 0x1e, 0xff, 0x07, 0x80,

  /* Unicode: U+00EB (ë) [LATIN SMALL LETTER E WITH DIAERESIS], Size: 8x8
   * Glyph (bounding: <x: 1, y: 0, width: 6, height: 7>):
   *   .##..##.
   *   ........
   *   ..####..
   *   .##..##.
   *   .######.
   *   .##.....
   *   ..####..
   *   ........
   * Encoded data: offset=889, size=6
   */
  0xcc, 0x07, 0xb3, 0xff, 0x07, 0x80,

  /* Unicode: U+00EC (ì) [LATIN SMALL LETTER I WITH GRAVE], Size: 8x8
   * Glyph (bounding: <x: 1, y: 0, width: 5, height: 7>):
   *   .###....
   *   ........
   *   ..###...
   *   ...##...
   *   ...##...
   *   ...##...
   *   ..####..
   *   ........
   * Encoded data: offset=895, size=5
   */
  0xe0, 0x1c, 0x63, 0x19, 0xe0,

  /* Unicode: U+00ED (í) [LATIN SMALL LETTER I WITH ACUTE], Size: 8x8
   * Glyph (bounding: <x: 2, y: 0, width: 5, height: 7>):
   *   ....###.
   *   ........
   *   ..###...
   *   ...##...
   *   ...##...
   *   ...##...
   *   ..####..
   *   ........
   * Encoded data: offset=900, size=5
   */
  0x38, 0x38, 0xc6, 0x33, 0xc0,

  /* Unicode: U+00EE (î) [LATIN SMALL LETTER I WITH CIRCUMFLEX], Size: 8x8
   * Glyph (bounding: <x: 1, y: 0, width: 6, height: 7>):
   *   ...##...
   *   .##..##.
   *   ........
   *   ..###...
   *   ...##...
   *   ...##...
   *   ..####..
   *   ........
   * Encoded data: offset=905, size=6
   */
  0x33, 0x30, 0x1c, 0x30, 0xc7, 0x80,

  /* Unicode: U+00EF (ï) [LATIN SMALL LETTER I WITH DIAERESIS], Size: 8x8
   * Glyph (bounding: <x: 1, y: 0, width: 6, height: 7>):
   *   .##..##.
   *   ........
   *   ..###...
   *   ...##...
   *   ...##...
   *   ...##...
   *   ..####..
   *   ........
   * Encoded data: offset=911, size=6
   */
  0xcc, 0x07, 0x0c, 0x30, 0xc7, 0x80,

  /* Unicode: U+00F0 (ð) [LATIN SMALL LETTER ETH], Size: 8x8
   * Glyph (bounding: <x: 0, y: 0, width: 7, height: 7>):
   *   ....##..
   *   ..#####.
   *   ....##..
   *   .#####..
   *   ##..##..
   *   ##..##..
   *   .####...
   *   ........
   * Encoded data: offset=917, size=7
   */
  0x0c, 0x7c, 0x33, 0xec, 0xd9, 0x9e, 0x00,

  /* Unicode: U+00F1 (ñ) [LATIN SMALL LETTER N WITH TILDE], Size: 8x8
   * Glyph (bounding: <x: 0, y: 0, width: 7, height: 7>):
   *   .###.##.
   *   ##.###..
   *   ........
   *   .#####..
   *   .##..##.
   *   .##..##.
   *   .##..##.
   *   ........
   * Encoded data: offset=924, size=7
   */
  0x77, 0xb8, 0x03, 0xe6, 0x6c, 0xd9, 0x80,

  /* Unicode: U+00F2 (ò) [LATIN SMALL LETTER O WITH GRAVE], Size: 8x8
   * Glyph (bounding: <x: 1, y: 0, width: 6, height: 7>):
   *   .###....
   *   ........
   *   ..####..
   *   .##..##.
   *   .##..##.
   *   .##..##.
   *   ..####..
   *   ........
   * Encoded data: offset=931, size=6
   */
  0xe0, 0x07, 0xb3, 0xcf, 0x37, 0x80,

  /* Unicode: U+00F3 (ó) [LATIN SMALL LETTER O WITH ACUTE], Size: 8x8
   * Glyph (bounding: <x: 1, y: 0, width: 6, height: 7>):
   *   ....###.
   *   ........
   *   ..####..
   *   .##..##.
   *   .##..##.
   *   .##..##.
   *   ..####..
   *   ........
   * Encoded data: offset=937, size=6
   */
  0x1c, 0x07, 0xb3, 0xcf, 0x37, 0x80,

  /* Unicode: U+00F4 (ô) [LATIN SMALL LETTER O WITH CIRCUMFLEX], Size: 8x8
   * Glyph (bounding: <x: 1, y: 0, width: 6, height: 7>):
   *   ...##...
   *   .##..##.
   *   ........
   *   ..####..
   *   .##..##.
   *   .##..##.
   *   ..####..
   *   ........
   * Encoded data: offset=943, size=6
   */
  0x33, 0x30, 0x1e, 0xcf, 0x37, 0x80,

  /* Unicode: U+00F5 (õ) [LATIN SMALL LETTER O WITH TILDE], Size: 8x8
   * Glyph (bounding: <x: 0, y: 0, width: 7, height: 7>):
   *   .###.##.
   *   ##.###..
   *   ........
   *   ..####..
   *   .##..##.
   *   .##..##.
   *   ..####..
   *   ........
   * Encoded data: offset=949, size=7
   */
  0x77, 0xb8, 0x01, 0xe6, 0x6c, 0xcf, 0x00,

  /* Unicode: U+00F6 (ö) [LATIN SMALL LETTER O WITH DIAERESIS], Size: 8x8
   * Glyph (bounding: <x: 1, y: 0, width: 6, height: 7>):
   *   .##..##.
   *   ........
   *   ..####..
   *   .##..##.
   *   .##..##.
   *   .##..##.
   *   ..####..
   *   ........
   * Encoded data: offset=956, size=6
   */
  0xcc, 0x07, 0xb3, 0xcf, 0x37, 0x80,

  /* Unicode: U+00F7 (÷) [DIVISION SIGN], Size: 8x8
   * Glyph (bounding: <x: 1, y: 0, width: 6, height: 7>):
   *   ...##...
   *   ...##...
   *   ........
   *   .######.
   *   ........
   *   ...##...
   *   ...##...
   *   ........
   * Encoded data: offset=962, size=6
   */
  0x30, 0xc0, 0x3f, 0x00, 0xc3, 0x00,

  /* Unicode: U+00F8 (ø) [LATIN SMALL LETTER O WITH STROKE], Size: 8x8
   * Glyph (bounding: <x: 0, y: 1, width: 7, height: 7>):
   *   ........
   *   ......#.
   *   .#####..
   *   ##..###.
   *   ##.#.##.
   *   ###..##.
   *   .#####..
   *   #.......
   * Encoded data: offset=968, size=7
   */
  0x02, 0xfb, 0x3e, 0xbe, 0x6f, 0xa0, 0x00,

  /* Unicode: U+00F9 (ù) [LATIN SMALL LETTER U WITH GRAVE], Size: 8x8
   * Glyph (bounding: <x: 1, y: 0, width: 6, height: 7>):
   *   .###....
   *   ........
   *   .##..##.
   *   .##..##.
   *   .##..##.
   *   .##..##.
   *   ..#####.
   *   ........
   * Encoded data: offset=975, size=6
   */
  0xe0, 0x0c, 0xf3, 0xcf, 0x37, 0xc0,

  /* Unicode: U+00FA (ú) [LATIN SMALL LETTER U WITH ACUTE], Size: 8x8
   * Glyph (bounding: <x: 1, y: 0, width: 6, height: 7>):
   *   ....###.
   *   ........
   *   .##..##.
   *   .##..##.
   *   .##..##.
   *   .##..##.
   *   ..#####.
   *   ........
   * Encoded data: offset=981, size=6
   */
  0x1c, 0x0c, 0xf3, 0xcf, 0x37, 0xc0,

  /* Unicode: U+00FB (û) [LATIN SMALL LETTER U WITH CIRCUMFLEX], Size: 8x8
   * Glyph (bounding: <x: 1, y: 0, width: 6, height: 7>):
   *   ...##...
   *   .##..##.
   *   ........
   *   .##..##.
   *   .##..##.
   *   .##..##.
   *   ..#####.
   *   ........
   * Encoded data: offset=987, size=6
   */
  0x33, 0x30, 0x33, 0xcf, 0x37, 0xc0,

  /* Unicode: U+00FC (ü) [LATIN SMALL LETTER U WITH DIAERESIS], Size: 8x8
   * Glyph (bounding: <x: 1, y: 0, width: 6, height: 7>):
   *   .##..##.
   *   ........
   *   .##..##.
   *   .##..##.
   *   .##..##.
   *   .##..##.
   *   ..#####.
   *   ........
   * Encoded data: offset=993, size=6
   */
  0xcc, 0x0c, 0xf3, 0xcf, 0x37, 0xc0,

  /* Unicode: U+00FD (ý) [LATIN SMALL LETTER Y WITH ACUTE], Size: 8x8
   * Glyph (bounding: <x: 1, y: 0, width: 6, height: 8>):
   *   ....###.
   *   ........
   *   .##..##.
   *   .##..##.
   *   .##..##.
   *   ..#####.
   *   .....##.
   *   ..####..
   * Encoded data: offset=999, size=6
   */
  0x1c, 0x0c, 0xf3, 0xcd, 0xf0, 0xde,

  /* Unicode: U+00FE (þ) [LATIN SMALL LETTER THORN], Size: 8x8
   * Glyph (bounding: <x: 1, y: 0, width: 6, height: 8>):
   *   .##.....
   *   .##.....
   *   .#####..
   *   .##..##.
   *   .##..##.
   *   .#####..
   *   .##.....
   *   .##.....
   * Encoded data: offset=1005, size=6
   */
  0xc3, 0x0f, 0xb3, 0xcf, 0xec, 0x30,

  /* Unicode: U+00FF (ÿ) [LATIN SMALL LETTER Y WITH DIAERESIS], Size: 8x8
   * Glyph (bounding: <x: 1, y: 0, width: 6, height: 8>):
   *   .##..##.
   *   ........
   *   .##..##.
   *   .##..##.
   *   .##..##.
   *   ..#####.
   *   .....##.
   *   ..####..
   * Encoded data: offset=1011, size=6
   */
  0xcc, 0x0c, 0xf3, 0xcd, 0xf0, 0xde,

  /* Unicode: U+2190 (←) [LEFTWARDS ARROW], Size: 8x8
   * Glyph (bounding: <x: 1, y: 1, width: 7, height: 6>):
   *   ........
   *   ...#....
   *   ..##....
   *   .#######
   *   .#######
   *   ..##....
   *   ...#....
   *   ........
   * Encoded data: offset=1017, size=6
   */
  0x20, 0xc3, 0xff, 0xf6, 0x04, 0x00,

  /* Unicode: U+2191 (↑) [UPWARDS ARROW], Size: 8x8
   * Glyph (bounding: <x: 1, y: 0, width: 6, height: 8>):
   *   ...##...
   *   ..####..
   *   .######.
   *   ...##...
   *   ...##...
   *   ...##...
   *   ...##...
   *   ...##...
   * Encoded data: offset=1023, size=6
   */
  0x31, 0xef, 0xcc, 0x30, 0xc3, 0x0c,

  /* Unicode: U+2192 (→) [RIGHTWARDS ARROW], Size: 8x8
   * Glyph (bounding: <x: 0, y: 1, width: 7, height: 6>):
   *   ........
   *   ....#...
   *   ....##..
   *   #######.
   *   #######.
   *   ....##..
   *   ....#...
   *   ........
   * Encoded data: offset=1029, size=6
   */
  0x08, 0x1b, 0xff, 0xf0, 0xc1, 0x00,

  /* Unicode: U+2194 (↔) [LEFT RIGHT ARROW], Size: 8x8
   * Glyph (bounding: <x: 0, y: 1, width: 8, height: 6>):
   *   ........
   *   ..#..#..
   *   .##..##.
   *   ########
   *   ########
   *   .##..##.
   *   ..#..#..
   *   ........
   * Encoded data: offset=1035, size=6
   */
  0x24, 0x66, 0xff, 0xff, 0x66, 0x24,

  /* Unicode: U+2196 (↖) [NORTH WEST ARROW], Size: 8x8
   * Glyph (bounding: <x: 0, y: 0, width: 7, height: 7>):
   *   ####....
   *   ###.....
   *   ####....
   *   #.###...
   *   ...###..
   *   ....###.
   *   .....#..
   *   ........
   * Encoded data: offset=1041, size=7
   */
  0xf1, 0xc3, 0xc5, 0xc1, 0xc1, 0xc1, 0x00,

  /* Unicode: U+2197 (↗) [NORTH EAST ARROW], Size: 8x8
   * Glyph (bounding: <x: 1, y: 0, width: 7, height: 7>):
   *   ....####
   *   .....###
   *   ....####
   *   ...###.#
   *   ..###...
   *   .###....
   *   ..#.....
   *   ........
   * Encoded data: offset=1048, size=7
   */
  0x1e, 0x1c, 0x79, 0xd7, 0x1c, 0x10, 0x00,

  /* Unicode: U+2198 (↘) [SOUTH EAST ARROW], Size: 8x8
   * Glyph (bounding: <x: 1, y: 1, width: 7, height: 7>):
   *   ........
   *   ..#.....
   *   .###....
   *   ..###...
   *   ...###.#
   *   ....####
   *   .....###
   *   ....####
   * Encoded data: offset=1055, size=7
   */
  0x41, 0xc1, 0xc1, 0xd1, 0xe1, 0xc7, 0x80,

  /* Unicode: U+2199 (↙) [SOUTH WEST ARROW], Size: 8x8
   * Glyph (bounding: <x: 0, y: 1, width: 7, height: 7>):
   *   ........
   *   .....#..
   *   ....###.
   *   ...###..
   *   #.###...
   *   ####....
   *   ###.....
   *   ####....
   * Encoded data: offset=1062, size=7
   */
  0x04, 0x1c, 0x75, 0xcf, 0x1c, 0x3c, 0x00,

  /* Unicode: U+21A5 (↥) [UPWARDS ARROW FROM BAR], Size: 8x8
   * Glyph (bounding: <x: 1, y: 0, width: 6, height: 8>):
   *   ...##...
   *   ..####..
   *   .######.
   *   ...##...
   *   ...##...
   *   .######.
   *   ..####..
   *   ...##...
   * Encoded data: offset=1069, size=6
   */
  0x31, 0xef, 0xcc, 0x33, 0xf7, 0x8c,

  /* Unicode: U+21A8 (↨) [UP DOWN ARROW WITH BASE], Size: 8x8
   * Glyph (bounding: <x: 0, y: 0, width: 8, height: 8>):
   *   ...##...
   *   ..####..
   *   .######.
   *   ...##...
   *   .######.
   *   ..####..
   *   ...##...
   *   ########
   * Encoded data: offset=1075, size=8
   */
  0x18, 0x3c, 0x7e, 0x18, 0x7e, 0x3c, 0x18, 0xff,

  /* Unicode: U+21E6 (⇦) [LEFTWARDS WHITE ARROW], Size: 8x8
   * Glyph (bounding: <x: 0, y: 0, width: 8, height: 8>):
   *   ...##...
   *   ..###...
   *   .##.####
   *   ##.....#
   *   ##.....#
   *   .##.####
   *   ..###...
   *   ...##...
   * Encoded data: offset=1083, size=8
   */
  0x18, 0x38, 0x6f, 0xc1, 0xc1, 0x6f, 0x38, 0x18,

  /* Unicode: U+21E7 (⇧) [UPWARDS WHITE ARROW], Size: 8x8
   * Glyph (bounding: <x: 0, y: 0, width: 8, height: 8>):
   *   ...##...
   *   ..####..
   *   .##..##.
   *   ##....##
   *   ###..###
   *   ..#..#..
   *   ..#..#..
   *   ..####..
   * Encoded data: offset=1091, size=8
   */
  0x18, 0x3c, 0x66, 0xc3, 0xe7, 0x24, 0x24, 0x3c,

  /* Unicode: U+21E8 (⇨) [RIGHTWARDS WHITE ARROW], Size: 8x8
   * Glyph (bounding: <x: 0, y: 0, width: 8, height: 8>):
   *   ...##...
   *   ...###..
   *   ####.##.
   *   #.....##
   *   #.....##
   *   ####.##.
   *   ...###..
   *   ...##...
   * Encoded data: offset=1099, size=8
   */
  0x18, 0x1c, 0xf6, 0x83, 0x83, 0xf6, 0x1c, 0x18,

  /* Unicode: U+21E9 (⇩) [DOWNWARDS WHITE ARROW], Size: 8x8
   * Glyph (bounding: <x: 0, y: 0, width: 8, height: 8>):
   *   ..####..
   *   ..#..#..
   *   ..#..#..
   *   ###..###
   *   ##....##
   *   .##..##.
   *   ..####..
   *   ...##...
   * Encoded data: offset=1107, size=8
   */
  0x3c, 0x24, 0x24, 0xe7, 0xc3, 0x66, 0x3c, 0x18,

  /* Unicode: U+2500 (─) [BOX DRAWINGS LIGHT HORIZONTAL], Size: 8x8
   * Glyph (bounding: <x: 0, y: 3, width: 8, height: 2>):
   *   ........
   *   ........
   *   ........
   *   ########
   *   ########
   *   ........
   *   ........
   *   ........
   * Encoded data: offset=1115, size=2
   */
  0xff, 0xff,

  /* Unicode: U+2501 (━) [BOX DRAWINGS HEAVY HORIZONTAL], Size: 8x8
   * Glyph (bounding: <x: 0, y: 2, width: 8, height: 4>):
   *   ........
   *   ........
   *   ########
   *   ########
   *   ########
   *   ########
   *   ........
   *   ........
   * Encoded data: offset=1117, size=4
   */
  0xff, 0xff, 0xff, 0xff,

  /* Unicode: U+2502 (│) [BOX DRAWINGS LIGHT VERTICAL], Size: 8x8
   * Glyph (bounding: <x: 3, y: 0, width: 2, height: 8>):
   *   ...##...
   *   ...##...
   *   ...##...
   *   ...##...
   *   ...##...
   *   ...##...
   *   ...##...
   *   ...##...
   * Encoded data: offset=1121, size=2
   */
  0xff, 0xff,

  /* Unicode: U+2503 (┃) [BOX DRAWINGS HEAVY VERTICAL], Size: 8x8
   * Glyph (bounding: <x: 2, y: 0, width: 4, height: 8>):
   *   ..####..
   *   ..####..
   *   ..####..
   *   ..####..
   *   ..####..
   *   ..####..
   *   ..####..
   *   ..####..
   * Encoded data: offset=1123, size=4
   */
  0xff, 0xff, 0xff, 0xff,

  /* Unicode: U+2504 (┄) [BOX DRAWINGS LIGHT TRIPLE DASH HORIZONTAL], Size: 8x8
   * Glyph (bounding: <x: 0, y: 3, width: 8, height: 2>):
   *   ........
   *   ........
   *   ........
   *   ##.##.##
   *   ##.##.##
   *   ........
   *   ........
   *   ........
   * Encoded data: offset=1127, size=2
   */
  0xdb, 0xdb,

  /* Unicode: U+2505 (┅) [BOX DRAWINGS HEAVY TRIPLE DASH HORIZONTAL], Size: 8x8
   * Glyph (bounding: <x: 0, y: 2, width: 8, height: 4>):
   *   ........
   *   ........
   *   ##.##.##
   *   ##.##.##
   *   ##.##.##
   *   ##.##.##
   *   ........
   *   ........
   * Encoded data: offset=1129, size=4
   */
  0xdb, 0xdb, 0xdb, 0xdb,

  /* Unicode: U+2506 (┆) [BOX DRAWINGS LIGHT TRIPLE DASH VERTICAL], Size: 8x8
   * Glyph (bounding: <x: 3, y: 0, width: 2, height: 8>):
   *   ...##...
   *   ...##...
   *   ........
   *   ...##...
   *   ...##...
   *   ...##...
   *   ........
   *   ...##...
   * Encoded data: offset=1133, size=2
   */
  0xf3, 0xf3,

  /* Unicode: U+2507 (┇) [BOX DRAWINGS HEAVY TRIPLE DASH VERTICAL], Size: 8x8
   * Glyph (bounding: <x: 2, y: 0, width: 4, height: 8>):
   *   ..####..
   *   ..####..
   *   ........
   *   ..####..
   *   ..####..
   *   ..####..
   *   ........
   *   ..####..
   * Encoded data: offset=1135, size=4
   */
  0xff, 0x0f, 0xff, 0x0f,

  /* Unicode: U+2508 (┈) [BOX DRAWINGS LIGHT QUADRUPLE DASH HORIZONTAL], Size: 8x8
   * Glyph (bounding: <x: 0, y: 3, width: 7, height: 2>):
   *   ........
   *   ........
   *   ........
   *   #.#.#.#.
   *   #.#.#.#.
   *   ........
   *   ........
   *   ........
   * Encoded data: offset=1139, size=2
   */
  0xab, 0x54,

  /* Unicode: U+2509 (┉) [BOX DRAWINGS HEAVY QUADRUPLE DASH HORIZONTAL], Size: 8x8
   * Glyph (bounding: <x: 0, y: 3, width: 7, height: 2>):
   *   ........
   *   ........
   *   ........
   *   #.#.#.#.
   *   #.#.#.#.
   *   ........
   *   ........
   *   ........
   * Encoded data: offset=1141, size=2
   */
  0xab, 0x54,

  /* Unicode: U+250A (┊) [BOX DRAWINGS LIGHT QUADRUPLE DASH VERTICAL], Size: 8x8
   * Glyph (bounding: <x: 3, y: 0, width: 2, height: 7>):
   *   ...##...
   *   ........
   *   ...##...
   *   ........
   *   ...##...
   *   ........
   *   ...##...
   *   ........
   * Encoded data: offset=1143, size=2
   */
  0xcc, 0xcc,

  /* Unicode: U+250B (┋) [BOX DRAWINGS HEAVY QUADRUPLE DASH VERTICAL], Size: 8x8
   * Glyph (bounding: <x: 2, y: 0, width: 4, height: 7>):
   *   ..####..
   *   ........
   *   ..####..
   *   ........
   *   ..####..
   *   ........
   *   ..####..
   *   ........
   * Encoded data: offset=1145, size=4
   */
  0xf0, 0xf0, 0xf0, 0xf0,

  /* Unicode: U+250C (┌) [BOX DRAWINGS LIGHT DOWN AND RIGHT], Size: 8x8
   * Glyph (bounding: <x: 3, y: 3, width: 5, height: 5>):
   *   ........
   *   ........
   *   ........
   *   ...#####
   *   ...#####
   *   ...##...
   *   ...##...
   *   ...##...
   * Encoded data: offset=1149, size=4
   */
  0xff, 0xf1, 0x8c, 0x00,

  /* Unicode: U+250D (┍) [BOX DRAWINGS DOWN LIGHT AND RIGHT HEAVY], Size: 8x8
   * Glyph (bounding: <x: 3, y: 2, width: 5, height: 6>):
   *   ........
   *   ........
   *   ....####
   *   ....####
   *   ...#####
   *   ...#####
   *   ...##...
   *   ...##...
   * Encoded data: offset=1153, size=4
   */
  0x7b, 0xff, 0xfc, 0x60,

  /* Unicode: U+250E (┎) [BOX DRAWINGS DOWN HEAVY AND RIGHT LIGHT], Size: 8x8
   * Glyph (bounding: <x: 2, y: 3, width: 6, height: 5>):
   *   ........
   *   ........
   *   ........
   *   ..######
   *   ..######
   *   ..####..
   *   ..####..
   *   ..####..
   * Encoded data: offset=1157, size=4
   */
  0xff, 0xff, 0x3c, 0xf0,

  /* Unicode: U+250F (┏) [BOX DRAWINGS HEAVY DOWN AND RIGHT], Size: 8x8
   * Glyph (bounding: <x: 2, y: 2, width: 6, height: 6>):
   *   ........
   *   ........
   *   ....####
   *   ..######
   *   ..######
   *   ..######
   *   ..####..
   *   ..####..
   * Encoded data: offset=1161, size=5
   */
  0x3f, 0xff, 0xff, 0xf3, 0xc0,

  /* Unicode: U+2510 (┐) [BOX DRAWINGS LIGHT DOWN AND LEFT], Size: 8x8
   * Glyph (bounding: <x: 0, y: 3, width: 5, height: 5>):
   *   ........
   *   ........
   *   ........
   *   #####...
   *   #####...
   *   ...##...
   *   ...##...
   *   ...##...
   * Encoded data: offset=1166, size=4
   */
  0xff, 0xc6, 0x31, 0x80,

  /* Unicode: U+2511 (┑) [BOX DRAWINGS DOWN LIGHT AND LEFT HEAVY], Size: 8x8
   * Glyph (bounding: <x: 0, y: 2, width: 5, height: 6>):
   *   ........
   *   ........
   *   #####...
   *   #####...
   *   #####...
   *   #####...
   *   ...##...
   *   ...##...
   * Encoded data: offset=1170, size=4
   */
  0xff, 0xff, 0xf1, 0x8c,

  /* Unicode: U+2512 (┒) [BOX DRAWINGS DOWN HEAVY AND LEFT LIGHT], Size: 8x8
   * Glyph (bounding: <x: 0, y: 3, width: 6, height: 5>):
   *   ........
   *   ........
   *   ........
   *   ######..
   *   ######..
   *   ..####..
   *   ..####..
   *   ..####..
   * Encoded data: offset=1174, size=4
   */
  0xff, 0xf3, 0xcf, 0x3c,

  /* Unicode: U+2513 (┓) [BOX DRAWINGS HEAVY DOWN AND LEFT], Size: 8x8
   * Glyph (bounding: <x: 0, y: 2, width: 6, height: 6>):
   *   ........
   *   ........
   *   #####...
   *   ######..
   *   ######..
   *   ######..
   *   ..####..
   *   ..####..
   * Encoded data: offset=1178, size=5
   */
  0xfb, 0xff, 0xff, 0x3c, 0xf0,

  /* Unicode: U+2514 (└) [BOX DRAWINGS LIGHT UP AND RIGHT], Size: 8x8
   * Glyph (bounding: <x: 3, y: 0, width: 5, height: 5>):
   *   ...##...
   *   ...##...
   *   ...##...
   *   ...#####
   *   ...#####
   *   ........
   *   ........
   *   ........
   * Encoded data: offset=1183, size=4
   */
  0xc6, 0x31, 0xff, 0x80,

  /* Unicode: U+2515 (┕) [BOX DRAWINGS UP LIGHT AND RIGHT HEAVY], Size: 8x8
   * Glyph (bounding: <x: 3, y: 0, width: 5, height: 6>):
   *   ...##...
   *   ...##...
   *   ...#####
   *   ...#####
   *   ....####
   *   ....####
   *   ........
   *   ........
   * Encoded data: offset=1187, size=4
   */
  0xc6, 0x3f, 0xf7, 0xbc,

  /* Unicode: U+2516 (┖) [BOX DRAWINGS UP HEAVY AND RIGHT LIGHT], Size: 8x8
   * Glyph (bounding: <x: 2, y: 0, width: 6, height: 5>):
   *   ..####..
   *   ..####..
   *   ..####..
   *   ..######
   *   ...#####
   *   ........
   *   ........
   *   ........
   * Encoded data: offset=1191, size=4
   */
  0xf3, 0xcf, 0x3f, 0x7c,

  /* Unicode: U+2517 (┗) [BOX DRAWINGS HEAVY UP AND RIGHT], Size: 8x8
   * Glyph (bounding: <x: 2, y: 0, width: 6, height: 6>):
   *   ..####..
   *   ..####..
   *   ..######
   *   ..######
   *   ....####
   *   ....####
   *   ........
   *   ........
   * Encoded data: offset=1195, size=5
   */
  0xf3, 0xcf, 0xff, 0x3c, 0xf0,

  /* Unicode: U+2518 (┘) [BOX DRAWINGS LIGHT UP AND LEFT], Size: 8x8
   * Glyph (bounding: <x: 0, y: 0, width: 5, height: 5>):
   *   ...##...
   *   ...##...
   *   ...##...
   *   #####...
   *   #####...
   *   ........
   *   ........
   *   ........
   * Encoded data: offset=1200, size=4
   */
  0x18, 0xc7, 0xff, 0x80,

  /* Unicode: U+2519 (┙) [BOX DRAWINGS UP LIGHT AND LEFT HEAVY], Size: 8x8
   * Glyph (bounding: <x: 0, y: 0, width: 5, height: 6>):
   *   ...##...
   *   ...##...
   *   #####...
   *   #####...
   *   #####...
   *   #####...
   *   ........
   *   ........
   * Encoded data: offset=1204, size=4
   */
  0x18, 0xff, 0xff, 0xfc,

  /* Unicode: U+251A (┚) [BOX DRAWINGS UP HEAVY AND LEFT LIGHT], Size: 8x8
   * Glyph (bounding: <x: 0, y: 0, width: 6, height: 5>):
   *   ..####..
   *   ..####..
   *   ..####..
   *   ######..
   *   #####...
   *   ........
   *   ........
   *   ........
   * Encoded data: offset=1208, size=4
   */
  0x3c, 0xf3, 0xff, 0xf8,

  /* Unicode: U+251B (┛) [BOX DRAWINGS HEAVY UP AND LEFT], Size: 8x8
   * Glyph (bounding: <x: 0, y: 0, width: 6, height: 6>):
   *   ..####..
   *   ..####..
   *   ######..
   *   ######..
   *   #####...
   *   #####...
   *   ........
   *   ........
   * Encoded data: offset=1212, size=5
   */
  0x3c, 0xff, 0xff, 0xfb, 0xe0,

  /* Unicode: U+251C (├) [BOX DRAWINGS LIGHT VERTICAL AND RIGHT], Size: 8x8
   * Glyph (bounding: <x: 3, y: 0, width: 5, height: 8>):
   *   ...##...
   *   ...##...
   *   ...##...
   *   ...#####
   *   ...#####
   *   ...##...
   *   ...##...
   *   ...##...
   * Encoded data: offset=1217, size=5
   */
  0xc6, 0x31, 0xff, 0xe3, 0x18,

  /* Unicode: U+251D (┝) [BOX DRAWINGS VERTICAL LIGHT AND RIGHT HEAVY], Size: 8x8
   * Glyph (bounding: <x: 3, y: 0, width: 5, height: 8>):
   *   ...##...
   *   ...##...
   *   ...#####
   *   ...#####
   *   ...#####
   *   ...#####
   *   ...##...
   *   ...##...
   * Encoded data: offset=1222, size=5
   */
  0xc6, 0x3f, 0xff, 0xff, 0x18,

  /* Unicode: U+251E (┞) [BOX DRAWINGS UP HEAVY AND RIGHT DOWN LIGHT], Size: 8x8
   * Glyph (bounding: <x: 2, y: 0, width: 6, height: 8>):
   *   ..####..
   *   ..####..
   *   ..####..
   *   ..######
   *   ...#####
   *   ...##...
   *   ...##...
   *   ...##...
   * Encoded data: offset=1227, size=6
   */
  0xf3, 0xcf, 0x3f, 0x7d, 0x86, 0x18,

  /* Unicode: U+251F (┟) [BOX DRAWINGS DOWN HEAVY AND RIGHT UP LIGHT], Size: 8x8
   * Glyph (bounding: <x: 2, y: 0, width: 6, height: 8>):
   *   ...##...
   *   ...##...
   *   ...#####
   *   ..######
   *   ..######
   *   ..######
   *   ..####..
   *   ..####..
   * Encoded data: offset=1233, size=6
   */
  0x61, 0x87, 0xff, 0xff, 0xff, 0x3c,

  /* Unicode: U+2520 (┠) [BOX DRAWINGS VERTICAL HEAVY AND RIGHT LIGHT], Size: 8x8
   * Glyph (bounding: <x: 2, y: 0, width: 6, height: 8>):
   *   ..####..
   *   ..####..
   *   ..####..
   *   ..######
   *   ..######
   *   ..####..
   *   ..####..
   *   ..####..
   * Encoded data: offset=1239, size=6
   */
  0xf3, 0xcf, 0x3f, 0xff, 0xcf, 0x3c,

  /* Unicode: U+2521 (┡) [BOX DRAWINGS DOWN LIGHT AND RIGHT UP HEAVY], Size: 8x8
   * Glyph (bounding: <x: 2, y: 0, width: 6, height: 8>):
   *   ..####..
   *   ..####..
   *   ..######
   *   ..######
   *   ...#####
   *   ...#####
   *   ...##...
   *   ...##...
   * Encoded data: offset=1245, size=6
   */
  0xf3, 0xcf, 0xff, 0x7d, 0xf6, 0x18,

  /* Unicode: U+2522 (┢) [BOX DRAWINGS UP LIGHT AND RIGHT DOWN HEAVY], Size: 8x8
   * Glyph (bounding: <x: 2, y: 0, width: 6, height: 8>):
   *   ...##...
   *   ...##...
   *   ...##...
   *   ..######
   *   ..######
   *   ..####..
   *   ..####..
   *   ..####..
   * Encoded data: offset=1251, size=6
   */
  0x61, 0x86, 0x3f, 0xff, 0xcf, 0x3c,

  /* Unicode: U+2523 (┣) [BOX DRAWINGS HEAVY VERTICAL AND RIGHT], Size: 8x8
   * Glyph (bounding: <x: 2, y: 0, width: 6, height: 8>):
   *   ..####..
   *   ..####..
   *   ..######
   *   ..######
   *   ..######
   *   ..######
   *   ..####..
   *   ..####..
   * Encoded data: offset=1257, size=6
   */
  0xf3, 0xcf, 0xff, 0xff, 0xff, 0x3c,

  /* Unicode: U+2524 (┤) [BOX DRAWINGS LIGHT VERTICAL AND LEFT], Size: 8x8
   * Glyph (bounding: <x: 0, y: 0, width: 5, height: 8>):
   *   ...##...
   *   ...##...
   *   ...##...
   *   #####...
   *   #####...
   *   ...##...
   *   ...##...
   *   ...##...
   * Encoded data: offset=1263, size=5
   */
  0x18, 0xc7, 0xff, 0x8c, 0x63,

  /* Unicode: U+2525 (┥) [BOX DRAWINGS VERTICAL LIGHT AND LEFT HEAVY], Size: 8x8
   * Glyph (bounding: <x: 0, y: 0, width: 5, height: 8>):
   *   ...##...
   *   ...##...
   *   #####...
   *   #####...
   *   #####...
   *   #####...
   *   ...##...
   *   ...##...
   * Encoded data: offset=1268, size=5
   */
  0x18, 0xff, 0xff, 0xfc, 0x63,

  /* Unicode: U+2526 (┦) [BOX DRAWINGS UP HEAVY AND LEFT DOWN LIGHT], Size: 8x8
   * Glyph (bounding: <x: 0, y: 0, width: 6, height: 8>):
   *   ..####..
   *   ..####..
   *   ..####..
   *   ######..
   *   #####...
   *   ...##...
   *   ...##...
   *   ...##...
   * Encoded data: offset=1273, size=6
   */
  0x3c, 0xf3, 0xff, 0xf8, 0x61, 0x86,

  /* Unicode: U+2527 (┧) [BOX DRAWINGS DOWN HEAVY AND LEFT UP LIGHT], Size: 8x8
   * Glyph (bounding: <x: 0, y: 0, width: 6, height: 8>):
   *   ...##...
   *   ...##...
   *   #####...
   *   ######..
   *   ######..
   *   ######..
   *   ..####..
   *   ..####..
   * Encoded data: offset=1279, size=6
   */
  0x18, 0x6f, 0xbf, 0xff, 0xf3, 0xcf,

  /* Unicode: U+2528 (┨) [BOX DRAWINGS VERTICAL HEAVY AND LEFT LIGHT], Size: 8x8
   * Glyph (bounding: <x: 0, y: 0, width: 6, height: 8>):
   *   ..####..
   *   ..####..
   *   ..####..
   *   ######..
   *   ######..
   *   ..####..
   *   ..####..
   *   ..####..
   * Encoded data: offset=1285, size=6
   */
  0x3c, 0xf3, 0xff, 0xfc, 0xf3, 0xcf,

  /* Unicode: U+2529 (┩) [BOX DRAWINGS DOWN LIGHT AND LEFT UP HEAVY], Size: 8x8
   * Glyph (bounding: <x: 0, y: 0, width: 6, height: 8>):
   *   ..####..
   *   ..####..
   *   ######..
   *   ######..
   *   #####...
   *   #####...
   *   ...##...
   *   ...##...
   * Encoded data: offset=1291, size=6
   */
  0x3c, 0xff, 0xff, 0xfb, 0xe1, 0x86,

  /* Unicode: U+252A (┪) [BOX DRAWINGS UP LIGHT AND LEFT DOWN HEAVY], Size: 8x8
   * Glyph (bounding: <x: 0, y: 0, width: 6, height: 8>):
   *   ...##...
   *   ...##...
   *   ...##...
   *   ######..
   *   ######..
   *   ..####..
   *   ..####..
   *   ..####..
   * Encoded data: offset=1297, size=6
   */
  0x18, 0x61, 0xbf, 0xfc, 0xf3, 0xcf,

  /* Unicode: U+252B (┫) [BOX DRAWINGS HEAVY VERTICAL AND LEFT], Size: 8x8
   * Glyph (bounding: <x: 0, y: 0, width: 6, height: 8>):
   *   ..####..
   *   ..####..
   *   ######..
   *   ######..
   *   ######..
   *   ######..
   *   ..####..
   *   ..####..
   * Encoded data: offset=1303, size=6
   */
  0x3c, 0xff, 0xff, 0xff, 0xf3, 0xcf,

  /* Unicode: U+252C (┬) [BOX DRAWINGS LIGHT DOWN AND HORIZONTAL], Size: 8x8
   * Glyph (bounding: <x: 0, y: 3, width: 8, height: 5>):
   *   ........
   *   ........
   *   ........
   *   ########
   *   ########
   *   ...##...
   *   ...##...
   *   ...##...
   * Encoded data: offset=1309, size=5
   */
  0xff, 0xff, 0x18, 0x18, 0x18,

  /* Unicode: U+252D (┭) [BOX DRAWINGS LEFT HEAVY AND RIGHT DOWN LIGHT], Size: 8x8
   * Glyph (bounding: <x: 0, y: 2, width: 8, height: 6>):
   *   ........
   *   ........
   *   #####...
   *   ########
   *   ########
   *   #####...
   *   ...##...
   *   ...##...
   * Encoded data: offset=1314, size=6
   */
  0xf8, 0xff, 0xff, 0xf8, 0x18, 0x18,

  /* Unicode: U+252E (┮) [BOX DRAWINGS RIGHT HEAVY AND LEFT DOWN LIGHT], Size: 8x8
   * Glyph (bounding: <x: 0, y: 2, width: 8, height: 6>):
   *   ........
   *   ........
   *   ....####
   *   ########
   *   ########
   *   ...#####
   *   ...##...
   *   ...##...
   * Encoded data: offset=1320, size=6
   */
  0x0f, 0xff, 0xff, 0x1f, 0x18, 0x18,

  /* Unicode: U+252F (┯) [BOX DRAWINGS DOWN LIGHT AND HORIZONTAL HEAVY], Size: 8x8
   * Glyph (bounding: <x: 0, y: 2, width: 8, height: 6>):
   *   ........
   *   ........
   *   ########
   *   ########
   *   ########
   *   ########
   *   ...##...
   *   ...##...
   * Encoded data: offset=1326, size=6
   */
  0xff, 0xff, 0xff, 0xff, 0x18, 0x18,

  /* Unicode: U+2530 (┰) [BOX DRAWINGS DOWN HEAVY AND HORIZONTAL LIGHT], Size: 8x8
   * Glyph (bounding: <x: 0, y: 3, width: 8, height: 5>):
   *   ........
   *   ........
   *   ........
   *   ########
   *   ########
   *   ..####..
   *   ..####..
   *   ..####..
   * Encoded data: offset=1332, size=5
   */
  0xff, 0xff, 0x3c, 0x3c, 0x3c,

  /* Unicode: U+2531 (┱) [BOX DRAWINGS RIGHT LIGHT AND LEFT DOWN HEAVY], Size: 8x8
   * Glyph (bounding: <x: 0, y: 2, width: 8, height: 6>):
   *   ........
   *   ........
   *   #####...
   *   ########
   *   ########
   *   ######..
   *   ..####..
   *   ..####..
   * Encoded data: offset=1337, size=6
   */
  0xf8, 0xff, 0xff, 0xfc, 0x3c, 0x3c,

  /* Unicode: U+2532 (┲) [BOX DRAWINGS LEFT LIGHT AND RIGHT DOWN HEAVY], Size: 8x8
   * Glyph (bounding: <x: 0, y: 2, width: 8, height: 6>):
   *   ........
   *   ........
   *   ....####
   *   ########
   *   ########
   *   ..######
   *   ..####..
   *   ..####..
   * Encoded data: offset=1343, size=6
   */
  0x0f, 0xff, 0xff, 0x3f, 0x3c, 0x3c,

  /* Unicode: U+2533 (┳) [BOX DRAWINGS HEAVY DOWN AND HORIZONTAL], Size: 8x8
   * Glyph (bounding: <x: 0, y: 2, width: 8, height: 6>):
   *   ........
   *   ........
   *   ########
   *   ########
   *   ########
   *   ########
   *   ..####..
   *   ..####..
   * Encoded data: offset=1349, size=6
   */
  0xff, 0xff, 0xff, 0xff, 0x3c, 0x3c,

  /* Unicode: U+2534 (┴) [BOX DRAWINGS LIGHT UP AND HORIZONTAL], Size: 8x8
   * Glyph (bounding: <x: 0, y: 0, width: 8, height: 5>):
   *   ...##...
   *   ...##...
   *   ...##...
   *   ########
   *   ########
   *   ........
   *   ........
   *   ........
   * Encoded data: offset=1355, size=5
   */
  0x18, 0x18, 0x18, 0xff, 0xff,

  /* Unicode: U+2535 (┵) [BOX DRAWINGS LEFT HEAVY AND RIGHT UP LIGHT], Size: 8x8
   * Glyph (bounding: <x: 0, y: 0, width: 8, height: 6>):
   *   ...##...
   *   ...##...
   *   #####...
   *   ########
   *   ########
   *   #####...
   *   ........
   *   ........
   * Encoded data: offset=1360, size=6
   */
  0x18, 0x18, 0xf8, 0xff, 0xff, 0xf8,

  /* Unicode: U+2536 (┶) [BOX DRAWINGS RIGHT HEAVY AND LEFT UP LIGHT], Size: 8x8
   * Glyph (bounding: <x: 0, y: 0, width: 8, height: 6>):
   *   ...##...
   *   ...##...
   *   ...#####
   *   ########
   *   ########
   *   ....####
   *   ........
   *   ........
   * Encoded data: offset=1366, size=6
   */
  0x18, 0x18, 0x1f, 0xff, 0xff, 0x0f,

  /* Unicode: U+2537 (┷) [BOX DRAWINGS UP LIGHT AND HORIZONTAL HEAVY], Size: 8x8
   * Glyph (bounding: <x: 0, y: 0, width: 8, height: 6>):
   *   ...##...
   *   ...##...
   *   ########
   *   ########
   *   ########
   *   ########
   *   ........
   *   ........
   * Encoded data: offset=1372, size=6
   */
  0x18, 0x18, 0xff, 0xff, 0xff, 0xff,

  /* Unicode: U+2538 (┸) [BOX DRAWINGS UP HEAVY AND HORIZONTAL LIGHT], Size: 8x8
   * Glyph (bounding: <x: 0, y: 0, width: 8, height: 5>):
   *   ..####..
   *   ..####..
   *   ..####..
   *   ########
   *   ########
   *   ........
   *   ........
   *   ........
   * Encoded data: offset=1378, size=5
   */
  0x3c, 0x3c, 0x3c, 0xff, 0xff,

  /* Unicode: U+2539 (┹) [BOX DRAWINGS RIGHT LIGHT AND LEFT UP HEAVY], Size: 8x8
   * Glyph (bounding: <x: 0, y: 0, width: 8, height: 6>):
   *   ..####..
   *   ..####..
   *   ######..
   *   ########
   *   ########
   *   #####...
   *   ........
   *   ........
   * Encoded data: offset=1383, size=6
   */
  0x3c, 0x3c, 0xfc, 0xff, 0xff, 0xf8,

  /* Unicode: U+253A (┺) [BOX DRAWINGS LEFT LIGHT AND RIGHT UP HEAVY], Size: 8x8
   * Glyph (bounding: <x: 0, y: 0, width: 8, height: 6>):
   *   ..####..
   *   ..####..
   *   ..######
   *   ########
   *   ########
   *   ....####
   *   ........
   *   ........
   * Encoded data: offset=1389, size=6
   */
  0x3c, 0x3c, 0x3f, 0xff, 0xff, 0x0f,

  /* Unicode: U+253B (┻) [BOX DRAWINGS HEAVY UP AND HORIZONTAL], Size: 8x8
   * Glyph (bounding: <x: 0, y: 0, width: 8, height: 6>):
   *   ..####..
   *   ..####..
   *   ########
   *   ########
   *   ########
   *   ########
   *   ........
   *   ........
   * Encoded data: offset=1395, size=6
   */
  0x3c, 0x3c, 0xff, 0xff, 0xff, 0xff,

  /* Unicode: U+253C (┼) [BOX DRAWINGS LIGHT VERTICAL AND HORIZONTAL], Size: 8x8
   * Glyph (bounding: <x: 0, y: 0, width: 8, height: 8>):
   *   ...##...
   *   ...##...
   *   ...##...
   *   ########
   *   ########
   *   ...##...
   *   ...##...
   *   ...##...
   * Encoded data: offset=1401, size=8
   */
  0x18, 0x18, 0x18, 0xff, 0xff, 0x18, 0x18, 0x18,

  /* Unicode: U+253D (┽) [BOX DRAWINGS LEFT HEAVY AND RIGHT VERTICAL LIGHT], Size: 8x8
   * Glyph (bounding: <x: 0, y: 0, width: 8, height: 8>):
   *   ...##...
   *   ...##...
   *   #####...
   *   ########
   *   ########
   *   #####...
   *   ...##...
   *   ...##...
   * Encoded data: offset=1409, size=8
   */
  0x18, 0x18, 0xf8, 0xff, 0xff, 0xf8, 0x18, 0x18,

  /* Unicode: U+253E (┾) [BOX DRAWINGS RIGHT HEAVY AND LEFT VERTICAL LIGHT], Size: 8x8
   * Glyph (bounding: <x: 0, y: 0, width: 8, height: 8>):
   *   ...##...
   *   ...##...
   *   ...#####
   *   ########
   *   ########
   *   ...#####
   *   ...##...
   *   ...##...
   * Encoded data: offset=1417, size=8
   */
  0x18, 0x18, 0x1f, 0xff, 0xff, 0x1f, 0x18, 0x18,

  /* Unicode: U+253F (┿) [BOX DRAWINGS VERTICAL LIGHT AND HORIZONTAL HEAVY], Size: 8x8
   * Glyph (bounding: <x: 0, y: 0, width: 8, height: 8>):
   *   ...##...
   *   ...##...
   *   ########
   *   ########
   *   ########
   *   ########
   *   ...##...
   *   ...##...
   * Encoded data: offset=1425, size=8
   */
  0x18, 0x18, 0xff, 0xff, 0xff, 0xff, 0x18, 0x18,

  /* Unicode: U+2540 (╀) [BOX DRAWINGS UP HEAVY AND DOWN HORIZONTAL LIGHT], Size: 8x8
   * Glyph (bounding: <x: 0, y: 0, width: 8, height: 8>):
   *   ..####..
   *   ..####..
   *   ..####..
   *   ########
   *   ########
   *   ...##...
   *   ...##...
   *   ...##...
   * Encoded data: offset=1433, size=8
   */
  0x3c, 0x3c, 0x3c, 0xff, 0xff, 0x18, 0x18, 0x18,

  /* Unicode: U+2541 (╁) [BOX DRAWINGS DOWN HEAVY AND UP HORIZONTAL LIGHT], Size: 8x8
   * Glyph (bounding: <x: 0, y: 0, width: 8, height: 8>):
   *   ...##...
   *   ...##...
   *   ...##...
   *   ########
   *   ########
   *   ..####..
   *   ..####..
   *   ..####..
   * Encoded data: offset=1441, size=8
   */
  0x18, 0x18, 0x18, 0xff, 0xff, 0x3c, 0x3c, 0x3c,

  /* Unicode: U+2542 (╂) [BOX DRAWINGS VERTICAL HEAVY AND HORIZONTAL LIGHT], Size: 8x8
   * Glyph (bounding: <x: 0, y: 0, width: 8, height: 8>):
   *   ..####..
   *   ..####..
   *   ..####..
   *   ########
   *   ########
   *   ..####..
   *   ..####..
   *   ..####..
   * Encoded data: offset=1449, size=8
   */
  0x3c, 0x3c, 0x3c, 0xff, 0xff, 0x3c, 0x3c, 0x3c,

  /* Unicode: U+2543 (╃) [BOX DRAWINGS LEFT UP HEAVY AND RIGHT DOWN LIGHT], Size: 8x8
   * Glyph (bounding: <x: 0, y: 0, width: 8, height: 8>):
   *   ..####..
   *   ..####..
   *   ######..
   *   ########
   *   ########
   *   #####...
   *   ...##...
   *   ...##...
   * Encoded data: offset=1457, size=8
   */
  0x3c, 0x3c, 0xfc, 0xff, 0xff, 0xf8, 0x18, 0x18,

  /* Unicode: U+2544 (╄) [BOX DRAWINGS RIGHT UP HEAVY AND LEFT DOWN LIGHT], Size: 8x8
   * Glyph (bounding: <x: 0, y: 0, width: 8, height: 8>):
   *   ..####..
   *   ..####..
   *   ..######
   *   ########
   *   ########
   *   ...#####
   *   ...##...
   *   ...##...
   * Encoded data: offset=1465, size=8
   */
  0x3c, 0x3c, 0x3f, 0xff, 0xff, 0x1f, 0x18, 0x18,

  /* Unicode: U+2545 (╅) [BOX DRAWINGS LEFT DOWN HEAVY AND RIGHT UP LIGHT], Size: 8x8
   * Glyph (bounding: <x: 0, y: 0, width: 8, height: 8>):
   *   ...##...
   *   ...##...
   *   #####...
   *   ########
   *   ########
   *   ######..
   *   ..####..
   *   ..####..
   * Encoded data: offset=1473, size=8
   */
  0x18, 0x18, 0xf8, 0xff, 0xff, 0xfc, 0x3c, 0x3c,

  /* Unicode: U+2546 (╆) [BOX DRAWINGS RIGHT DOWN HEAVY AND LEFT UP LIGHT], Size: 8x8
   * Glyph (bounding: <x: 0, y: 0, width: 8, height: 8>):
   *   ...##...
   *   ...##...
   *   ...#####
   *   ########
   *   ########
   *   ..######
   *   ..####..
   *   ..####..
   * Encoded data: offset=1481, size=8
   */
  0x18, 0x18, 0x1f, 0xff, 0xff, 0x3f, 0x3c, 0x3c,

  /* Unicode: U+2547 (╇) [BOX DRAWINGS DOWN LIGHT AND UP HORIZONTAL HEAVY], Size: 8x8
   * Glyph (bounding: <x: 0, y: 0, width: 8, height: 8>):
   *   ..####..
   *   ..####..
   *   ########
   *   ########
   *   ########
   *   ########
   *   ...##...
   *   ...##...
   * Encoded data: offset=1489, size=8
   */
  0x3c, 0x3c, 0xff, 0xff, 0xff, 0xff, 0x18, 0x18,

  /* Unicode: U+2548 (╈) [BOX DRAWINGS UP LIGHT AND DOWN HORIZONTAL HEAVY], Size: 8x8
   * Glyph (bounding: <x: 0, y: 0, width: 8, height: 8>):
   *   ...##...
   *   ...##...
   *   ########
   *   ########
   *   ########
   *   ########
   *   ..####..
   *   ..####..
   * Encoded data: offset=1497, size=8
   */
  0x18, 0x18, 0xff, 0xff, 0xff, 0xff, 0x3c, 0x3c,

  /* Unicode: U+2549 (╉) [BOX DRAWINGS RIGHT LIGHT AND LEFT VERTICAL HEAVY], Size: 8x8
   * Glyph (bounding: <x: 0, y: 0, width: 8, height: 8>):
   *   ..####..
   *   ..####..
   *   ######..
   *   ########
   *   ########
   *   ######..
   *   ..####..
   *   ..####..
   * Encoded data: offset=1505, size=8
   */
  0x3c, 0x3c, 0xfc, 0xff, 0xff, 0xfc, 0x3c, 0x3c,

  /* Unicode: U+254A (╊) [BOX DRAWINGS LEFT LIGHT AND RIGHT VERTICAL HEAVY], Size: 8x8
   * Glyph (bounding: <x: 0, y: 0, width: 8, height: 8>):
   *   ..####..
   *   ..####..
   *   ..######
   *   ########
   *   ########
   *   ..######
   *   ..####..
   *   ..####..
   * Encoded data: offset=1513, size=8
   */
  0x3c, 0x3c, 0x3f, 0xff, 0xff, 0x3f, 0x3c, 0x3c,

  /* Unicode: U+254B (╋) [BOX DRAWINGS HEAVY VERTICAL AND HORIZONTAL], Size: 8x8
   * Glyph (bounding: <x: 0, y: 0, width: 8, height: 8>):
   *   ..####..
   *   ..####..
   *   ########
   *   ########
   *   ########
   *   ########
   *   ..####..
   *   ..####..
   * Encoded data: offset=1521, size=8
   */
  0x3c, 0x3c, 0xff, 0xff, 0xff, 0xff, 0x3c, 0x3c,

  /* Unicode: U+254C (╌) [BOX DRAWINGS LIGHT DOUBLE DASH HORIZONTAL], Size: 8x8
   * Glyph (bounding: <x: 0, y: 3, width: 7, height: 2>):
   *   ........
   *   ........
   *   ........
   *   ###.###.
   *   ###.###.
   *   ........
   *   ........
   *   ........
   * Encoded data: offset=1529, size=2
   */
  0xef, 0xdc,

  /* Unicode: U+254D (╍) [BOX DRAWINGS HEAVY DOUBLE DASH HORIZONTAL], Size: 8x8
   * Glyph (bounding: <x: 0, y: 2, width: 7, height: 4>):
   *   ........
   *   ........
   *   ###.###.
   *   ###.###.
   *   ###.###.
   *   ###.###.
   *   ........
   *   ........
   * Encoded data: offset=1531, size=4
   */
  0xef, 0xdf, 0xbf, 0x70,

  /* Unicode: U+254E (╎) [BOX DRAWINGS LIGHT DOUBLE DASH VERTICAL], Size: 8x8
   * Glyph (bounding: <x: 3, y: 0, width: 2, height: 7>):
   *   ...##...
   *   ...##...
   *   ...##...
   *   ........
   *   ...##...
   *   ...##...
   *   ...##...
   *   ........
   * Encoded data: offset=1535, size=2
   */
  0xfc, 0xfc,

  /* Unicode: U+254F (╏) [BOX DRAWINGS HEAVY DOUBLE DASH VERTICAL], Size: 8x8
   * Glyph (bounding: <x: 2, y: 0, width: 4, height: 7>):
   *   ..####..
   *   ..####..
   *   ..####..
   *   ........
   *   ..####..
   *   ..####..
   *   ..####..
   *   ........
   * Encoded data: offset=1537, size=4
   */
  0xff, 0xf0, 0xff, 0xf0,

  /* Unicode: U+2550 (═) [BOX DRAWINGS DOUBLE HORIZONTAL], Size: 8x8
   * Glyph (bounding: <x: 0, y: 2, width: 8, height: 3>):
   *   ........
   *   ........
   *   ########
   *   ........
   *   ########
   *   ........
   *   ........
   *   ........
   * Encoded data: offset=1541, size=3
   */
  0xff, 0x00, 0xff,

  /* Unicode: U+2551 (║) [BOX DRAWINGS DOUBLE VERTICAL], Size: 8x8
   * Glyph (bounding: <x: 1, y: 0, width: 5, height: 8>):
   *   .##.##..
   *   .##.##..
   *   .##.##..
   *   .##.##..
   *   .##.##..
   *   .##.##..
   *   .##.##..
   *   .##.##..
   * Encoded data: offset=1544, size=5
   */
  0xde, 0xf7, 0xbd, 0xef, 0x7b,

  /* Unicode: U+2552 (╒) [BOX DRAWINGS DOWN SINGLE AND RIGHT DOUBLE], Size: 8x8
   * Glyph (bounding: <x: 3, y: 2, width: 5, height: 6>):
   *   ........
   *   ........
   *   ...#####
   *   ...##...
   *   ...#####
   *   ...##...
   *   ...##...
   *   ...##...
   * Encoded data: offset=1549, size=4
   */
  0xfe, 0x3f, 0x8c, 0x60,

  /* Unicode: U+2553 (╓) [BOX DRAWINGS DOWN DOUBLE AND RIGHT SINGLE], Size: 8x8
   * Glyph (bounding: <x: 1, y: 3, width: 7, height: 5>):
   *   ........
   *   ........
   *   ........
   *   .#######
   *   .#######
   *   .##.##..
   *   .##.##..
   *   .##.##..
   * Encoded data: offset=1553, size=5
   */
  0xff, 0xff, 0x66, 0xcd, 0x80,

  /* Unicode: U+2554 (╔) [BOX DRAWINGS DOUBLE DOWN AND RIGHT], Size: 8x8
   * Glyph (bounding: <x: 1, y: 2, width: 7, height: 6>):
   *   ........
   *   ........
   *   .#######
   *   .##.....
   *   .##.####
   *   .##.##..
   *   .##.##..
   *   .##.##..
   * Encoded data: offset=1558, size=6
   */
  0xff, 0x83, 0x7e, 0xcd, 0x9b, 0x00,

  /* Unicode: U+2555 (╕) [BOX DRAWINGS DOWN SINGLE AND LEFT DOUBLE], Size: 8x8
   * Glyph (bounding: <x: 0, y: 2, width: 5, height: 6>):
   *   ........
   *   ........
   *   #####...
   *   ...##...
   *   #####...
   *   ...##...
   *   ...##...
   *   ...##...
   * Encoded data: offset=1564, size=4
   */
  0xf8, 0xfe, 0x31, 0x8c,

  /* Unicode: U+2556 (╖) [BOX DRAWINGS DOWN DOUBLE AND LEFT SINGLE], Size: 8x8
   * Glyph (bounding: <x: 0, y: 3, width: 6, height: 5>):
   *   ........
   *   ........
   *   ........
   *   ######..
   *   ######..
   *   .##.##..
   *   .##.##..
   *   .##.##..
   * Encoded data: offset=1568, size=4
   */
  0xff, 0xf6, 0xdb, 0x6c,

  /* Unicode: U+2557 (╗) [BOX DRAWINGS DOUBLE DOWN AND LEFT], Size: 8x8
   * Glyph (bounding: <x: 0, y: 2, width: 6, height: 6>):
   *   ........
   *   ........
   *   ######..
   *   ....##..
   *   ###.##..
   *   .##.##..
   *   .##.##..
   *   .##.##..
   * Encoded data: offset=1572, size=5
   */
  0xfc, 0x3e, 0xdb, 0x6d, 0xb0,

  /* Unicode: U+2558 (╘) [BOX DRAWINGS UP SINGLE AND RIGHT DOUBLE], Size: 8x8
   * Glyph (bounding: <x: 3, y: 0, width: 5, height: 5>):
   *   ...##...
   *   ...##...
   *   ...#####
   *   ...##...
   *   ...#####
   *   ........
   *   ........
   *   ........
   * Encoded data: offset=1577, size=4
   */
  0xc6, 0x3f, 0x8f, 0x80,

  /* Unicode: U+2559 (╙) [BOX DRAWINGS UP DOUBLE AND RIGHT SINGLE], Size: 8x8
   * Glyph (bounding: <x: 1, y: 0, width: 7, height: 4>):
   *   .##.##..
   *   .##.##..
   *   .#######
   *   .#######
   *   ........
   *   ........
   *   ........
   *   ........
   * Encoded data: offset=1581, size=4
   */
  0xd9, 0xb3, 0xff, 0xf0,

  /* Unicode: U+255A (╚) [BOX DRAWINGS DOUBLE UP AND RIGHT], Size: 8x8
   * Glyph (bounding: <x: 1, y: 0, width: 7, height: 5>):
   *   .##.##..
   *   .##.##..
   *   .##.####
   *   .##.....
   *   .#######
   *   ........
   *   ........
   *   ........
   * Encoded data: offset=1585, size=5
   */
  0xd9, 0xb3, 0x7e, 0x0f, 0xe0,

  /* Unicode: U+255B (╛) [BOX DRAWINGS UP SINGLE AND LEFT DOUBLE], Size: 8x8
   * Glyph (bounding: <x: 0, y: 0, width: 5, height: 5>):
   *   ...##...
   *   ...##...
   *   #####...
   *   ...##...
   *   #####...
   *   ........
   *   ........
   *   ........
   * Encoded data: offset=1590, size=4
   */
  0x18, 0xfe, 0x3f, 0x80,

  /* Unicode: U+255C (╜) [BOX DRAWINGS UP DOUBLE AND LEFT SINGLE], Size: 8x8
   * Glyph (bounding: <x: 0, y: 0, width: 6, height: 4>):
   *   .##.##..
   *   .##.##..
   *   ######..
   *   ######..
   *   ........
   *   ........
   *   ........
   *   ........
   * Encoded data: offset=1594, size=3
   */
  0x6d, 0xbf, 0xff,

  /* Unicode: U+255D (╝) [BOX DRAWINGS DOUBLE UP AND LEFT], Size: 8x8
   * Glyph (bounding: <x: 0, y: 0, width: 6, height: 5>):
   *   .##.##..
   *   .##.##..
   *   ###.##..
   *   ....##..
   *   ######..
   *   ........
   *   ........
   *   ........
   * Encoded data: offset=1597, size=4
   */
  0x6d, 0xbe, 0xc3, 0xfc,

  /* Unicode: U+255E (╞) [BOX DRAWINGS VERTICAL SINGLE AND RIGHT DOUBLE], Size: 8x8
   * Glyph (bounding: <x: 3, y: 0, width: 5, height: 8>):
   *   ...##...
   *   ...##...
   *   ...#####
   *   ...##...
   *   ...#####
   *   ...##...
   *   ...##...
   *   ...##...
   * Encoded data: offset=1601, size=5
   */
  0xc6, 0x3f, 0x8f, 0xe3, 0x18,

  /* Unicode: U+255F (╟) [BOX DRAWINGS VERTICAL DOUBLE AND RIGHT SINGLE], Size: 8x8
   * Glyph (bounding: <x: 1, y: 0, width: 7, height: 8>):
   *   .##.##..
   *   .##.##..
   *   .##.##..
   *   .##.####
   *   .##.####
   *   .##.##..
   *   .##.##..
   *   .##.##..
   * Encoded data: offset=1606, size=7
   */
  0xd9, 0xb3, 0x66, 0xfd, 0xfb, 0x36, 0x6c,

  /* Unicode: U+2560 (╠) [BOX DRAWINGS DOUBLE VERTICAL AND RIGHT], Size: 8x8
   * Glyph (bounding: <x: 1, y: 0, width: 7, height: 8>):
   *   .##.##..
   *   .##.##..
   *   .##.####
   *   .##.....
   *   .##.####
   *   .##.##..
   *   .##.##..
   *   .##.##..
   * Encoded data: offset=1613, size=7
   */
  0xd9, 0xb3, 0x7e, 0x0d, 0xfb, 0x36, 0x6c,

  /* Unicode: U+2561 (╡) [BOX DRAWINGS VERTICAL SINGLE AND LEFT DOUBLE], Size: 8x8
   * Glyph (bounding: <x: 0, y: 0, width: 5, height: 8>):
   *   ...##...
   *   ...##...
   *   #####...
   *   ...##...
   *   #####...
   *   ...##...
   *   ...##...
   *   ...##...
   * Encoded data: offset=1620, size=5
   */
  0x18, 0xfe, 0x3f, 0x8c, 0x63,

  /* Unicode: U+2562 (╢) [BOX DRAWINGS VERTICAL DOUBLE AND LEFT SINGLE], Size: 8x8
   * Glyph (bounding: <x: 0, y: 0, width: 6, height: 8>):
   *   .##.##..
   *   .##.##..
   *   .##.##..
   *   ###.##..
   *   ###.##..
   *   .##.##..
   *   .##.##..
   *   .##.##..
   * Encoded data: offset=1625, size=6
   */
  0x6d, 0xb6, 0xfb, 0xed, 0xb6, 0xdb,

  /* Unicode: U+2563 (╣) [BOX DRAWINGS DOUBLE VERTICAL AND LEFT], Size: 8x8
   * Glyph (bounding: <x: 0, y: 0, width: 6, height: 8>):
   *   .##.##..
   *   .##.##..
   *   ###.##..
   *   ....##..
   *   ###.##..
   *   .##.##..
   *   .##.##..
   *   .##.##..
   * Encoded data: offset=1631, size=6
   */
  0x6d, 0xbe, 0xc3, 0xed, 0xb6, 0xdb,

  /* Unicode: U+2564 (╤) [BOX DRAWINGS DOWN SINGLE AND HORIZONTAL DOUBLE], Size: 8x8
   * Glyph (bounding: <x: 0, y: 2, width: 8, height: 6>):
   *   ........
   *   ........
   *   ########
   *   ........
   *   ########
   *   ...##...
   *   ...##...
   *   ...##...
   * Encoded data: offset=1637, size=6
   */
  0xff, 0x00, 0xff, 0x18, 0x18, 0x18,

  /* Unicode: U+2565 (╥) [BOX DRAWINGS DOWN DOUBLE AND HORIZONTAL SINGLE], Size: 8x8
   * Glyph (bounding: <x: 0, y: 3, width: 8, height: 5>):
   *   ........
   *   ........
   *   ........
   *   ########
   *   ########
   *   .##.##..
   *   .##.##..
   *   .##.##..
   * Encoded data: offset=1643, size=5
   */
  0xff, 0xff, 0x6c, 0x6c, 0x6c,

  /* Unicode: U+2566 (╦) [BOX DRAWINGS DOUBLE DOWN AND HORIZONTAL], Size: 8x8
   * Glyph (bounding: <x: 0, y: 2, width: 8, height: 6>):
   *   ........
   *   ........
   *   ########
   *   ........
   *   ###.####
   *   .##.##..
   *   .##.##..
   *   .##.##..
   * Encoded data: offset=1648, size=6
   */
  0xff, 0x00, 0xef, 0x6c, 0x6c, 0x6c,

  /* Unicode: U+2567 (╧) [BOX DRAWINGS UP SINGLE AND HORIZONTAL DOUBLE], Size: 8x8
   * Glyph (bounding: <x: 0, y: 0, width: 8, height: 5>):
   *   ...##...
   *   ...##...
   *   ########
   *   ........
   *   ########
   *   ........
   *   ........
   *   ........
   * Encoded data: offset=1654, size=5
   */
  0x18, 0x18, 0xff, 0x00, 0xff,

  /* Unicode: U+2568 (╨) [BOX DRAWINGS UP DOUBLE AND HORIZONTAL SINGLE], Size: 8x8
   * Glyph (bounding: <x: 0, y: 0, width: 8, height: 5>):
   *   ...##...
   *   ...##...
   *   ...#####
   *   ########
   *   ########
   *   ........
   *   ........
   *   ........
   * Encoded data: offset=1659, size=5
   */
  0x18, 0x18, 0x1f, 0xff, 0xff,

  /* Unicode: U+2569 (╩) [BOX DRAWINGS DOUBLE UP AND HORIZONTAL], Size: 8x8
   * Glyph (bounding: <x: 0, y: 0, width: 8, height: 5>):
   *   .##.##..
   *   .##.##..
   *   ###.####
   *   ........
   *   ########
   *   ........
   *   ........
   *   ........
   * Encoded data: offset=1664, size=5
   */
  0x6c, 0x6c, 0xef, 0x00, 0xff,

  /* Unicode: U+256A (╪) [BOX DRAWINGS VERTICAL SINGLE AND HORIZONTAL DOUBLE], Size: 8x8
   * Glyph (bounding: <x: 0, y: 0, width: 8, height: 8>):
   *   ...##...
   *   ...##...
   *   ########
   *   ...##...
   *   ########
   *   ...##...
   *   ...##...
   *   ...##...
   * Encoded data: offset=1669, size=8
   */
  0x18, 0x18, 0xff, 0x18, 0xff, 0x18, 0x18, 0x18,

  /* Unicode: U+256B (╫) [BOX DRAWINGS VERTICAL DOUBLE AND HORIZONTAL SINGLE], Size: 8x8
   * Glyph (bounding: <x: 0, y: 0, width: 8, height: 8>):
   *   .##.##..
   *   .##.##..
   *   .##.##..
   *   ########
   *   ########
   *   .##.##..
   *   .##.##..
   *   .##.##..
   * Encoded data: offset=1677, size=8
   */
  0x6c, 0x6c, 0x6c, 0xff, 0xff, 0x6c, 0x6c, 0x6c,

  /* Unicode: U+256C (╬) [BOX DRAWINGS DOUBLE VERTICAL AND HORIZONTAL], Size: 8x8
   * Glyph (bounding: <x: 0, y: 0, width: 8, height: 8>):
   *   .##.##..
   *   .##.##..
   *   ###.####
   *   ........
   *   ####.###
   *   .##.##..
   *   .##.##..
   *   .##.##..
   * Encoded data: offset=1685, size=8
   */
  0x6c, 0x6c, 0xef, 0x00, 0xf7, 0x6c, 0x6c, 0x6c,

  /* Unicode: U+256D (╭) [BOX DRAWINGS LIGHT ARC DOWN AND RIGHT], Size: 8x8
   * Glyph (bounding: <x: 3, y: 3, width: 5, height: 5>):
   *   ........
   *   ........
   *   ........
   *   .....###
   *   ....####
   *   ...###..
   *   ...##...
   *   ...##...
   * Encoded data: offset=1693, size=4
   */
  0x3b, 0xf9, 0x8c, 0x00,

  /* Unicode: U+256E (╮) [BOX DRAWINGS LIGHT ARC DOWN AND LEFT], Size: 8x8
   * Glyph (bounding: <x: 0, y: 3, width: 5, height: 5>):
   *   ........
   *   ........
   *   ........
   *   ###.....
   *   ####....
   *   ..###...
   *   ...##...
   *   ...##...
   * Encoded data: offset=1697, size=4
   */
  0xe7, 0x8e, 0x31, 0x80,

  /* Unicode: U+256F (╯) [BOX DRAWINGS LIGHT ARC UP AND LEFT], Size: 8x8
   * Glyph (bounding: <x: 0, y: 0, width: 5, height: 5>):
   *   ...##...
   *   ...##...
   *   ..###...
   *   ####....
   *   ###.....
   *   ........
   *   ........
   *   ........
   * Encoded data: offset=1701, size=4
   */
  0x18, 0xcf, 0xee, 0x00,

  /* Unicode: U+2580 (▀) [UPPER HALF BLOCK], Size: 8x8
   * Glyph (bounding: <x: 0, y: 0, width: 8, height: 4>):
   *   ########
   *   ########
   *   ########
   *   ########
   *   ........
   *   ........
   *   ........
   *   ........
   * Encoded data: offset=1705, size=4
   */
  0xff, 0xff, 0xff, 0xff,

  /* Unicode: U+2581 (▁) [LOWER ONE EIGHTH BLOCK], Size: 8x8
   * Glyph (bounding: <x: 0, y: 7, width: 8, height: 1>):
   *   ........
   *   ........
   *   ........
   *   ........
   *   ........
   *   ........
   *   ........
   *   ########
   * Encoded data: offset=1709, size=1
   */
  0xff,

  /* Unicode: U+2582 (▂) [LOWER ONE QUARTER BLOCK], Size: 8x8
   * Glyph (bounding: <x: 0, y: 6, width: 8, height: 2>):
   *   ........
   *   ........
   *   ........
   *   ........
   *   ........
   *   ........
   *   ########
   *   ########
   * Encoded data: offset=1710, size=2
   */
  0xff, 0xff,

  /* Unicode: U+2583 (▃) [LOWER THREE EIGHTHS BLOCK], Size: 8x8
   * Glyph (bounding: <x: 0, y: 5, width: 8, height: 3>):
   *   ........
   *   ........
   *   ........
   *   ........
   *   ........
   *   ########
   *   ########
   *   ########
   * Encoded data: offset=1712, size=3
   */
  0xff, 0xff, 0xff,

  /* Unicode: U+2584 (▄) [LOWER HALF BLOCK], Size: 8x8
   * Glyph (bounding: <x: 0, y: 4, width: 8, height: 4>):
   *   ........
   *   ........
   *   ........
   *   ........
   *   ########
   *   ########
   *   ########
   *   ########
   * Encoded data: offset=1715, size=4
   */
  0xff, 0xff, 0xff, 0xff,

  /* Unicode: U+2585 (▅) [LOWER FIVE EIGHTHS BLOCK], Size: 8x8
   * Glyph (bounding: <x: 0, y: 3, width: 8, height: 5>):
   *   ........
   *   ........
   *   ........
   *   ########
   *   ########
   *   ########
   *   ########
   *   ########
   * Encoded data: offset=1719, size=5
   */
  0xff, 0xff, 0xff, 0xff, 0xff,

  /* Unicode: U+2586 (▆) [LOWER THREE QUARTERS BLOCK], Size: 8x8
   * Glyph (bounding: <x: 0, y: 2, width: 8, height: 6>):
   *   ........
   *   ........
   *   ########
   *   ########
   *   ########
   *   ########
   *   ########
   *   ########
   * Encoded data: offset=1724, size=6
   */
  0xff, 0xff, 0xff, 0xff, 0xff, 0xff,

  /* Unicode: U+2587 (▇) [LOWER SEVEN EIGHTHS BLOCK], Size: 8x8
   * Glyph (bounding: <x: 0, y: 1, width: 8, height: 7>):
   *   ........
   *   ########
   *   ########
   *   ########
   *   ########
   *   ########
   *   ########
   *   ########
   * Encoded data: offset=1730, size=7
   */
  0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,

  /* Unicode: U+2588 (█) [FULL BLOCK], Size: 8x8
   * Glyph (bounding: <x: 0, y: 0, width: 8, height: 8>):
   *   ########
   *   ########
   *   ########
   *   ########
   *   ########
   *   ########
   *   ########
   *   ########
   * Encoded data: offset=1737, size=8
   */
  0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,

  /* Unicode: U+2589 (▉) [LEFT SEVEN EIGHTHS BLOCK], Size: 8x8
   * Glyph (bounding: <x: 0, y: 0, width: 7, height: 8>):
   *   #######.
   *   #######.
   *   #######.
   *   #######.
   *   #######.
   *   #######.
   *   #######.
   *   #######.
   * Encoded data: offset=1745, size=7
   */
  0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,

  /* Unicode: U+258A (▊) [LEFT THREE QUARTERS BLOCK], Size: 8x8
   * Glyph (bounding: <x: 0, y: 0, width: 6, height: 8>):
   *   ######..
   *   ######..
   *   ######..
   *   ######..
   *   ######..
   *   ######..
   *   ######..
   *   ######..
   * Encoded data: offset=1752, size=6
   */
  0xff, 0xff, 0xff, 0xff, 0xff, 0xff,

  /* Unicode: U+258B (▋) [LEFT FIVE EIGHTHS BLOCK], Size: 8x8
   * Glyph (bounding: <x: 0, y: 0, width: 5, height: 8>):
   *   #####...
   *   #####...
   *   #####...
   *   #####...
   *   #####...
   *   #####...
   *   #####...
   *   #####...
   * Encoded data: offset=1758, size=5
   */
  0xff, 0xff, 0xff, 0xff, 0xff,

  /* Unicode: U+258C (▌) [LEFT HALF BLOCK], Size: 8x8
   * Glyph (bounding: <x: 0, y: 0, width: 4, height: 8>):
   *   ####....
   *   ####....
   *   ####....
   *   ####....
   *   ####....
   *   ####....
   *   ####....
   *   ####....
   * Encoded data: offset=1763, size=4
   */
  0xff, 0xff, 0xff, 0xff,

  /* Unicode: U+258D (▍) [LEFT THREE EIGHTHS BLOCK], Size: 8x8
   * Glyph (bounding: <x: 0, y: 0, width: 3, height: 8>):
   *   ###.....
   *   ###.....
   *   ###.....
   *   ###.....
   *   ###.....
   *   ###.....
   *   ###.....
   *   ###.....
   * Encoded data: offset=1767, size=3
   */
  0xff, 0xff, 0xff,

  /* Unicode: U+258E (▎) [LEFT ONE QUARTER BLOCK], Size: 8x8
   * Glyph (bounding: <x: 0, y: 0, width: 2, height: 8>):
   *   ##......
   *   ##......
   *   ##......
   *   ##......
   *   ##......
   *   ##......
   *   ##......
   *   ##......
   * Encoded data: offset=1770, size=2
   */
  0xff, 0xff,

  /* Unicode: U+258F (▏) [LEFT ONE EIGHTH BLOCK], Size: 8x8
   * Glyph (bounding: <x: 0, y: 0, width: 1, height: 8>):
   *   #.......
   *   #.......
   *   #.......
   *   #.......
   *   #.......
   *   #.......
   *   #.......
   *   #.......
   * Encoded data: offset=1772, size=1
   */
  0xff,

  /* Unicode: U+2590 (▐) [RIGHT HALF BLOCK], Size: 8x8
   * Glyph (bounding: <x: 4, y: 0, width: 4, height: 8>):
   *   ....####
   *   ....####
   *   ....####
   *   ....####
   *   ....####
   *   ....####
   *   ....####
   *   ....####
   * Encoded data: offset=1773, size=4
   */
  0xff, 0xff, 0xff, 0xff,

  /* Unicode: U+2591 (░) [LIGHT SHADE], Size: 8x8
   * Glyph (bounding: <x: 0, y: 0, width: 7, height: 8>):
   *   ..#...#.
   *   #...#...
   *   ..#...#.
   *   #...#...
   *   ..#...#.
   *   #...#...
   *   ..#...#.
   *   #...#...
   * Encoded data: offset=1777, size=7
   */
  0x23, 0x10, 0x8c, 0x42, 0x31, 0x08, 0xc4,

  /* Unicode: U+2592 (▒) [MEDIUM SHADE], Size: 8x8
   * Glyph (bounding: <x: 0, y: 0, width: 8, height: 8>):
   *   #.#.#.#.
   *   .#.#.#.#
   *   #.#.#.#.
   *   .#.#.#.#
   *   #.#.#.#.
   *   .#.#.#.#
   *   #.#.#.#.
   *   .#.#.#.#
   * Encoded data: offset=1784, size=8
   */
  0xaa, 0x55, 0xaa, 0x55, 0xaa, 0x55, 0xaa, 0x55,

  /* Unicode: U+2593 (▓) [DARK SHADE], Size: 8x8
   * Glyph (bounding: <x: 0, y: 0, width: 8, height: 8>):
   *   ##.##.##
   *   .###.###
   *   ##.##.##
   *   ###.###.
   *   ##.##.##
   *   .###.###
   *   ##.##.##
   *   ###.###.
   * Encoded data: offset=1792, size=8
   */
  0xdb, 0x77, 0xdb, 0xee, 0xdb, 0x77, 0xdb, 0xee,

  /* Unicode: U+2594 (▔) [UPPER ONE EIGHTH BLOCK], Size: 8x8
   * Glyph (bounding: <x: 0, y: 0, width: 8, height: 1>):
   *   ########
   *   ........
   *   ........
   *   ........
   *   ........
   *   ........
   *   ........
   *   ........
   * Encoded data: offset=1800, size=1
   */
  0xff,

  /* Unicode: U+2595 (▕) [RIGHT ONE EIGHTH BLOCK], Size: 8x8
   * Glyph (bounding: <x: 7, y: 0, width: 1, height: 8>):
   *   .......#
   *   .......#
   *   .......#
   *   .......#
   *   .......#
   *   .......#
   *   .......#
   *   .......#
   * Encoded data: offset=1801, size=1
   */
  0xff,

  /* Unicode: U+2596 (▖) [QUADRANT LOWER LEFT], Size: 8x8
   * Glyph (bounding: <x: 0, y: 4, width: 4, height: 4>):
   *   ........
   *   ........
   *   ........
   *   ........
   *   ####....
   *   ####....
   *   ####....
   *   ####....
   * Encoded data: offset=1802, size=2
   */
  0xff, 0xff,

  /* Unicode: U+2597 (▗) [QUADRANT LOWER RIGHT], Size: 8x8
   * Glyph (bounding: <x: 4, y: 4, width: 4, height: 4>):
   *   ........
   *   ........
   *   ........
   *   ........
   *   ....####
   *   ....####
   *   ....####
   *   ....####
   * Encoded data: offset=1804, size=2
   */
  0xff, 0xff,

  /* Unicode: U+2598 (▘) [QUADRANT UPPER LEFT], Size: 8x8
   * Glyph (bounding: <x: 0, y: 0, width: 4, height: 4>):
   *   ####....
   *   ####....
   *   ####....
   *   ####....
   *   ........
   *   ........
   *   ........
   *   ........
   * Encoded data: offset=1806, size=2
   */
  0xff, 0xff,

  /* Unicode: U+2599 (▙) [QUADRANT UPPER LEFT AND LOWER LEFT AND LOWER RIGHT], Size: 8x8
   * Glyph (bounding: <x: 0, y: 0, width: 8, height: 8>):
   *   ####....
   *   ####....
   *   ####....
   *   ####....
   *   ########
   *   ########
   *   ########
   *   ########
   * Encoded data: offset=1808, size=8
   */
  0xf0, 0xf0, 0xf0, 0xf0, 0xff, 0xff, 0xff, 0xff,

  /* Unicode: U+259A (▚) [QUADRANT UPPER LEFT AND LOWER RIGHT], Size: 8x8
   * Glyph (bounding: <x: 0, y: 0, width: 8, height: 8>):
   *   ####....
   *   ####....
   *   ####....
   *   ####....
   *   ....####
   *   ....####
   *   ....####
   *   ....####
   * Encoded data: offset=1816, size=8
   */
  0xf0, 0xf0, 0xf0, 0xf0, 0x0f, 0x0f, 0x0f, 0x0f,

  /* Unicode: U+259B (▛) [QUADRANT UPPER LEFT AND UPPER RIGHT AND LOWER LEFT], Size: 8x8
   * Glyph (bounding: <x: 0, y: 0, width: 8, height: 8>):
   *   ########
   *   ########
   *   ########
   *   ########
   *   ####....
   *   ####....
   *   ####....
   *   ####....
   * Encoded data: offset=1824, size=8
   */
  0xff, 0xff, 0xff, 0xff, 0xf0, 0xf0, 0xf0, 0xf0,

  /* Unicode: U+259C (▜) [QUADRANT UPPER LEFT AND UPPER RIGHT AND LOWER RIGHT], Size: 8x8
   * Glyph (bounding: <x: 0, y: 0, width: 8, height: 8>):
   *   ########
   *   ########
   *   ########
   *   ########
   *   ....####
   *   ....####
   *   ....####
   *   ....####
   * Encoded data: offset=1832, size=8
   */
  0xff, 0xff, 0xff, 0xff, 0x0f, 0x0f, 0x0f, 0x0f,

  /* Unicode: U+259D (▝) [QUADRANT UPPER RIGHT], Size: 8x8
   * Glyph (bounding: <x: 4, y: 0, width: 4, height: 4>):
   *   ....####
   *   ....####
   *   ....####
   *   ....####
   *   ........
   *   ........
   *   ........
   *   ........
   * Encoded data: offset=1840, size=2
   */
  0xff, 0xff,

  /* Unicode: U+259E (▞) [QUADRANT UPPER RIGHT AND LOWER LEFT], Size: 8x8
   * Glyph (bounding: <x: 0, y: 0, width: 8, height: 8>):
   *   ....####
   *   ....####
   *   ....####
   *   ....####
   *   ####....
   *   ####....
   *   ####....
   *   ####....
   * Encoded data: offset=1842, size=8
   */
  0x0f, 0x0f, 0x0f, 0x0f, 0xf0, 0xf0, 0xf0, 0xf0,

  /* Unicode: U+259F (▟) [QUADRANT UPPER RIGHT AND LOWER LEFT AND LOWER RIGHT], Size: 8x8
   * Glyph (bounding: <x: 0, y: 0, width: 8, height: 8>):
   *   ....####
   *   ....####
   *   ....####
   *   ....####
   *   ########
   *   ########
   *   ########
   *   ########
   * Encoded data: offset=1850, size=8
   */
  0x0f, 0x0f, 0x0f, 0x0f, 0xff, 0xff, 0xff, 0xff,

  /* Unicode: U+25A0 (■) [BLACK SQUARE], Size: 8x8
   * Glyph (bounding: <x: 1, y: 1, width: 6, height: 6>):
   *   ........
   *   .######.
   *   .######.
   *   .######.
   *   .######.
   *   .######.
   *   .######.
   *   ........
   * Encoded data: offset=1858, size=5
   */
  0xff, 0xff, 0xff, 0xff, 0xf0,

  /* Unicode: U+25A1 (□) [WHITE SQUARE], Size: 8x8
   * Glyph (bounding: <x: 1, y: 1, width: 6, height: 6>):
   *   ........
   *   .######.
   *   .##..##.
   *   .##..##.
   *   .##..##.
   *   .##..##.
   *   .######.
   *   ........
   * Encoded data: offset=1863, size=5
   */
  0xff, 0x3c, 0xf3, 0xcf, 0xf0,

  /* Unicode: U+25A2 (▢) [WHITE SQUARE WITH ROUNDED CORNERS], Size: 8x8
   * Glyph (bounding: <x: 0, y: 0, width: 7, height: 7>):
   *   .#####..
   *   ##...##.
   *   ##...##.
   *   ##...##.
   *   ##...##.
   *   ##...##.
   *   .#####..
   *   ........
   * Encoded data: offset=1868, size=7
   */
  0x7d, 0x8f, 0x1e, 0x3c, 0x78, 0xdf, 0x00,

  /* Unicode: U+25A3 (▣) [WHITE SQUARE CONTAINING BLACK SMALL SQUARE], Size: 8x8
   * Glyph (bounding: <x: 0, y: 0, width: 7, height: 7>):
   *   #######.
   *   #.....#.
   *   #.###.#.
   *   #.###.#.
   *   #.###.#.
   *   #.....#.
   *   #######.
   *   ........
   * Encoded data: offset=1875, size=7
   */
  0xff, 0x06, 0xed, 0xdb, 0xb0, 0x7f, 0x80,

  /* Unicode: U+25A4 (▤) [SQUARE WITH HORIZONTAL FILL], Size: 8x8
   * Glyph (bounding: <x: 0, y: 0, width: 8, height: 7>):
   *   ########
   *   ........
   *   ########
   *   ........
   *   ########
   *   ........
   *   ########
   *   ........
   * Encoded data: offset=1882, size=7
   */
  0xff, 0x00, 0xff, 0x00, 0xff, 0x00, 0xff,

  /* Unicode: U+25A5 (▥) [SQUARE WITH VERTICAL FILL], Size: 8x8
   * Glyph (bounding: <x: 0, y: 0, width: 7, height: 8>):
   *   #.#.#.#.
   *   #.#.#.#.
   *   #.#.#.#.
   *   #.#.#.#.
   *   #.#.#.#.
   *   #.#.#.#.
   *   #.#.#.#.
   *   #.#.#.#.
   * Encoded data: offset=1889, size=7
   */
  0xab, 0x56, 0xad, 0x5a, 0xb5, 0x6a, 0xd5,

  /* Unicode: U+25A6 (▦) [SQUARE WITH ORTHOGONAL CROSSHATCH FILL], Size: 8x8
   * Glyph (bounding: <x: 0, y: 0, width: 8, height: 8>):
   *   .#...#..
   *   ########
   *   .#...#..
   *   .#...#..
   *   .#...#..
   *   ########
   *   .#...#..
   *   .#...#..
   * Encoded data: offset=1896, size=8
   */
  0x44, 0xff, 0x44, 0x44, 0x44, 0xff, 0x44, 0x44,

  /* Unicode: U+25A7 (▧) [SQUARE WITH UPPER LEFT TO LOWER RIGHT FILL], Size: 8x8
   * Glyph (bounding: <x: 0, y: 0, width: 8, height: 8>):
   *   #...#...
   *   .#...#..
   *   ..#...#.
   *   ...#...#
   *   #...#...
   *   .#...#..
   *   ..#...#.
   *   ...#...#
   * Encoded data: offset=1904, size=8
   */
  0x88, 0x44, 0x22, 0x11, 0x88, 0x44, 0x22, 0x11,

  /* Unicode: U+25A8 (▨) [SQUARE WITH UPPER RIGHT TO LOWER LEFT FILL], Size: 8x8
   * Glyph (bounding: <x: 0, y: 0, width: 8, height: 8>):
   *   ...#...#
   *   ..#...#.
   *   .#...#..
   *   #...#...
   *   ...#...#
   *   ..#...#.
   *   .#...#..
   *   #...#...
   * Encoded data: offset=1912, size=8
   */
  0x11, 0x22, 0x44, 0x88, 0x11, 0x22, 0x44, 0x88,

  /* Unicode: U+25A9 (▩) [SQUARE WITH DIAGONAL CROSSHATCH FILL], Size: 8x8
   * Glyph (bounding: <x: 0, y: 0, width: 8, height: 8>):
   *   #.#.#.#.
   *   .#...#..
   *   #.#.#.#.
   *   ...#...#
   *   #.#.#.#.
   *   .#...#..
   *   #.#.#.#.
   *   ...#...#
   * Encoded data: offset=1920, size=8
   */
  0xaa, 0x44, 0xaa, 0x11, 0xaa, 0x44, 0xaa, 0x11,

  /* Unicode: U+25AA (▪) [BLACK SMALL SQUARE], Size: 8x8
   * Glyph (bounding: <x: 2, y: 2, width: 3, height: 3>):
   *   ........
   *   ........
   *   ..###...
   *   ..###...
   *   ..###...
   *   ........
   *   ........
   *   ........
   * Encoded data: offset=1928, size=2
   */
  0xff, 0x80,

  /* Unicode: U+25AB (▫) [WHITE SMALL SQUARE], Size: 8x8
   * Glyph (bounding: <x: 2, y: 2, width: 3, height: 3>):
   *   ........
   *   ........
   *   ..###...
   *   ..#.#...
   *   ..###...
   *   ........
   *   ........
   *   ........
   * Encoded data: offset=1930, size=2
   */
  0xf7, 0x80,

  /* Unicode: U+25AC (▬) [BLACK RECTANGLE], Size: 8x8
   * Glyph (bounding: <x: 1, y: 4, width: 6, height: 3>):
   *   ........
   *   ........
   *   ........
   *   ........
   *   .######.
   *   .######.
   *   .######.
   *   ........
   * Encoded data: offset=1932, size=3
   */
  0xff, 0xff, 0xc0,

  /* Unicode: U+25AD (▭) [WHITE RECTANGLE], Size: 8x8
   * Glyph (bounding: <x: 1, y: 4, width: 6, height: 3>):
   *   ........
   *   ........
   *   ........
   *   ........
   *   .######.
   *   .##..##.
   *   .######.
   *   ........
   * Encoded data: offset=1935, size=3
   */
  0xff, 0x3f, 0xc0,

  /* Unicode: U+25AE (▮) [BLACK VERTICAL RECTANGLE], Size: 8x8
   * Glyph (bounding: <x: 2, y: 0, width: 4, height: 7>):
   *   ..####..
   *   ..####..
   *   ..####..
   *   ..####..
   *   ..####..
   *   ..####..
   *   ..####..
   *   ........
   * Encoded data: offset=1938, size=4
   */
  0xff, 0xff, 0xff, 0xf0,

  /* Unicode: U+25AF (▯) [WHITE VERTICAL RECTANGLE], Size: 8x8
   * Glyph (bounding: <x: 2, y: 0, width: 4, height: 7>):
   *   ..####..
   *   ..#..#..
   *   ..#..#..
   *   ..#..#..
   *   ..#..#..
   *   ..#..#..
   *   ..####..
   *   ........
   * Encoded data: offset=1942, size=4
   */
  0xf9, 0x99, 0x99, 0xf0,

  /* Unicode: U+25B0 (▰) [BLACK PARALLELOGRAM], Size: 8x8
   * Glyph (bounding: <x: 0, y: 4, width: 8, height: 3>):
   *   ........
   *   ........
   *   ........
   *   ........
   *   ..######
   *   .######.
   *   ######..
   *   ........
   * Encoded data: offset=1946, size=3
   */
  0x3f, 0x7e, 0xfc,

  /* Unicode: U+25B1 (▱) [WHITE PARALLELOGRAM], Size: 8x8
   * Glyph (bounding: <x: 0, y: 4, width: 8, height: 3>):
   *   ........
   *   ........
   *   ........
   *   ........
   *   ..######
   *   .##..##.
   *   ######..
   *   ........
   * Encoded data: offset=1949, size=3
   */
  0x3f, 0x66, 0xfc,

  /* Unicode: U+25B2 (▲) [BLACK UP-POINTING TRIANGLE], Size: 8x8
   * Glyph (bounding: <x: 0, y: 0, width: 7, height: 8>):
   *   ...#....
   *   ...#....
   *   ..###...
   *   ..###...
   *   .#####..
   *   .#####..
   *   #######.
   *   #######.
   * Encoded data: offset=1952, size=7
   */
  0x10, 0x20, 0xe1, 0xc7, 0xcf, 0xbf, 0xff,

  /* Unicode: U+25B3 (△) [WHITE UP-POINTING TRIANGLE], Size: 8x8
   * Glyph (bounding: <x: 0, y: 0, width: 8, height: 8>):
   *   ...##...
   *   ...##...
   *   ..####..
   *   .##..##.
   *   .##..##.
   *   ##....##
   *   ##....##
   *   ########
   * Encoded data: offset=1959, size=8
   */
  0x18, 0x18, 0x3c, 0x66, 0x66, 0xc3, 0xc3, 0xff,

  /* Unicode: U+25B4 (▴) [BLACK UP-POINTING SMALL TRIANGLE], Size: 8x8
   * Glyph (bounding: <x: 1, y: 1, width: 5, height: 6>):
   *   ........
   *   ...#....
   *   ...#....
   *   ..###...
   *   ..###...
   *   .#####..
   *   .#####..
   *   ........
   * Encoded data: offset=1967, size=4
   */
  0x21, 0x1c, 0xef, 0xfc,

  /* Unicode: U+25B5 (▵) [WHITE UP-POINTING SMALL TRIANGLE], Size: 8x8
   * Glyph (bounding: <x: 0, y: 1, width: 7, height: 6>):
   *   ........
   *   ...#....
   *   ..#.#...
   *   .#...#..
   *   #.....#.
   *   #.....#.
   *   #######.
   *   ........
   * Encoded data: offset=1971, size=6
   */
  0x10, 0x51, 0x14, 0x18, 0x3f, 0xc0,

  /* Unicode: U+25B6 (▶) [BLACK RIGHT-POINTING TRIANGLE], Size: 8x8
   * Glyph (bounding: <x: 0, y: 0, width: 8, height: 7>):
   *   ##......
   *   ####....
   *   ######..
   *   ########
   *   ######..
   *   ####....
   *   ##......
   *   ........
   * Encoded data: offset=1977, size=7
   */
  0xc0, 0xf0, 0xfc, 0xff, 0xfc, 0xf0, 0xc0,

  /* Unicode: U+25B7 (▷) [WHITE RIGHT-POINTING TRIANGLE], Size: 8x8
   * Glyph (bounding: <x: 0, y: 0, width: 8, height: 7>):
   *   ##......
   *   ####....
   *   ##.###..
   *   ##...###
   *   ##.###..
   *   ####....
   *   ##......
   *   ........
   * Encoded data: offset=1984, size=7
   */
  0xc0, 0xf0, 0xdc, 0xc7, 0xdc, 0xf0, 0xc0,

  /* Unicode: U+25B8 (▸) [BLACK RIGHT-POINTING SMALL TRIANGLE], Size: 8x8
   * Glyph (bounding: <x: 1, y: 1, width: 6, height: 5>):
   *   ........
   *   .##.....
   *   .####...
   *   .######.
   *   .####...
   *   .##.....
   *   ........
   *   ........
   * Encoded data: offset=1991, size=4
   */
  0xc3, 0xcf, 0xfc, 0xc0,

  /* Unicode: U+25B9 (▹) [WHITE RIGHT-POINTING SMALL TRIANGLE], Size: 8x8
   * Glyph (bounding: <x: 1, y: 1, width: 6, height: 5>):
   *   ........
   *   .##.....
   *   .#.##...
   *   .#...##.
   *   .#.##...
   *   .##.....
   *   ........
   *   ........
   * Encoded data: offset=1995, size=4
   */
  0xc2, 0xc8, 0xec, 0xc0,

  /* Unicode: U+25BA (►) [BLACK RIGHT-POINTING POINTER], Size: 8x8
   * Glyph (bounding: <x: 0, y: 1, width: 8, height: 5>):
   *   ........
   *   ###.....
   *   ######..
   *   ########
   *   ######..
   *   ###.....
   *   ........
   *   ........
   * Encoded data: offset=1999, size=5
   */
  0xe0, 0xfc, 0xff, 0xfc, 0xe0,

  /* Unicode: U+25BB (▻) [WHITE RIGHT-POINTING POINTER], Size: 8x8
   * Glyph (bounding: <x: 0, y: 1, width: 8, height: 5>):
   *   ........
   *   ####....
   *   ##.###..
   *   ##...###
   *   ##.###..
   *   ####....
   *   ........
   *   ........
   * Encoded data: offset=2004, size=5
   */
  0xf0, 0xdc, 0xc7, 0xdc, 0xf0,

  /* Unicode: U+25BC (▼) [BLACK DOWN-POINTING TRIANGLE], Size: 8x8
   * Glyph (bounding: <x: 0, y: 0, width: 7, height: 8>):
   *   #######.
   *   #######.
   *   .#####..
   *   .#####..
   *   ..###...
   *   ..###...
   *   ...#....
   *   ...#....
   * Encoded data: offset=2009, size=7
   */
  0xff, 0xfd, 0xf3, 0xe3, 0x87, 0x04, 0x08,

  /* Unicode: U+25BD (▽) [WHITE DOWN-POINTING TRIANGLE], Size: 8x8
   * Glyph (bounding: <x: 0, y: 0, width: 8, height: 8>):
   *   ########
   *   ##....##
   *   ##....##
   *   .##..##.
   *   .##..##.
   *   ..####..
   *   ...##...
   *   ...##...
   * Encoded data: offset=2016, size=8
   */
  0xff, 0xc3, 0xc3, 0x66, 0x66, 0x3c, 0x18, 0x18,

  /* Unicode: U+25BE (▾) [BLACK DOWN-POINTING SMALL TRIANGLE], Size: 8x8
   * Glyph (bounding: <x: 1, y: 1, width: 5, height: 6>):
   *   ........
   *   .#####..
   *   .#####..
   *   ..###...
   *   ..###...
   *   ...#....
   *   ...#....
   *   ........
   * Encoded data: offset=2024, size=4
   */
  0xff, 0xdc, 0xe2, 0x10,

  /* Unicode: U+25BF (▿) [WHITE DOWN-POINTING SMALL TRIANGLE], Size: 8x8
   * Glyph (bounding: <x: 0, y: 1, width: 7, height: 6>):
   *   ........
   *   #######.
   *   #.....#.
   *   #.....#.
   *   .#...#..
   *   ..#.#...
   *   ...#....
   *   ........
   * Encoded data: offset=2028, size=6
   */
  0xff, 0x06, 0x0a, 0x22, 0x82, 0x00,

  /* Unicode: U+25C0 (◀) [BLACK LEFT-POINTING TRIANGLE], Size: 8x8
   * Glyph (bounding: <x: 0, y: 0, width: 8, height: 7>):
   *   ......##
   *   ....####
   *   ..######
   *   ########
   *   ..######
   *   ....####
   *   ......##
   *   ........
   * Encoded data: offset=2034, size=7
   */
  0x03, 0x0f, 0x3f, 0xff, 0x3f, 0x0f, 0x03,

  /* Unicode: U+25C1 (◁) [WHITE LEFT-POINTING TRIANGLE], Size: 8x8
   * Glyph (bounding: <x: 0, y: 0, width: 8, height: 7>):
   *   ......##
   *   ....####
   *   ..###.##
   *   ###...##
   *   ..###.##
   *   ....####
   *   ......##
   *   ........
   * Encoded data: offset=2041, size=7
   */
  0x03, 0x0f, 0x3b, 0xe3, 0x3b, 0x0f, 0x03,

  /* Unicode: U+25C2 (◂) [BLACK LEFT-POINTING SMALL TRIANGLE], Size: 8x8
   * Glyph (bounding: <x: 1, y: 1, width: 6, height: 5>):
   *   ........
   *   .....##.
   *   ...####.
   *   .######.
   *   ...####.
   *   .....##.
   *   ........
   *   ........
   * Encoded data: offset=2048, size=4
   */
  0x0c, 0xff, 0xcf, 0x0c,

  /* Unicode: U+25C3 (◃) [WHITE LEFT-POINTING SMALL TRIANGLE], Size: 8x8
   * Glyph (bounding: <x: 1, y: 1, width: 6, height: 5>):
   *   ........
   *   .....##.
   *   ...##.#.
   *   .##...#.
   *   ...##.#.
   *   .....##.
   *   ........
   *   ........
   * Encoded data: offset=2052, size=4
   */
  0x0c, 0xdc, 0x4d, 0x0c,

  /* Unicode: U+25C4 (◄) [BLACK LEFT-POINTING POINTER], Size: 8x8
   * Glyph (bounding: <x: 0, y: 1, width: 8, height: 5>):
   *   ........
   *   .....###
   *   ..######
   *   ########
   *   ..######
   *   .....###
   *   ........
   *   ........
   * Encoded data: offset=2056, size=5
   */
  0x07, 0x3f, 0xff, 0x3f, 0x07,

  /* Unicode: U+25C5 (◅) [WHITE LEFT-POINTING POINTER], Size: 8x8
   * Glyph (bounding: <x: 0, y: 1, width: 8, height: 5>):
   *   ........
   *   ....####
   *   ..###.##
   *   ###...##
   *   ..###.##
   *   ....####
   *   ........
   *   ........
   * Encoded data: offset=2061, size=5
   */
  0x0f, 0x3b, 0xe3, 0x3b, 0x0f,

  /* Unicode: U+25C6 (◆) [BLACK DIAMOND], Size: 8x8
   * Glyph (bounding: <x: 0, y: 0, width: 8, height: 8>):
   *   ...##...
   *   ..####..
   *   .######.
   *   ########
   *   ########
   *   .######.
   *   ..####..
   *   ...##...
   * Encoded data: offset=2066, size=8
   */
  0x18, 0x3c, 0x7e, 0xff, 0xff, 0x7e, 0x3c, 0x18,

  /* Unicode: U+25C7 (◇) [WHITE DIAMOND], Size: 8x8
   * Glyph (bounding: <x: 0, y: 0, width: 8, height: 8>):
   *   ...##...
   *   ..####..
   *   .##..##.
   *   ##....##
   *   ##....##
   *   .##..##.
   *   ..####..
   *   ...##...
   * Encoded data: offset=2074, size=8
   */
  0x18, 0x3c, 0x66, 0xc3, 0xc3, 0x66, 0x3c, 0x18,

  /* Unicode: U+25C8 (◈) [WHITE DIAMOND CONTAINING BLACK SMALL DIAMOND], Size: 8x8
   * Glyph (bounding: <x: 0, y: 0, width: 8, height: 8>):
   *   ...##...
   *   ..#..#..
   *   .#.##.#.
   *   #.####.#
   *   #.####.#
   *   .#.##.#.
   *   ..#..#..
   *   ...##...
   * Encoded data: offset=2082, size=8
   */
  0x18, 0x24, 0x5a, 0xbd, 0xbd, 0x5a, 0x24, 0x18,

  /* Unicode: U+25C9 (◉) [FISHEYE], Size: 8x8
   * Glyph (bounding: <x: 0, y: 0, width: 8, height: 8>):
   *   ..####..
   *   .##..##.
   *   ##....##
   *   ##.##.##
   *   ##.##.##
   *   ##....##
   *   .##..##.
   *   ..####..
   * Encoded data: offset=2090, size=8
   */
  0x3c, 0x66, 0xc3, 0xdb, 0xdb, 0xc3, 0x66, 0x3c,

  /* Unicode: U+25CA (◊) [LOZENGE], Size: 8x8
   * Glyph (bounding: <x: 0, y: 0, width: 7, height: 8>):
   *   ...#....
   *   ..###...
   *   .##.##..
   *   ##...##.
   *   ##...##.
   *   .##.##..
   *   ..###...
   *   ...#....
   * Encoded data: offset=2098, size=7
   */
  0x10, 0x71, 0xb6, 0x3c, 0x6d, 0x8e, 0x08,

  /* Unicode: U+25CB (○) [WHITE CIRCLE], Size: 8x8
   * Glyph (bounding: <x: 1, y: 1, width: 6, height: 6>):
   *   ........
   *   ..####..
   *   .##..##.
   *   .#....#.
   *   .#....#.
   *   .##..##.
   *   ..####..
   *   ........
   * Encoded data: offset=2105, size=5
   */
  0x7b, 0x38, 0x61, 0xcd, 0xe0,

  /* Unicode: U+25CC (◌) [DOTTED CIRCLE], Size: 8x8
   * Glyph (bounding: <x: 0, y: 0, width: 7, height: 7>):
   *   ...#....
   *   .#...#..
   *   ........
   *   #.....#.
   *   ........
   *   .#...#..
   *   ...#....
   *   ........
   * Encoded data: offset=2110, size=7
   */
  0x10, 0x88, 0x04, 0x10, 0x08, 0x84, 0x00,

  /* Unicode: U+25CD (◍) [CIRCLE WITH VERTICAL FILL], Size: 8x8
   * Glyph (bounding: <x: 0, y: 1, width: 7, height: 6>):
   *   ........
   *   ..###...
   *   .##.##..
   *   #.#.#.#.
   *   #.#.#.#.
   *   .##.##..
   *   ..###...
   *   ........
   * Encoded data: offset=2117, size=6
   */
  0x38, 0xda, 0xad, 0x56, 0xc7, 0x00,

  /* Unicode: U+25CE (◎) [BULLSEYE], Size: 8x8
   * Glyph (bounding: <x: 0, y: 0, width: 7, height: 7>):
   *   ..###...
   *   .#...#..
   *   #..#..#.
   *   #.#.#.#.
   *   #..#..#.
   *   .#...#..
   *   ..###...
   *   ........
   * Encoded data: offset=2123, size=7
   */
  0x38, 0x8a, 0x4d, 0x59, 0x28, 0x8e, 0x00,

  /* Unicode: U+25CF (●) [BLACK CIRCLE], Size: 8x8
   * Glyph (bounding: <x: 1, y: 1, width: 6, height: 6>):
   *   ........
   *   ..####..
   *   .######.
   *   .######.
   *   .######.
   *   .######.
   *   ..####..
   *   ........
   * Encoded data: offset=2130, size=5
   */
  0x7b, 0xff, 0xff, 0xfd, 0xe0,

  /* Unicode: U+25D0 (◐) [CIRCLE WITH LEFT HALF BLACK], Size: 8x8
   * Glyph (bounding: <x: 0, y: 0, width: 8, height: 8>):
   *   ..####..
   *   .###.##.
   *   ####..##
   *   ####..##
   *   ####..##
   *   ####..##
   *   .###.##.
   *   ..####..
   * Encoded data: offset=2135, size=8
   */
  0x3c, 0x76, 0xf3, 0xf3, 0xf3, 0xf3, 0x76, 0x3c,

  /* Unicode: U+25D1 (◑) [CIRCLE WITH RIGHT HALF BLACK], Size: 8x8
   * Glyph (bounding: <x: 0, y: 0, width: 8, height: 8>):
   *   ..####..
   *   .##.###.
   *   ##..####
   *   ##..####
   *   ##..####
   *   ##..####
   *   .##.###.
   *   ..####..
   * Encoded data: offset=2143, size=8
   */
  0x3c, 0x6e, 0xcf, 0xcf, 0xcf, 0xcf, 0x6e, 0x3c,

  /* Unicode: U+25D2 (◒) [CIRCLE WITH LOWER HALF BLACK], Size: 8x8
   * Glyph (bounding: <x: 0, y: 0, width: 8, height: 8>):
   *   ..####..
   *   .##..##.
   *   ##....##
   *   ##....##
   *   ########
   *   ########
   *   .######.
   *   ..####..
   * Encoded data: offset=2151, size=8
   */
  0x3c, 0x66, 0xc3, 0xc3, 0xff, 0xff, 0x7e, 0x3c,

  /* Unicode: U+25D3 (◓) [CIRCLE WITH UPPER HALF BLACK], Size: 8x8
   * Glyph (bounding: <x: 0, y: 0, width: 8, height: 8>):
   *   ..####..
   *   .######.
   *   ########
   *   ########
   *   ##....##
   *   ##....##
   *   .##..##.
   *   ..####..
   * Encoded data: offset=2159, size=8
   */
  0x3c, 0x7e, 0xff, 0xff, 0xc3, 0xc3, 0x66, 0x3c,

  /* Unicode: U+25D4 (◔) [CIRCLE WITH UPPER RIGHT QUADRANT BLACK], Size: 8x8
   * Glyph (bounding: <x: 0, y: 0, width: 8, height: 8>):
   *   ..####..
   *   .##.###.
   *   ##..####
   *   ##..####
   *   ##....##
   *   ##....##
   *   .##..##.
   *   ..####..
   * Encoded data: offset=2167, size=8
   */
  0x3c, 0x6e, 0xcf, 0xcf, 0xc3, 0xc3, 0x66, 0x3c,

  /* Unicode: U+25D5 (◕) [CIRCLE WITH ALL BUT UPPER LEFT QUADRANT BLACK], Size: 8x8
   * Glyph (bounding: <x: 0, y: 0, width: 8, height: 8>):
   *   ..####..
   *   .##.###.
   *   ##..####
   *   ##..####
   *   ########
   *   ########
   *   .######.
   *   ..####..
   * Encoded data: offset=2175, size=8
   */
  0x3c, 0x6e, 0xcf, 0xcf, 0xff, 0xff, 0x7e, 0x3c,

  /* Unicode: U+25D6 (◖) [LEFT HALF BLACK CIRCLE], Size: 8x8
   * Glyph (bounding: <x: 2, y: 1, width: 6, height: 6>):
   *   ........
   *   .....###
   *   ...#####
   *   ..######
   *   ..######
   *   ...#####
   *   .....###
   *   ........
   * Encoded data: offset=2183, size=5
   */
  0x1d, 0xff, 0xff, 0x7c, 0x70,

  /* Unicode: U+25D7 (◗) [RIGHT HALF BLACK CIRCLE], Size: 8x8
   * Glyph (bounding: <x: 0, y: 1, width: 6, height: 6>):
   *   ........
   *   ###.....
   *   #####...
   *   ######..
   *   ######..
   *   #####...
   *   ###.....
   *   ........
   * Encoded data: offset=2188, size=5
   */
  0xe3, 0xef, 0xff, 0xfb, 0x80,

  /* Unicode: U+25D8 (◘) [INVERSE BULLET], Size: 8x8
   * Glyph (bounding: <x: 0, y: 0, width: 8, height: 8>):
   *   ########
   *   ########
   *   ###..###
   *   ##....##
   *   ##....##
   *   ###..###
   *   ########
   *   ########
   * Encoded data: offset=2193, size=8
   */
  0xff, 0xff, 0xe7, 0xc3, 0xc3, 0xe7, 0xff, 0xff,

  /* Unicode: U+25D9 (◙) [INVERSE WHITE CIRCLE], Size: 8x8
   * Glyph (bounding: <x: 0, y: 0, width: 8, height: 8>):
   *   ########
   *   ##....##
   *   #..##..#
   *   #.####.#
   *   #.####.#
   *   #..##..#
   *   ##....##
   *   ########
   * Encoded data: offset=2201, size=8
   */
  0xff, 0xc3, 0x99, 0xbd, 0xbd, 0x99, 0xc3, 0xff,

  /* Unicode: U+25DA (◚) [UPPER HALF INVERSE WHITE CIRCLE], Size: 8x8
   * Glyph (bounding: <x: 0, y: 0, width: 8, height: 4>):
   *   ########
   *   ##....##
   *   #..##..#
   *   #.####.#
   *   ........
   *   ........
   *   ........
   *   ........
   * Encoded data: offset=2209, size=4
   */
  0xff, 0xc3, 0x99, 0xbd,

  /* Unicode: U+25DB (◛) [LOWER HALF INVERSE WHITE CIRCLE], Size: 8x8
   * Glyph (bounding: <x: 0, y: 4, width: 8, height: 4>):
   *   ........
   *   ........
   *   ........
   *   ........
   *   #.####.#
   *   #..##..#
   *   ##....##
   *   ########
   * Encoded data: offset=2213, size=4
   */
  0xbd, 0x99, 0xc3, 0xff,

  /* Unicode: U+25DC (◜) [UPPER LEFT QUADRANT CIRCULAR ARC], Size: 8x8
   * Glyph (bounding: <x: 0, y: 0, width: 5, height: 4>):
   *   ..###...
   *   .##.....
   *   ##......
   *   ##......
   *   ........
   *   ........
   *   ........
   *   ........
   * Encoded data: offset=2217, size=3
   */
  0x3b, 0x31, 0x80,

  /* Unicode: U+25DD (◝) [UPPER RIGHT QUADRANT CIRCULAR ARC], Size: 8x8
   * Glyph (bounding: <x: 3, y: 0, width: 5, height: 4>):
   *   ...###..
   *   .....##.
   *   ......##
   *   ......##
   *   ........
   *   ........
   *   ........
   *   ........
   * Encoded data: offset=2220, size=3
   */
  0xe1, 0x86, 0x30,

  /* Unicode: U+25DE (◞) [LOWER RIGHT QUADRANT CIRCULAR ARC], Size: 8x8
   * Glyph (bounding: <x: 3, y: 4, width: 5, height: 4>):
   *   ........
   *   ........
   *   ........
   *   ........
   *   ......##
   *   ......##
   *   .....##.
   *   ...###..
   * Encoded data: offset=2223, size=3
   */
  0x18, 0xcd, 0xc0,

  /* Unicode: U+25DF (◟) [LOWER LEFT QUADRANT CIRCULAR ARC], Size: 8x8
   * Glyph (bounding: <x: 0, y: 4, width: 5, height: 4>):
   *   ........
   *   ........
   *   ........
   *   ........
   *   ##......
   *   ##......
   *   .##.....
   *   ..###...
   * Encoded data: offset=2226, size=3
   */
  0xc6, 0x18, 0x70,

  /* Unicode: U+25E0 (◠) [UPPER HALF CIRCLE], Size: 8x8
   * Glyph (bounding: <x: 0, y: 0, width: 8, height: 4>):
   *   ..####..
   *   .##..##.
   *   ##....##
   *   ##....##
   *   ........
   *   ........
   *   ........
   *   ........
   * Encoded data: offset=2229, size=4
   */
  0x3c, 0x66, 0xc3, 0xc3,

  /* Unicode: U+25E1 (◡) [LOWER HALF CIRCLE], Size: 8x8
   * Glyph (bounding: <x: 0, y: 4, width: 8, height: 4>):
   *   ........
   *   ........
   *   ........
   *   ........
   *   ##....##
   *   ##....##
   *   .##..##.
   *   ..####..
   * Encoded data: offset=2233, size=4
   */
  0xc3, 0xc3, 0x66, 0x3c,

  /* Unicode: U+25E2 (◢) [BLACK LOWER RIGHT TRIANGLE], Size: 8x8
   * Glyph (bounding: <x: 0, y: 0, width: 8, height: 8>):
   *   .......#
   *   ......##
   *   .....###
   *   ....####
   *   ...#####
   *   ..######
   *   .#######
   *   ########
   * Encoded data: offset=2237, size=8
   */
  0x01, 0x03, 0x07, 0x0f, 0x1f, 0x3f, 0x7f, 0xff,

  /* Unicode: U+25E3 (◣) [BLACK LOWER LEFT TRIANGLE], Size: 8x8
   * Glyph (bounding: <x: 0, y: 0, width: 8, height: 8>):
   *   #.......
   *   ##......
   *   ###.....
   *   ####....
   *   #####...
   *   ######..
   *   #######.
   *   ########
   * Encoded data: offset=2245, size=8
   */
  0x80, 0xc0, 0xe0, 0xf0, 0xf8, 0xfc, 0xfe, 0xff,

  /* Unicode: U+25E4 (◤) [BLACK UPPER LEFT TRIANGLE], Size: 8x8
   * Glyph (bounding: <x: 0, y: 0, width: 8, height: 8>):
   *   ########
   *   #######.
   *   ######..
   *   #####...
   *   ####....
   *   ###.....
   *   ##......
   *   #.......
   * Encoded data: offset=2253, size=8
   */
  0xff, 0xfe, 0xfc, 0xf8, 0xf0, 0xe0, 0xc0, 0x80,

  /* Unicode: U+25E5 (◥) [BLACK UPPER RIGHT TRIANGLE], Size: 8x8
   * Glyph (bounding: <x: 0, y: 0, width: 8, height: 8>):
   *   ########
   *   .#######
   *   ..######
   *   ...#####
   *   ....####
   *   .....###
   *   ......##
   *   .......#
   * Encoded data: offset=2261, size=8
   */
  0xff, 0x7f, 0x3f, 0x1f, 0x0f, 0x07, 0x03, 0x01,

  /* Unicode: U+25E6 (◦) [WHITE BULLET], Size: 8x8
   * Glyph (bounding: <x: 2, y: 2, width: 4, height: 4>):
   *   ........
   *   ........
   *   ...##...
   *   ..#..#..
   *   ..#..#..
   *   ...##...
   *   ........
   *   ........
   * Encoded data: offset=2269, size=2
   */
  0x69, 0x96,

  /* Unicode: U+25E7 (◧) [SQUARE WITH LEFT HALF BLACK], Size: 8x8
   * Glyph (bounding: <x: 0, y: 0, width: 8, height: 8>):
   *   ########
   *   ####..##
   *   ####..##
   *   ####..##
   *   ####..##
   *   ####..##
   *   ####..##
   *   ########
   * Encoded data: offset=2271, size=8
   */
  0xff, 0xf3, 0xf3, 0xf3, 0xf3, 0xf3, 0xf3, 0xff,

  /* Unicode: U+25E8 (◨) [SQUARE WITH RIGHT HALF BLACK], Size: 8x8
   * Glyph (bounding: <x: 0, y: 0, width: 8, height: 8>):
   *   ########
   *   ##..####
   *   ##..####
   *   ##..####
   *   ##..####
   *   ##..####
   *   ##..####
   *   ########
   * Encoded data: offset=2279, size=8
   */
  0xff, 0xcf, 0xcf, 0xcf, 0xcf, 0xcf, 0xcf, 0xff,

  /* Unicode: U+25E9 (◩) [SQUARE WITH UPPER LEFT DIAGONAL HALF BLACK], Size: 8x8
   * Glyph (bounding: <x: 0, y: 0, width: 8, height: 8>):
   *   ########
   *   ########
   *   ########
   *   #####.##
   *   ####..##
   *   ###...##
   *   ##....##
   *   ########
   * Encoded data: offset=2287, size=8
   */
  0xff, 0xff, 0xff, 0xfb, 0xf3, 0xe3, 0xc3, 0xff,

  /* Unicode: U+25EA (◪) [SQUARE WITH LOWER RIGHT DIAGONAL HALF BLACK], Size: 8x8
   * Glyph (bounding: <x: 0, y: 0, width: 8, height: 8>):
   *   ########
   *   ##....##
   *   ##...###
   *   ##..####
   *   ##.#####
   *   ########
   *   ########
   *   ########
   * Encoded data: offset=2295, size=8
   */
  0xff, 0xc3, 0xc7, 0xcf, 0xdf, 0xff, 0xff, 0xff,

  /* Unicode: U+25EB (◫) [WHITE SQUARE WITH VERTICAL BISECTING LINE], Size: 8x8
   * Glyph (bounding: <x: 0, y: 0, width: 8, height: 8>):
   *   ########
   *   ##.##.##
   *   ##.##.##
   *   ##.##.##
   *   ##.##.##
   *   ##.##.##
   *   ##.##.##
   *   ########
   * Encoded data: offset=2303, size=8
   */
  0xff, 0xdb, 0xdb, 0xdb, 0xdb, 0xdb, 0xdb, 0xff,

  /* Unicode: U+25EC (◬) [WHITE UP-POINTING TRIANGLE WITH DOT], Size: 8x8
   * Glyph (bounding: <x: 0, y: 0, width: 8, height: 8>):
   *   ...##...
   *   ...##...
   *   ..#..#..
   *   .#....#.
   *   .#.##.#.
   *   #..##..#
   *   #......#
   *   ########
   * Encoded data: offset=2311, size=8
   */
  0x18, 0x18, 0x24, 0x42, 0x5a, 0x99, 0x81, 0xff,

  /* Unicode: U+25ED (◭) [UP-POINTING TRIANGLE WITH LEFT HALF BLACK], Size: 8x8
   * Glyph (bounding: <x: 0, y: 0, width: 8, height: 8>):
   *   ...##...
   *   ...##...
   *   ..####..
   *   .###.##.
   *   .###.##.
   *   ####..##
   *   ####..##
   *   ########
   * Encoded data: offset=2319, size=8
   */
  0x18, 0x18, 0x3c, 0x76, 0x76, 0xf3, 0xf3, 0xff,

  /* Unicode: U+25EE (◮) [UP-POINTING TRIANGLE WITH RIGHT HALF BLACK], Size: 8x8
   * Glyph (bounding: <x: 0, y: 0, width: 8, height: 8>):
   *   ...##...
   *   ...##...
   *   ..####..
   *   .##.###.
   *   .##.###.
   *   ##..####
   *   ##..####
   *   ########
   * Encoded data: offset=2327, size=8
   */
  0x18, 0x18, 0x3c, 0x6e, 0x6e, 0xcf, 0xcf, 0xff,

  /* Unicode: U+25EF (◯) [LARGE CIRCLE], Size: 8x8
   * Glyph (bounding: <x: 0, y: 0, width: 8, height: 8>):
   *   ..####..
   *   .##..##.
   *   ##....##
   *   ##....##
   *   ##....##
   *   ##....##
   *   .##..##.
   *   ..####..
   * Encoded data: offset=2335, size=8
   */
  0x3c, 0x66, 0xc3, 0xc3, 0xc3, 0xc3, 0x66, 0x3c,

  /* Unicode: U+25F0 (◰) [WHITE SQUARE WITH UPPER LEFT QUADRANT], Size: 8x8
   * Glyph (bounding: <x: 0, y: 0, width: 8, height: 8>):
   *   ########
   *   ##.##.##
   *   ##.##.##
   *   ##.##.##
   *   #####.##
   *   ##....##
   *   ##....##
   *   ########
   * Encoded data: offset=2343, size=8
   */
  0xff, 0xdb, 0xdb, 0xdb, 0xfb, 0xc3, 0xc3, 0xff,

  /* Unicode: U+25F1 (◱) [WHITE SQUARE WITH LOWER LEFT QUADRANT], Size: 8x8
   * Glyph (bounding: <x: 0, y: 0, width: 8, height: 8>):
   *   ########
   *   ##....##
   *   ##....##
   *   ##....##
   *   #####.##
   *   ##.##.##
   *   ##.##.##
   *   ########
   * Encoded data: offset=2351, size=8
   */
  0xff, 0xc3, 0xc3, 0xc3, 0xfb, 0xdb, 0xdb, 0xff,

  /* Unicode: U+25F2 (◲) [WHITE SQUARE WITH LOWER RIGHT QUADRANT], Size: 8x8
   * Glyph (bounding: <x: 0, y: 0, width: 8, height: 8>):
   *   ########
   *   ##....##
   *   ##....##
   *   ##....##
   *   ##.#####
   *   ##.##.##
   *   ##.##.##
   *   ########
   * Encoded data: offset=2359, size=8
   */
  0xff, 0xc3, 0xc3, 0xc3, 0xdf, 0xdb, 0xdb, 0xff,

  /* Unicode: U+25F3 (◳) [WHITE SQUARE WITH UPPER RIGHT QUADRANT], Size: 8x8
   * Glyph (bounding: <x: 0, y: 0, width: 8, height: 8>):
   *   ########
   *   ##.##.##
   *   ##.##.##
   *   ##.##.##
   *   ##.#####
   *   ##....##
   *   ##....##
   *   ########
   * Encoded data: offset=2367, size=8
   */
  0xff, 0xdb, 0xdb, 0xdb, 0xdf, 0xc3, 0xc3, 0xff,

  /* Unicode: U+25F4 (◴) [WHITE CIRCLE WITH UPPER LEFT QUADRANT], Size: 8x8
   * Glyph (bounding: <x: 0, y: 0, width: 8, height: 8>):
   *   ..####..
   *   .######.
   *   ##.##.##
   *   ##.##.##
   *   #####.##
   *   ##....##
   *   .##..##.
   *   ..####..
   * Encoded data: offset=2375, size=8
   */
  0x3c, 0x7e, 0xdb, 0xdb, 0xfb, 0xc3, 0x66, 0x3c,

  /* Unicode: U+25F5 (◵) [WHITE CIRCLE WITH LOWER LEFT QUADRANT], Size: 8x8
   * Glyph (bounding: <x: 0, y: 0, width: 8, height: 8>):
   *   ..####..
   *   .##..##.
   *   ##....##
   *   #####.##
   *   ##.##.##
   *   ##.##.##
   *   .######.
   *   ..####..
   * Encoded data: offset=2383, size=8
   */
  0x3c, 0x66, 0xc3, 0xfb, 0xdb, 0xdb, 0x7e, 0x3c,

  /* Unicode: U+25F6 (◶) [WHITE CIRCLE WITH LOWER RIGHT QUADRANT], Size: 8x8
   * Glyph (bounding: <x: 0, y: 0, width: 8, height: 8>):
   *   ..####..
   *   .##..##.
   *   ##....##
   *   ##.#####
   *   ##.##.##
   *   ##.##.##
   *   .######.
   *   ..####..
   * Encoded data: offset=2391, size=8
   */
  0x3c, 0x66, 0xc3, 0xdf, 0xdb, 0xdb, 0x7e, 0x3c,

  /* Unicode: U+25F7 (◷) [WHITE CIRCLE WITH UPPER RIGHT QUADRANT], Size: 8x8
   * Glyph (bounding: <x: 0, y: 0, width: 8, height: 8>):
   *   ..####..
   *   .######.
   *   ##.##.##
   *   ##.##.##
   *   ##.#####
   *   ##....##
   *   .##..##.
   *   ..####..
   * Encoded data: offset=2399, size=8
   */
  0x3c, 0x7e, 0xdb, 0xdb, 0xdf, 0xc3, 0x66, 0x3c,

  /* Unicode: U+25F8 (◸) [UPPER LEFT TRIANGLE], Size: 8x8
   * Glyph (bounding: <x: 0, y: 0, width: 8, height: 8>):
   *   ########
   *   ##...##.
   *   ##..##..
   *   ##.##...
   *   ####....
   *   ###.....
   *   ##......
   *   #.......
   * Encoded data: offset=2407, size=8
   */
  0xff, 0xc6, 0xcc, 0xd8, 0xf0, 0xe0, 0xc0, 0x80,

  /* Unicode: U+25F9 (◹) [UPPER RIGHT TRIANGLE], Size: 8x8
   * Glyph (bounding: <x: 0, y: 0, width: 8, height: 8>):
   *   ########
   *   .##...##
   *   ..##..##
   *   ...##.##
   *   ....####
   *   .....###
   *   ......##
   *   .......#
   * Encoded data: offset=2415, size=8
   */
  0xff, 0x63, 0x33, 0x1b, 0x0f, 0x07, 0x03, 0x01,

  /* Unicode: U+25FA (◺) [LOWER LEFT TRIANGLE], Size: 8x8
   * Glyph (bounding: <x: 0, y: 0, width: 8, height: 8>):
   *   .......#
   *   ......##
   *   .....###
   *   ....####
   *   ...##.##
   *   ..##..##
   *   .##...##
   *   ########
   * Encoded data: offset=2423, size=8
   */
  0x01, 0x03, 0x07, 0x0f, 0x1b, 0x33, 0x63, 0xff,

  /* Unicode: U+25FB (◻) [WHITE MEDIUM SQUARE], Size: 8x8
   * Glyph (bounding: <x: 1, y: 1, width: 5, height: 5>):
   *   ........
   *   .#####..
   *   .##.##..
   *   .##.##..
   *   .##.##..
   *   .#####..
   *   ........
   *   ........
   * Encoded data: offset=2431, size=4
   */
  0xfe, 0xf7, 0xbf, 0x80,

  /* Unicode: U+25FC (◼) [BLACK MEDIUM SQUARE], Size: 8x8
   * Glyph (bounding: <x: 1, y: 1, width: 5, height: 5>):
   *   ........
   *   .#####..
   *   .#####..
   *   .#####..
   *   .#####..
   *   .#####..
   *   ........
   *   ........
   * Encoded data: offset=2435, size=4
   */
  0xff, 0xff, 0xff, 0x80,

  /* Unicode: U+25FD (◽) [WHITE MEDIUM SMALL SQUARE], Size: 8x8
   * Glyph (bounding: <x: 2, y: 2, width: 4, height: 4>):
   *   ........
   *   ........
   *   ..####..
   *   ..#..#..
   *   ..#..#..
   *   ..####..
   *   ........
   *   ........
   * Encoded data: offset=2439, size=2
   */
  0xf9, 0x9f,

  /* Unicode: U+25FE (◾) [BLACK MEDIUM SMALL SQUARE], Size: 8x8
   * Glyph (bounding: <x: 2, y: 2, width: 4, height: 4>):
   *   ........
   *   ........
   *   ..####..
   *   ..####..
   *   ..####..
   *   ..####..
   *   ........
   *   ........
   * Encoded data: offset=2441, size=2
   */
  0xff, 0xff,

  /* Unicode: U+25FF (◿) [LOWER RIGHT TRIANGLE], Size: 8x8
   * Glyph (bounding: <x: 0, y: 0, width: 8, height: 8>):
   *   #.......
   *   ##......
   *   ###.....
   *   ####....
   *   ##.##...
   *   ##..##..
   *   ##...##.
   *   ########
   * Encoded data: offset=2443, size=8
   */
  0x80, 0xc0, 0xe0, 0xf0, 0xd8, 0xcc, 0xc6, 0xff,

  /* Unicode: U+2625 (☥) [ANKH], Size: 8x8
   * Glyph (bounding: <x: 0, y: 0, width: 7, height: 7>):
   *   .#####..
   *   ##...##.
   *   .##.##..
   *   ..###...
   *   #######.
   *   ..###...
   *   ..###...
   *   ........
   * Encoded data: offset=2451, size=7
   */
  0x7d, 0x8d, 0xb1, 0xcf, 0xe7, 0x0e, 0x00,

  /* Unicode: U+2628 (☨) [CROSS OF LORRAINE], Size: 8x8
   * Glyph (bounding: <x: 1, y: 0, width: 6, height: 7>):
   *   ...##...
   *   ..####..
   *   ...##...
   *   .######.
   *   ...##...
   *   ...##...
   *   ...##...
   *   ........
   * Encoded data: offset=2458, size=6
   */
  0x31, 0xe3, 0x3f, 0x30, 0xc3, 0x00,

  /* Unicode: U+262F (☯) [YIN YANG], Size: 8x8
   * Glyph (bounding: <x: 0, y: 1, width: 8, height: 6>):
   *   ........
   *   .######.
   *   #......#
   *   #.##...#
   *   ####..##
   *   ########
   *   .######.
   *   ........
   * Encoded data: offset=2464, size=6
   */
  0x7e, 0x81, 0xb1, 0xf3, 0xff, 0x7e,

  /* Unicode: U+2630 (☰) [TRIGRAM FOR HEAVEN], Size: 8x8
   * Glyph (bounding: <x: 0, y: 2, width: 7, height: 5>):
   *   ........
   *   ........
   *   #######.
   *   ........
   *   #######.
   *   ........
   *   #######.
   *   ........
   * Encoded data: offset=2470, size=5
   */
  0xfe, 0x03, 0xf8, 0x0f, 0xe0,

  /* Unicode: U+2631 (☱) [TRIGRAM FOR LAKE], Size: 8x8
   * Glyph (bounding: <x: 0, y: 2, width: 7, height: 5>):
   *   ........
   *   ........
   *   ##...##.
   *   ........
   *   #######.
   *   ........
   *   #######.
   *   ........
   * Encoded data: offset=2475, size=5
   */
  0xc6, 0x03, 0xf8, 0x0f, 0xe0,

  /* Unicode: U+2632 (☲) [TRIGRAM FOR FIRE], Size: 8x8
   * Glyph (bounding: <x: 0, y: 2, width: 7, height: 5>):
   *   ........
   *   ........
   *   #######.
   *   ........
   *   ##...##.
   *   ........
   *   #######.
   *   ........
   * Encoded data: offset=2480, size=5
   */
  0xfe, 0x03, 0x18, 0x0f, 0xe0,

  /* Unicode: U+2633 (☳) [TRIGRAM FOR THUNDER], Size: 8x8
   * Glyph (bounding: <x: 0, y: 2, width: 7, height: 5>):
   *   ........
   *   ........
   *   ##...##.
   *   ........
   *   ##...##.
   *   ........
   *   #######.
   *   ........
   * Encoded data: offset=2485, size=5
   */
  0xc6, 0x03, 0x18, 0x0f, 0xe0,

  /* Unicode: U+2634 (☴) [TRIGRAM FOR WIND], Size: 8x8
   * Glyph (bounding: <x: 0, y: 2, width: 7, height: 5>):
   *   ........
   *   ........
   *   #######.
   *   ........
   *   #######.
   *   ........
   *   ##...##.
   *   ........
   * Encoded data: offset=2490, size=5
   */
  0xfe, 0x03, 0xf8, 0x0c, 0x60,

  /* Unicode: U+2635 (☵) [TRIGRAM FOR WATER], Size: 8x8
   * Glyph (bounding: <x: 0, y: 2, width: 7, height: 5>):
   *   ........
   *   ........
   *   ##...##.
   *   ........
   *   #######.
   *   ........
   *   ##...##.
   *   ........
   * Encoded data: offset=2495, size=5
   */
  0xc6, 0x03, 0xf8, 0x0c, 0x60,

  /* Unicode: U+2636 (☶) [TRIGRAM FOR MOUNTAIN], Size: 8x8
   * Glyph (bounding: <x: 0, y: 2, width: 7, height: 5>):
   *   ........
   *   ........
   *   #######.
   *   ........
   *   ##...##.
   *   ........
   *   ##...##.
   *   ........
   * Encoded data: offset=2500, size=5
   */
  0xfe, 0x03, 0x18, 0x0c, 0x60,

  /* Unicode: U+2637 (☷) [TRIGRAM FOR EARTH], Size: 8x8
   * Glyph (bounding: <x: 0, y: 2, width: 7, height: 5>):
   *   ........
   *   ........
   *   ##...##.
   *   ........
   *   ##...##.
   *   ........
   *   ##...##.
   *   ........
   * Encoded data: offset=2505, size=5
   */
  0xc6, 0x03, 0x18, 0x0c, 0x60,

  /* Unicode: U+2639 (☹) [WHITE FROWNING FACE], Size: 8x8
   * Glyph (bounding: <x: 0, y: 0, width: 8, height: 8>):
   *   .######.
   *   #......#
   *   #.#..#.#
   *   #......#
   *   #..##..#
   *   #.####.#
   *   #......#
   *   .######.
   * Encoded data: offset=2510, size=8
   */
  0x7e, 0x81, 0xa5, 0x81, 0x99, 0xbd, 0x81, 0x7e,

  /* Unicode: U+263A (☺) [WHITE SMILING FACE], Size: 8x8
   * Glyph (bounding: <x: 0, y: 0, width: 8, height: 8>):
   *   .######.
   *   #......#
   *   #.#..#.#
   *   #......#
   *   #.####.#
   *   #..##..#
   *   #......#
   *   .######.
   * Encoded data: offset=2518, size=8
   */
  0x7e, 0x81, 0xa5, 0x81, 0xbd, 0x99, 0x81, 0x7e,

  /* Unicode: U+263B (☻) [BLACK SMILING FACE], Size: 8x8
   * Glyph (bounding: <x: 0, y: 0, width: 8, height: 8>):
   *   .######.
   *   ########
   *   ##.##.##
   *   ########
   *   ##....##
   *   ###..###
   *   ########
   *   .######.
   * Encoded data: offset=2526, size=8
   */
  0x7e, 0xff, 0xdb, 0xff, 0xc3, 0xe7, 0xff, 0x7e,

  /* Unicode: U+2660 (♠) [BLACK SPADE SUIT], Size: 8x8
   * Glyph (bounding: <x: 0, y: 0, width: 7, height: 7>):
   *   ...#....
   *   ..###...
   *   .#####..
   *   #######.
   *   #######.
   *   ..###...
   *   .#####..
   *   ........
   * Encoded data: offset=2534, size=7
   */
  0x10, 0x71, 0xf7, 0xff, 0xe7, 0x1f, 0x00,

  /* Unicode: U+2661 (♡) [WHITE HEART SUIT], Size: 8x8
   * Glyph (bounding: <x: 0, y: 0, width: 7, height: 7>):
   *   .##.##..
   *   #######.
   *   ##...##.
   *   ##...##.
   *   .##.##..
   *   ..###...
   *   ...#....
   *   ........
   * Encoded data: offset=2541, size=7
   */
  0x6d, 0xff, 0x1e, 0x36, 0xc7, 0x04, 0x00,

  /* Unicode: U+2662 (♢) [WHITE DIAMOND SUIT], Size: 8x8
   * Glyph (bounding: <x: 0, y: 0, width: 7, height: 7>):
   *   ...#....
   *   ..###...
   *   .##.##..
   *   ##...##.
   *   .##.##..
   *   ..###...
   *   ...#....
   *   ........
   * Encoded data: offset=2548, size=7
   */
  0x10, 0x71, 0xb6, 0x36, 0xc7, 0x04, 0x00,

  /* Unicode: U+2663 (♣) [BLACK CLUB SUIT], Size: 8x8
   * Glyph (bounding: <x: 0, y: 0, width: 7, height: 7>):
   *   ...#....
   *   ..###...
   *   .#.#.#..
   *   #######.
   *   .#.#.#..
   *   ...#....
   *   ..###...
   *   ........
   * Encoded data: offset=2555, size=7
   */
  0x10, 0x71, 0x57, 0xf5, 0x42, 0x0e, 0x00,

  /* Unicode: U+2664 (♤) [WHITE SPADE SUIT], Size: 8x8
   * Glyph (bounding: <x: 0, y: 0, width: 7, height: 7>):
   *   ...#....
   *   ..###...
   *   .##.##..
   *   ##...##.
   *   ###.###.
   *   ..###...
   *   .#####..
   *   ........
   * Encoded data: offset=2562, size=7
   */
  0x10, 0x71, 0xb6, 0x3e, 0xe7, 0x1f, 0x00,

  /* Unicode: U+2665 (♥) [BLACK HEART SUIT], Size: 8x8
   * Glyph (bounding: <x: 0, y: 0, width: 7, height: 7>):
   *   .##.##..
   *   #######.
   *   #######.
   *   #######.
   *   .#####..
   *   ..###...
   *   ...#....
   *   ........
   * Encoded data: offset=2569, size=7
   */
  0x6d, 0xff, 0xff, 0xf7, 0xc7, 0x04, 0x00,

  /* Unicode: U+2666 (♦) [BLACK DIAMOND SUIT], Size: 8x8
   * Glyph (bounding: <x: 0, y: 0, width: 7, height: 7>):
   *   ...#....
   *   ..###...
   *   .#####..
   *   #######.
   *   .#####..
   *   ..###...
   *   ...#....
   *   ........
   * Encoded data: offset=2576, size=7
   */
  0x10, 0x71, 0xf7, 0xf7, 0xc7, 0x04, 0x00,

  /* Unicode: U+2667 (♧) [WHITE CLUB SUIT], Size: 8x8
   * Glyph (bounding: <x: 0, y: 0, width: 7, height: 7>):
   *   ..###...
   *   ..###...
   *   ##...##.
   *   ##...##.
   *   ###.###.
   *   ..#.#...
   *   .#####..
   *   ........
   * Encoded data: offset=2583, size=7
   */
  0x38, 0x73, 0x1e, 0x3e, 0xe5, 0x1f, 0x00,

  /* Unicode: U+2669 (♩) [QUARTER NOTE], Size: 8x8
   * Glyph (bounding: <x: 1, y: 0, width: 5, height: 8>):
   *   ....##..
   *   ....##..
   *   ....##..
   *   ....##..
   *   ....##..
   *   ..####..
   *   .#####..
   *   ..###...
   * Encoded data: offset=2590, size=5
   */
  0x18, 0xc6, 0x31, 0xbf, 0xee,

  /* Unicode: U+266A (♪) [EIGHTH NOTE], Size: 8x8
   * Glyph (bounding: <x: 0, y: 0, width: 8, height: 8>):
   *   ...##...
   *   ...###..
   *   ...####.
   *   ...##.##
   *   ...##...
   *   .####...
   *   #####...
   *   .###....
   * Encoded data: offset=2595, size=8
   */
  0x18, 0x1c, 0x1e, 0x1b, 0x18, 0x78, 0xf8, 0x70,

  /* Unicode: U+266B (♫) [BEAMED EIGHTH NOTES], Size: 8x8
   * Glyph (bounding: <x: 0, y: 0, width: 8, height: 8>):
   *   .#######
   *   .##...##
   *   .##...##
   *   .##...##
   *   .##...##
   *   .##..###
   *   ###..##.
   *   ##......
   * Encoded data: offset=2603, size=8
   */
  0x7f, 0x63, 0x63, 0x63, 0x63, 0x67, 0xe6, 0xc0,

  /* Unicode: U+266C (♬) [BEAMED SIXTEENTH NOTES], Size: 8x8
   * Glyph (bounding: <x: 0, y: 0, width: 8, height: 8>):
   *   .#######
   *   .##...##
   *   .#######
   *   .##...##
   *   .##...##
   *   .##..###
   *   ###..##.
   *   ##......
   * Encoded data: offset=2611, size=8
   */
  0x7f, 0x63, 0x7f, 0x63, 0x63, 0x67, 0xe6, 0xc0,

  /* Unicode: U+268A (⚊) [MONOGRAM FOR YANG], Size: 8x8
   * Glyph (bounding: <x: 0, y: 6, width: 7, height: 1>):
   *   ........
   *   ........
   *   ........
   *   ........
   *   ........
   *   ........
   *   #######.
   *   ........
   * Encoded data: offset=2619, size=1
   */
  0xfe,

  /* Unicode: U+268B (⚋) [MONOGRAM FOR YIN], Size: 8x8
   * Glyph (bounding: <x: 0, y: 6, width: 7, height: 1>):
   *   ........
   *   ........
   *   ........
   *   ........
   *   ........
   *   ........
   *   ##...##.
   *   ........
   * Encoded data: offset=2620, size=1
   */
  0xc6,

  /* Unicode: U+268C (⚌) [DIGRAM FOR GREATER YANG], Size: 8x8
   * Glyph (bounding: <x: 0, y: 4, width: 7, height: 3>):
   *   ........
   *   ........
   *   ........
   *   ........
   *   #######.
   *   ........
   *   #######.
   *   ........
   * Encoded data: offset=2621, size=3
   */
  0xfe, 0x03, 0xf8,

  /* Unicode: U+268D (⚍) [DIGRAM FOR LESSER YIN], Size: 8x8
   * Glyph (bounding: <x: 0, y: 4, width: 7, height: 3>):
   *   ........
   *   ........
   *   ........
   *   ........
   *   ##...##.
   *   ........
   *   #######.
   *   ........
   * Encoded data: offset=2624, size=3
   */
  0xc6, 0x03, 0xf8,

  /* Unicode: U+268E (⚎) [DIGRAM FOR LESSER YANG], Size: 8x8
   * Glyph (bounding: <x: 0, y: 4, width: 7, height: 3>):
   *   ........
   *   ........
   *   ........
   *   ........
   *   #######.
   *   ........
   *   ##...##.
   *   ........
   * Encoded data: offset=2627, size=3
   */
  0xfe, 0x03, 0x18,

  /* Unicode: U+268F (⚏) [DIGRAM FOR GREATER YIN], Size: 8x8
   * Glyph (bounding: <x: 0, y: 4, width: 7, height: 3>):
   *   ........
   *   ........
   *   ........
   *   ........
   *   ##...##.
   *   ........
   *   ##...##.
   *   ........
   * Encoded data: offset=2630, size=3
   */
  0xc6, 0x03, 0x18,

  /* Unicode: U+26AA (⚪) [MEDIUM WHITE CIRCLE], Size: 8x8
   * Glyph (bounding: <x: 1, y: 2, width: 6, height: 5>):
   *   ........
   *   ........
   *   ..####..
   *   .#....#.
   *   .#....#.
   *   .#....#.
   *   ..####..
   *   ........
   * Encoded data: offset=2633, size=4
   */
  0x7a, 0x18, 0x61, 0x78,

  /* Unicode: U+26AB (⚫) [MEDIUM BLACK CIRCLE], Size: 8x8
   * Glyph (bounding: <x: 1, y: 2, width: 6, height: 5>):
   *   ........
   *   ........
   *   ..####..
   *   .######.
   *   .######.
   *   .######.
   *   ..####..
   *   ........
   * Encoded data: offset=2637, size=4
   */
  0x7b, 0xff, 0xff, 0x78,

  /* Unicode: U+26AC (⚬) [MEDIUM SMALL WHITE CIRCLE], Size: 8x8
   * Glyph (bounding: <x: 1, y: 1, width: 5, height: 5>):
   *   ........
   *   ..###...
   *   .#####..
   *   .##.##..
   *   .#####..
   *   ..###...
   *   ........
   *   ........
   * Encoded data: offset=2641, size=4
   */
  0x77, 0xf7, 0xf7, 0x00,

  /* Unicode: U+2708 (✈) [AIRPLANE], Size: 8x8
   * Glyph (bounding: <x: 0, y: 0, width: 8, height: 8>):
   *   ..##....
   *   ..###...
   *   #..###..
   *   ########
   *   ########
   *   #..###..
   *   ..###...
   *   ..##....
   * Encoded data: offset=2645, size=8
   */
  0x30, 0x38, 0x9c, 0xff, 0xff, 0x9c, 0x38, 0x30,

  /* Unicode: U+2734 (✴) [EIGHT POINTED BLACK STAR], Size: 8x8
   * Glyph (bounding: <x: 0, y: 0, width: 7, height: 7>):
   *   #..#..#.
   *   .#.#.#..
   *   ..###...
   *   #######.
   *   ..###...
   *   .#.#.#..
   *   #..#..#.
   *   ........
   * Encoded data: offset=2653, size=7
   */
  0x92, 0xa8, 0xe7, 0xf3, 0x8a, 0xa4, 0x80,

};

/* Store the glyph descriptions
 */
static const lv_font_fmt_txt_glyph_dsc_t glyph_dsc[] = {
  /* Reserved */
  { 0 },
  /* Unicode: U+0020 ( ) [SPACE] */
  { .bitmap_index=0, .adv_w=128, .box_h=0, .box_w=0, .ofs_x=0, .ofs_y=8 },
  /* Unicode: U+0021 (!) [EXCLAMATION MARK] */
  { .bitmap_index=0, .adv_w=128, .box_h=7, .box_w=2, .ofs_x=3, .ofs_y=1 },
  /* Unicode: U+0022 (") [QUOTATION MARK] */
  { .bitmap_index=2, .adv_w=128, .box_h=3, .box_w=6, .ofs_x=1, .ofs_y=5 },
  /* Unicode: U+0023 (#) [NUMBER SIGN] */
  { .bitmap_index=5, .adv_w=128, .box_h=7, .box_w=7, .ofs_x=0, .ofs_y=1 },
  /* Unicode: U+0024 ($) [DOLLAR SIGN] */
  { .bitmap_index=12, .adv_w=128, .box_h=7, .box_w=6, .ofs_x=1, .ofs_y=1 },
  /* Unicode: U+0025 (%) [PERCENT SIGN] */
  { .bitmap_index=18, .adv_w=128, .box_h=6, .box_w=7, .ofs_x=0, .ofs_y=1 },
  /* Unicode: U+0026 (&) [AMPERSAND] */
  { .bitmap_index=24, .adv_w=128, .box_h=7, .box_w=7, .ofs_x=0, .ofs_y=1 },
  /* Unicode: U+0027 (') [APOSTROPHE] */
  { .bitmap_index=31, .adv_w=128, .box_h=3, .box_w=3, .ofs_x=2, .ofs_y=5 },
  /* Unicode: U+0028 (() [LEFT PARENTHESIS] */
  { .bitmap_index=33, .adv_w=128, .box_h=7, .box_w=4, .ofs_x=2, .ofs_y=1 },
  /* Unicode: U+0029 ()) [RIGHT PARENTHESIS] */
  { .bitmap_index=37, .adv_w=128, .box_h=7, .box_w=4, .ofs_x=2, .ofs_y=1 },
  /* Unicode: U+002A (*) [ASTERISK] */
  { .bitmap_index=41, .adv_w=128, .box_h=5, .box_w=8, .ofs_x=0, .ofs_y=2 },
  /* Unicode: U+002B (+) [PLUS SIGN] */
  { .bitmap_index=46, .adv_w=128, .box_h=5, .box_w=6, .ofs_x=1, .ofs_y=2 },
  /* Unicode: U+002C (,) [COMMA] */
  { .bitmap_index=50, .adv_w=128, .box_h=3, .box_w=3, .ofs_x=2, .ofs_y=0 },
  /* Unicode: U+002D (-) [HYPHEN-MINUS] */
  { .bitmap_index=52, .adv_w=128, .box_h=1, .box_w=6, .ofs_x=1, .ofs_y=4 },
  /* Unicode: U+002E (.) [FULL STOP] */
  { .bitmap_index=53, .adv_w=128, .box_h=2, .box_w=2, .ofs_x=3, .ofs_y=1 },
  /* Unicode: U+002F (/) [SOLIDUS] */
  { .bitmap_index=54, .adv_w=128, .box_h=7, .box_w=8, .ofs_x=0, .ofs_y=1 },
  /* Unicode: U+0030 (0) [DIGIT ZERO] */
  { .bitmap_index=61, .adv_w=128, .box_h=7, .box_w=6, .ofs_x=1, .ofs_y=1 },
  /* Unicode: U+0031 (1) [DIGIT ONE] */
  { .bitmap_index=67, .adv_w=128, .box_h=7, .box_w=6, .ofs_x=1, .ofs_y=1 },
  /* Unicode: U+0032 (2) [DIGIT TWO] */
  { .bitmap_index=73, .adv_w=128, .box_h=7, .box_w=6, .ofs_x=1, .ofs_y=1 },
  /* Unicode: U+0033 (3) [DIGIT THREE] */
  { .bitmap_index=79, .adv_w=128, .box_h=7, .box_w=6, .ofs_x=1, .ofs_y=1 },
  /* Unicode: U+0034 (4) [DIGIT FOUR] */
  { .bitmap_index=85, .adv_w=128, .box_h=7, .box_w=7, .ofs_x=0, .ofs_y=1 },
  /* Unicode: U+0035 (5) [DIGIT FIVE] */
  { .bitmap_index=92, .adv_w=128, .box_h=7, .box_w=6, .ofs_x=1, .ofs_y=1 },
  /* Unicode: U+0036 (6) [DIGIT SIX] */
  { .bitmap_index=98, .adv_w=128, .box_h=7, .box_w=6, .ofs_x=1, .ofs_y=1 },
  /* Unicode: U+0037 (7) [DIGIT SEVEN] */
  { .bitmap_index=104, .adv_w=128, .box_h=7, .box_w=6, .ofs_x=1, .ofs_y=1 },
  /* Unicode: U+0038 (8) [DIGIT EIGHT] */
  { .bitmap_index=110, .adv_w=128, .box_h=7, .box_w=6, .ofs_x=1, .ofs_y=1 },
  /* Unicode: U+0039 (9) [DIGIT NINE] */
  { .bitmap_index=116, .adv_w=128, .box_h=7, .box_w=6, .ofs_x=1, .ofs_y=1 },
  /* Unicode: U+003A (:) [COLON] */
  { .bitmap_index=122, .adv_w=128, .box_h=6, .box_w=2, .ofs_x=3, .ofs_y=1 },
  /* Unicode: U+003B (;) [SEMICOLON] */
  { .bitmap_index=124, .adv_w=128, .box_h=7, .box_w=3, .ofs_x=2, .ofs_y=0 },
  /* Unicode: U+003C (<) [LESS-THAN SIGN] */
  { .bitmap_index=127, .adv_w=128, .box_h=7, .box_w=5, .ofs_x=1, .ofs_y=1 },
  /* Unicode: U+003D (=) [EQUALS SIGN] */
  { .bitmap_index=132, .adv_w=128, .box_h=3, .box_w=6, .ofs_x=1, .ofs_y=3 },
  /* Unicode: U+003E (>) [GREATER-THAN SIGN] */
  { .bitmap_index=135, .adv_w=128, .box_h=7, .box_w=5, .ofs_x=1, .ofs_y=1 },
  /* Unicode: U+003F (?) [QUESTION MARK] */
  { .bitmap_index=140, .adv_w=128, .box_h=7, .box_w=6, .ofs_x=1, .ofs_y=1 },
  /* Unicode: U+0040 (@) [COMMERCIAL AT] */
  { .bitmap_index=146, .adv_w=128, .box_h=7, .box_w=7, .ofs_x=0, .ofs_y=1 },
  /* Unicode: U+0041 (A) [LATIN CAPITAL LETTER A] */
  { .bitmap_index=153, .adv_w=128, .box_h=7, .box_w=6, .ofs_x=1, .ofs_y=1 },
  /* Unicode: U+0042 (B) [LATIN CAPITAL LETTER B] */
  { .bitmap_index=159, .adv_w=128, .box_h=7, .box_w=6, .ofs_x=1, .ofs_y=1 },
  /* Unicode: U+0043 (C) [LATIN CAPITAL LETTER C] */
  { .bitmap_index=165, .adv_w=128, .box_h=7, .box_w=6, .ofs_x=1, .ofs_y=1 },
  /* Unicode: U+0044 (D) [LATIN CAPITAL LETTER D] */
  { .bitmap_index=171, .adv_w=128, .box_h=7, .box_w=6, .ofs_x=1, .ofs_y=1 },
  /* Unicode: U+0045 (E) [LATIN CAPITAL LETTER E] */
  { .bitmap_index=177, .adv_w=128, .box_h=7, .box_w=6, .ofs_x=1, .ofs_y=1 },
  /* Unicode: U+0046 (F) [LATIN CAPITAL LETTER F] */
  { .bitmap_index=183, .adv_w=128, .box_h=7, .box_w=6, .ofs_x=1, .ofs_y=1 },
  /* Unicode: U+0047 (G) [LATIN CAPITAL LETTER G] */
  { .bitmap_index=189, .adv_w=128, .box_h=7, .box_w=6, .ofs_x=1, .ofs_y=1 },
  /* Unicode: U+0048 (H) [LATIN CAPITAL LETTER H] */
  { .bitmap_index=195, .adv_w=128, .box_h=7, .box_w=6, .ofs_x=1, .ofs_y=1 },
  /* Unicode: U+0049 (I) [LATIN CAPITAL LETTER I] */
  { .bitmap_index=201, .adv_w=128, .box_h=7, .box_w=6, .ofs_x=1, .ofs_y=1 },
  /* Unicode: U+004A (J) [LATIN CAPITAL LETTER J] */
  { .bitmap_index=207, .adv_w=128, .box_h=7, .box_w=6, .ofs_x=1, .ofs_y=1 },
  /* Unicode: U+004B (K) [LATIN CAPITAL LETTER K] */
  { .bitmap_index=213, .adv_w=128, .box_h=7, .box_w=7, .ofs_x=0, .ofs_y=1 },
  /* Unicode: U+004C (L) [LATIN CAPITAL LETTER L] */
  { .bitmap_index=220, .adv_w=128, .box_h=7, .box_w=6, .ofs_x=1, .ofs_y=1 },
  /* Unicode: U+004D (M) [LATIN CAPITAL LETTER M] */
  { .bitmap_index=226, .adv_w=128, .box_h=7, .box_w=7, .ofs_x=0, .ofs_y=1 },
  /* Unicode: U+004E (N) [LATIN CAPITAL LETTER N] */
  { .bitmap_index=233, .adv_w=128, .box_h=7, .box_w=7, .ofs_x=0, .ofs_y=1 },
  /* Unicode: U+004F (O) [LATIN CAPITAL LETTER O] */
  { .bitmap_index=240, .adv_w=128, .box_h=7, .box_w=6, .ofs_x=1, .ofs_y=1 },
  /* Unicode: U+0050 (P) [LATIN CAPITAL LETTER P] */
  { .bitmap_index=246, .adv_w=128, .box_h=7, .box_w=6, .ofs_x=1, .ofs_y=1 },
  /* Unicode: U+0051 (Q) [LATIN CAPITAL LETTER Q] */
  { .bitmap_index=252, .adv_w=128, .box_h=7, .box_w=6, .ofs_x=1, .ofs_y=1 },
  /* Unicode: U+0052 (R) [LATIN CAPITAL LETTER R] */
  { .bitmap_index=258, .adv_w=128, .box_h=7, .box_w=6, .ofs_x=1, .ofs_y=1 },
  /* Unicode: U+0053 (S) [LATIN CAPITAL LETTER S] */
  { .bitmap_index=264, .adv_w=128, .box_h=7, .box_w=6, .ofs_x=1, .ofs_y=1 },
  /* Unicode: U+0054 (T) [LATIN CAPITAL LETTER T] */
  { .bitmap_index=270, .adv_w=128, .box_h=7, .box_w=6, .ofs_x=1, .ofs_y=1 },
  /* Unicode: U+0055 (U) [LATIN CAPITAL LETTER U] */
  { .bitmap_index=276, .adv_w=128, .box_h=7, .box_w=6, .ofs_x=1, .ofs_y=1 },
  /* Unicode: U+0056 (V) [LATIN CAPITAL LETTER V] */
  { .bitmap_index=282, .adv_w=128, .box_h=7, .box_w=6, .ofs_x=1, .ofs_y=1 },
  /* Unicode: U+0057 (W) [LATIN CAPITAL LETTER W] */
  { .bitmap_index=288, .adv_w=128, .box_h=7, .box_w=7, .ofs_x=0, .ofs_y=1 },
  /* Unicode: U+0058 (X) [LATIN CAPITAL LETTER X] */
  { .bitmap_index=295, .adv_w=128, .box_h=7, .box_w=8, .ofs_x=0, .ofs_y=1 },
  /* Unicode: U+0059 (Y) [LATIN CAPITAL LETTER Y] */
  { .bitmap_index=302, .adv_w=128, .box_h=7, .box_w=8, .ofs_x=0, .ofs_y=1 },
  /* Unicode: U+005A (Z) [LATIN CAPITAL LETTER Z] */
  { .bitmap_index=309, .adv_w=128, .box_h=7, .box_w=6, .ofs_x=1, .ofs_y=1 },
  /* Unicode: U+005B ([) [LEFT SQUARE BRACKET] */
  { .bitmap_index=315, .adv_w=128, .box_h=7, .box_w=4, .ofs_x=2, .ofs_y=1 },
  /* Unicode: U+005C (\) [REVERSE SOLIDUS] */
  { .bitmap_index=319, .adv_w=128, .box_h=7, .box_w=8, .ofs_x=0, .ofs_y=1 },
  /* Unicode: U+005D (]) [RIGHT SQUARE BRACKET] */
  { .bitmap_index=326, .adv_w=128, .box_h=7, .box_w=4, .ofs_x=2, .ofs_y=1 },
  /* Unicode: U+005E (^) [CIRCUMFLEX ACCENT] */
  { .bitmap_index=330, .adv_w=128, .box_h=4, .box_w=7, .ofs_x=0, .ofs_y=4 },
  /* Unicode: U+005F (_) [LOW LINE] */
  { .bitmap_index=334, .adv_w=128, .box_h=1, .box_w=8, .ofs_x=0, .ofs_y=0 },
  /* Unicode: U+0060 (`) [GRAVE ACCENT] */
  { .bitmap_index=335, .adv_w=128, .box_h=3, .box_w=4, .ofs_x=3, .ofs_y=5 },
  /* Unicode: U+0061 (a) [LATIN SMALL LETTER A] */
  { .bitmap_index=337, .adv_w=128, .box_h=5, .box_w=6, .ofs_x=1, .ofs_y=1 },
  /* Unicode: U+0062 (b) [LATIN SMALL LETTER B] */
  { .bitmap_index=341, .adv_w=128, .box_h=7, .box_w=6, .ofs_x=1, .ofs_y=1 },
  /* Unicode: U+0063 (c) [LATIN SMALL LETTER C] */
  { .bitmap_index=347, .adv_w=128, .box_h=5, .box_w=5, .ofs_x=1, .ofs_y=1 },
  /* Unicode: U+0064 (d) [LATIN SMALL LETTER D] */
  { .bitmap_index=351, .adv_w=128, .box_h=7, .box_w=6, .ofs_x=1, .ofs_y=1 },
  /* Unicode: U+0065 (e) [LATIN SMALL LETTER E] */
  { .bitmap_index=357, .adv_w=128, .box_h=5, .box_w=6, .ofs_x=1, .ofs_y=1 },
  /* Unicode: U+0066 (f) [LATIN SMALL LETTER F] */
  { .bitmap_index=361, .adv_w=128, .box_h=7, .box_w=5, .ofs_x=1, .ofs_y=1 },
  /* Unicode: U+0067 (g) [LATIN SMALL LETTER G] */
  { .bitmap_index=366, .adv_w=128, .box_h=6, .box_w=6, .ofs_x=1, .ofs_y=0 },
  /* Unicode: U+0068 (h) [LATIN SMALL LETTER H] */
  { .bitmap_index=371, .adv_w=128, .box_h=7, .box_w=6, .ofs_x=1, .ofs_y=1 },
  /* Unicode: U+0069 (i) [LATIN SMALL LETTER I] */
  { .bitmap_index=377, .adv_w=128, .box_h=7, .box_w=5, .ofs_x=2, .ofs_y=1 },
  /* Unicode: U+006A (j) [LATIN SMALL LETTER J] */
  { .bitmap_index=382, .adv_w=128, .box_h=8, .box_w=5, .ofs_x=1, .ofs_y=0 },
  /* Unicode: U+006B (k) [LATIN SMALL LETTER K] */
  { .bitmap_index=387, .adv_w=128, .box_h=7, .box_w=6, .ofs_x=1, .ofs_y=1 },
  /* Unicode: U+006C (l) [LATIN SMALL LETTER L] */
  { .bitmap_index=393, .adv_w=128, .box_h=7, .box_w=5, .ofs_x=2, .ofs_y=1 },
  /* Unicode: U+006D (m) [LATIN SMALL LETTER M] */
  { .bitmap_index=398, .adv_w=128, .box_h=5, .box_w=7, .ofs_x=0, .ofs_y=1 },
  /* Unicode: U+006E (n) [LATIN SMALL LETTER N] */
  { .bitmap_index=403, .adv_w=128, .box_h=5, .box_w=6, .ofs_x=1, .ofs_y=1 },
  /* Unicode: U+006F (o) [LATIN SMALL LETTER O] */
  { .bitmap_index=407, .adv_w=128, .box_h=5, .box_w=6, .ofs_x=1, .ofs_y=1 },
  /* Unicode: U+0070 (p) [LATIN SMALL LETTER P] */
  { .bitmap_index=411, .adv_w=128, .box_h=6, .box_w=6, .ofs_x=1, .ofs_y=0 },
  /* Unicode: U+0071 (q) [LATIN SMALL LETTER Q] */
  { .bitmap_index=416, .adv_w=128, .box_h=6, .box_w=6, .ofs_x=1, .ofs_y=0 },
  /* Unicode: U+0072 (r) [LATIN SMALL LETTER R] */
  { .bitmap_index=421, .adv_w=128, .box_h=5, .box_w=6, .ofs_x=1, .ofs_y=1 },
  /* Unicode: U+0073 (s) [LATIN SMALL LETTER S] */
  { .bitmap_index=425, .adv_w=128, .box_h=5, .box_w=6, .ofs_x=1, .ofs_y=1 },
  /* Unicode: U+0074 (t) [LATIN SMALL LETTER T] */
  { .bitmap_index=429, .adv_w=128, .box_h=7, .box_w=6, .ofs_x=1, .ofs_y=1 },
  /* Unicode: U+0075 (u) [LATIN SMALL LETTER U] */
  { .bitmap_index=435, .adv_w=128, .box_h=5, .box_w=6, .ofs_x=1, .ofs_y=1 },
  /* Unicode: U+0076 (v) [LATIN SMALL LETTER V] */
  { .bitmap_index=439, .adv_w=128, .box_h=5, .box_w=6, .ofs_x=1, .ofs_y=1 },
  /* Unicode: U+0077 (w) [LATIN SMALL LETTER W] */
  { .bitmap_index=443, .adv_w=128, .box_h=5, .box_w=7, .ofs_x=0, .ofs_y=1 },
  /* Unicode: U+0078 (x) [LATIN SMALL LETTER X] */
  { .bitmap_index=448, .adv_w=128, .box_h=5, .box_w=7, .ofs_x=0, .ofs_y=1 },
  /* Unicode: U+0079 (y) [LATIN SMALL LETTER Y] */
  { .bitmap_index=453, .adv_w=128, .box_h=6, .box_w=6, .ofs_x=1, .ofs_y=0 },
  /* Unicode: U+007A (z) [LATIN SMALL LETTER Z] */
  { .bitmap_index=458, .adv_w=128, .box_h=5, .box_w=6, .ofs_x=1, .ofs_y=1 },
  /* Unicode: U+007B ({) [LEFT CURLY BRACKET] */
  { .bitmap_index=462, .adv_w=128, .box_h=7, .box_w=6, .ofs_x=1, .ofs_y=1 },
  /* Unicode: U+007C (|) [VERTICAL LINE] */
  { .bitmap_index=468, .adv_w=128, .box_h=7, .box_w=2, .ofs_x=3, .ofs_y=1 },
  /* Unicode: U+007D (}) [RIGHT CURLY BRACKET] */
  { .bitmap_index=470, .adv_w=128, .box_h=7, .box_w=6, .ofs_x=1, .ofs_y=1 },
  /* Unicode: U+007E (~) [TILDE] */
  { .bitmap_index=476, .adv_w=128, .box_h=2, .box_w=7, .ofs_x=0, .ofs_y=6 },
  /* Unicode: U+00A0 ( ) [NO-BREAK SPACE] */
  { .bitmap_index=478, .adv_w=128, .box_h=0, .box_w=0, .ofs_x=0, .ofs_y=8 },
  /* Unicode: U+00A1 (¡) [INVERTED EXCLAMATION MARK] */
  { .bitmap_index=478, .adv_w=128, .box_h=7, .box_w=2, .ofs_x=3, .ofs_y=1 },
  /* Unicode: U+00A2 (¢) [CENT SIGN] */
  { .bitmap_index=480, .adv_w=128, .box_h=6, .box_w=7, .ofs_x=0, .ofs_y=1 },
  /* Unicode: U+00A3 (£) [POUND SIGN] */
  { .bitmap_index=486, .adv_w=128, .box_h=7, .box_w=7, .ofs_x=0, .ofs_y=1 },
  /* Unicode: U+00A4 (¤) [CURRENCY SIGN] */
  { .bitmap_index=493, .adv_w=128, .box_h=5, .box_w=6, .ofs_x=1, .ofs_y=3 },
  /* Unicode: U+00A5 (¥) [YEN SIGN] */
  { .bitmap_index=497, .adv_w=128, .box_h=7, .box_w=8, .ofs_x=0, .ofs_y=1 },
  /* Unicode: U+00A6 (¦) [BROKEN BAR] */
  { .bitmap_index=504, .adv_w=128, .box_h=7, .box_w=2, .ofs_x=3, .ofs_y=1 },
  /* Unicode: U+00A7 (§) [SECTION SIGN] */
  { .bitmap_index=506, .adv_w=128, .box_h=8, .box_w=6, .ofs_x=1, .ofs_y=0 },
  /* Unicode: U+00A8 (¨) [DIAERESIS] */
  { .bitmap_index=512, .adv_w=128, .box_h=1, .box_w=6, .ofs_x=1, .ofs_y=7 },
  /* Unicode: U+00A9 (©) [COPYRIGHT SIGN] */
  { .bitmap_index=513, .adv_w=128, .box_h=8, .box_w=8, .ofs_x=0, .ofs_y=0 },
  /* Unicode: U+00AA (ª) [FEMININE ORDINAL INDICATOR] */
  { .bitmap_index=521, .adv_w=128, .box_h=6, .box_w=6, .ofs_x=1, .ofs_y=2 },
  /* Unicode: U+00AB («) [LEFT-POINTING DOUBLE ANGLE QUOTATION MARK] */
  { .bitmap_index=526, .adv_w=128, .box_h=5, .box_w=8, .ofs_x=0, .ofs_y=2 },
  /* Unicode: U+00AC (¬) [NOT SIGN] */
  { .bitmap_index=531, .adv_w=128, .box_h=3, .box_w=6, .ofs_x=1, .ofs_y=4 },
  /* Unicode: U+00AD (­) [SOFT HYPHEN] */
  { .bitmap_index=534, .adv_w=128, .box_h=1, .box_w=4, .ofs_x=2, .ofs_y=4 },
  /* Unicode: U+00AE (®) [REGISTERED SIGN] */
  { .bitmap_index=535, .adv_w=128, .box_h=8, .box_w=8, .ofs_x=0, .ofs_y=0 },
  /* Unicode: U+00AF (¯) [MACRON] */
  { .bitmap_index=543, .adv_w=128, .box_h=1, .box_w=6, .ofs_x=1, .ofs_y=7 },
  /* Unicode: U+00B0 (°) [DEGREE SIGN] */
  { .bitmap_index=544, .adv_w=128, .box_h=3, .box_w=6, .ofs_x=1, .ofs_y=5 },
  /* Unicode: U+00B1 (±) [PLUS-MINUS SIGN] */
  { .bitmap_index=547, .adv_w=128, .box_h=7, .box_w=6, .ofs_x=1, .ofs_y=1 },
  /* Unicode: U+00B2 (²) [SUPERSCRIPT TWO] */
  { .bitmap_index=553, .adv_w=128, .box_h=5, .box_w=4, .ofs_x=1, .ofs_y=3 },
  /* Unicode: U+00B3 (³) [SUPERSCRIPT THREE] */
  { .bitmap_index=556, .adv_w=128, .box_h=5, .box_w=5, .ofs_x=1, .ofs_y=3 },
  /* Unicode: U+00B4 (´) [ACUTE ACCENT] */
  { .bitmap_index=560, .adv_w=128, .box_h=3, .box_w=4, .ofs_x=2, .ofs_y=5 },
  /* Unicode: U+00B5 (µ) [MICRO SIGN] */
  { .bitmap_index=562, .adv_w=128, .box_h=6, .box_w=7, .ofs_x=0, .ofs_y=0 },
  /* Unicode: U+00B6 (¶) [PILCROW SIGN] */
  { .bitmap_index=568, .adv_w=128, .box_h=7, .box_w=6, .ofs_x=1, .ofs_y=1 },
  /* Unicode: U+00B7 (·) [MIDDLE DOT] */
  { .bitmap_index=574, .adv_w=128, .box_h=1, .box_w=2, .ofs_x=3, .ofs_y=4 },
  /* Unicode: U+00B8 (¸) [CEDILLA] */
  { .bitmap_index=575, .adv_w=128, .box_h=2, .box_w=2, .ofs_x=3, .ofs_y=1 },
  /* Unicode: U+00B9 (¹) [SUPERSCRIPT ONE] */
  { .bitmap_index=576, .adv_w=128, .box_h=5, .box_w=3, .ofs_x=1, .ofs_y=3 },
  /* Unicode: U+00BA (º) [MASCULINE ORDINAL INDICATOR] */
  { .bitmap_index=578, .adv_w=128, .box_h=6, .box_w=5, .ofs_x=1, .ofs_y=2 },
  /* Unicode: U+00BB (») [RIGHT-POINTING DOUBLE ANGLE QUOTATION MARK] */
  { .bitmap_index=582, .adv_w=128, .box_h=5, .box_w=8, .ofs_x=0, .ofs_y=2 },
  /* Unicode: U+00BC (¼) [VULGAR FRACTION ONE QUARTER] */
  { .bitmap_index=587, .adv_w=128, .box_h=8, .box_w=8, .ofs_x=0, .ofs_y=0 },
  /* Unicode: U+00BD (½) [VULGAR FRACTION ONE HALF] */
  { .bitmap_index=595, .adv_w=128, .box_h=8, .box_w=8, .ofs_x=0, .ofs_y=0 },
  /* Unicode: U+00BE (¾) [VULGAR FRACTION THREE QUARTERS] */
  { .bitmap_index=603, .adv_w=128, .box_h=8, .box_w=8, .ofs_x=0, .ofs_y=0 },
  /* Unicode: U+00BF (¿) [INVERTED QUESTION MARK] */
  { .bitmap_index=611, .adv_w=128, .box_h=7, .box_w=6, .ofs_x=1, .ofs_y=1 },
  /* Unicode: U+00C0 (À) [LATIN CAPITAL LETTER A WITH GRAVE] */
  { .bitmap_index=617, .adv_w=128, .box_h=7, .box_w=6, .ofs_x=1, .ofs_y=1 },
  /* Unicode: U+00C1 (Á) [LATIN CAPITAL LETTER A WITH ACUTE] */
  { .bitmap_index=623, .adv_w=128, .box_h=7, .box_w=6, .ofs_x=1, .ofs_y=1 },
  /* Unicode: U+00C2 (Â) [LATIN CAPITAL LETTER A WITH CIRCUMFLEX] */
  { .bitmap_index=629, .adv_w=128, .box_h=7, .box_w=6, .ofs_x=1, .ofs_y=1 },
  /* Unicode: U+00C3 (Ã) [LATIN CAPITAL LETTER A WITH TILDE] */
  { .bitmap_index=635, .adv_w=128, .box_h=7, .box_w=7, .ofs_x=0, .ofs_y=1 },
  /* Unicode: U+00C4 (Ä) [LATIN CAPITAL LETTER A WITH DIAERESIS] */
  { .bitmap_index=642, .adv_w=128, .box_h=7, .box_w=6, .ofs_x=1, .ofs_y=1 },
  /* Unicode: U+00C5 (Å) [LATIN CAPITAL LETTER A WITH RING ABOVE] */
  { .bitmap_index=648, .adv_w=128, .box_h=7, .box_w=6, .ofs_x=1, .ofs_y=1 },
  /* Unicode: U+00C6 (Æ) [LATIN CAPITAL LETTER AE] */
  { .bitmap_index=654, .adv_w=128, .box_h=7, .box_w=8, .ofs_x=0, .ofs_y=1 },
  /* Unicode: U+00C7 (Ç) [LATIN CAPITAL LETTER C WITH CEDILLA] */
  { .bitmap_index=661, .adv_w=128, .box_h=8, .box_w=6, .ofs_x=1, .ofs_y=0 },
  /* Unicode: U+00C8 (È) [LATIN CAPITAL LETTER E WITH GRAVE] */
  { .bitmap_index=667, .adv_w=128, .box_h=7, .box_w=6, .ofs_x=1, .ofs_y=1 },
  /* Unicode: U+00C9 (É) [LATIN CAPITAL LETTER E WITH ACUTE] */
  { .bitmap_index=673, .adv_w=128, .box_h=7, .box_w=6, .ofs_x=1, .ofs_y=1 },
  /* Unicode: U+00CA (Ê) [LATIN CAPITAL LETTER E WITH CIRCUMFLEX] */
  { .bitmap_index=679, .adv_w=128, .box_h=7, .box_w=7, .ofs_x=0, .ofs_y=1 },
  /* Unicode: U+00CB (Ë) [LATIN CAPITAL LETTER E WITH DIAERESIS] */
  { .bitmap_index=686, .adv_w=128, .box_h=7, .box_w=6, .ofs_x=1, .ofs_y=1 },
  /* Unicode: U+00CC (Ì) [LATIN CAPITAL LETTER I WITH GRAVE] */
  { .bitmap_index=692, .adv_w=128, .box_h=7, .box_w=6, .ofs_x=1, .ofs_y=1 },
  /* Unicode: U+00CD (Í) [LATIN CAPITAL LETTER I WITH ACUTE] */
  { .bitmap_index=698, .adv_w=128, .box_h=7, .box_w=6, .ofs_x=1, .ofs_y=1 },
  /* Unicode: U+00CE (Î) [LATIN CAPITAL LETTER I WITH CIRCUMFLEX] */
  { .bitmap_index=704, .adv_w=128, .box_h=7, .box_w=6, .ofs_x=1, .ofs_y=1 },
  /* Unicode: U+00CF (Ï) [LATIN CAPITAL LETTER I WITH DIAERESIS] */
  { .bitmap_index=710, .adv_w=128, .box_h=7, .box_w=6, .ofs_x=1, .ofs_y=1 },
  /* Unicode: U+00D0 (Ð) [LATIN CAPITAL LETTER ETH] */
  { .bitmap_index=716, .adv_w=128, .box_h=7, .box_w=7, .ofs_x=0, .ofs_y=1 },
  /* Unicode: U+00D1 (Ñ) [LATIN CAPITAL LETTER N WITH TILDE] */
  { .bitmap_index=723, .adv_w=128, .box_h=7, .box_w=7, .ofs_x=0, .ofs_y=1 },
  /* Unicode: U+00D2 (Ò) [LATIN CAPITAL LETTER O WITH GRAVE] */
  { .bitmap_index=730, .adv_w=128, .box_h=7, .box_w=7, .ofs_x=0, .ofs_y=1 },
  /* Unicode: U+00D3 (Ó) [LATIN CAPITAL LETTER O WITH ACUTE] */
  { .bitmap_index=737, .adv_w=128, .box_h=7, .box_w=7, .ofs_x=0, .ofs_y=1 },
  /* Unicode: U+00D4 (Ô) [LATIN CAPITAL LETTER O WITH CIRCUMFLEX] */
  { .bitmap_index=744, .adv_w=128, .box_h=7, .box_w=7, .ofs_x=0, .ofs_y=1 },
  /* Unicode: U+00D5 (Õ) [LATIN CAPITAL LETTER O WITH TILDE] */
  { .bitmap_index=751, .adv_w=128, .box_h=7, .box_w=7, .ofs_x=0, .ofs_y=1 },
  /* Unicode: U+00D6 (Ö) [LATIN CAPITAL LETTER O WITH DIAERESIS] */
  { .bitmap_index=758, .adv_w=128, .box_h=7, .box_w=7, .ofs_x=0, .ofs_y=1 },
  /* Unicode: U+00D7 (×) [MULTIPLICATION SIGN] */
  { .bitmap_index=765, .adv_w=128, .box_h=5, .box_w=7, .ofs_x=0, .ofs_y=2 },
  /* Unicode: U+00D8 (Ø) [LATIN CAPITAL LETTER O WITH STROKE] */
  { .bitmap_index=770, .adv_w=128, .box_h=7, .box_w=6, .ofs_x=1, .ofs_y=1 },
  /* Unicode: U+00D9 (Ù) [LATIN CAPITAL LETTER U WITH GRAVE] */
  { .bitmap_index=776, .adv_w=128, .box_h=7, .box_w=7, .ofs_x=0, .ofs_y=1 },
  /* Unicode: U+00DA (Ú) [LATIN CAPITAL LETTER U WITH ACUTE] */
  { .bitmap_index=783, .adv_w=128, .box_h=7, .box_w=7, .ofs_x=0, .ofs_y=1 },
  /* Unicode: U+00DB (Û) [LATIN CAPITAL LETTER U WITH CIRCUMFLEX] */
  { .bitmap_index=790, .adv_w=128, .box_h=7, .box_w=7, .ofs_x=0, .ofs_y=1 },
  /* Unicode: U+00DC (Ü) [LATIN CAPITAL LETTER U WITH DIAERESIS] */
  { .bitmap_index=797, .adv_w=128, .box_h=7, .box_w=7, .ofs_x=0, .ofs_y=1 },
  /* Unicode: U+00DD (Ý) [LATIN CAPITAL LETTER Y WITH ACUTE] */
  { .bitmap_index=804, .adv_w=128, .box_h=7, .box_w=6, .ofs_x=1, .ofs_y=1 },
  /* Unicode: U+00DE (Þ) [LATIN CAPITAL LETTER THORN] */
  { .bitmap_index=810, .adv_w=128, .box_h=7, .box_w=7, .ofs_x=0, .ofs_y=1 },
  /* Unicode: U+00DF (ß) [LATIN SMALL LETTER SHARP S] */
  { .bitmap_index=817, .adv_w=128, .box_h=7, .box_w=6, .ofs_x=1, .ofs_y=1 },
  /* Unicode: U+00E0 (à) [LATIN SMALL LETTER A WITH GRAVE] */
  { .bitmap_index=823, .adv_w=128, .box_h=7, .box_w=6, .ofs_x=1, .ofs_y=1 },
  /* Unicode: U+00E1 (á) [LATIN SMALL LETTER A WITH ACUTE] */
  { .bitmap_index=829, .adv_w=128, .box_h=7, .box_w=6, .ofs_x=1, .ofs_y=1 },
  /* Unicode: U+00E2 (â) [LATIN SMALL LETTER A WITH CIRCUMFLEX] */
  { .bitmap_index=835, .adv_w=128, .box_h=7, .box_w=7, .ofs_x=0, .ofs_y=1 },
  /* Unicode: U+00E3 (ã) [LATIN SMALL LETTER A WITH TILDE] */
  { .bitmap_index=842, .adv_w=128, .box_h=7, .box_w=7, .ofs_x=0, .ofs_y=1 },
  /* Unicode: U+00E4 (ä) [LATIN SMALL LETTER A WITH DIAERESIS] */
  { .bitmap_index=849, .adv_w=128, .box_h=7, .box_w=6, .ofs_x=1, .ofs_y=1 },
  /* Unicode: U+00E5 (å) [LATIN SMALL LETTER A WITH RING ABOVE] */
  { .bitmap_index=855, .adv_w=128, .box_h=7, .box_w=7, .ofs_x=0, .ofs_y=1 },
  /* Unicode: U+00E6 (æ) [LATIN SMALL LETTER AE] */
  { .bitmap_index=862, .adv_w=128, .box_h=5, .box_w=8, .ofs_x=0, .ofs_y=1 },
  /* Unicode: U+00E7 (ç) [LATIN SMALL LETTER C WITH CEDILLA] */
  { .bitmap_index=867, .adv_w=128, .box_h=6, .box_w=5, .ofs_x=1, .ofs_y=0 },
  /* Unicode: U+00E8 (è) [LATIN SMALL LETTER E WITH GRAVE] */
  { .bitmap_index=871, .adv_w=128, .box_h=7, .box_w=6, .ofs_x=1, .ofs_y=1 },
  /* Unicode: U+00E9 (é) [LATIN SMALL LETTER E WITH ACUTE] */
  { .bitmap_index=877, .adv_w=128, .box_h=7, .box_w=6, .ofs_x=1, .ofs_y=1 },
  /* Unicode: U+00EA (ê) [LATIN SMALL LETTER E WITH CIRCUMFLEX] */
  { .bitmap_index=883, .adv_w=128, .box_h=7, .box_w=6, .ofs_x=1, .ofs_y=1 },
  /* Unicode: U+00EB (ë) [LATIN SMALL LETTER E WITH DIAERESIS] */
  { .bitmap_index=889, .adv_w=128, .box_h=7, .box_w=6, .ofs_x=1, .ofs_y=1 },
  /* Unicode: U+00EC (ì) [LATIN SMALL LETTER I WITH GRAVE] */
  { .bitmap_index=895, .adv_w=128, .box_h=7, .box_w=5, .ofs_x=1, .ofs_y=1 },
  /* Unicode: U+00ED (í) [LATIN SMALL LETTER I WITH ACUTE] */
  { .bitmap_index=900, .adv_w=128, .box_h=7, .box_w=5, .ofs_x=2, .ofs_y=1 },
  /* Unicode: U+00EE (î) [LATIN SMALL LETTER I WITH CIRCUMFLEX] */
  { .bitmap_index=905, .adv_w=128, .box_h=7, .box_w=6, .ofs_x=1, .ofs_y=1 },
  /* Unicode: U+00EF (ï) [LATIN SMALL LETTER I WITH DIAERESIS] */
  { .bitmap_index=911, .adv_w=128, .box_h=7, .box_w=6, .ofs_x=1, .ofs_y=1 },
  /* Unicode: U+00F0 (ð) [LATIN SMALL LETTER ETH] */
  { .bitmap_index=917, .adv_w=128, .box_h=7, .box_w=7, .ofs_x=0, .ofs_y=1 },
  /* Unicode: U+00F1 (ñ) [LATIN SMALL LETTER N WITH TILDE] */
  { .bitmap_index=924, .adv_w=128, .box_h=7, .box_w=7, .ofs_x=0, .ofs_y=1 },
  /* Unicode: U+00F2 (ò) [LATIN SMALL LETTER O WITH GRAVE] */
  { .bitmap_index=931, .adv_w=128, .box_h=7, .box_w=6, .ofs_x=1, .ofs_y=1 },
  /* Unicode: U+00F3 (ó) [LATIN SMALL LETTER O WITH ACUTE] */
  { .bitmap_index=937, .adv_w=128, .box_h=7, .box_w=6, .ofs_x=1, .ofs_y=1 },
  /* Unicode: U+00F4 (ô) [LATIN SMALL LETTER O WITH CIRCUMFLEX] */
  { .bitmap_index=943, .adv_w=128, .box_h=7, .box_w=6, .ofs_x=1, .ofs_y=1 },
  /* Unicode: U+00F5 (õ) [LATIN SMALL LETTER O WITH TILDE] */
  { .bitmap_index=949, .adv_w=128, .box_h=7, .box_w=7, .ofs_x=0, .ofs_y=1 },
  /* Unicode: U+00F6 (ö) [LATIN SMALL LETTER O WITH DIAERESIS] */
  { .bitmap_index=956, .adv_w=128, .box_h=7, .box_w=6, .ofs_x=1, .ofs_y=1 },
  /* Unicode: U+00F7 (÷) [DIVISION SIGN] */
  { .bitmap_index=962, .adv_w=128, .box_h=7, .box_w=6, .ofs_x=1, .ofs_y=1 },
  /* Unicode: U+00F8 (ø) [LATIN SMALL LETTER O WITH STROKE] */
  { .bitmap_index=968, .adv_w=128, .box_h=7, .box_w=7, .ofs_x=0, .ofs_y=0 },
  /* Unicode: U+00F9 (ù) [LATIN SMALL LETTER U WITH GRAVE] */
  { .bitmap_index=975, .adv_w=128, .box_h=7, .box_w=6, .ofs_x=1, .ofs_y=1 },
  /* Unicode: U+00FA (ú) [LATIN SMALL LETTER U WITH ACUTE] */
  { .bitmap_index=981, .adv_w=128, .box_h=7, .box_w=6, .ofs_x=1, .ofs_y=1 },
  /* Unicode: U+00FB (û) [LATIN SMALL LETTER U WITH CIRCUMFLEX] */
  { .bitmap_index=987, .adv_w=128, .box_h=7, .box_w=6, .ofs_x=1, .ofs_y=1 },
  /* Unicode: U+00FC (ü) [LATIN SMALL LETTER U WITH DIAERESIS] */
  { .bitmap_index=993, .adv_w=128, .box_h=7, .box_w=6, .ofs_x=1, .ofs_y=1 },
  /* Unicode: U+00FD (ý) [LATIN SMALL LETTER Y WITH ACUTE] */
  { .bitmap_index=999, .adv_w=128, .box_h=8, .box_w=6, .ofs_x=1, .ofs_y=0 },
  /* Unicode: U+00FE (þ) [LATIN SMALL LETTER THORN] */
  { .bitmap_index=1005, .adv_w=128, .box_h=8, .box_w=6, .ofs_x=1, .ofs_y=0 },
  /* Unicode: U+00FF (ÿ) [LATIN SMALL LETTER Y WITH DIAERESIS] */
  { .bitmap_index=1011, .adv_w=128, .box_h=8, .box_w=6, .ofs_x=1, .ofs_y=0 },
  /* Unicode: U+2190 (←) [LEFTWARDS ARROW] */
  { .bitmap_index=1017, .adv_w=128, .box_h=6, .box_w=7, .ofs_x=1, .ofs_y=1 },
  /* Unicode: U+2191 (↑) [UPWARDS ARROW] */
  { .bitmap_index=1023, .adv_w=128, .box_h=8, .box_w=6, .ofs_x=1, .ofs_y=0 },
  /* Unicode: U+2192 (→) [RIGHTWARDS ARROW] */
  { .bitmap_index=1029, .adv_w=128, .box_h=6, .box_w=7, .ofs_x=0, .ofs_y=1 },
  /* Unicode: U+2194 (↔) [LEFT RIGHT ARROW] */
  { .bitmap_index=1035, .adv_w=128, .box_h=6, .box_w=8, .ofs_x=0, .ofs_y=1 },
  /* Unicode: U+2196 (↖) [NORTH WEST ARROW] */
  { .bitmap_index=1041, .adv_w=128, .box_h=7, .box_w=7, .ofs_x=0, .ofs_y=1 },
  /* Unicode: U+2197 (↗) [NORTH EAST ARROW] */
  { .bitmap_index=1048, .adv_w=128, .box_h=7, .box_w=7, .ofs_x=1, .ofs_y=1 },
  /* Unicode: U+2198 (↘) [SOUTH EAST ARROW] */
  { .bitmap_index=1055, .adv_w=128, .box_h=7, .box_w=7, .ofs_x=1, .ofs_y=0 },
  /* Unicode: U+2199 (↙) [SOUTH WEST ARROW] */
  { .bitmap_index=1062, .adv_w=128, .box_h=7, .box_w=7, .ofs_x=0, .ofs_y=0 },
  /* Unicode: U+21A5 (↥) [UPWARDS ARROW FROM BAR] */
  { .bitmap_index=1069, .adv_w=128, .box_h=8, .box_w=6, .ofs_x=1, .ofs_y=0 },
  /* Unicode: U+21A8 (↨) [UP DOWN ARROW WITH BASE] */
  { .bitmap_index=1075, .adv_w=128, .box_h=8, .box_w=8, .ofs_x=0, .ofs_y=0 },
  /* Unicode: U+21E6 (⇦) [LEFTWARDS WHITE ARROW] */
  { .bitmap_index=1083, .adv_w=128, .box_h=8, .box_w=8, .ofs_x=0, .ofs_y=0 },
  /* Unicode: U+21E7 (⇧) [UPWARDS WHITE ARROW] */
  { .bitmap_index=1091, .adv_w=128, .box_h=8, .box_w=8, .ofs_x=0, .ofs_y=0 },
  /* Unicode: U+21E8 (⇨) [RIGHTWARDS WHITE ARROW] */
  { .bitmap_index=1099, .adv_w=128, .box_h=8, .box_w=8, .ofs_x=0, .ofs_y=0 },
  /* Unicode: U+21E9 (⇩) [DOWNWARDS WHITE ARROW] */
  { .bitmap_index=1107, .adv_w=128, .box_h=8, .box_w=8, .ofs_x=0, .ofs_y=0 },
  /* Unicode: U+2500 (─) [BOX DRAWINGS LIGHT HORIZONTAL] */
  { .bitmap_index=1115, .adv_w=128, .box_h=2, .box_w=8, .ofs_x=0, .ofs_y=3 },
  /* Unicode: U+2501 (━) [BOX DRAWINGS HEAVY HORIZONTAL] */
  { .bitmap_index=1117, .adv_w=128, .box_h=4, .box_w=8, .ofs_x=0, .ofs_y=2 },
  /* Unicode: U+2502 (│) [BOX DRAWINGS LIGHT VERTICAL] */
  { .bitmap_index=1121, .adv_w=128, .box_h=8, .box_w=2, .ofs_x=3, .ofs_y=0 },
  /* Unicode: U+2503 (┃) [BOX DRAWINGS HEAVY VERTICAL] */
  { .bitmap_index=1123, .adv_w=128, .box_h=8, .box_w=4, .ofs_x=2, .ofs_y=0 },
  /* Unicode: U+2504 (┄) [BOX DRAWINGS LIGHT TRIPLE DASH HORIZONTAL] */
  { .bitmap_index=1127, .adv_w=128, .box_h=2, .box_w=8, .ofs_x=0, .ofs_y=3 },
  /* Unicode: U+2505 (┅) [BOX DRAWINGS HEAVY TRIPLE DASH HORIZONTAL] */
  { .bitmap_index=1129, .adv_w=128, .box_h=4, .box_w=8, .ofs_x=0, .ofs_y=2 },
  /* Unicode: U+2506 (┆) [BOX DRAWINGS LIGHT TRIPLE DASH VERTICAL] */
  { .bitmap_index=1133, .adv_w=128, .box_h=8, .box_w=2, .ofs_x=3, .ofs_y=0 },
  /* Unicode: U+2507 (┇) [BOX DRAWINGS HEAVY TRIPLE DASH VERTICAL] */
  { .bitmap_index=1135, .adv_w=128, .box_h=8, .box_w=4, .ofs_x=2, .ofs_y=0 },
  /* Unicode: U+2508 (┈) [BOX DRAWINGS LIGHT QUADRUPLE DASH HORIZONTAL] */
  { .bitmap_index=1139, .adv_w=128, .box_h=2, .box_w=7, .ofs_x=0, .ofs_y=3 },
  /* Unicode: U+2509 (┉) [BOX DRAWINGS HEAVY QUADRUPLE DASH HORIZONTAL] */
  { .bitmap_index=1141, .adv_w=128, .box_h=2, .box_w=7, .ofs_x=0, .ofs_y=3 },
  /* Unicode: U+250A (┊) [BOX DRAWINGS LIGHT QUADRUPLE DASH VERTICAL] */
  { .bitmap_index=1143, .adv_w=128, .box_h=7, .box_w=2, .ofs_x=3, .ofs_y=1 },
  /* Unicode: U+250B (┋) [BOX DRAWINGS HEAVY QUADRUPLE DASH VERTICAL] */
  { .bitmap_index=1145, .adv_w=128, .box_h=7, .box_w=4, .ofs_x=2, .ofs_y=1 },
  /* Unicode: U+250C (┌) [BOX DRAWINGS LIGHT DOWN AND RIGHT] */
  { .bitmap_index=1149, .adv_w=128, .box_h=5, .box_w=5, .ofs_x=3, .ofs_y=0 },
  /* Unicode: U+250D (┍) [BOX DRAWINGS DOWN LIGHT AND RIGHT HEAVY] */
  { .bitmap_index=1153, .adv_w=128, .box_h=6, .box_w=5, .ofs_x=3, .ofs_y=0 },
  /* Unicode: U+250E (┎) [BOX DRAWINGS DOWN HEAVY AND RIGHT LIGHT] */
  { .bitmap_index=1157, .adv_w=128, .box_h=5, .box_w=6, .ofs_x=2, .ofs_y=0 },
  /* Unicode: U+250F (┏) [BOX DRAWINGS HEAVY DOWN AND RIGHT] */
  { .bitmap_index=1161, .adv_w=128, .box_h=6, .box_w=6, .ofs_x=2, .ofs_y=0 },
  /* Unicode: U+2510 (┐) [BOX DRAWINGS LIGHT DOWN AND LEFT] */
  { .bitmap_index=1166, .adv_w=128, .box_h=5, .box_w=5, .ofs_x=0, .ofs_y=0 },
  /* Unicode: U+2511 (┑) [BOX DRAWINGS DOWN LIGHT AND LEFT HEAVY] */
  { .bitmap_index=1170, .adv_w=128, .box_h=6, .box_w=5, .ofs_x=0, .ofs_y=0 },
  /* Unicode: U+2512 (┒) [BOX DRAWINGS DOWN HEAVY AND LEFT LIGHT] */
  { .bitmap_index=1174, .adv_w=128, .box_h=5, .box_w=6, .ofs_x=0, .ofs_y=0 },
  /* Unicode: U+2513 (┓) [BOX DRAWINGS HEAVY DOWN AND LEFT] */
  { .bitmap_index=1178, .adv_w=128, .box_h=6, .box_w=6, .ofs_x=0, .ofs_y=0 },
  /* Unicode: U+2514 (└) [BOX DRAWINGS LIGHT UP AND RIGHT] */
  { .bitmap_index=1183, .adv_w=128, .box_h=5, .box_w=5, .ofs_x=3, .ofs_y=3 },
  /* Unicode: U+2515 (┕) [BOX DRAWINGS UP LIGHT AND RIGHT HEAVY] */
  { .bitmap_index=1187, .adv_w=128, .box_h=6, .box_w=5, .ofs_x=3, .ofs_y=2 },
  /* Unicode: U+2516 (┖) [BOX DRAWINGS UP HEAVY AND RIGHT LIGHT] */
  { .bitmap_index=1191, .adv_w=128, .box_h=5, .box_w=6, .ofs_x=2, .ofs_y=3 },
  /* Unicode: U+2517 (┗) [BOX DRAWINGS HEAVY UP AND RIGHT] */
  { .bitmap_index=1195, .adv_w=128, .box_h=6, .box_w=6, .ofs_x=2, .ofs_y=2 },
  /* Unicode: U+2518 (┘) [BOX DRAWINGS LIGHT UP AND LEFT] */
  { .bitmap_index=1200, .adv_w=128, .box_h=5, .box_w=5, .ofs_x=0, .ofs_y=3 },
  /* Unicode: U+2519 (┙) [BOX DRAWINGS UP LIGHT AND LEFT HEAVY] */
  { .bitmap_index=1204, .adv_w=128, .box_h=6, .box_w=5, .ofs_x=0, .ofs_y=2 },
  /* Unicode: U+251A (┚) [BOX DRAWINGS UP HEAVY AND LEFT LIGHT] */
  { .bitmap_index=1208, .adv_w=128, .box_h=5, .box_w=6, .ofs_x=0, .ofs_y=3 },
  /* Unicode: U+251B (┛) [BOX DRAWINGS HEAVY UP AND LEFT] */
  { .bitmap_index=1212, .adv_w=128, .box_h=6, .box_w=6, .ofs_x=0, .ofs_y=2 },
  /* Unicode: U+251C (├) [BOX DRAWINGS LIGHT VERTICAL AND RIGHT] */
  { .bitmap_index=1217, .adv_w=128, .box_h=8, .box_w=5, .ofs_x=3, .ofs_y=0 },
  /* Unicode: U+251D (┝) [BOX DRAWINGS VERTICAL LIGHT AND RIGHT HEAVY] */
  { .bitmap_index=1222, .adv_w=128, .box_h=8, .box_w=5, .ofs_x=3, .ofs_y=0 },
  /* Unicode: U+251E (┞) [BOX DRAWINGS UP HEAVY AND RIGHT DOWN LIGHT] */
  { .bitmap_index=1227, .adv_w=128, .box_h=8, .box_w=6, .ofs_x=2, .ofs_y=0 },
  /* Unicode: U+251F (┟) [BOX DRAWINGS DOWN HEAVY AND RIGHT UP LIGHT] */
  { .bitmap_index=1233, .adv_w=128, .box_h=8, .box_w=6, .ofs_x=2, .ofs_y=0 },
  /* Unicode: U+2520 (┠) [BOX DRAWINGS VERTICAL HEAVY AND RIGHT LIGHT] */
  { .bitmap_index=1239, .adv_w=128, .box_h=8, .box_w=6, .ofs_x=2, .ofs_y=0 },
  /* Unicode: U+2521 (┡) [BOX DRAWINGS DOWN LIGHT AND RIGHT UP HEAVY] */
  { .bitmap_index=1245, .adv_w=128, .box_h=8, .box_w=6, .ofs_x=2, .ofs_y=0 },
  /* Unicode: U+2522 (┢) [BOX DRAWINGS UP LIGHT AND RIGHT DOWN HEAVY] */
  { .bitmap_index=1251, .adv_w=128, .box_h=8, .box_w=6, .ofs_x=2, .ofs_y=0 },
  /* Unicode: U+2523 (┣) [BOX DRAWINGS HEAVY VERTICAL AND RIGHT] */
  { .bitmap_index=1257, .adv_w=128, .box_h=8, .box_w=6, .ofs_x=2, .ofs_y=0 },
  /* Unicode: U+2524 (┤) [BOX DRAWINGS LIGHT VERTICAL AND LEFT] */
  { .bitmap_index=1263, .adv_w=128, .box_h=8, .box_w=5, .ofs_x=0, .ofs_y=0 },
  /* Unicode: U+2525 (┥) [BOX DRAWINGS VERTICAL LIGHT AND LEFT HEAVY] */
  { .bitmap_index=1268, .adv_w=128, .box_h=8, .box_w=5, .ofs_x=0, .ofs_y=0 },
  /* Unicode: U+2526 (┦) [BOX DRAWINGS UP HEAVY AND LEFT DOWN LIGHT] */
  { .bitmap_index=1273, .adv_w=128, .box_h=8, .box_w=6, .ofs_x=0, .ofs_y=0 },
  /* Unicode: U+2527 (┧) [BOX DRAWINGS DOWN HEAVY AND LEFT UP LIGHT] */
  { .bitmap_index=1279, .adv_w=128, .box_h=8, .box_w=6, .ofs_x=0, .ofs_y=0 },
  /* Unicode: U+2528 (┨) [BOX DRAWINGS VERTICAL HEAVY AND LEFT LIGHT] */
  { .bitmap_index=1285, .adv_w=128, .box_h=8, .box_w=6, .ofs_x=0, .ofs_y=0 },
  /* Unicode: U+2529 (┩) [BOX DRAWINGS DOWN LIGHT AND LEFT UP HEAVY] */
  { .bitmap_index=1291, .adv_w=128, .box_h=8, .box_w=6, .ofs_x=0, .ofs_y=0 },
  /* Unicode: U+252A (┪) [BOX DRAWINGS UP LIGHT AND LEFT DOWN HEAVY] */
  { .bitmap_index=1297, .adv_w=128, .box_h=8, .box_w=6, .ofs_x=0, .ofs_y=0 },
  /* Unicode: U+252B (┫) [BOX DRAWINGS HEAVY VERTICAL AND LEFT] */
  { .bitmap_index=1303, .adv_w=128, .box_h=8, .box_w=6, .ofs_x=0, .ofs_y=0 },
  /* Unicode: U+252C (┬) [BOX DRAWINGS LIGHT DOWN AND HORIZONTAL] */
  { .bitmap_index=1309, .adv_w=128, .box_h=5, .box_w=8, .ofs_x=0, .ofs_y=0 },
  /* Unicode: U+252D (┭) [BOX DRAWINGS LEFT HEAVY AND RIGHT DOWN LIGHT] */
  { .bitmap_index=1314, .adv_w=128, .box_h=6, .box_w=8, .ofs_x=0, .ofs_y=0 },
  /* Unicode: U+252E (┮) [BOX DRAWINGS RIGHT HEAVY AND LEFT DOWN LIGHT] */
  { .bitmap_index=1320, .adv_w=128, .box_h=6, .box_w=8, .ofs_x=0, .ofs_y=0 },
  /* Unicode: U+252F (┯) [BOX DRAWINGS DOWN LIGHT AND HORIZONTAL HEAVY] */
  { .bitmap_index=1326, .adv_w=128, .box_h=6, .box_w=8, .ofs_x=0, .ofs_y=0 },
  /* Unicode: U+2530 (┰) [BOX DRAWINGS DOWN HEAVY AND HORIZONTAL LIGHT] */
  { .bitmap_index=1332, .adv_w=128, .box_h=5, .box_w=8, .ofs_x=0, .ofs_y=0 },
  /* Unicode: U+2531 (┱) [BOX DRAWINGS RIGHT LIGHT AND LEFT DOWN HEAVY] */
  { .bitmap_index=1337, .adv_w=128, .box_h=6, .box_w=8, .ofs_x=0, .ofs_y=0 },
  /* Unicode: U+2532 (┲) [BOX DRAWINGS LEFT LIGHT AND RIGHT DOWN HEAVY] */
  { .bitmap_index=1343, .adv_w=128, .box_h=6, .box_w=8, .ofs_x=0, .ofs_y=0 },
  /* Unicode: U+2533 (┳) [BOX DRAWINGS HEAVY DOWN AND HORIZONTAL] */
  { .bitmap_index=1349, .adv_w=128, .box_h=6, .box_w=8, .ofs_x=0, .ofs_y=0 },
  /* Unicode: U+2534 (┴) [BOX DRAWINGS LIGHT UP AND HORIZONTAL] */
  { .bitmap_index=1355, .adv_w=128, .box_h=5, .box_w=8, .ofs_x=0, .ofs_y=3 },
  /* Unicode: U+2535 (┵) [BOX DRAWINGS LEFT HEAVY AND RIGHT UP LIGHT] */
  { .bitmap_index=1360, .adv_w=128, .box_h=6, .box_w=8, .ofs_x=0, .ofs_y=2 },
  /* Unicode: U+2536 (┶) [BOX DRAWINGS RIGHT HEAVY AND LEFT UP LIGHT] */
  { .bitmap_index=1366, .adv_w=128, .box_h=6, .box_w=8, .ofs_x=0, .ofs_y=2 },
  /* Unicode: U+2537 (┷) [BOX DRAWINGS UP LIGHT AND HORIZONTAL HEAVY] */
  { .bitmap_index=1372, .adv_w=128, .box_h=6, .box_w=8, .ofs_x=0, .ofs_y=2 },
  /* Unicode: U+2538 (┸) [BOX DRAWINGS UP HEAVY AND HORIZONTAL LIGHT] */
  { .bitmap_index=1378, .adv_w=128, .box_h=5, .box_w=8, .ofs_x=0, .ofs_y=3 },
  /* Unicode: U+2539 (┹) [BOX DRAWINGS RIGHT LIGHT AND LEFT UP HEAVY] */
  { .bitmap_index=1383, .adv_w=128, .box_h=6, .box_w=8, .ofs_x=0, .ofs_y=2 },
  /* Unicode: U+253A (┺) [BOX DRAWINGS LEFT LIGHT AND RIGHT UP HEAVY] */
  { .bitmap_index=1389, .adv_w=128, .box_h=6, .box_w=8, .ofs_x=0, .ofs_y=2 },
  /* Unicode: U+253B (┻) [BOX DRAWINGS HEAVY UP AND HORIZONTAL] */
  { .bitmap_index=1395, .adv_w=128, .box_h=6, .box_w=8, .ofs_x=0, .ofs_y=2 },
  /* Unicode: U+253C (┼) [BOX DRAWINGS LIGHT VERTICAL AND HORIZONTAL] */
  { .bitmap_index=1401, .adv_w=128, .box_h=8, .box_w=8, .ofs_x=0, .ofs_y=0 },
  /* Unicode: U+253D (┽) [BOX DRAWINGS LEFT HEAVY AND RIGHT VERTICAL LIGHT] */
  { .bitmap_index=1409, .adv_w=128, .box_h=8, .box_w=8, .ofs_x=0, .ofs_y=0 },
  /* Unicode: U+253E (┾) [BOX DRAWINGS RIGHT HEAVY AND LEFT VERTICAL LIGHT] */
  { .bitmap_index=1417, .adv_w=128, .box_h=8, .box_w=8, .ofs_x=0, .ofs_y=0 },
  /* Unicode: U+253F (┿) [BOX DRAWINGS VERTICAL LIGHT AND HORIZONTAL HEAVY] */
  { .bitmap_index=1425, .adv_w=128, .box_h=8, .box_w=8, .ofs_x=0, .ofs_y=0 },
  /* Unicode: U+2540 (╀) [BOX DRAWINGS UP HEAVY AND DOWN HORIZONTAL LIGHT] */
  { .bitmap_index=1433, .adv_w=128, .box_h=8, .box_w=8, .ofs_x=0, .ofs_y=0 },
  /* Unicode: U+2541 (╁) [BOX DRAWINGS DOWN HEAVY AND UP HORIZONTAL LIGHT] */
  { .bitmap_index=1441, .adv_w=128, .box_h=8, .box_w=8, .ofs_x=0, .ofs_y=0 },
  /* Unicode: U+2542 (╂) [BOX DRAWINGS VERTICAL HEAVY AND HORIZONTAL LIGHT] */
  { .bitmap_index=1449, .adv_w=128, .box_h=8, .box_w=8, .ofs_x=0, .ofs_y=0 },
  /* Unicode: U+2543 (╃) [BOX DRAWINGS LEFT UP HEAVY AND RIGHT DOWN LIGHT] */
  { .bitmap_index=1457, .adv_w=128, .box_h=8, .box_w=8, .ofs_x=0, .ofs_y=0 },
  /* Unicode: U+2544 (╄) [BOX DRAWINGS RIGHT UP HEAVY AND LEFT DOWN LIGHT] */
  { .bitmap_index=1465, .adv_w=128, .box_h=8, .box_w=8, .ofs_x=0, .ofs_y=0 },
  /* Unicode: U+2545 (╅) [BOX DRAWINGS LEFT DOWN HEAVY AND RIGHT UP LIGHT] */
  { .bitmap_index=1473, .adv_w=128, .box_h=8, .box_w=8, .ofs_x=0, .ofs_y=0 },
  /* Unicode: U+2546 (╆) [BOX DRAWINGS RIGHT DOWN HEAVY AND LEFT UP LIGHT] */
  { .bitmap_index=1481, .adv_w=128, .box_h=8, .box_w=8, .ofs_x=0, .ofs_y=0 },
  /* Unicode: U+2547 (╇) [BOX DRAWINGS DOWN LIGHT AND UP HORIZONTAL HEAVY] */
  { .bitmap_index=1489, .adv_w=128, .box_h=8, .box_w=8, .ofs_x=0, .ofs_y=0 },
  /* Unicode: U+2548 (╈) [BOX DRAWINGS UP LIGHT AND DOWN HORIZONTAL HEAVY] */
  { .bitmap_index=1497, .adv_w=128, .box_h=8, .box_w=8, .ofs_x=0, .ofs_y=0 },
  /* Unicode: U+2549 (╉) [BOX DRAWINGS RIGHT LIGHT AND LEFT VERTICAL HEAVY] */
  { .bitmap_index=1505, .adv_w=128, .box_h=8, .box_w=8, .ofs_x=0, .ofs_y=0 },
  /* Unicode: U+254A (╊) [BOX DRAWINGS LEFT LIGHT AND RIGHT VERTICAL HEAVY] */
  { .bitmap_index=1513, .adv_w=128, .box_h=8, .box_w=8, .ofs_x=0, .ofs_y=0 },
  /* Unicode: U+254B (╋) [BOX DRAWINGS HEAVY VERTICAL AND HORIZONTAL] */
  { .bitmap_index=1521, .adv_w=128, .box_h=8, .box_w=8, .ofs_x=0, .ofs_y=0 },
  /* Unicode: U+254C (╌) [BOX DRAWINGS LIGHT DOUBLE DASH HORIZONTAL] */
  { .bitmap_index=1529, .adv_w=128, .box_h=2, .box_w=7, .ofs_x=0, .ofs_y=3 },
  /* Unicode: U+254D (╍) [BOX DRAWINGS HEAVY DOUBLE DASH HORIZONTAL] */
  { .bitmap_index=1531, .adv_w=128, .box_h=4, .box_w=7, .ofs_x=0, .ofs_y=2 },
  /* Unicode: U+254E (╎) [BOX DRAWINGS LIGHT DOUBLE DASH VERTICAL] */
  { .bitmap_index=1535, .adv_w=128, .box_h=7, .box_w=2, .ofs_x=3, .ofs_y=1 },
  /* Unicode: U+254F (╏) [BOX DRAWINGS HEAVY DOUBLE DASH VERTICAL] */
  { .bitmap_index=1537, .adv_w=128, .box_h=7, .box_w=4, .ofs_x=2, .ofs_y=1 },
  /* Unicode: U+2550 (═) [BOX DRAWINGS DOUBLE HORIZONTAL] */
  { .bitmap_index=1541, .adv_w=128, .box_h=3, .box_w=8, .ofs_x=0, .ofs_y=3 },
  /* Unicode: U+2551 (║) [BOX DRAWINGS DOUBLE VERTICAL] */
  { .bitmap_index=1544, .adv_w=128, .box_h=8, .box_w=5, .ofs_x=1, .ofs_y=0 },
  /* Unicode: U+2552 (╒) [BOX DRAWINGS DOWN SINGLE AND RIGHT DOUBLE] */
  { .bitmap_index=1549, .adv_w=128, .box_h=6, .box_w=5, .ofs_x=3, .ofs_y=0 },
  /* Unicode: U+2553 (╓) [BOX DRAWINGS DOWN DOUBLE AND RIGHT SINGLE] */
  { .bitmap_index=1553, .adv_w=128, .box_h=5, .box_w=7, .ofs_x=1, .ofs_y=0 },
  /* Unicode: U+2554 (╔) [BOX DRAWINGS DOUBLE DOWN AND RIGHT] */
  { .bitmap_index=1558, .adv_w=128, .box_h=6, .box_w=7, .ofs_x=1, .ofs_y=0 },
  /* Unicode: U+2555 (╕) [BOX DRAWINGS DOWN SINGLE AND LEFT DOUBLE] */
  { .bitmap_index=1564, .adv_w=128, .box_h=6, .box_w=5, .ofs_x=0, .ofs_y=0 },
  /* Unicode: U+2556 (╖) [BOX DRAWINGS DOWN DOUBLE AND LEFT SINGLE] */
  { .bitmap_index=1568, .adv_w=128, .box_h=5, .box_w=6, .ofs_x=0, .ofs_y=0 },
  /* Unicode: U+2557 (╗) [BOX DRAWINGS DOUBLE DOWN AND LEFT] */
  { .bitmap_index=1572, .adv_w=128, .box_h=6, .box_w=6, .ofs_x=0, .ofs_y=0 },
  /* Unicode: U+2558 (╘) [BOX DRAWINGS UP SINGLE AND RIGHT DOUBLE] */
  { .bitmap_index=1577, .adv_w=128, .box_h=5, .box_w=5, .ofs_x=3, .ofs_y=3 },
  /* Unicode: U+2559 (╙) [BOX DRAWINGS UP DOUBLE AND RIGHT SINGLE] */
  { .bitmap_index=1581, .adv_w=128, .box_h=4, .box_w=7, .ofs_x=1, .ofs_y=4 },
  /* Unicode: U+255A (╚) [BOX DRAWINGS DOUBLE UP AND RIGHT] */
  { .bitmap_index=1585, .adv_w=128, .box_h=5, .box_w=7, .ofs_x=1, .ofs_y=3 },
  /* Unicode: U+255B (╛) [BOX DRAWINGS UP SINGLE AND LEFT DOUBLE] */
  { .bitmap_index=1590, .adv_w=128, .box_h=5, .box_w=5, .ofs_x=0, .ofs_y=3 },
  /* Unicode: U+255C (╜) [BOX DRAWINGS UP DOUBLE AND LEFT SINGLE] */
  { .bitmap_index=1594, .adv_w=128, .box_h=4, .box_w=6, .ofs_x=0, .ofs_y=4 },
  /* Unicode: U+255D (╝) [BOX DRAWINGS DOUBLE UP AND LEFT] */
  { .bitmap_index=1597, .adv_w=128, .box_h=5, .box_w=6, .ofs_x=0, .ofs_y=3 },
  /* Unicode: U+255E (╞) [BOX DRAWINGS VERTICAL SINGLE AND RIGHT DOUBLE] */
  { .bitmap_index=1601, .adv_w=128, .box_h=8, .box_w=5, .ofs_x=3, .ofs_y=0 },
  /* Unicode: U+255F (╟) [BOX DRAWINGS VERTICAL DOUBLE AND RIGHT SINGLE] */
  { .bitmap_index=1606, .adv_w=128, .box_h=8, .box_w=7, .ofs_x=1, .ofs_y=0 },
  /* Unicode: U+2560 (╠) [BOX DRAWINGS DOUBLE VERTICAL AND RIGHT] */
  { .bitmap_index=1613, .adv_w=128, .box_h=8, .box_w=7, .ofs_x=1, .ofs_y=0 },
  /* Unicode: U+2561 (╡) [BOX DRAWINGS VERTICAL SINGLE AND LEFT DOUBLE] */
  { .bitmap_index=1620, .adv_w=128, .box_h=8, .box_w=5, .ofs_x=0, .ofs_y=0 },
  /* Unicode: U+2562 (╢) [BOX DRAWINGS VERTICAL DOUBLE AND LEFT SINGLE] */
  { .bitmap_index=1625, .adv_w=128, .box_h=8, .box_w=6, .ofs_x=0, .ofs_y=0 },
  /* Unicode: U+2563 (╣) [BOX DRAWINGS DOUBLE VERTICAL AND LEFT] */
  { .bitmap_index=1631, .adv_w=128, .box_h=8, .box_w=6, .ofs_x=0, .ofs_y=0 },
  /* Unicode: U+2564 (╤) [BOX DRAWINGS DOWN SINGLE AND HORIZONTAL DOUBLE] */
  { .bitmap_index=1637, .adv_w=128, .box_h=6, .box_w=8, .ofs_x=0, .ofs_y=0 },
  /* Unicode: U+2565 (╥) [BOX DRAWINGS DOWN DOUBLE AND HORIZONTAL SINGLE] */
  { .bitmap_index=1643, .adv_w=128, .box_h=5, .box_w=8, .ofs_x=0, .ofs_y=0 },
  /* Unicode: U+2566 (╦) [BOX DRAWINGS DOUBLE DOWN AND HORIZONTAL] */
  { .bitmap_index=1648, .adv_w=128, .box_h=6, .box_w=8, .ofs_x=0, .ofs_y=0 },
  /* Unicode: U+2567 (╧) [BOX DRAWINGS UP SINGLE AND HORIZONTAL DOUBLE] */
  { .bitmap_index=1654, .adv_w=128, .box_h=5, .box_w=8, .ofs_x=0, .ofs_y=3 },
  /* Unicode: U+2568 (╨) [BOX DRAWINGS UP DOUBLE AND HORIZONTAL SINGLE] */
  { .bitmap_index=1659, .adv_w=128, .box_h=5, .box_w=8, .ofs_x=0, .ofs_y=3 },
  /* Unicode: U+2569 (╩) [BOX DRAWINGS DOUBLE UP AND HORIZONTAL] */
  { .bitmap_index=1664, .adv_w=128, .box_h=5, .box_w=8, .ofs_x=0, .ofs_y=3 },
  /* Unicode: U+256A (╪) [BOX DRAWINGS VERTICAL SINGLE AND HORIZONTAL DOUBLE] */
  { .bitmap_index=1669, .adv_w=128, .box_h=8, .box_w=8, .ofs_x=0, .ofs_y=0 },
  /* Unicode: U+256B (╫) [BOX DRAWINGS VERTICAL DOUBLE AND HORIZONTAL SINGLE] */
  { .bitmap_index=1677, .adv_w=128, .box_h=8, .box_w=8, .ofs_x=0, .ofs_y=0 },
  /* Unicode: U+256C (╬) [BOX DRAWINGS DOUBLE VERTICAL AND HORIZONTAL] */
  { .bitmap_index=1685, .adv_w=128, .box_h=8, .box_w=8, .ofs_x=0, .ofs_y=0 },
  /* Unicode: U+256D (╭) [BOX DRAWINGS LIGHT ARC DOWN AND RIGHT] */
  { .bitmap_index=1693, .adv_w=128, .box_h=5, .box_w=5, .ofs_x=3, .ofs_y=0 },
  /* Unicode: U+256E (╮) [BOX DRAWINGS LIGHT ARC DOWN AND LEFT] */
  { .bitmap_index=1697, .adv_w=128, .box_h=5, .box_w=5, .ofs_x=0, .ofs_y=0 },
  /* Unicode: U+256F (╯) [BOX DRAWINGS LIGHT ARC UP AND LEFT] */
  { .bitmap_index=1701, .adv_w=128, .box_h=5, .box_w=5, .ofs_x=0, .ofs_y=3 },
  /* Unicode: U+2580 (▀) [UPPER HALF BLOCK] */
  { .bitmap_index=1705, .adv_w=128, .box_h=4, .box_w=8, .ofs_x=0, .ofs_y=4 },
  /* Unicode: U+2581 (▁) [LOWER ONE EIGHTH BLOCK] */
  { .bitmap_index=1709, .adv_w=128, .box_h=1, .box_w=8, .ofs_x=0, .ofs_y=0 },
  /* Unicode: U+2582 (▂) [LOWER ONE QUARTER BLOCK] */
  { .bitmap_index=1710, .adv_w=128, .box_h=2, .box_w=8, .ofs_x=0, .ofs_y=0 },
  /* Unicode: U+2583 (▃) [LOWER THREE EIGHTHS BLOCK] */
  { .bitmap_index=1712, .adv_w=128, .box_h=3, .box_w=8, .ofs_x=0, .ofs_y=0 },
  /* Unicode: U+2584 (▄) [LOWER HALF BLOCK] */
  { .bitmap_index=1715, .adv_w=128, .box_h=4, .box_w=8, .ofs_x=0, .ofs_y=0 },
  /* Unicode: U+2585 (▅) [LOWER FIVE EIGHTHS BLOCK] */
  { .bitmap_index=1719, .adv_w=128, .box_h=5, .box_w=8, .ofs_x=0, .ofs_y=0 },
  /* Unicode: U+2586 (▆) [LOWER THREE QUARTERS BLOCK] */
  { .bitmap_index=1724, .adv_w=128, .box_h=6, .box_w=8, .ofs_x=0, .ofs_y=0 },
  /* Unicode: U+2587 (▇) [LOWER SEVEN EIGHTHS BLOCK] */
  { .bitmap_index=1730, .adv_w=128, .box_h=7, .box_w=8, .ofs_x=0, .ofs_y=0 },
  /* Unicode: U+2588 (█) [FULL BLOCK] */
  { .bitmap_index=1737, .adv_w=128, .box_h=8, .box_w=8, .ofs_x=0, .ofs_y=0 },
  /* Unicode: U+2589 (▉) [LEFT SEVEN EIGHTHS BLOCK] */
  { .bitmap_index=1745, .adv_w=128, .box_h=8, .box_w=7, .ofs_x=0, .ofs_y=0 },
  /* Unicode: U+258A (▊) [LEFT THREE QUARTERS BLOCK] */
  { .bitmap_index=1752, .adv_w=128, .box_h=8, .box_w=6, .ofs_x=0, .ofs_y=0 },
  /* Unicode: U+258B (▋) [LEFT FIVE EIGHTHS BLOCK] */
  { .bitmap_index=1758, .adv_w=128, .box_h=8, .box_w=5, .ofs_x=0, .ofs_y=0 },
  /* Unicode: U+258C (▌) [LEFT HALF BLOCK] */
  { .bitmap_index=1763, .adv_w=128, .box_h=8, .box_w=4, .ofs_x=0, .ofs_y=0 },
  /* Unicode: U+258D (▍) [LEFT THREE EIGHTHS BLOCK] */
  { .bitmap_index=1767, .adv_w=128, .box_h=8, .box_w=3, .ofs_x=0, .ofs_y=0 },
  /* Unicode: U+258E (▎) [LEFT ONE QUARTER BLOCK] */
  { .bitmap_index=1770, .adv_w=128, .box_h=8, .box_w=2, .ofs_x=0, .ofs_y=0 },
  /* Unicode: U+258F (▏) [LEFT ONE EIGHTH BLOCK] */
  { .bitmap_index=1772, .adv_w=128, .box_h=8, .box_w=1, .ofs_x=0, .ofs_y=0 },
  /* Unicode: U+2590 (▐) [RIGHT HALF BLOCK] */
  { .bitmap_index=1773, .adv_w=128, .box_h=8, .box_w=4, .ofs_x=4, .ofs_y=0 },
  /* Unicode: U+2591 (░) [LIGHT SHADE] */
  { .bitmap_index=1777, .adv_w=128, .box_h=8, .box_w=7, .ofs_x=0, .ofs_y=0 },
  /* Unicode: U+2592 (▒) [MEDIUM SHADE] */
  { .bitmap_index=1784, .adv_w=128, .box_h=8, .box_w=8, .ofs_x=0, .ofs_y=0 },
  /* Unicode: U+2593 (▓) [DARK SHADE] */
  { .bitmap_index=1792, .adv_w=128, .box_h=8, .box_w=8, .ofs_x=0, .ofs_y=0 },
  /* Unicode: U+2594 (▔) [UPPER ONE EIGHTH BLOCK] */
  { .bitmap_index=1800, .adv_w=128, .box_h=1, .box_w=8, .ofs_x=0, .ofs_y=7 },
  /* Unicode: U+2595 (▕) [RIGHT ONE EIGHTH BLOCK] */
  { .bitmap_index=1801, .adv_w=128, .box_h=8, .box_w=1, .ofs_x=7, .ofs_y=0 },
  /* Unicode: U+2596 (▖) [QUADRANT LOWER LEFT] */
  { .bitmap_index=1802, .adv_w=128, .box_h=4, .box_w=4, .ofs_x=0, .ofs_y=0 },
  /* Unicode: U+2597 (▗) [QUADRANT LOWER RIGHT] */
  { .bitmap_index=1804, .adv_w=128, .box_h=4, .box_w=4, .ofs_x=4, .ofs_y=0 },
  /* Unicode: U+2598 (▘) [QUADRANT UPPER LEFT] */
  { .bitmap_index=1806, .adv_w=128, .box_h=4, .box_w=4, .ofs_x=0, .ofs_y=4 },
  /* Unicode: U+2599 (▙) [QUADRANT UPPER LEFT AND LOWER LEFT AND LOWER RIGHT] */
  { .bitmap_index=1808, .adv_w=128, .box_h=8, .box_w=8, .ofs_x=0, .ofs_y=0 },
  /* Unicode: U+259A (▚) [QUADRANT UPPER LEFT AND LOWER RIGHT] */
  { .bitmap_index=1816, .adv_w=128, .box_h=8, .box_w=8, .ofs_x=0, .ofs_y=0 },
  /* Unicode: U+259B (▛) [QUADRANT UPPER LEFT AND UPPER RIGHT AND LOWER LEFT] */
  { .bitmap_index=1824, .adv_w=128, .box_h=8, .box_w=8, .ofs_x=0, .ofs_y=0 },
  /* Unicode: U+259C (▜) [QUADRANT UPPER LEFT AND UPPER RIGHT AND LOWER RIGHT] */
  { .bitmap_index=1832, .adv_w=128, .box_h=8, .box_w=8, .ofs_x=0, .ofs_y=0 },
  /* Unicode: U+259D (▝) [QUADRANT UPPER RIGHT] */
  { .bitmap_index=1840, .adv_w=128, .box_h=4, .box_w=4, .ofs_x=4, .ofs_y=4 },
  /* Unicode: U+259E (▞) [QUADRANT UPPER RIGHT AND LOWER LEFT] */
  { .bitmap_index=1842, .adv_w=128, .box_h=8, .box_w=8, .ofs_x=0, .ofs_y=0 },
  /* Unicode: U+259F (▟) [QUADRANT UPPER RIGHT AND LOWER LEFT AND LOWER RIGHT] */
  { .bitmap_index=1850, .adv_w=128, .box_h=8, .box_w=8, .ofs_x=0, .ofs_y=0 },
  /* Unicode: U+25A0 (■) [BLACK SQUARE] */
  { .bitmap_index=1858, .adv_w=128, .box_h=6, .box_w=6, .ofs_x=1, .ofs_y=1 },
  /* Unicode: U+25A1 (□) [WHITE SQUARE] */
  { .bitmap_index=1863, .adv_w=128, .box_h=6, .box_w=6, .ofs_x=1, .ofs_y=1 },
  /* Unicode: U+25A2 (▢) [WHITE SQUARE WITH ROUNDED CORNERS] */
  { .bitmap_index=1868, .adv_w=128, .box_h=7, .box_w=7, .ofs_x=0, .ofs_y=1 },
  /* Unicode: U+25A3 (▣) [WHITE SQUARE CONTAINING BLACK SMALL SQUARE] */
  { .bitmap_index=1875, .adv_w=128, .box_h=7, .box_w=7, .ofs_x=0, .ofs_y=1 },
  /* Unicode: U+25A4 (▤) [SQUARE WITH HORIZONTAL FILL] */
  { .bitmap_index=1882, .adv_w=128, .box_h=7, .box_w=8, .ofs_x=0, .ofs_y=1 },
  /* Unicode: U+25A5 (▥) [SQUARE WITH VERTICAL FILL] */
  { .bitmap_index=1889, .adv_w=128, .box_h=8, .box_w=7, .ofs_x=0, .ofs_y=0 },
  /* Unicode: U+25A6 (▦) [SQUARE WITH ORTHOGONAL CROSSHATCH FILL] */
  { .bitmap_index=1896, .adv_w=128, .box_h=8, .box_w=8, .ofs_x=0, .ofs_y=0 },
  /* Unicode: U+25A7 (▧) [SQUARE WITH UPPER LEFT TO LOWER RIGHT FILL] */
  { .bitmap_index=1904, .adv_w=128, .box_h=8, .box_w=8, .ofs_x=0, .ofs_y=0 },
  /* Unicode: U+25A8 (▨) [SQUARE WITH UPPER RIGHT TO LOWER LEFT FILL] */
  { .bitmap_index=1912, .adv_w=128, .box_h=8, .box_w=8, .ofs_x=0, .ofs_y=0 },
  /* Unicode: U+25A9 (▩) [SQUARE WITH DIAGONAL CROSSHATCH FILL] */
  { .bitmap_index=1920, .adv_w=128, .box_h=8, .box_w=8, .ofs_x=0, .ofs_y=0 },
  /* Unicode: U+25AA (▪) [BLACK SMALL SQUARE] */
  { .bitmap_index=1928, .adv_w=128, .box_h=3, .box_w=3, .ofs_x=2, .ofs_y=3 },
  /* Unicode: U+25AB (▫) [WHITE SMALL SQUARE] */
  { .bitmap_index=1930, .adv_w=128, .box_h=3, .box_w=3, .ofs_x=2, .ofs_y=3 },
  /* Unicode: U+25AC (▬) [BLACK RECTANGLE] */
  { .bitmap_index=1932, .adv_w=128, .box_h=3, .box_w=6, .ofs_x=1, .ofs_y=1 },
  /* Unicode: U+25AD (▭) [WHITE RECTANGLE] */
  { .bitmap_index=1935, .adv_w=128, .box_h=3, .box_w=6, .ofs_x=1, .ofs_y=1 },
  /* Unicode: U+25AE (▮) [BLACK VERTICAL RECTANGLE] */
  { .bitmap_index=1938, .adv_w=128, .box_h=7, .box_w=4, .ofs_x=2, .ofs_y=1 },
  /* Unicode: U+25AF (▯) [WHITE VERTICAL RECTANGLE] */
  { .bitmap_index=1942, .adv_w=128, .box_h=7, .box_w=4, .ofs_x=2, .ofs_y=1 },
  /* Unicode: U+25B0 (▰) [BLACK PARALLELOGRAM] */
  { .bitmap_index=1946, .adv_w=128, .box_h=3, .box_w=8, .ofs_x=0, .ofs_y=1 },
  /* Unicode: U+25B1 (▱) [WHITE PARALLELOGRAM] */
  { .bitmap_index=1949, .adv_w=128, .box_h=3, .box_w=8, .ofs_x=0, .ofs_y=1 },
  /* Unicode: U+25B2 (▲) [BLACK UP-POINTING TRIANGLE] */
  { .bitmap_index=1952, .adv_w=128, .box_h=8, .box_w=7, .ofs_x=0, .ofs_y=0 },
  /* Unicode: U+25B3 (△) [WHITE UP-POINTING TRIANGLE] */
  { .bitmap_index=1959, .adv_w=128, .box_h=8, .box_w=8, .ofs_x=0, .ofs_y=0 },
  /* Unicode: U+25B4 (▴) [BLACK UP-POINTING SMALL TRIANGLE] */
  { .bitmap_index=1967, .adv_w=128, .box_h=6, .box_w=5, .ofs_x=1, .ofs_y=1 },
  /* Unicode: U+25B5 (▵) [WHITE UP-POINTING SMALL TRIANGLE] */
  { .bitmap_index=1971, .adv_w=128, .box_h=6, .box_w=7, .ofs_x=0, .ofs_y=1 },
  /* Unicode: U+25B6 (▶) [BLACK RIGHT-POINTING TRIANGLE] */
  { .bitmap_index=1977, .adv_w=128, .box_h=7, .box_w=8, .ofs_x=0, .ofs_y=1 },
  /* Unicode: U+25B7 (▷) [WHITE RIGHT-POINTING TRIANGLE] */
  { .bitmap_index=1984, .adv_w=128, .box_h=7, .box_w=8, .ofs_x=0, .ofs_y=1 },
  /* Unicode: U+25B8 (▸) [BLACK RIGHT-POINTING SMALL TRIANGLE] */
  { .bitmap_index=1991, .adv_w=128, .box_h=5, .box_w=6, .ofs_x=1, .ofs_y=2 },
  /* Unicode: U+25B9 (▹) [WHITE RIGHT-POINTING SMALL TRIANGLE] */
  { .bitmap_index=1995, .adv_w=128, .box_h=5, .box_w=6, .ofs_x=1, .ofs_y=2 },
  /* Unicode: U+25BA (►) [BLACK RIGHT-POINTING POINTER] */
  { .bitmap_index=1999, .adv_w=128, .box_h=5, .box_w=8, .ofs_x=0, .ofs_y=2 },
  /* Unicode: U+25BB (▻) [WHITE RIGHT-POINTING POINTER] */
  { .bitmap_index=2004, .adv_w=128, .box_h=5, .box_w=8, .ofs_x=0, .ofs_y=2 },
  /* Unicode: U+25BC (▼) [BLACK DOWN-POINTING TRIANGLE] */
  { .bitmap_index=2009, .adv_w=128, .box_h=8, .box_w=7, .ofs_x=0, .ofs_y=0 },
  /* Unicode: U+25BD (▽) [WHITE DOWN-POINTING TRIANGLE] */
  { .bitmap_index=2016, .adv_w=128, .box_h=8, .box_w=8, .ofs_x=0, .ofs_y=0 },
  /* Unicode: U+25BE (▾) [BLACK DOWN-POINTING SMALL TRIANGLE] */
  { .bitmap_index=2024, .adv_w=128, .box_h=6, .box_w=5, .ofs_x=1, .ofs_y=1 },
  /* Unicode: U+25BF (▿) [WHITE DOWN-POINTING SMALL TRIANGLE] */
  { .bitmap_index=2028, .adv_w=128, .box_h=6, .box_w=7, .ofs_x=0, .ofs_y=1 },
  /* Unicode: U+25C0 (◀) [BLACK LEFT-POINTING TRIANGLE] */
  { .bitmap_index=2034, .adv_w=128, .box_h=7, .box_w=8, .ofs_x=0, .ofs_y=1 },
  /* Unicode: U+25C1 (◁) [WHITE LEFT-POINTING TRIANGLE] */
  { .bitmap_index=2041, .adv_w=128, .box_h=7, .box_w=8, .ofs_x=0, .ofs_y=1 },
  /* Unicode: U+25C2 (◂) [BLACK LEFT-POINTING SMALL TRIANGLE] */
  { .bitmap_index=2048, .adv_w=128, .box_h=5, .box_w=6, .ofs_x=1, .ofs_y=2 },
  /* Unicode: U+25C3 (◃) [WHITE LEFT-POINTING SMALL TRIANGLE] */
  { .bitmap_index=2052, .adv_w=128, .box_h=5, .box_w=6, .ofs_x=1, .ofs_y=2 },
  /* Unicode: U+25C4 (◄) [BLACK LEFT-POINTING POINTER] */
  { .bitmap_index=2056, .adv_w=128, .box_h=5, .box_w=8, .ofs_x=0, .ofs_y=2 },
  /* Unicode: U+25C5 (◅) [WHITE LEFT-POINTING POINTER] */
  { .bitmap_index=2061, .adv_w=128, .box_h=5, .box_w=8, .ofs_x=0, .ofs_y=2 },
  /* Unicode: U+25C6 (◆) [BLACK DIAMOND] */
  { .bitmap_index=2066, .adv_w=128, .box_h=8, .box_w=8, .ofs_x=0, .ofs_y=0 },
  /* Unicode: U+25C7 (◇) [WHITE DIAMOND] */
  { .bitmap_index=2074, .adv_w=128, .box_h=8, .box_w=8, .ofs_x=0, .ofs_y=0 },
  /* Unicode: U+25C8 (◈) [WHITE DIAMOND CONTAINING BLACK SMALL DIAMOND] */
  { .bitmap_index=2082, .adv_w=128, .box_h=8, .box_w=8, .ofs_x=0, .ofs_y=0 },
  /* Unicode: U+25C9 (◉) [FISHEYE] */
  { .bitmap_index=2090, .adv_w=128, .box_h=8, .box_w=8, .ofs_x=0, .ofs_y=0 },
  /* Unicode: U+25CA (◊) [LOZENGE] */
  { .bitmap_index=2098, .adv_w=128, .box_h=8, .box_w=7, .ofs_x=0, .ofs_y=0 },
  /* Unicode: U+25CB (○) [WHITE CIRCLE] */
  { .bitmap_index=2105, .adv_w=128, .box_h=6, .box_w=6, .ofs_x=1, .ofs_y=1 },
  /* Unicode: U+25CC (◌) [DOTTED CIRCLE] */
  { .bitmap_index=2110, .adv_w=128, .box_h=7, .box_w=7, .ofs_x=0, .ofs_y=1 },
  /* Unicode: U+25CD (◍) [CIRCLE WITH VERTICAL FILL] */
  { .bitmap_index=2117, .adv_w=128, .box_h=6, .box_w=7, .ofs_x=0, .ofs_y=1 },
  /* Unicode: U+25CE (◎) [BULLSEYE] */
  { .bitmap_index=2123, .adv_w=128, .box_h=7, .box_w=7, .ofs_x=0, .ofs_y=1 },
  /* Unicode: U+25CF (●) [BLACK CIRCLE] */
  { .bitmap_index=2130, .adv_w=128, .box_h=6, .box_w=6, .ofs_x=1, .ofs_y=1 },
  /* Unicode: U+25D0 (◐) [CIRCLE WITH LEFT HALF BLACK] */
  { .bitmap_index=2135, .adv_w=128, .box_h=8, .box_w=8, .ofs_x=0, .ofs_y=0 },
  /* Unicode: U+25D1 (◑) [CIRCLE WITH RIGHT HALF BLACK] */
  { .bitmap_index=2143, .adv_w=128, .box_h=8, .box_w=8, .ofs_x=0, .ofs_y=0 },
  /* Unicode: U+25D2 (◒) [CIRCLE WITH LOWER HALF BLACK] */
  { .bitmap_index=2151, .adv_w=128, .box_h=8, .box_w=8, .ofs_x=0, .ofs_y=0 },
  /* Unicode: U+25D3 (◓) [CIRCLE WITH UPPER HALF BLACK] */
  { .bitmap_index=2159, .adv_w=128, .box_h=8, .box_w=8, .ofs_x=0, .ofs_y=0 },
  /* Unicode: U+25D4 (◔) [CIRCLE WITH UPPER RIGHT QUADRANT BLACK] */
  { .bitmap_index=2167, .adv_w=128, .box_h=8, .box_w=8, .ofs_x=0, .ofs_y=0 },
  /* Unicode: U+25D5 (◕) [CIRCLE WITH ALL BUT UPPER LEFT QUADRANT BLACK] */
  { .bitmap_index=2175, .adv_w=128, .box_h=8, .box_w=8, .ofs_x=0, .ofs_y=0 },
  /* Unicode: U+25D6 (◖) [LEFT HALF BLACK CIRCLE] */
  { .bitmap_index=2183, .adv_w=128, .box_h=6, .box_w=6, .ofs_x=2, .ofs_y=1 },
  /* Unicode: U+25D7 (◗) [RIGHT HALF BLACK CIRCLE] */
  { .bitmap_index=2188, .adv_w=128, .box_h=6, .box_w=6, .ofs_x=0, .ofs_y=1 },
  /* Unicode: U+25D8 (◘) [INVERSE BULLET] */
  { .bitmap_index=2193, .adv_w=128, .box_h=8, .box_w=8, .ofs_x=0, .ofs_y=0 },
  /* Unicode: U+25D9 (◙) [INVERSE WHITE CIRCLE] */
  { .bitmap_index=2201, .adv_w=128, .box_h=8, .box_w=8, .ofs_x=0, .ofs_y=0 },
  /* Unicode: U+25DA (◚) [UPPER HALF INVERSE WHITE CIRCLE] */
  { .bitmap_index=2209, .adv_w=128, .box_h=4, .box_w=8, .ofs_x=0, .ofs_y=4 },
  /* Unicode: U+25DB (◛) [LOWER HALF INVERSE WHITE CIRCLE] */
  { .bitmap_index=2213, .adv_w=128, .box_h=4, .box_w=8, .ofs_x=0, .ofs_y=0 },
  /* Unicode: U+25DC (◜) [UPPER LEFT QUADRANT CIRCULAR ARC] */
  { .bitmap_index=2217, .adv_w=128, .box_h=4, .box_w=5, .ofs_x=0, .ofs_y=4 },
  /* Unicode: U+25DD (◝) [UPPER RIGHT QUADRANT CIRCULAR ARC] */
  { .bitmap_index=2220, .adv_w=128, .box_h=4, .box_w=5, .ofs_x=3, .ofs_y=4 },
  /* Unicode: U+25DE (◞) [LOWER RIGHT QUADRANT CIRCULAR ARC] */
  { .bitmap_index=2223, .adv_w=128, .box_h=4, .box_w=5, .ofs_x=3, .ofs_y=0 },
  /* Unicode: U+25DF (◟) [LOWER LEFT QUADRANT CIRCULAR ARC] */
  { .bitmap_index=2226, .adv_w=128, .box_h=4, .box_w=5, .ofs_x=0, .ofs_y=0 },
  /* Unicode: U+25E0 (◠) [UPPER HALF CIRCLE] */
  { .bitmap_index=2229, .adv_w=128, .box_h=4, .box_w=8, .ofs_x=0, .ofs_y=4 },
  /* Unicode: U+25E1 (◡) [LOWER HALF CIRCLE] */
  { .bitmap_index=2233, .adv_w=128, .box_h=4, .box_w=8, .ofs_x=0, .ofs_y=0 },
  /* Unicode: U+25E2 (◢) [BLACK LOWER RIGHT TRIANGLE] */
  { .bitmap_index=2237, .adv_w=128, .box_h=8, .box_w=8, .ofs_x=0, .ofs_y=0 },
  /* Unicode: U+25E3 (◣) [BLACK LOWER LEFT TRIANGLE] */
  { .bitmap_index=2245, .adv_w=128, .box_h=8, .box_w=8, .ofs_x=0, .ofs_y=0 },
  /* Unicode: U+25E4 (◤) [BLACK UPPER LEFT TRIANGLE] */
  { .bitmap_index=2253, .adv_w=128, .box_h=8, .box_w=8, .ofs_x=0, .ofs_y=0 },
  /* Unicode: U+25E5 (◥) [BLACK UPPER RIGHT TRIANGLE] */
  { .bitmap_index=2261, .adv_w=128, .box_h=8, .box_w=8, .ofs_x=0, .ofs_y=0 },
  /* Unicode: U+25E6 (◦) [WHITE BULLET] */
  { .bitmap_index=2269, .adv_w=128, .box_h=4, .box_w=4, .ofs_x=2, .ofs_y=2 },
  /* Unicode: U+25E7 (◧) [SQUARE WITH LEFT HALF BLACK] */
  { .bitmap_index=2271, .adv_w=128, .box_h=8, .box_w=8, .ofs_x=0, .ofs_y=0 },
  /* Unicode: U+25E8 (◨) [SQUARE WITH RIGHT HALF BLACK] */
  { .bitmap_index=2279, .adv_w=128, .box_h=8, .box_w=8, .ofs_x=0, .ofs_y=0 },
  /* Unicode: U+25E9 (◩) [SQUARE WITH UPPER LEFT DIAGONAL HALF BLACK] */
  { .bitmap_index=2287, .adv_w=128, .box_h=8, .box_w=8, .ofs_x=0, .ofs_y=0 },
  /* Unicode: U+25EA (◪) [SQUARE WITH LOWER RIGHT DIAGONAL HALF BLACK] */
  { .bitmap_index=2295, .adv_w=128, .box_h=8, .box_w=8, .ofs_x=0, .ofs_y=0 },
  /* Unicode: U+25EB (◫) [WHITE SQUARE WITH VERTICAL BISECTING LINE] */
  { .bitmap_index=2303, .adv_w=128, .box_h=8, .box_w=8, .ofs_x=0, .ofs_y=0 },
  /* Unicode: U+25EC (◬) [WHITE UP-POINTING TRIANGLE WITH DOT] */
  { .bitmap_index=2311, .adv_w=128, .box_h=8, .box_w=8, .ofs_x=0, .ofs_y=0 },
  /* Unicode: U+25ED (◭) [UP-POINTING TRIANGLE WITH LEFT HALF BLACK] */
  { .bitmap_index=2319, .adv_w=128, .box_h=8, .box_w=8, .ofs_x=0, .ofs_y=0 },
  /* Unicode: U+25EE (◮) [UP-POINTING TRIANGLE WITH RIGHT HALF BLACK] */
  { .bitmap_index=2327, .adv_w=128, .box_h=8, .box_w=8, .ofs_x=0, .ofs_y=0 },
  /* Unicode: U+25EF (◯) [LARGE CIRCLE] */
  { .bitmap_index=2335, .adv_w=128, .box_h=8, .box_w=8, .ofs_x=0, .ofs_y=0 },
  /* Unicode: U+25F0 (◰) [WHITE SQUARE WITH UPPER LEFT QUADRANT] */
  { .bitmap_index=2343, .adv_w=128, .box_h=8, .box_w=8, .ofs_x=0, .ofs_y=0 },
  /* Unicode: U+25F1 (◱) [WHITE SQUARE WITH LOWER LEFT QUADRANT] */
  { .bitmap_index=2351, .adv_w=128, .box_h=8, .box_w=8, .ofs_x=0, .ofs_y=0 },
  /* Unicode: U+25F2 (◲) [WHITE SQUARE WITH LOWER RIGHT QUADRANT] */
  { .bitmap_index=2359, .adv_w=128, .box_h=8, .box_w=8, .ofs_x=0, .ofs_y=0 },
  /* Unicode: U+25F3 (◳) [WHITE SQUARE WITH UPPER RIGHT QUADRANT] */
  { .bitmap_index=2367, .adv_w=128, .box_h=8, .box_w=8, .ofs_x=0, .ofs_y=0 },
  /* Unicode: U+25F4 (◴) [WHITE CIRCLE WITH UPPER LEFT QUADRANT] */
  { .bitmap_index=2375, .adv_w=128, .box_h=8, .box_w=8, .ofs_x=0, .ofs_y=0 },
  /* Unicode: U+25F5 (◵) [WHITE CIRCLE WITH LOWER LEFT QUADRANT] */
  { .bitmap_index=2383, .adv_w=128, .box_h=8, .box_w=8, .ofs_x=0, .ofs_y=0 },
  /* Unicode: U+25F6 (◶) [WHITE CIRCLE WITH LOWER RIGHT QUADRANT] */
  { .bitmap_index=2391, .adv_w=128, .box_h=8, .box_w=8, .ofs_x=0, .ofs_y=0 },
  /* Unicode: U+25F7 (◷) [WHITE CIRCLE WITH UPPER RIGHT QUADRANT] */
  { .bitmap_index=2399, .adv_w=128, .box_h=8, .box_w=8, .ofs_x=0, .ofs_y=0 },
  /* Unicode: U+25F8 (◸) [UPPER LEFT TRIANGLE] */
  { .bitmap_index=2407, .adv_w=128, .box_h=8, .box_w=8, .ofs_x=0, .ofs_y=0 },
  /* Unicode: U+25F9 (◹) [UPPER RIGHT TRIANGLE] */
  { .bitmap_index=2415, .adv_w=128, .box_h=8, .box_w=8, .ofs_x=0, .ofs_y=0 },
  /* Unicode: U+25FA (◺) [LOWER LEFT TRIANGLE] */
  { .bitmap_index=2423, .adv_w=128, .box_h=8, .box_w=8, .ofs_x=0, .ofs_y=0 },
  /* Unicode: U+25FB (◻) [WHITE MEDIUM SQUARE] */
  { .bitmap_index=2431, .adv_w=128, .box_h=5, .box_w=5, .ofs_x=1, .ofs_y=2 },
  /* Unicode: U+25FC (◼) [BLACK MEDIUM SQUARE] */
  { .bitmap_index=2435, .adv_w=128, .box_h=5, .box_w=5, .ofs_x=1, .ofs_y=2 },
  /* Unicode: U+25FD (◽) [WHITE MEDIUM SMALL SQUARE] */
  { .bitmap_index=2439, .adv_w=128, .box_h=4, .box_w=4, .ofs_x=2, .ofs_y=2 },
  /* Unicode: U+25FE (◾) [BLACK MEDIUM SMALL SQUARE] */
  { .bitmap_index=2441, .adv_w=128, .box_h=4, .box_w=4, .ofs_x=2, .ofs_y=2 },
  /* Unicode: U+25FF (◿) [LOWER RIGHT TRIANGLE] */
  { .bitmap_index=2443, .adv_w=128, .box_h=8, .box_w=8, .ofs_x=0, .ofs_y=0 },
  /* Unicode: U+2625 (☥) [ANKH] */
  { .bitmap_index=2451, .adv_w=128, .box_h=7, .box_w=7, .ofs_x=0, .ofs_y=1 },
  /* Unicode: U+2628 (☨) [CROSS OF LORRAINE] */
  { .bitmap_index=2458, .adv_w=128, .box_h=7, .box_w=6, .ofs_x=1, .ofs_y=1 },
  /* Unicode: U+262F (☯) [YIN YANG] */
  { .bitmap_index=2464, .adv_w=128, .box_h=6, .box_w=8, .ofs_x=0, .ofs_y=1 },
  /* Unicode: U+2630 (☰) [TRIGRAM FOR HEAVEN] */
  { .bitmap_index=2470, .adv_w=128, .box_h=5, .box_w=7, .ofs_x=0, .ofs_y=1 },
  /* Unicode: U+2631 (☱) [TRIGRAM FOR LAKE] */
  { .bitmap_index=2475, .adv_w=128, .box_h=5, .box_w=7, .ofs_x=0, .ofs_y=1 },
  /* Unicode: U+2632 (☲) [TRIGRAM FOR FIRE] */
  { .bitmap_index=2480, .adv_w=128, .box_h=5, .box_w=7, .ofs_x=0, .ofs_y=1 },
  /* Unicode: U+2633 (☳) [TRIGRAM FOR THUNDER] */
  { .bitmap_index=2485, .adv_w=128, .box_h=5, .box_w=7, .ofs_x=0, .ofs_y=1 },
  /* Unicode: U+2634 (☴) [TRIGRAM FOR WIND] */
  { .bitmap_index=2490, .adv_w=128, .box_h=5, .box_w=7, .ofs_x=0, .ofs_y=1 },
  /* Unicode: U+2635 (☵) [TRIGRAM FOR WATER] */
  { .bitmap_index=2495, .adv_w=128, .box_h=5, .box_w=7, .ofs_x=0, .ofs_y=1 },
  /* Unicode: U+2636 (☶) [TRIGRAM FOR MOUNTAIN] */
  { .bitmap_index=2500, .adv_w=128, .box_h=5, .box_w=7, .ofs_x=0, .ofs_y=1 },
  /* Unicode: U+2637 (☷) [TRIGRAM FOR EARTH] */
  { .bitmap_index=2505, .adv_w=128, .box_h=5, .box_w=7, .ofs_x=0, .ofs_y=1 },
  /* Unicode: U+2639 (☹) [WHITE FROWNING FACE] */
  { .bitmap_index=2510, .adv_w=128, .box_h=8, .box_w=8, .ofs_x=0, .ofs_y=0 },
  /* Unicode: U+263A (☺) [WHITE SMILING FACE] */
  { .bitmap_index=2518, .adv_w=128, .box_h=8, .box_w=8, .ofs_x=0, .ofs_y=0 },
  /* Unicode: U+263B (☻) [BLACK SMILING FACE] */
  { .bitmap_index=2526, .adv_w=128, .box_h=8, .box_w=8, .ofs_x=0, .ofs_y=0 },
  /* Unicode: U+2660 (♠) [BLACK SPADE SUIT] */
  { .bitmap_index=2534, .adv_w=128, .box_h=7, .box_w=7, .ofs_x=0, .ofs_y=1 },
  /* Unicode: U+2661 (♡) [WHITE HEART SUIT] */
  { .bitmap_index=2541, .adv_w=128, .box_h=7, .box_w=7, .ofs_x=0, .ofs_y=1 },
  /* Unicode: U+2662 (♢) [WHITE DIAMOND SUIT] */
  { .bitmap_index=2548, .adv_w=128, .box_h=7, .box_w=7, .ofs_x=0, .ofs_y=1 },
  /* Unicode: U+2663 (♣) [BLACK CLUB SUIT] */
  { .bitmap_index=2555, .adv_w=128, .box_h=7, .box_w=7, .ofs_x=0, .ofs_y=1 },
  /* Unicode: U+2664 (♤) [WHITE SPADE SUIT] */
  { .bitmap_index=2562, .adv_w=128, .box_h=7, .box_w=7, .ofs_x=0, .ofs_y=1 },
  /* Unicode: U+2665 (♥) [BLACK HEART SUIT] */
  { .bitmap_index=2569, .adv_w=128, .box_h=7, .box_w=7, .ofs_x=0, .ofs_y=1 },
  /* Unicode: U+2666 (♦) [BLACK DIAMOND SUIT] */
  { .bitmap_index=2576, .adv_w=128, .box_h=7, .box_w=7, .ofs_x=0, .ofs_y=1 },
  /* Unicode: U+2667 (♧) [WHITE CLUB SUIT] */
  { .bitmap_index=2583, .adv_w=128, .box_h=7, .box_w=7, .ofs_x=0, .ofs_y=1 },
  /* Unicode: U+2669 (♩) [QUARTER NOTE] */
  { .bitmap_index=2590, .adv_w=128, .box_h=8, .box_w=5, .ofs_x=1, .ofs_y=0 },
  /* Unicode: U+266A (♪) [EIGHTH NOTE] */
  { .bitmap_index=2595, .adv_w=128, .box_h=8, .box_w=8, .ofs_x=0, .ofs_y=0 },
  /* Unicode: U+266B (♫) [BEAMED EIGHTH NOTES] */
  { .bitmap_index=2603, .adv_w=128, .box_h=8, .box_w=8, .ofs_x=0, .ofs_y=0 },
  /* Unicode: U+266C (♬) [BEAMED SIXTEENTH NOTES] */
  { .bitmap_index=2611, .adv_w=128, .box_h=8, .box_w=8, .ofs_x=0, .ofs_y=0 },
  /* Unicode: U+268A (⚊) [MONOGRAM FOR YANG] */
  { .bitmap_index=2619, .adv_w=128, .box_h=1, .box_w=7, .ofs_x=0, .ofs_y=1 },
  /* Unicode: U+268B (⚋) [MONOGRAM FOR YIN] */
  { .bitmap_index=2620, .adv_w=128, .box_h=1, .box_w=7, .ofs_x=0, .ofs_y=1 },
  /* Unicode: U+268C (⚌) [DIGRAM FOR GREATER YANG] */
  { .bitmap_index=2621, .adv_w=128, .box_h=3, .box_w=7, .ofs_x=0, .ofs_y=1 },
  /* Unicode: U+268D (⚍) [DIGRAM FOR LESSER YIN] */
  { .bitmap_index=2624, .adv_w=128, .box_h=3, .box_w=7, .ofs_x=0, .ofs_y=1 },
  /* Unicode: U+268E (⚎) [DIGRAM FOR LESSER YANG] */
  { .bitmap_index=2627, .adv_w=128, .box_h=3, .box_w=7, .ofs_x=0, .ofs_y=1 },
  /* Unicode: U+268F (⚏) [DIGRAM FOR GREATER YIN] */
  { .bitmap_index=2630, .adv_w=128, .box_h=3, .box_w=7, .ofs_x=0, .ofs_y=1 },
  /* Unicode: U+26AA (⚪) [MEDIUM WHITE CIRCLE] */
  { .bitmap_index=2633, .adv_w=128, .box_h=5, .box_w=6, .ofs_x=1, .ofs_y=1 },
  /* Unicode: U+26AB (⚫) [MEDIUM BLACK CIRCLE] */
  { .bitmap_index=2637, .adv_w=128, .box_h=5, .box_w=6, .ofs_x=1, .ofs_y=1 },
  /* Unicode: U+26AC (⚬) [MEDIUM SMALL WHITE CIRCLE] */
  { .bitmap_index=2641, .adv_w=128, .box_h=5, .box_w=5, .ofs_x=1, .ofs_y=2 },
  /* Unicode: U+2708 (✈) [AIRPLANE] */
  { .bitmap_index=2645, .adv_w=128, .box_h=8, .box_w=8, .ofs_x=0, .ofs_y=0 },
  /* Unicode: U+2734 (✴) [EIGHT POINTED BLACK STAR] */
  { .bitmap_index=2653, .adv_w=128, .box_h=7, .box_w=7, .ofs_x=0, .ofs_y=1 },
};

/* Unicode list for sparse set 0:
 * U+2190, U+2191, U+2192, U+2194, U+2196, U+2197, U+2198, U+2199,
 * U+21A5, U+21A8, U+21E6, U+21E7, U+21E8, U+21E9,
 */
static const uint16_t unicode_list_0[] = {
    0x0000, 0x0001, 0x0002, 0x0004, 0x0006, 0x0007, 0x0008, 0x0009,
    0x0015, 0x0018, 0x0056, 0x0057, 0x0058, 0x0059,
};

/* Unicode list for sparse set 1:
 * U+2625, U+2628, U+262F, U+2630, U+2631, U+2632, U+2633, U+2634,
 * U+2635, U+2636, U+2637, U+2639, U+263A, U+263B, U+2660, U+2661,
 * U+2662, U+2663, U+2664, U+2665, U+2666, U+2667, U+2669, U+266A,
 * U+266B, U+266C, U+268A, U+268B, U+268C, U+268D, U+268E, U+268F,
 * U+26AA, U+26AB, U+26AC, U+2708, U+2734,
 */
static const uint16_t unicode_list_1[] = {
    0x0000, 0x0003, 0x000a, 0x000b, 0x000c, 0x000d, 0x000e, 0x000f,
    0x0010, 0x0011, 0x0012, 0x0014, 0x0015, 0x0016, 0x003b, 0x003c,
    0x003d, 0x003e, 0x003f, 0x0040, 0x0041, 0x0042, 0x0044, 0x0045,
    0x0046, 0x0047, 0x0065, 0x0066, 0x0067, 0x0068, 0x0069, 0x006a,
    0x0085, 0x0086, 0x0087, 0x00e3, 0x010f,
};

/* Collect the unicode lists and glyph_id offsets
 */
static const lv_font_fmt_txt_cmap_t cmaps[] = {
    { .range_start       = 32,
      .range_length      = 95,
      .type              = LV_FONT_FMT_TXT_CMAP_FORMAT0_TINY,
      .glyph_id_start    = 1,
    },
    { .range_start       = 160,
      .range_length      = 96,
      .type              = LV_FONT_FMT_TXT_CMAP_FORMAT0_TINY,
      .glyph_id_start    = 96,
    },
    { .range_start       = 8592,
      .range_length      = 90,
      .type              = LV_FONT_FMT_TXT_CMAP_SPARSE_TINY,
      .glyph_id_start    = 192,
      .unicode_list      = unicode_list_0,
      .list_length       = 14,
    },
    { .range_start       = 9472,
      .range_length      = 112,
      .type              = LV_FONT_FMT_TXT_CMAP_FORMAT0_TINY,
      .glyph_id_start    = 206,
    },
    { .range_start       = 9600,
      .range_length      = 128,
      .type              = LV_FONT_FMT_TXT_CMAP_FORMAT0_TINY,
      .glyph_id_start    = 318,
    },
    { .range_start       = 9765,
      .range_length      = 272,
      .type              = LV_FONT_FMT_TXT_CMAP_SPARSE_TINY,
      .glyph_id_start    = 446,
      .unicode_list      = unicode_list_1,
      .list_length       = 37,
    },
};

/* Store all the custom data of the font
 */
static lv_font_fmt_txt_dsc_t font_dsc = {
    .glyph_bitmap = glyph_bitmap,
    .glyph_dsc    = glyph_dsc,
    .cmaps        = cmaps,
    .cmap_num     = 6,
    .bpp          = 1,
};

/* Initialize a public general font descriptor
 */
lv_font_t font_unscii_8 = {
    .dsc               = &font_dsc,
    .get_glyph_bitmap  = lv_font_get_bitmap_fmt_txt,
    .get_glyph_dsc     = lv_font_get_glyph_dsc_fmt_txt,
    .line_height       = 8,
    .base_line         = 0,
};

#endif
