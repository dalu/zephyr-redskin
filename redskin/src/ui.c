/*
 * Copyright (c) 2019-2020
 * Stephane D'Alu, Inria Chroma / Inria Agora, INSA Lyon, CITI Lab.
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#include <zephyr/kernel.h>
#include <zephyr/device.h>
#include <zephyr/drivers/gpio.h>
#include <zephyr/logging/log.h>

#include <spank/spank.h>
#include <blinker/blink.h>
#include <button/button.h>

#include "ui.h"

LOG_MODULE_DECLARE(main);


/*
 * Retrieve device tree information.
 *
 * XXX: UI_BTN0_FLAGS (aka button0) should be fixed in decawave board
 *      https://github.com/zephyrproject-rtos/zephyr/pull/25460
 */

/* UI LED 0 */
#define UI_LED0_NODE	DT_ALIAS(ui_led0)
#define UI_LED0         DT_GPIO_CTLR(UI_LED0_NODE, gpios)
#define UI_LED0_PIN     DT_GPIO_PIN  (UI_LED0_NODE, gpios)
#define UI_LED0_FLAGS   DT_GPIO_FLAGS(UI_LED0_NODE, gpios)

/* UI LED 1 */
#define UI_LED1_NODE    DT_ALIAS(ui_led1)
#define UI_LED1         DT_GPIO_CTLR(UI_LED1_NODE, gpios)
#define UI_LED1_PIN     DT_GPIO_PIN  (UI_LED1_NODE, gpios)
#define UI_LED1_FLAGS   DT_GPIO_FLAGS(UI_LED1_NODE, gpios)

/* UI Button 0 */
#define UI_BTN0_NODE	DT_ALIAS(ui_btn0)
#define UI_BTN0		DT_GPIO_CTLR(UI_BTN0_NODE, gpios)
#define UI_BTN0_PIN	DT_GPIO_PIN(UI_BTN0_NODE, gpios)
#define UI_BTN0_FLAGS	(DT_GPIO_FLAGS(UI_BTN0_NODE, gpios)| GPIO_ACTIVE_LOW)


/*
 * Interfacing gpio with blinker api
 */

static const struct device *ui_led0_dev = DEVICE_DT_GET(UI_LED0);
static const struct device *ui_led1_dev = DEVICE_DT_GET(UI_LED1);

static void ui_led1_set_state(bool state) {
    gpio_pin_set(ui_led1_dev, UI_LED1_PIN, state);
}

static void ui_led0_set_state(bool state) {
    gpio_pin_set(ui_led0_dev, UI_LED0_PIN, state);
}

static blink_t ui_blink_led0  = {
    .set_state = ui_led0_set_state,
    .separator = 4,
};
static blink_t ui_blink_led1  = {
    .set_state = ui_led1_set_state,
    .separator = 4,
};


/*
 * Interfacing gpio with button api
 */

struct gpio_button {
    const struct device *dev;
    uint32_t             pin;
    struct gpio_callback gpio_cb;
    button_t *           btn;
};

static struct gpio_button gpio_button[1];

static void
gpio_button_handler(const struct device *dev,
		    struct gpio_callback *cb, uint32_t pins)
{
    struct gpio_button *gb = CONTAINER_OF(cb, struct gpio_button, gpio_cb);   
    bool val               = gpio_pin_get(gb->dev, gb->pin);

    button_set_low_level_state(gb->btn, val);
}

static int
gpio_button_init(struct gpio_button *gb,
		 const struct device *dev, int flags, uint32_t pin,
		 gpio_callback_handler_t handler, button_t *btn)
{
    int rc;

    /* Configure pin */
    if (((rc = gpio_pin_configure(dev, pin,
		    flags | GPIO_INPUT)) < 0) ||
	((rc = gpio_pin_interrupt_configure(dev, pin,
		    GPIO_INT_EDGE_BOTH)) < 0)) {
	return rc;
    }

    /* Initialise gpio_button structure */
    gb->dev = dev;
    gb->pin = pin;
    gb->btn = btn;
    gpio_init_callback(&gb->gpio_cb, handler, BIT(pin));

    /* Add interrupt callback */
    if ((rc = gpio_add_callback(dev, &gb->gpio_cb)) < 0) {
	return rc;
    }

    /* Done */
    return 0;
}



/*
 * Buttons
 */

static button_t btns[] = {
     { .id       = 1,
       .mode     = BUTTON_FLG_PRESSED | BUTTON_FLG_DOUBLED | BUTTON_FLG_LONG,
     },
};

static void
button_callback(button_id_t id, uint8_t type, uint8_t flags)
{
    if (type != BUTTON_ACTION) // Only interested by action
	return;
    
    switch(id) {
    case 1:
	// Look at action, 
	// but don't make special handling for repeated or missed
	switch(flags & BUTTON_MASK_BASIC) {
        case BUTTON_CLICK        :                break;
        case BUTTON_LONG_CLICK   :                break;
        case BUTTON_DBLCLICK     : spank_start(); break;
        case BUTTON_LONG_DBLCLICK: spank_stop();  break;
        }
        break;
    }
}


/*
 * UI
 */

int ui_init(void) {
    /* Leds
     */
    gpio_pin_configure(ui_led0_dev, UI_LED0_PIN,
		       UI_LED0_FLAGS | GPIO_OUTPUT_INACTIVE);
    blink_init(&ui_blink_led0);

	
    gpio_pin_configure(ui_led1_dev, UI_LED1_PIN,
		       UI_LED1_FLAGS | GPIO_OUTPUT_INACTIVE);
    blink_init(&ui_blink_led1);

    /* Buttons
     */
    button_init(btns, 1, button_callback);
    
    if (gpio_button_init(&gpio_button[0], DEVICE_DT_GET(UI_BTN0),
			 UI_BTN0_FLAGS, UI_BTN0_PIN,
			 gpio_button_handler, &btns[0]) < 0) {
	LOG_ERR("Failed to get/initialise '%s' device",
		DEVICE_DT_NAME(UI_BTN0));
    }
    
    /* Job's done 
     */
    return 0;
}


void ui_blink(int id, blink_code_t code) {
    blink_t *blink = NULL;
    switch (id) {
    case UI_LED_0: blink = &ui_blink_led0; break;
    case UI_LED_1: blink = &ui_blink_led1; break;
    default: return;
    }
    blink_code(blink, code, BLINK_SCHEDULED_WAIT_BLINK);
}
