/*
 * Copyright (c) 2021
 * Stephane D'Alu, Inria Chroma / Inria Agora, INSA Lyon, CITI Lab.
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#include <spank/uav.h>
#include <spank/uav-puppet.h>

#include "extio.h"

#include <zephyr/logging/log.h>
LOG_MODULE_REGISTER(extio, CONFIG_REDSKIN_EXTIO_LOG_LEVEL);

#if defined(CONFIG_REDSKIN_SHELL)
#include <string.h>
#include <zephyr/shell/shell.h>
#endif


static struct extio_flavor *extio_flavor_ctx[] = {
    [EXTIO_FLAVOR_I2C] = NULL,
    [EXTIO_FLAVOR_USB] = NULL,    
};


static void
spank_extio_uav_distance_cb(void *args) {
    for (int i = 0 ; i < ARRAY_SIZE(extio_flavor_ctx) ; i++) {
	struct extio_flavor *flavor = extio_flavor_ctx[i];
	if (flavor && flavor->distance_ready_cb)
	    flavor->distance_ready_cb(args);
    }
}


void
extio_init(void) {
    // Register UAV callbacks (uav-puppet specfic)
    spank_uav_set_callbacks(spank_extio_uav_distance_cb);
}


void
extio_register_flavor(int id, struct extio_flavor *flavor)
{
    extio_flavor_ctx[id] = flavor;

    LOG_INF("ExtIO %s flavor registered",  flavor->name);
}


#if defined(CONFIG_REDSKIN_SHELL)
void
extio_shell_info(const struct shell *shell)
{
    char flavors[32] = "";
    for (int i = 0, count = 0 ; i < ARRAY_SIZE(extio_flavor_ctx) ; i++) {
	if (extio_flavor_ctx[i] == NULL) continue;
	if (count != 0) { strncat(flavors, ", ", sizeof(flavors)-1); }
	strncat(flavors, extio_flavor_ctx[i]->name, sizeof(flavors)-1);
    }

    shell_print(shell, "Flavor     : %s", flavors);
    for (int i = 0 ; i < ARRAY_SIZE(extio_flavor_ctx) ; i++) {
	if (extio_flavor_ctx[i] == NULL) continue;
	extio_flavor_ctx[i]->shell.info(shell);
    }
}
#endif

		 
