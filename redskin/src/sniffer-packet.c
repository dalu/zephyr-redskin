/*
 * Copyright (c) 2020-2022
 * Stephane D'Alu, Inria Chroma / Inria Agora, INSA Lyon, CITI Lab.
 *
 * SPDX-License-Identifier: Apache-2.0
 */


#include <zephyr/sys/printk.h>

#include <spank/packet.h>
#include "sniffer.h"

static volatile uint16_t sniffer_packet_filter = SNIFFER_PACKET_FILTER_NONE;

void
sniffer_packet_set_filter(uint16_t filter)
{
    sniffer_packet_filter = filter;
}


uint16_t
sniffer_packet_get_filter(void)
{
    return sniffer_packet_filter;
}



/*
 * Implement the weak symbol (spank_io_sniffer_packet) to report 
 * on incoming packets.
 * NOTE: SPANK need to be compiled with SPANK_IO_SNIFFER_PACKET_SUPPORT enabled
 */

void
spank_io_sniffer_packet(spank_io_rx_ctx_t *rx_ctx, ssize_t size)
{
    spank_packet_t *pkt  = rx_ctx->packet;
    spank_addr_t    addr = rx_ctx->src_addr;

    if (size < 0) {
	printk("Packet sniffer: received error: %d\n", size);
	return;
    } else if (size < sizeof(spank_packet_hdr_t)) {
	printk("Packet sniffer: received short packet (len=%d)\n", size);
	return;
    }

    switch(pkt->hdr.type) {
    case SPANK_PKT_TYPE_ELECT:
	if (sniffer_packet_filter & SNIFFER_PACKET_FILTER_ELECT) {
	    printk("SPANK ELECT[%lx ->]: %s\n",
		   (long)addr, pkt->msg.data);
	}
	break;

    case SPANK_PKT_TYPE_NAVCTRL:
	if (sniffer_packet_filter & SNIFFER_PACKET_FILTER_NAVCTRL) {
	    printk("SPANK NAVCTRL[%lx ->]: %s\n",
		   (long)addr, pkt->msg.data);
	}
	break;

    case SPANK_PKT_TYPE_MSG:
	if (sniffer_packet_filter & SNIFFER_PACKET_FILTER_MSG) {
	    printk("SPANK MSG[%lx ->]: %s\n",
		   (long)addr, pkt->msg.data);
	}
	break;

    case SPANK_PKT_TYPE_TWR_POLL:
	if (sniffer_packet_filter & SNIFFER_PACKET_FILTER_TWR_POLL) {
	    printk("SPANK POLL[%lx -> %lx]\n",
		   (long)addr,  (long)rx_ctx->dst_addr);
	}
	break;

    case SPANK_PKT_TYPE_TWR_ANSWER:
	if (sniffer_packet_filter & SNIFFER_PACKET_FILTER_TWR_ANSWER) {
	    printk("SPANK ANSWER[%lx -> %lx]\n",
		   (long)addr,  (long)rx_ctx->dst_addr);
	}
	break;

    case SPANK_PKT_TYPE_TWR_FINAL:
	if (sniffer_packet_filter & SNIFFER_PACKET_FILTER_TWR_FINAL) {
	    printk("SPANK FINAL[%lx -> %lx]\n",
		   (long)addr,  (long)rx_ctx->dst_addr);
	}
	break;

    case SPANK_PKT_TYPE_TWR_REPORT:
	if (sniffer_packet_filter & SNIFFER_PACKET_FILTER_TWR_REPORT) {
	    printk("SPANK REPORT[%lx -> %lx]\n",
		   (long)addr,  (long)rx_ctx->dst_addr);
	}
	break;
    }
}
