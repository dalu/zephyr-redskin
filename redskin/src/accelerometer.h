/*
 * Copyright (c) 2019
 * Stephane D'Alu, Inria Chroma / Inria Agora, INSA Lyon, CITI Lab.
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#ifndef __ACCELEROMETER__H
#define __ACCELEROMETER__H

#include <zephyr/drivers/sensor.h>

int accelerometer_init(void);
int accelerometer_config_sampling(uint16_t frequency, uint16_t fullscale);
int accelerometer_config_slope(uint32_t threshold, uint32_t duration);
int accelerometer_trigger(sensor_trigger_handler_t handler);

void accelerometer_trigger_handler(const struct device *dev, struct sensor_trigger *trig);

#endif
