/*
 * Copyright (c) 2020-2022
 * Stephane D'Alu, Inria Chroma / Inria Agora, INSA Lyon, CITI Lab.
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#ifndef __SNIFFER__H
#define __SNIFFER__H

#include <stdint.h>

#define SNIFFER_PACKET_FILTER_TWR_POLL		0x0001
#define SNIFFER_PACKET_FILTER_TWR_ANSWER	0x0002
#define SNIFFER_PACKET_FILTER_TWR_FINAL		0x0004
#define SNIFFER_PACKET_FILTER_TWR_REPORT	0x0008

#define SNIFFER_PACKET_FILTER_ELECT		0x1000
#define SNIFFER_PACKET_FILTER_NAVCTRL		0x2000
#define SNIFFER_PACKET_FILTER_MSG		0x4000

#define SNIFFER_PACKET_FILTER_ALL		0xffff 
#define SNIFFER_PACKET_FILTER_NONE		0x0000


/**
 * Select the filter used to dump packet
 *
 * @param filter	or-ed list of SNIFFER_PACKET_FILTER_*
 */
void sniffer_packet_set_filter(uint16_t filter);


/**
 * Get the filter currently applied for dumping packet
 *
 * @return or-ed list of SNIFFER_PACKET_FILTER_*
 */
uint16_t sniffer_packet_get_filter(void);




#define SNIFFER_FRAME_OUTPUT_EXTIO_USB		0x0001
#define SNIFFER_FRAME_OUTPUT_CONSOLE		0x0002

#define SNIFFER_FRAME_OUTPUT_ALL		0xffff 
#define SNIFFER_FRAME_OUTPUT_NONE		0x0000


/**
 * Select the backend for reporting dumped frames.
 * @note Event if a backend is selected in doesn't mean
 *       it has been compiled in.
 *
 * @param output	or-ed list of SNIFFER_FRAME_OUTPUT_*
 */
void sniffer_frame_set_output(uint16_t output);

/**
 * Get the selected backend used for reporting dumped frames.
 *
 * @return or-ed list of SNIFFER_FRAME_OUTPUT_*
 */
uint16_t sniffer_frame_get_output(void);


#endif
