/*
 * Copyright (c) 2019-2023
 * Stephane D'Alu, Inria Chroma / Inria Agora, INSA Lyon, CITI Lab.
 *
 * SPDX-License-Identifier: Apache-2.0
 */

/* NOTE: the Nordic SDK (nrfx) is directly used to create the I2C slave
 *       as this is currently not supported by Zephyr
 * SEE : https://github.com/zephyrproject-rtos/zephyr/issues/21445
 *       https://devzone.nordicsemi.com/f/nordic-q-a/57138/nrfx-and-twis-samples
 */

/* SEE: https://www.nxp.com/docs/en/user-guide/UM10204.pdf
 *      https://stackoverflow.com/questions/37040696/nack-and-ack-responses-on-i2c-bus
 *
 * There are five conditions that lead to the generation of a NACK:
 * - No receiver is present on the bus with the transmitted address so
 *   there is no device to respond with an acknowledge.
 * - The receiver is unable to receive or transmit because it is
 *   performing some real-time function and is not ready to start
 *   communication with the master. 
 * - During the transfer, the receiver gets data or commands that it
 *   does not understand.
 * - During the transfer, the receiver cannot receive any more data bytes.
 * - A master-receiver must signal the end of the transfer to the
 *   slave transmitter.
 */

/* BUGS in Raspberry Pi (BCM2835):
 *  - Clock stretching
 *    https://elinux.org/BCM2835_datasheet_errata#p35_I2C_clock_stretching
 *
 *  - Not able to support more than one read message per I2C transfer
 *    https://elixir.bootlin.com/linux/v5.7.8/source/drivers/i2c/busses/i2c-bcm2835.c#L350
 */

/* TODO:
 *  - Fix possible incoherent information when retrieving information
 *    after distance measurement in the same I2C transaction.
 *    Should save information provided by spank_uav_get_distance_measurement
 *    instead of calling spank_uav_get_metainfo later.
 *  - Implement RESET pin
 */

#include <zephyr/sys/util.h>
#include <zephyr/kernel.h>
#include <zephyr/device.h>
#include <zephyr/drivers/gpio.h>
#include <zephyr/drivers/i2c.h>
#include <zephyr/drivers/pinctrl.h>
#include <nrfx_twis.h>

#include <string.h>
#include <math.h>

#include <spank/osal.h>
#include <spank/types.h>
#include <spank/uav.h>
#include <spank/uav-puppet.h>
#include <spank/io.h>
#include <spank/version.h>
#include <spank/extio/types.h>
#include <spank/extio/i2c.h>

#include <info-str/common.h>
#include "../extio.h"
#include "../extio-helpers.h"
#include "extio.h"

#include <zephyr/logging/log.h>
LOG_MODULE_DECLARE(extio, CONFIG_REDSKIN_EXTIO_LOG_LEVEL);
#if defined(CONFIG_REDSKIN_SHELL)
#include <zephyr/shell/shell.h>
#endif



/*======================================================================*/
/* Device tree information                                              */
/*======================================================================*/


#define NODE_EXTIO_I2C 		DT_PATH      (extio_i2c)
#define NODE_EXTIO_I2C_SLAVE	DT_PROP      (NODE_EXTIO_I2C, i2c_slave)
#define EXTIO_I2C_ADDRESS_0  	DT_PROP      (NODE_EXTIO_I2C_SLAVE, address_0)
#define EXTIO_I2C_IRQ 		DT_IRQ_BY_IDX(NODE_EXTIO_I2C_SLAVE, 0, irq)
#define EXTIO_I2C_IRQ_PRIORITY 	DT_IRQ_BY_IDX(NODE_EXTIO_I2C_SLAVE, 0, priority)

#define EXTIO_READY 		DT_GPIO_CTLR (NODE_EXTIO_I2C, ready_gpios)
#define EXTIO_READY_PIN 	DT_GPIO_PIN  (NODE_EXTIO_I2C, ready_gpios)
#define EXTIO_READY_FLAGS 	DT_GPIO_FLAGS(NODE_EXTIO_I2C, ready_gpios)

#define EXTIO_RESET		DT_GPIO_CTLR (NODE_EXTIO_I2C, reset_gpios)
#define EXTIO_RESET_PIN		DT_GPIO_PIN  (NODE_EXTIO_I2C, reset_gpios)
#define EXTIO_RESET_FLAGS	DT_GPIO_FLAGS(NODE_EXTIO_I2C, reset_gpios)




/*======================================================================*/
/* Forward declaration                                                  */
/*======================================================================*/

struct extio_i2c_ctx;




/*======================================================================*/
/* Local types                                                          */
/*======================================================================*/

/* List of deferred work
 */
enum extio_i2c_deferred_type {
    EXTIO_I2C_DEFERRED_WRITE_CONFIG_BEHAVIOUR,
    EXTIO_I2C_DEFERRED_READ_CONFIG_BEHAVIOUR,    
    EXTIO_I2C_DEFERRED_WRITE_POSTURE,
    EXTIO_I2C_DEFERRED_READ_POSTURE,
    EXTIO_I2C_DEFERRED_READ_DISTANCE_MEASUREMENT,
    EXTIO_I2C_DEFERRED_READ_INFO,
};


/* Deferred work (to be run in a thread context)
 */
struct extio_i2c_deferred {
    struct k_work 		   work;	// Zephyr work
    struct extio_i2c_ctx 	  *ctx;		// Context
    enum   extio_i2c_deferred_type type;	// Action to perform
    bool 			   in_progress; // In progress (for debug)
};


/* Stalled work (delayable run in a thread context)
 * Should be canceled if we abort the transaction extio_i2c_abort(),
 * or the transaction successfully terminate (stop bit).
 * NOTE: we consider the whole transaction, not individual read/write
 */
struct extio_i2c_stalled {
    struct k_work_delayable 	   delayable_work; // Zephyr delayable work
    struct extio_i2c_ctx 	  *ctx;		   // Context
};


/* ExtIO I2C Context
 */
struct extio_i2c_ctx {
    nrfx_twis_t        twis;		// NRFx TWIS peripheral
    nrfx_twis_config_t twis_config;	// NRFx TWIS configuration
    const struct pinctrl_dev_config *pcfg; // Pin Control configuration
    bool               i2c_stop_bit;	// Keep track of I2C stop bit
    struct extio_i2c_deferred deferred; // Use to defer action to a work queue
#if CONFIG_REDSKIN_EXTIO_I2C_STALLED > 0
    struct extio_i2c_stalled  stalled;	// Stalled transaction detection
#endif
    uint8_t            dummy;		// Dummy byte, to discard DMA transfer

    // I2C transaction
    struct {
	uint8_t state;			// Transaction state
#define EXTIO_I2C_TRX_STATE_WAITING_REG  0
#define EXTIO_I2C_TRX_STATE_WAITING_VAL  1
#define EXTIO_I2C_TRX_STATE_WAITING_INFO 2
#define EXTIO_I2C_TRX_STATE_ERROR	 3
	uint8_t reg;			// Identified I2C register
	union {				// Temporary memory for RX/TX
	    uint8_t			       behaviour;
	    spank_extio_posture_t 	       posture;
	    spank_extio_distance_measurement_t distance_measurement;
	    spank_extio_info_t                 info;
	} buffer;
    } trx;

    // Data Ready 
    struct {				// Keep track of pending data
	uint8_t flags;
#define EXTIO_I2C_FLG_DATA_READY_DISTANCE_MEASUREMENT			\
	SPANK_I2C_FLG_DATA_READY_DISTANCE_MEASUREMENT
#define EXTIO_I2C_FLG_DATA_READY_CONTROL				\
	SPANK_I2C_FLG_DATA_READY_CONTROL
    } data_ready;

    // GPIO Lines
    struct {
	struct {			// Reset line
	    const struct device *dev;
	    uint32_t       pin;
	} reset;
	struct {			// Data Ready line
	    const struct device *dev;
	    uint32_t       pin;
	} ready;
    } gpio;

    // Configuration
    struct {
	uint8_t report;			// Mask for data-ready interrupt
    } config;
};




/*======================================================================*/
/* Local variables                                                      */
/*======================================================================*/

PINCTRL_DT_DEFINE(NODE_EXTIO_I2C_SLAVE);

static struct extio_i2c_ctx extio_i2c_ctx = {
    .twis          = NRFX_TWIS_INSTANCE(1),
    .twis_config   = { .addr = { EXTIO_I2C_ADDRESS_0, 0x00 },
		       .skip_gpio_cfg = true,
		       .skip_psel_cfg = true },
    .pcfg          = PINCTRL_DT_DEV_CONFIG_GET(NODE_EXTIO_I2C_SLAVE),
    .trx.state     = EXTIO_I2C_TRX_STATE_WAITING_REG,
    .config.report = 0xff,
};


static char _spank_version[]   = SPANK_VERSION;
static char _redskin_version[] = INFO_STR_APP_VERSION;




/*======================================================================*/
/* Local functions                                                      */
/*======================================================================*/

#if defined(CONFIG_REDSKIN_SHELL)
static void
extio_i2c_shell_info(const struct shell *shell)
{
    shell_print(shell, "I2C addr   : 0x%02x",  EXTIO_I2C_ADDRESS_0);
#if CONFIG_REDSKIN_EXTIO_I2C_STALLED > 0
    shell_print(shell, "I2C stalled: %d µsec", CONFIG_REDSKIN_EXTIO_I2C_STALLED);
#else
    shell_print(shell, "I2C stalled: n/a");
#endif
}
#endif


/* Force an abort of the I2C by triggering the TWIS TASK STOP
 */
static inline void
_extio_i2c_abort(struct extio_i2c_ctx *ctx)
{
    ctx->trx.state = EXTIO_I2C_TRX_STATE_ERROR;
    nrf_twis_task_trigger(ctx->twis.p_reg, NRF_TWIS_TASK_STOP);
}
static inline void
extio_i2c_abort(struct extio_i2c_ctx *ctx)
{
    _extio_i2c_abort(ctx);
#if CONFIG_REDSKIN_EXTIO_I2C_STALLED > 0
    k_work_cancel_delayable(&ctx->stalled.delayable_work);
#endif
}



/* Assert interrupt on the gpio ready.
 */
static inline void
extio_i2c_assert_data_ready_interrupt(struct extio_i2c_ctx *ctx)
{
    // Only notify for selected reporting
    if (! (ctx->data_ready.flags & ctx->config.report))
	return;

    // It's ok to not test if GPIO line is configured
    // as gpio_pin_set() will fail if not configured.
    gpio_pin_set(ctx->gpio.ready.dev, ctx->gpio.ready.pin, 1);
}



/* De-assert interrupt on the gpio ready.
 */
static inline void
extio_i2c_deassert_data_ready_interrupt(struct extio_i2c_ctx *ctx)
{
    // It's ok to not test if GPIO line is configured
    // as gpio_pin_set() will fail if not configured.
    gpio_pin_set(ctx->gpio.ready.dev, ctx->gpio.ready.pin, 0);

    // Wait a little. Is it necessary? Is it enough?
    // If more is needed it should be run outside ISR context
    // as currently called in I2C interrupt handler
    k_busy_wait(1);
}



/* Set data ready line (if necessary), and generate interrupt.
 */
static inline void
extio_i2c_set_data_ready(struct extio_i2c_ctx *ctx, int flags)
{
    ctx->data_ready.flags |= flags;
    extio_i2c_assert_data_ready_interrupt(ctx);
}



/* Clear data ready line (if necessary)
 */
static inline void
extio_i2c_clear_data_ready(struct extio_i2c_ctx *ctx, int flags)
{
    ctx->data_ready.flags &= ~flags;
}



/* Set the UAV behaviour
 */
static inline void
extio_i2c_set_config_behaviour(struct extio_i2c_ctx *ctx,
			       uint8_t *extio_behaviour)
{
    int uav_behaviour;
    switch(*extio_behaviour) {
    case SPANK_I2C_VAL_CONFIG_BEHAVIOUR_DROP_DEFAULT:
	uav_behaviour = SPANK_UAV_DROP_BEHAVIOUR_DEFAULT;
	break;
    case SPANK_I2C_VAL_CONFIG_BEHAVIOUR_DROP_OLDEST:
	uav_behaviour = SPANK_UAV_DROP_BEHAVIOUR_OLDEST;
	break;
    case SPANK_I2C_VAL_CONFIG_BEHAVIOUR_DROP_RECENT:
	uav_behaviour = SPANK_UAV_DROP_BEHAVIOUR_RECENT;
	break;
    case SPANK_I2C_VAL_CONFIG_BEHAVIOUR_DROP_RANDOM:
	uav_behaviour = SPANK_UAV_DROP_BEHAVIOUR_RANDOM;
	break;
    default:
	LOG_WRN("unknown behaviour requested (%d)", *extio_behaviour);
	return;
    }
    
    spank_uav_set_drop_behaviour(uav_behaviour);
}



/* Set the UAV behaviour
 */
static inline void
extio_i2c_get_config_behaviour(struct extio_i2c_ctx *ctx,
			       uint8_t *extio_behaviour)
{
    switch (spank_uav_get_drop_behaviour()) {
    case SPANK_UAV_DROP_BEHAVIOUR_DEFAULT:
	*extio_behaviour = SPANK_I2C_VAL_CONFIG_BEHAVIOUR_DROP_DEFAULT;
	break;
    case SPANK_UAV_DROP_BEHAVIOUR_OLDEST:
	*extio_behaviour = SPANK_I2C_VAL_CONFIG_BEHAVIOUR_DROP_OLDEST;
	break;
    case SPANK_UAV_DROP_BEHAVIOUR_RECENT:
	*extio_behaviour = SPANK_I2C_VAL_CONFIG_BEHAVIOUR_DROP_RECENT;
	break;
    case SPANK_UAV_DROP_BEHAVIOUR_RANDOM:
	*extio_behaviour = SPANK_I2C_VAL_CONFIG_BEHAVIOUR_DROP_RANDOM;
	break;
    default:
	__ASSERT(0, "unhandled uav drop behaviour case");
	break;
    }
}


/* Set the UAV posture (and perform format conversion)
 */
static inline void
extio_i2c_set_posture(struct extio_i2c_ctx *ctx,
		      spank_extio_posture_t *extio_posture)
{
    spank_posture_t posture;
    spank_posture_from_extio_to_native(extio_posture, &posture);
    spank_uav_set_posture(&posture);
}



/* Get the UAV posture (and perform format conversion)
 */
static inline void
extio_i2c_get_posture(struct extio_i2c_ctx *ctx,
		      spank_extio_posture_t *extio_posture)
{
    spank_posture_t posture;
    spank_uav_get_posture(&posture);
    spank_posture_from_native_to_extio(&posture, extio_posture);
}



/* Get UAV distance measurement (and perform format conversion)
 */
static inline void
extio_i2c_get_distance_measurement(struct extio_i2c_ctx *ctx,
			spank_extio_distance_measurement_t *extio_dm)
{
    // Retrieve distance measure from UAV (uav-puppet specific)
    spank_uav_distance_measurement_t *uav_dm =
	spank_uav_get_distance_measurement(NULL);

    // If no distance available, return 0xff-filled data
    // => User didn't check for data-ready, it is its fault for getting trash
    if (uav_dm == NULL) {
	memset(extio_dm, 0xff, sizeof(*extio_dm));
	return;
    }
    
    // Perform conversion
    spank_distance_measurement_from_native_to_extio(uav_dm, extio_dm);

    // If no more data, clear the data ready flag
    if (! uav_dm->has_more) {
	extio_i2c_clear_data_ready(ctx,
			   EXTIO_I2C_FLG_DATA_READY_DISTANCE_MEASUREMENT);
    }

    // Release distance 
    spank_uav_release_distance_measurement(uav_dm);
}



/* Get UAV information about collected distance measurements
 */
static inline void
extio_i2c_get_info(struct extio_i2c_ctx *ctx, spank_extio_info_t *info)
{
    spank_uav_metainfo_t uav_info;
    spank_uav_get_metainfo(&uav_info);
    info->pending = uav_info.pending;
    info->lost    = uav_info.lost;
}



/*
 * Callback for new distance available.
 */
static void
extio_i2c_distance_ready_cb(void *args)
{
    struct extio_i2c_ctx *ctx = &extio_i2c_ctx;

    extio_i2c_set_data_ready(ctx,
			     EXTIO_I2C_FLG_DATA_READY_DISTANCE_MEASUREMENT);
}



/* Create a deferred action.
 * A zephyr work queue will be used.
 */
static void
extio_i2c_defer(struct extio_i2c_ctx *ctx, enum extio_i2c_deferred_type type) {
    // Suspend TWIS peripheral
    // It will be resumed in a thread context by the submitted work 
    nrf_twis_task_trigger(ctx->twis.p_reg, NRF_TWIS_TASK_SUSPEND);

    // Specify context and type of deferred work
    __ASSERT(! ctx->deferred.in_progress,
	     "trying to submit in-progress work");
    ctx->deferred.ctx         = ctx;
    ctx->deferred.type        = type;
    ctx->deferred.in_progress = true;

    // Submit work to system work queue
    __ASSERT(! k_work_is_pending(&ctx->deferred.work),
	     "trying to submit pending work");
    k_work_submit(&ctx->deferred.work);
}



/* Handler associated to the stalled action (zephyr work item)
 * It will abort the transaction / reset the i2c bus
 */
#if CONFIG_REDSKIN_EXTIO_I2C_STALLED > 0
static void
extio_i2c_stalled_work(struct k_work *work)
{
    struct extio_i2c_stalled *stalled =
	CONTAINER_OF(k_work_delayable_from_work(work),
		     struct extio_i2c_stalled, delayable_work);
    struct extio_i2c_ctx     *ctx     = stalled->ctx;

    LOG_WRN("stalled transaction detected (aborting)");
    _extio_i2c_abort(ctx);
}
#endif



/* Handler associated to the deferred action (zephyr work item)
 * It will dispatch read/write action to specific functions,
 * and as a last action it will resume the TWIS peripheral 
 * ending clock stretching, this should ensure that we only need
 * to defer a single action (and have a single work item).
 *
 * => Deferred Write *must* perform state transition
 */
static void
extio_i2c_deferred_work(struct k_work *work)
{
    struct extio_i2c_deferred *deferred = CONTAINER_OF(work,
					       struct extio_i2c_deferred, work);
    struct extio_i2c_ctx      *ctx      = deferred->ctx;
    void    		      *buffer   = NULL;
    size_t                     bufsize  = 0;
    
    switch(deferred->type) {
    case EXTIO_I2C_DEFERRED_WRITE_CONFIG_BEHAVIOUR:
	extio_i2c_set_config_behaviour(ctx, &ctx->trx.buffer.behaviour);
	ctx->trx.state = EXTIO_I2C_TRX_STATE_WAITING_REG;
	goto write_done;

    case EXTIO_I2C_DEFERRED_READ_CONFIG_BEHAVIOUR:
	buffer  = &ctx->trx.buffer.behaviour;
	bufsize = sizeof(ctx->trx.buffer.behaviour);
	extio_i2c_get_config_behaviour(ctx, &ctx->trx.buffer.behaviour);
	goto read_done;
	
    case EXTIO_I2C_DEFERRED_WRITE_POSTURE:
	extio_i2c_set_posture(ctx, &ctx->trx.buffer.posture);
	ctx->trx.state = EXTIO_I2C_TRX_STATE_WAITING_REG;
	goto write_done;
	
    case EXTIO_I2C_DEFERRED_READ_POSTURE:
	buffer  = &ctx->trx.buffer.posture;
	bufsize = sizeof(ctx->trx.buffer.posture);
	extio_i2c_get_posture(ctx, &ctx->trx.buffer.posture);
	goto read_done;
	
    case EXTIO_I2C_DEFERRED_READ_DISTANCE_MEASUREMENT:
	buffer  = &ctx->trx.buffer.distance_measurement;
	bufsize = sizeof(ctx->trx.buffer.distance_measurement);
	extio_i2c_get_distance_measurement(ctx, &ctx->trx.buffer.distance_measurement);
	goto read_done;

    case EXTIO_I2C_DEFERRED_READ_INFO:
	buffer  = &ctx->trx.buffer.info;
	bufsize = sizeof(ctx->trx.buffer.info);
	extio_i2c_get_info(ctx, &ctx->trx.buffer.info);
	goto read_done;
    }

    __ASSERT(0, "i2c deferred work type not implemented");
    // Resume i2c as it was suspended by extio_i2c_defer() ...
    nrf_twis_task_trigger(ctx->twis.p_reg, NRF_TWIS_TASK_RESUME);
    // ... and stop it (abort)!
    extio_i2c_abort(ctx);
    goto done;
    
 read_done:
    // Notify i2c driver of buffer ready
    nrfx_twis_tx_prepare(&ctx->twis, buffer, bufsize);
 write_done:
    // Resume i2c as it was suspended by extio_i2c_defer()
    nrf_twis_task_trigger(ctx->twis.p_reg, NRF_TWIS_TASK_RESUME);
 done:
    deferred->in_progress = false;
}



/* Handling of write request event (WRITE_REQ)
 */
static inline void
extio_i2c_write_req(struct extio_i2c_ctx *ctx) {
    void    *buffer  = NULL;
    size_t   bufsize = 0;
    
    switch(ctx->trx.state) {
    // For the register we will only check later that it has been fully written.
    // Register existence will be validated at the next read/write request.
    case EXTIO_I2C_TRX_STATE_WAITING_REG:
	buffer  = &ctx->trx.reg;
	bufsize = sizeof(ctx->trx.reg);
#if CONFIG_REDSKIN_EXTIO_I2C_STALLED > 0
	k_work_schedule(&ctx->stalled.delayable_work,
			K_MSEC(CONFIG_REDSKIN_EXTIO_I2C_STALLED));
#endif
	goto zeroing;

    // Prepare for the data buffer
    case EXTIO_I2C_TRX_STATE_WAITING_VAL:
	switch(ctx->trx.reg) {

	// 1-byte configuration directly written to final memory
	// done event will be processed in ISR context
        // NOTE: don't clear memory before!

	case SPANK_I2C_REG_CONFIG_REPORT:
	    buffer  = &ctx->config.report;
	    bufsize = sizeof(ctx->config.report);
	    goto no_clear;

	 // The following will require used of temporary shared memory
	 // (ctx->trx.buffer), so ensure that previous read/write has
	 // finished with it by giving enough time (perform clock stretching)
	    
	case SPANK_I2C_REG_CONFIG_BEHAVIOUR:
	    buffer  = &ctx->trx.buffer.behaviour;
	    bufsize = sizeof(ctx->trx.buffer.behaviour);
	    goto zeroing;
	    
	case SPANK_I2C_REG_POSTURE:
	    buffer  = &ctx->trx.buffer.posture;
	    bufsize = sizeof(ctx->trx.buffer.posture);
	    goto zeroing;
	}

	// -- Shouldn't happen if user read the doc
	goto unexpected_request;
    }

    // Unexpected request
 unexpected_request:
    LOG_WRN("moving to ERROR (unexpected write request) [state=%d, reg=0x%02x]",
	    ctx->trx.state, ctx->trx.reg);
    extio_i2c_abort(ctx);
    return;

    // Ensure memory clean state
 zeroing:
    memset(buffer, 0, bufsize);

 no_clear:
    // Notify TWIS for DMA transfert
    //  - end clock stretching
    //  - allow driver to change state
    nrfx_twis_rx_prepare(&ctx->twis, buffer, bufsize);
    return;
}



// -- Break-up of NRFX TWIS events handling -----------------------------
//
// As a general rule:
// - the DMA transfer    will be started in the {READ,WRITE}_REQ
// - the data processing will be starter in the {READ_REQ, WRITE_DONE}
//
// It will be necessary to defer some action to a thread to be able
// to acquire lock on subsystem components, this deferring will be
// framed by a SUSPEND/RESUME to ensure that only 1 action at a time
// is deferred (TWIS event being suspended, and clock stretching is done)

/* Handling of write done event (WRITE_DONE)
 * => State transition is handled here only for immediate write, the
 *    deferred-write must perform the transition as well.
 * => Incorrect state are supposed to be dealt with in the WRITE_REQ
 */
static inline void
extio_i2c_write_done(struct extio_i2c_ctx *ctx, size_t size) {
    switch(ctx->trx.state) {
    // For the register we only checked that it has been fully written.
    // Register existence will be validated at the next read/write request.
    case EXTIO_I2C_TRX_STATE_WAITING_REG:
	if (size != 1) {
	    LOG_WRN("moving to ERROR (register write must be 1-byte)");
	    extio_i2c_abort(ctx);
	    return;
	}
	ctx->trx.state = EXTIO_I2C_TRX_STATE_WAITING_VAL;
	break;

    // Writing value to the register.
    // Trivial cases which are directly written in memory are handled
    // here in ISR context, the one requiring complexe validation,
    // memory lock, or use of the temporary shared memory (ctx->trx.buffer)
    // will be processed in a thread
    case EXTIO_I2C_TRX_STATE_WAITING_VAL:
	
	switch(ctx->trx.reg) {
	// 1-byte configuration requiring no validation and for which
	// we are ok if it is volatile
	case SPANK_I2C_REG_CONFIG_REPORT:
	    ctx->trx.state = EXTIO_I2C_TRX_STATE_WAITING_REG;
	    break;

	// Need to push execution to thread
	case SPANK_I2C_REG_CONFIG_BEHAVIOUR:
	    extio_i2c_defer(ctx, EXTIO_I2C_DEFERRED_WRITE_CONFIG_BEHAVIOUR);
	    break;
	    
	case SPANK_I2C_REG_POSTURE:
	    extio_i2c_defer(ctx, EXTIO_I2C_DEFERRED_WRITE_POSTURE);
	    break;
	}
	break;
    }
}

    

/* Handling of read request event (READ_REQ)
 */
static inline void
extio_i2c_read_req(struct extio_i2c_ctx *ctx) {
    void    *buffer  = NULL;
    size_t   bufsize = 0;

    switch(ctx->trx.state) {
    case EXTIO_I2C_TRX_STATE_WAITING_REG:
	// That's not a valid case !
	// But we handle it specifically here ...
	//  ... to say that's the other side is buggy (hardware or driver)
	LOG_WRN("transfer started with a register read, that's fucked-up!"
		" (resetting bus)");
	extio_i2c_abort(ctx);
	return;

    case EXTIO_I2C_TRX_STATE_WAITING_VAL:
	switch(ctx->trx.reg) {

	// -- 1-byte can be read directly from memory
	//    (no risk of inconsistent state)

	case SPANK_I2C_REG_CONFIG_REPORT:
	    buffer  = &ctx->config.report;
	    bufsize = sizeof(ctx->config.report);
	    goto transfer;

	case SPANK_I2C_REG_DATA_READY:
	    buffer  = &ctx->data_ready.flags;
	    bufsize = sizeof(ctx->data_ready.flags);
	    extio_i2c_deassert_data_ready_interrupt(ctx);
	    goto transfer;

	// -- Read-only data are safe reading without locking

	case SPANK_I2C_REG_VERSION:
	    buffer  = _redskin_version;
	    bufsize = sizeof(_redskin_version);
	    goto transfer;

	case SPANK_I2C_REG_SPANK_VERSION:
	    buffer  = _spank_version;
	    bufsize = sizeof(_spank_version);
	    goto transfer;

	// -- Requires special processing, need to be deferred to a thread
	 
	case SPANK_I2C_REG_CONFIG_BEHAVIOUR:
	    extio_i2c_defer(ctx, EXTIO_I2C_DEFERRED_READ_CONFIG_BEHAVIOUR);
	    return;
	    
	case SPANK_I2C_REG_POSTURE:
	    extio_i2c_defer(ctx, EXTIO_I2C_DEFERRED_READ_POSTURE);
	    return;

	case SPANK_I2C_REG_DISTANCE_MEASUREMENT:
	    extio_i2c_defer(ctx, EXTIO_I2C_DEFERRED_READ_DISTANCE_MEASUREMENT);
	    return;
	}

	// -- Shouldn't happen if user read the doc
	goto unexpected_request;

    case EXTIO_I2C_TRX_STATE_WAITING_INFO:
	switch(ctx->trx.reg) {
	// -- Requires special processing, need to be deferred to a thread
	case SPANK_I2C_REG_DISTANCE_MEASUREMENT:
	    extio_i2c_defer(ctx, EXTIO_I2C_DEFERRED_READ_INFO);
	    return;
	}

	// -- Shouldn't happen if user read the doc
	goto unexpected_request;
    }

    // Unexpected request
    // That's not as misbehaved as starting with a register read
    // but still try to recover from it...
 unexpected_request:
    LOG_WRN("moving to ERROR (unexpected read request) [state=%d, reg=0x%02x]",
	    ctx->trx.state, ctx->trx.reg);
    extio_i2c_abort(ctx);
    return;

 transfer:
    nrfx_twis_tx_prepare(&ctx->twis, buffer, bufsize);
}



/* Handling of read done event (READ_DONE)
 * => All the READ state transition are handled here
 * => Incorrect state are supposed to be dealt with in the READ_REQ
 */
static inline void
extio_i2c_read_done(struct extio_i2c_ctx *ctx, size_t size) {
    switch(ctx->trx.state) {
    case EXTIO_I2C_TRX_STATE_WAITING_REG:
	__ASSERT(0, "should have triggered a bus reset"
		    " in extio_i2c_read_req()");
	break;
    case EXTIO_I2C_TRX_STATE_WAITING_VAL:
	switch(ctx->trx.reg) {
	case SPANK_I2C_REG_DISTANCE_MEASUREMENT:
	    ctx->trx.state = EXTIO_I2C_TRX_STATE_WAITING_INFO;
	    break;
	default:
	    ctx->trx.state = EXTIO_I2C_TRX_STATE_WAITING_REG;
	    break;
	}
	break;
    case EXTIO_I2C_TRX_STATE_WAITING_INFO:
	ctx->trx.state = EXTIO_I2C_TRX_STATE_WAITING_REG;
	break;
    case EXTIO_I2C_TRX_STATE_ERROR:
	// Stay in error state
	break;
    default:
	// Look's like a programming error
	ctx->trx.state = EXTIO_I2C_TRX_STATE_ERROR;
	__ASSERT(0, "impossible switch case");
	break;
    }
}



// -- Handling of TWIS IRQ and NRFX TWIS driver events ------------------

/* Event handler used to respond to NRFx TWIS driver
 * It is called by the state machine inside nrfx_twis_?_irq_handler
 */
static void
extio_i2c_nrfx_twis_event_handler(nrfx_twis_evt_t const *event)
{
    struct extio_i2c_ctx *ctx = &extio_i2c_ctx;

    /* Check if we are already in an error state and discard every thing.
     * Be carefull that if we have a read/write request we will need
     * to do a TX/RX prepare to release clock stretching.
     *
     * NOTE: dummy buffer can't be allocated into the stack 
     *       as it is used for DMA transfer
     */
    if (ctx->trx.state == EXTIO_I2C_TRX_STATE_ERROR) {
	switch(event->type) {
	case NRFX_TWIS_EVT_READ_REQ:
	    LOG_DBG("performing dummy read (ERROR state)");
	    nrfx_twis_tx_prepare(&ctx->twis, &ctx->dummy, sizeof(ctx->dummy));
	    break;
	case NRFX_TWIS_EVT_WRITE_REQ:
	    LOG_DBG("performing dummy write (ERROR state)");
	    nrfx_twis_rx_prepare(&ctx->twis, &ctx->dummy, sizeof(ctx->dummy));
	    break;
	case NRFX_TWIS_EVT_READ_DONE:
	case NRFX_TWIS_EVT_WRITE_DONE:
	    // We should already have logged something about it
	    // in the {READ,WRITE}_REQ
	    break;
	default:
	    LOG_DBG("discarding NRFX event as (ERROR state)");
	    break;
	}
	return;
    }

    /* Dispatch read/write req/done events to helper functions.
     * If we have an error event directly move to the error state.
     */
    switch(event->type) {
    case NRFX_TWIS_EVT_READ_REQ:
	if (event->data.buf_req)
	    extio_i2c_read_req(ctx);
	break;
	
    case NRFX_TWIS_EVT_READ_DONE:
	extio_i2c_read_done(ctx, event->data.tx_amount);
	break;
	
    case NRFX_TWIS_EVT_WRITE_REQ:
	if (event->data.buf_req)
	    extio_i2c_write_req(ctx);
	break;
	
    case NRFX_TWIS_EVT_WRITE_DONE:
	extio_i2c_write_done(ctx, event->data.rx_amount);
	break;

    case NRFX_TWIS_EVT_READ_ERROR:
    case NRFX_TWIS_EVT_WRITE_ERROR:
    case NRFX_TWIS_EVT_GENERAL_ERROR:
	LOG_DBG("forcing i2c reset (NRFX error event)");
	extio_i2c_abort(ctx);
	break;
    }
}



/* IRQ handler wrapper to the NRFx drivers
 * This is necessary to be able to perform clean-up on the I2C stop-bit,
 * as the NRFx TWIS driver doesn't generate event for this.
 */
void
extio_i2c_nrfx_twis_1_irq_handler(void *arg)
{
    struct extio_i2c_ctx *ctx    = &extio_i2c_ctx;
    NRF_TWIS_Type        *p_reg  = ctx->twis.p_reg;

    // Memorize the I2C stop bit presence for later use in the
    // nrfx_twis_event_handler
    ctx->i2c_stop_bit = nrf_twis_event_check(p_reg, NRF_TWIS_EVENT_STOPPED);

    // Call the nrfx twis irq handler which will generate events
    nrfx_isr(nrfx_twis_1_irq_handler);

    // Ensure that after a stop bit we return to the normal state
    if (ctx->i2c_stop_bit) {
	ctx->trx.state = EXTIO_I2C_TRX_STATE_WAITING_REG;
#if CONFIG_REDSKIN_EXTIO_I2C_STALLED > 0
	k_work_cancel_delayable(&ctx->stalled.delayable_work);
#endif
    }
}




/*======================================================================*/
/* Flavor definition                                                    */
/*======================================================================*/

static struct extio_flavor extio_i2c_flavor = {
    .name              = "I2C (nrf)",
    .distance_ready_cb = extio_i2c_distance_ready_cb,
#if defined(CONFIG_REDSKIN_SHELL)
    .shell.name        = "i2c",
    .shell.info        = extio_i2c_shell_info,
    .shell.cmd         = NULL,
#endif
};




/*======================================================================*/
/* Exported functions                                                   */
/*======================================================================*/

/**
 * Initialize the I2C slave implementation for ExtIO.
 * I2C slave address is to be specified in the device tree
 */
int
extio_i2c_init(void)
{
    struct extio_i2c_ctx *ctx = &extio_i2c_ctx;

    // Finalize context initialization
    k_work_init(&ctx->deferred.work, extio_i2c_deferred_work);
#if CONFIG_REDSKIN_EXTIO_I2C_STALLED > 0
    k_work_init_delayable(&ctx->stalled.delayable_work, extio_i2c_stalled_work);
#endif

    // Pin Control
    int ret = pinctrl_apply_state(ctx->pcfg, PINCTRL_STATE_DEFAULT);
    if (ret < 0) {
	return ret;
    }

    // Install IRQ handler in Zephyr
    IRQ_CONNECT(EXTIO_I2C_IRQ, EXTIO_I2C_IRQ_PRIORITY,
		extio_i2c_nrfx_twis_1_irq_handler, NULL, 0);
    
    // Initialize and enable TWIS in NRFx
    nrfx_twis_init(&ctx->twis, &ctx->twis_config,
		   extio_i2c_nrfx_twis_event_handler);
    nrfx_twis_enable(&ctx->twis);

    // Enable IRQ handler in Zephyr
    irq_enable(EXTIO_I2C_IRQ);

    // Ready pin
    ctx->gpio.ready.dev = DEVICE_DT_GET(EXTIO_READY);
    ctx->gpio.ready.pin = EXTIO_READY_PIN;

    gpio_pin_configure(ctx->gpio.ready.dev, ctx->gpio.ready.pin,
		       EXTIO_READY_FLAGS | GPIO_OUTPUT_INACTIVE);
    
    // Register
    extio_register_flavor(EXTIO_FLAVOR_I2C, &extio_i2c_flavor);
    
    // Job's done
    return 0;
}



/**
 * Reset the I2C processing
 */
int
extio_i2c_reset(void)
{
    LOG_INF("i2c-reset requested");

    struct extio_i2c_ctx *ctx = &extio_i2c_ctx;

    extio_i2c_abort(ctx);
    
    return 0;
}
