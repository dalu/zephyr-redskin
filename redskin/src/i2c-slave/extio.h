/*
 * Copyright (c) 2020-2021
 * Stephane D'Alu, Inria Chroma / Inria Agora, INSA Lyon, CITI Lab.
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#ifndef __EXTIO__I2C__H
#define __EXTIO__I2C__H

/**
 * Initialize I2C slave.
 */
int extio_i2c_init(void);


/**
 * Reset the I2C processing
 */
int extio_i2c_reset(void);

#endif
