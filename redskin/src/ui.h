/*
 * Copyright (c) 2019 
 * Stephane D'Alu, Inria Chroma / Inria Agora, INSA Lyon, CITI Lab.
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#ifndef __UI_H
#define __UI_H

#include <blinker/blink.h>

int ui_init(void);
void ui_blink(int id, blink_code_t code);

#define UI_LED_0 	0
#define UI_LED_1 	1

#endif
