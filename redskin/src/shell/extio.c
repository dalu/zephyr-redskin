/*
 * Copyright (c) 2020
 * Stephane D'Alu, Inria Chroma / Inria Agora, INSA Lyon, CITI Lab.
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <zephyr/kernel.h>
#include <zephyr/shell/shell.h>

#if defined(CONFIG_REDSKIN_EXTIO_I2C)
#include "../i2c-slave/extio.h"
#endif


/*== i2c-reset =====================================================*/

#if defined(CONFIG_REDSKIN_EXTIO_I2C)

static int
cmd_extio_i2c_reset(const struct shell *shell, size_t argc, char **argv)
{
    extio_i2c_reset();

    return 0;
}

#endif

/*======================================================================*/



SHELL_STATIC_SUBCMD_SET_CREATE(
  sub_extio,
#if defined(CONFIG_REDSKIN_EXTIO_I2C)
  SHELL_CMD(i2c-reset,  NULL, "reset ExtIO I2C bus", cmd_extio_i2c_reset),
#endif
  SHELL_SUBCMD_SET_END);

SHELL_CMD_REGISTER(extio, &sub_extio, "ExtIO", NULL);
