/*
 * Copyright (c) 2020
 * Stephane D'Alu, Inria Chroma / Inria Agora, INSA Lyon, CITI Lab.
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#include <stdlib.h>
#include <stdbool.h>
#include <stdio.h>
#include <limits.h>
#include <string.h>

#include <zephyr/kernel.h>
#include <zephyr/shell/shell.h>

#include <spank/types.h>
#include <spank/uav.h>
#include <spank/uav-puppet.h>

/*== Helpers =======================================================*/

static bool
_scan_xyz(char *str, int *x, int *y, int *z)
{
    int scan_end;
    int s = sscanf(str, "%d,%d,%d%n", x, y, z, &scan_end);
    return (s == 3) && (str[scan_end] == '\0');
}

static inline bool
_scan_position(char *str, spank_position_t *v)
{
    int x, y, z;
    if      (!strcmp(str, "0")) { spank_position_set_zero(v);   return true;  }
    else if (!strcmp(str, "-")) { spank_position_set_nan(v);    return true;  }
    else if (!strcmp(str, ".")) {                               return true;  }
    else if (!_scan_xyz(str, &x, &y, &z)) {                     return false; }
    else               { spank_position_from_int(v, x, y, z);   return true;  }
}

static inline bool
_scan_velocity(char *str, spank_velocity_t *v)
{
    int x, y, z;
    if      (!strcmp(str, "0")) { spank_velocity_set_zero(v);   return true;  }
    else if (!strcmp(str, "-")) { spank_velocity_set_nan(v);    return true;  }
    else if (!strcmp(str, ".")) {                               return true;  }
    else if (!_scan_xyz(str, &x, &y, &z)) {                     return false; }
    else               { spank_velocity_from_int(v, x, y, z);   return true;  }
}

static inline bool
_scan_stddev_xyz(char *str, spank_stddev_xyz_t *v)
{
    int x, y, z;
    if      (!strcmp(str, "0")) { spank_stddev_xyz_set_zero(v); return true;  }
    else if (!strcmp(str, "-")) { spank_stddev_xyz_set_nan(v);  return true;  }
    else if (!strcmp(str, ".")) {                               return true;  }
    else if (!_scan_xyz(str, &x, &y, &z)) {                     return false; }
    else               { spank_stddev_xyz_from_int(v, x, y, z); return true;  }
}


static bool
_posture_dump(spank_posture_t *posture, char *bufptr, size_t buflen)
{
    int rc;
    
#define POSTURE_DUMP_SNPRINTF(args...) do {	\
	rc = snprintf(bufptr, buflen, args);	\
	if ((rc < 0) || (buflen < rc))		\
	    return false;			\
	bufptr += rc;				\
	buflen -= rc;				\
    } while(0) 

    if (SPANK_POSITION_IS_NAN(posture->position.value)) {
	POSTURE_DUMP_SNPRINTF("%s", "n/a");
	return true;
    }

    POSTURE_DUMP_SNPRINTF("<%"SPANK_PRIposition_axis","
			   "%"SPANK_PRIposition_axis","
			   "%"SPANK_PRIposition_axis">",
			  posture->position.value.x,
			  posture->position.value.y,
			  posture->position.value.z);
    
    if (SPANK_STDDEV_XYZ_IS_NAN(posture->position.stddev))
	goto position_done;
    
    POSTURE_DUMP_SNPRINTF("(%"SPANK_PRIstddev_axis","
			   "%"SPANK_PRIstddev_axis","
			   "%"SPANK_PRIstddev_axis")",
			  posture->position.stddev.x,
			  posture->position.stddev.y,
			  posture->position.stddev.z);
 position_done:
    
    if (SPANK_VELOCITY_IS_NAN(posture->velocity.value))
	goto velocity_done;

    POSTURE_DUMP_SNPRINTF(" " "<%"SPANK_PRIvelocity_axis","
			       "%"SPANK_PRIvelocity_axis","
			       "%"SPANK_PRIvelocity_axis">",
			  posture->velocity.value.x,
			  posture->velocity.value.y,
			  posture->velocity.value.z);

    if (SPANK_STDDEV_XYZ_IS_NAN(posture->velocity.stddev))
	goto velocity_done;
	
    POSTURE_DUMP_SNPRINTF("(%"SPANK_PRIstddev_axis","
			   "%"SPANK_PRIstddev_axis","
			   "%"SPANK_PRIstddev_axis")",
			  posture->velocity.stddev.x,
			  posture->velocity.stddev.y,
			  posture->velocity.stddev.z);
 velocity_done:
    
    return true;
    
#undef POSTURE_DUMP_SNPRINTF
}





/*== Posture =======================================================*/

static int
cmd_uav_posture(const struct shell *shell, size_t argc, char **argv)
{
    if ((argc != 1) && (argc != 5)) {
	goto usage;
    }

    spank_posture_t posture;
    spank_uav_get_posture(&posture);

    if (argc == 1) {
	// That's nearly ~100 bytes into the stack :(
	 char buf[sizeof("<-16384,-16384,-16384>(-16384,-16384,-16384)"
			 " "
			 "<-16384,-16384,-16384>(-16384,-16384,-16384)")];
	 _posture_dump(&posture, buf, sizeof(buf));
	 shell_print(shell, "Posture = %s", buf);
	 return 0;
    }

    if (argc == 5) {
	if (!_scan_position  (argv[1], &posture.position.value ) ||
	    !_scan_stddev_xyz(argv[2], &posture.position.stddev) ||
	    !_scan_velocity  (argv[3], &posture.velocity.value ) ||
	    !_scan_stddev_xyz(argv[4], &posture.velocity.stddev))
	    goto usage;
	spank_uav_set_posture(&posture);
	return 0;
    }
    
 usage:
    shell_help(shell);
    shell_print(shell, "Usage: posture");
    shell_print(shell, "       posture <position> <stddev>"
		                     " <velocity> <stddev>");
    shell_print(shell, " <position|velocity|stddev> = x,y,z -> set axis value");
    shell_print(shell, "                            | 0     -> set to zero");
    shell_print(shell, "                            | -     -> set to NaN");
    shell_print(shell, "                            | .     -> keep current value");
    return 1;
}

/*======================================================================*/



SHELL_STATIC_SUBCMD_SET_CREATE(
  sub_uav,
  SHELL_CMD(posture,  NULL, "Access UAV posture",      cmd_uav_posture),
  SHELL_SUBCMD_SET_END);

SHELL_CMD_REGISTER(uav, &sub_uav, "UAV", NULL);
