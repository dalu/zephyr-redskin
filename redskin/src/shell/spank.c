/*
 * Copyright (c) 2019 
 * Stephane D'Alu, Inria Chroma / Inria Agora, INSA Lyon, CITI Lab.
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#include <zephyr/kernel.h>
#include <zephyr/shell/shell.h>
#include <zephyr/logging/log.h>

#include <strings.h>
#include <stdlib.h>

#include "spank/osal.h"
#include "spank/syslog.h"
#include "spank/spank.h"

#include "../sniffer.h"
#include "../report.h"

struct spank_config_key {
    char *name;
    char type;
    size_t offset;
};


/* Supported parsing:
 *  C = uint8_t
 *  T = ticks
 *  B = bool
 */
static struct spank_config_key spank_config_keys[] = {
   { "packet.retries", 'C',
     offsetof(spank_config_t, packet.retries) },
   { "twr.timeout", 'T',
     offsetof(spank_config_t, twr.timeout) },
   { "orchestration.election_timeout", 'T',
     offsetof(spank_config_t, orchestration.election_timeout) },
   { "orchestration.scheduled_next_ranging", 'T',
     offsetof(spank_config_t, orchestration.scheduled_next_ranging) },
   { NULL }
};


/*== Start =============================================================*/

static int
cmd_spank_start(const struct shell *shell, size_t argc, char **argv)
{
    ARG_UNUSED(argc);
    ARG_UNUSED(argv);

    spank_start();

    return 0;
}


/*== Stop ==============================================================*/

static int
cmd_spank_stop(const struct shell *shell, size_t argc, char **argv)
{
    ARG_UNUSED(argc);
    ARG_UNUSED(argv);

    spank_stop();

    return 0;
}


/*== Config ============================================================*/

static int
cmd_spank_config(const struct shell *shell, size_t argc, char **argv)
{
    struct spank_config_key *key = NULL;
    char *                   cfg = (char *)spank_getconfig();

    argc--;
    argv++;

    if (argc == 0) { // Display config
        for (key = spank_config_keys; key->name; key++) {
            shell_fprintf(shell, SHELL_NORMAL, "%s: ", key->name);
            switch (key->type) {
                case 'C': {
                    uint8_t i = *(uint8_t *)(cfg + key->offset);
                    shell_fprintf(shell, SHELL_NORMAL, "%d", i);
                    break;
                }
                case 'B': {
                    bool b = *(bool *)(cfg + key->offset);
                    shell_fprintf(shell, SHELL_NORMAL, "%s", b ? "on" : "off");
                    break;
                }
                case 'T': {
                    spank_systicks_t ticks =
                      *(spank_systicks_t *)(cfg + key->offset);
                    shell_fprintf(shell, SHELL_NORMAL, "%ld ticks (~ %ld ms)",
                                  (long)ticks,
                                  (long)(1000*ticks)/SPANK_SYSTICKS_PER_SECOND);
                    break;
                }
                default:
                    shell_fprintf(shell, SHELL_NORMAL, "<unable to decode>");
                    break;
            }
            shell_fprintf(shell, SHELL_NORMAL, "\n");
        }
    } else { // Perform config
        for (; argc > 0; argc--, argv++) {
            char *k = *argv;
            char *v = strchr(*argv, '=');
            if (v == NULL)
                goto usage;
            *v++ = '\0';

            for (key = spank_config_keys; key->name; key++) {
                if (!strcasecmp(k, key->name)) {
                    switch (key->type) {
                        case 'C': {
                            char *        endptr = NULL;
                            unsigned long val    = strtoul(v, &endptr, 10);
                            if (*endptr != '\0') {
                                v = "<unable to parse value>";
                                break;
                            }
                            if (val > 255) {
                                v = "<value too big>";
                                break;
                            }
                            *(uint8_t *)(cfg + key->offset) = val;
                            break;
                        }
                        case 'B': {
                            bool val = true;
                            if (!strcmp(v, "on")) {
                                val = true;
                            } else if (!strcmp(v, "off")) {
                                val = false;
                            } else {
                                v = "<unable to parse value>";
                                break;
                            }
                            *(bool *)(cfg + key->offset) = val;
                            break;
                        }
                        case 'T': {
                            char *        endptr = NULL;
                            unsigned long ticks  = strtoul(v, &endptr, 10);
                            if (*endptr != '\0') {
                                v = "<unable to parse ticks>";
                                break;
                            }
                            *(spank_systicks_t *)(cfg + key->offset) = ticks;
                            break;
                        }
                        default: v = "<unable to encode>"; break;
                    }
                    goto found;
                }
            }
            v = "<invalid key>";
        found:
            shell_fprintf(shell, SHELL_NORMAL, "%s = %s\n", k, v);
        }
    }
    return 0;

usage:
    shell_print(shell, "Usage: spank config key1=val1 key2=val2 ...");
    return 1;
}


/*== Sniffer ===========================================================*/

#if defined(CONFIG_SPANK_IO_SNIFFER_PACKET)

static int
cmd_spank_sniffer_packet(const struct shell *shell, size_t argc, char **argv)
{
    uint16_t filter   = 0;
    
    if (argc <= 1) {
	filter = sniffer_packet_get_filter();
    } else {
	for (int i = 1 ; i < argc ; i++) {
	    char *str = argv[i];	    
	    if        (!strcmp(str, "all" )) {
		filter = SNIFFER_PACKET_FILTER_ALL;
	    } else if (!strcmp(str, "none")) {
		filter = SNIFFER_PACKET_FILTER_NONE;
	    } else {
		char     op  = '=';
		uint16_t val = 0;
		if ((str[0] == '=') || (str[0] == '+') || (str[0] == '-')) {
		    op = *str++;
		}
			  
		if        (!strcmp(str, "msg" )) {
		    val = SNIFFER_PACKET_FILTER_MSG;
		} else if (!strcmp(str, "navctrl" )) {
		    val = SNIFFER_PACKET_FILTER_NAVCTRL;
		} else if (!strcmp(str, "elect" )) {
		    val = SNIFFER_PACKET_FILTER_ELECT;
		} else if (!strcmp(str, "twr-poll" )) {
		    val = SNIFFER_PACKET_FILTER_TWR_POLL;
		} else if (!strcmp(str, "twr-answer" )) {
		    val = SNIFFER_PACKET_FILTER_TWR_ANSWER;
		} else if (!strcmp(str, "twr-final" )) {
		    val = SNIFFER_PACKET_FILTER_TWR_FINAL;
		} else if (!strcmp(str, "twr-report" )) {
		    val = SNIFFER_PACKET_FILTER_TWR_REPORT;
		} else if (!strcmp(str, "twr" )) {
		    val = SNIFFER_PACKET_FILTER_TWR_POLL   |
			  SNIFFER_PACKET_FILTER_TWR_ANSWER |
		          SNIFFER_PACKET_FILTER_TWR_FINAL  |
			  SNIFFER_PACKET_FILTER_TWR_REPORT ;
		} else if (!strncmp(str, "0x", 2)) {
		    char *endptr = NULL;
		    str += 2;
		    val = strtol(str, &endptr, 16);
		    if ((*endptr != '\0') || (*str == '\0')) {
			goto usage;
		    }
		} else {
		    goto usage;
		}

		switch(op) {
		case '=': filter  =  val; break;
		case '+': filter |=  val; break;
		case '-': filter &= ~val; break;
		}
	    }
	}

	bool next_enabled = filter > 0;
	bool curr_enabled = sniffer_packet_get_filter() > 0;

	if (next_enabled != curr_enabled) {
	    spank_sniffer(SPANK_SNIFFER_PACKET,
			  next_enabled ? SPANK_SNIFFER_ENABLE
		                      : SPANK_SNIFFER_DISABLE);
	}
    
	sniffer_packet_set_filter(filter);
    }
    
    shell_print(shell, "Sniffer filter is set to 0x%04x.", filter);
    return 0;
    
 usage:
    shell_print(shell,
	"Usage: spank sniffer packet <0x....|all|none|elect|navctrl|msg|\n"
	"                             twr|twr-poll|twr-answer|twr-final|"
	                             "twr-report>");
    return 1;
}

#endif


#if defined(CONFIG_SPANK_IO_SNIFFER_FRAME)

static int
cmd_spank_sniffer_frame(const struct shell *shell, size_t argc, char **argv)
{
    int enabled = 0;
    if (argc <= 1) {
	enabled = spank_sniffer(SPANK_SNIFFER_FRAME, SPANK_SNIFFER_STATUS);
    } else {
	char *str = argv[1];
	if        (!strcmp(str, "on" )) {
	    enabled = SPANK_SNIFFER_ENABLE;
	} else if (!strcmp(str, "off")) {
	    enabled = SPANK_SNIFFER_DISABLE;
	} else {
	    goto usage;
	}
	spank_sniffer(SPANK_SNIFFER_FRAME, enabled);
    }

    shell_print(shell, "Frame filter is %s",
		enabled == SPANK_SNIFFER_ENABLE ? "enabled" : "disabled");
    return 0;
    
 usage:
    shell_print(shell, "Usage: spank sniffer frame <on|off>");
    return 1;
}

static int
cmd_spank_sniffer_frame_output(const struct shell *shell, size_t argc, char **argv)
{
    uint16_t output = 0;
    if (argc <= 1) {
	output = sniffer_frame_get_output();
    } else {
	for (int i = 1 ; i < argc ; i++) {
	    char *str = argv[i];	    
	    if        (!strcmp(str, "all" )) {
		output = SNIFFER_FRAME_OUTPUT_ALL;
	    } else if (!strcmp(str, "none")) {
		output = SNIFFER_FRAME_OUTPUT_NONE;
	    } else {
		char     op  = '=';
		uint16_t val = 0;
		if ((str[0] == '=') || (str[0] == '+') || (str[0] == '-')) {
		    op = *str++;
		}
			  
		if        (!strcmp(str, "console" )) {
		    val = SNIFFER_FRAME_OUTPUT_CONSOLE;
#if defined(CONFIG_REDSKIN_EXTIO_USB)
		} else if (!strcmp(str, "extio-usb" )) {
		    val = SNIFFER_FRAME_OUTPUT_EXTIO_USB;
		} else if (!strcmp(str, "usb" )) {
		    val = SNIFFER_FRAME_OUTPUT_EXTIO_USB;
#endif
		} else if (!strncmp(str, "0x", 2)) {
		    char *endptr = NULL;
		    str += 2;
		    val = strtol(str, &endptr, 16);
		    if ((*endptr != '\0') || (*str == '\0')) {
			goto usage;
		    }
		} else {
		    goto usage;
		}
		
		switch(op) {
		case '=': output  =  val; break;
		case '+': output |=  val; break;
		case '-': output &= ~val; break;
		}
	    }
	}
	sniffer_frame_set_output(output);
    }

    shell_print(shell, "Frame output is 0x%04x", output);
    return 0;
    
 usage:
    shell_print(shell, "Usage: spank sniffer output <none"
		"|console"
#if defined(CONFIG_REDSKIN_EXTIO_USB)
		"|extio-usb"
#endif
		"|all>\n");
    return 1;
}

#endif

/*== Report ============================================================*/

#if defined(CONFIG_SPANK_REPORT)

static int
cmd_spank_report(const struct shell *shell, size_t argc, char **argv)
{
    uint16_t filter = 0;
    
    if (argc <= 1) {
	filter = report_get_filter();
    } else {
	for (int i = 1 ; i < argc ; i++) {
	    char *str = argv[i];	    
	    if        (!strcmp(str, "all" )) {
		filter = REPORT_FILTER_ALL;
	    } else if (!strcmp(str, "none")) {
		filter = REPORT_FILTER_NONE;
	    } else {
		char     op  = '=';
		uint16_t val = 0;
		if ((str[0] == '=') || (str[0] == '+') || (str[0] == '-')) {
		    op = *str++;
		}
			  
		if        (!strcmp(str, "twr-distance" )) {
		    val = REPORT_FILTER_TWR_DISTANCE;
		} else if (!strcmp(str, "state" )) {
		    val = REPORT_FILTER_STATE;
		} else if (!strncmp(str, "0x", 2)) {
		    char *endptr = NULL;
		    str += 2;
		    val = strtol(str, &endptr, 16);
		    if ((*endptr != '\0') || (*str == '\0')) {
			goto usage;
		    }
		} else {
		    goto usage;
		}

		switch(op) {
		case '=': filter  =  val; break;
		case '+': filter |=  val; break;
		case '-': filter &= ~val; break;
		}
	    }
	}
    }

    report_set_filter(filter);
    shell_print(shell, "Report filter is set to 0x%04x.", filter);
    return 0;
    
 usage:
    shell_print(shell, "Usage: spank report <0x....|all|none"
		                                  "|twr-distance|state>");
    return 1;
}

#endif


/*== Syslog ============================================================*/

#if defined(CONFIG_SPANK_SYSLOG_RUNTIME)
static int
cmd_spank_syslog_set(const struct shell *shell, size_t argc, char **argv)
{
    if (argc != 1) {
	return 1;
    }

    int lvl = spank_syslog_level_lookup(*argv);
    if (lvl < 0) {
	shell_error(shell, "Unknown syslog level <%s>", *argv);
	return -EINVAL;
    }
	
    shell_print(shell, "Setting syslog to <%s>", spank_syslog_string(lvl));
    spank_syslog_setlevel(lvl);
    return 0;
}


static int
cmd_spank_syslog_get(const struct shell *shell, size_t argc, char **argv)
{
    shell_print(shell, "Syslog is set to <%s>",
		spank_syslog_string(spank_syslog_getlevel()));
    return 0;
}


SHELL_STATIC_SUBCMD_SET_CREATE(sub_spank_syslog,
  SHELL_CMD(none,    NULL, "None",    cmd_spank_syslog_set),
  SHELL_CMD(fatal,   NULL, "Fatal",   cmd_spank_syslog_set),
  SHELL_CMD(error,   NULL, "Error",   cmd_spank_syslog_set),
  SHELL_CMD(warning, NULL, "Warning", cmd_spank_syslog_set),
  SHELL_CMD(info,    NULL, "Info",    cmd_spank_syslog_set),
  SHELL_CMD(debug,   NULL, "Debug",   cmd_spank_syslog_set),
  SHELL_SUBCMD_SET_END
);
#endif


/*== NavCtrl ===========================================================*/

static int
cmd_spank_navctrl_broadcast(const struct shell *shell, size_t argc, char **argv)
{
    if (argc-- <= 0) goto usage;
    char *cmd = *argv++;

    if (argc > 0) goto usage;
    
    uint8_t cmd_navctrl = 0xff;
    uint8_t countdown   = 2;
	
    if        (!strcmp(cmd, "takeoff")) {
	cmd_navctrl = SPANK_NAVCTRL_CMD_TAKEOFF;
    } else if (!strcmp(cmd, "landing")) {
	cmd_navctrl = SPANK_NAVCTRL_CMD_LANDING;
    } else if (!strcmp(cmd, "shutdown")) {
	cmd_navctrl = SPANK_NAVCTRL_CMD_EMERGENCY_SHUTDOWN;
    } else {
	goto usage;
    }

    shell_print(shell, "Broadcasting %d %s packet(s) with %dms delay.",
		countdown + 1, cmd, SPANK_NAVCTRL_COUNTDOWN_MS);
    do {
	k_msleep(SPANK_NAVCTRL_COUNTDOWN_MS);
	shell_print(shell, "Sending packet #%d", countdown);
	spank_broadcast_navctrl(cmd_navctrl, countdown);
    } while(countdown-- > 0);
    
    return 0;
    
 usage:
    shell_error(shell, "Valid values for broadcasting navctrl are: "
		"takeoff, landing, shutdown");
    return -EINVAL;
}


SHELL_STATIC_SUBCMD_SET_CREATE(sub_spank_navctrl_broadcast,
  SHELL_CMD(takeoff,  NULL, "Takeoff order", cmd_spank_navctrl_broadcast),
  SHELL_CMD(landing,  NULL, "Landing order", cmd_spank_navctrl_broadcast),
  SHELL_CMD(shutdown, NULL, "Emergency shutdown order", cmd_spank_navctrl_broadcast),
  SHELL_SUBCMD_SET_END
);

SHELL_STATIC_SUBCMD_SET_CREATE(sub_spank_navctrl,
  SHELL_CMD(broadcast, &sub_spank_navctrl_broadcast, "Broadcast navctrl command", NULL),
  SHELL_SUBCMD_SET_END
);


#if defined(CONFIG_SPANK_IO_SNIFFER_FRAME)		       
SHELL_STATIC_SUBCMD_SET_CREATE(sub_spank_sniffer_frame,
  SHELL_CMD(on,     NULL, "Enable frame sniffing", NULL),
  SHELL_CMD(off,    NULL, "Disable frame sniffing", NULL),
  SHELL_CMD(output, NULL, "Select output backend", cmd_spank_sniffer_frame_output),
  SHELL_SUBCMD_SET_END
);
#endif

SHELL_STATIC_SUBCMD_SET_CREATE(sub_spank_sniffer,
#if defined(CONFIG_SPANK_IO_SNIFFER_PACKET)
  SHELL_CMD(packet, NULL, "Packet level sniffer", cmd_spank_sniffer_packet),
#endif
#if defined(CONFIG_SPANK_IO_SNIFFER_FRAME)		       
  SHELL_CMD(frame,  &sub_spank_sniffer_frame, "Frame level sniffer", cmd_spank_sniffer_frame),
#endif
  SHELL_SUBCMD_SET_END
);



/*======================================================================*/



SHELL_STATIC_SUBCMD_SET_CREATE(sub_spank,
  SHELL_CMD(start, NULL, "Start SPANK", cmd_spank_start),
  SHELL_CMD(stop, NULL, "Stop SPANK", cmd_spank_stop),
  SHELL_CMD(navctrl, &sub_spank_navctrl, "Broadcast navctrl command", NULL),
  SHELL_CMD(config, NULL, "Protocol configuration", cmd_spank_config),
#if defined(CONFIG_SPANK_IO_SNIFFER_FRAME ) || \
    defined(CONFIG_SPANK_IO_SNIFFER_PACKET)
  SHELL_CMD(sniffer, &sub_spank_sniffer, "Sniffer (frame/packet)", NULL),
#endif
#if defined(CONFIG_SPANK_SYSLOG_RUNTIME)
  SHELL_CMD(syslog, &sub_spank_syslog, "Log level selection", cmd_spank_syslog_get),
#endif
#if defined(CONFIG_SPANK_REPORT)
  SHELL_CMD(report, NULL, "Report internal information", cmd_spank_report),
#endif
  SHELL_SUBCMD_SET_END
);

SHELL_CMD_REGISTER(spank, &sub_spank, "Swarm Protocol And Navigation Kontrol", NULL);
