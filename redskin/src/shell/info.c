/*
 * Copyright (c) 2019-2020
 * Stephane D'Alu, Inria Chroma / Inria Agora, INSA Lyon, CITI Lab.
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#include <zephyr/kernel.h>
#include <zephyr/shell/shell.h>
#include <stdlib.h>
#include <stdio.h>
#include <strings.h>

#include <info-str.h>

#include "spank/osal.h"
#include "spank/spank.h"
#include "spank/version.h"

#if defined(CONFIG_REDSKIN_EXTIO)
#include "../extio.h"
#endif

void __attribute__((weak))
spank_io_sniffer_frame(uint8_t *data, size_t size,
		       spank_io_frame_info_t *info);

void __attribute__((weak))
spank_io_sniffer_packet(spank_io_rx_ctx_t *rx_ctx, ssize_t size);


static int
cmd_info_spank(const struct shell *shell, size_t argc, char **argv)
{
    ARG_UNUSED(argc);
    ARG_UNUSED(argv);

    uint64_t mac_addr = spank_whoami();
    
    shell_print(shell, "Version    : " SPANK_VERSION);
#if defined(SPANK_VERSION_COMMIT_ID_FULL)
    shell_print(shell, "Git commit : " SPANK_VERSION_COMMIT_ID_FULL);
#endif
    shell_print(shell, "MAC addr   : %04x:%04x:%04x:%04x",
		(uint16_t)(mac_addr >> 48),
		(uint16_t)(mac_addr >> 32),
		(uint16_t)(mac_addr >> 16),
		(uint16_t)(mac_addr >>  0));
    shell_print(shell, "Ticks/sec  : %d (1ms ~ %d ticks)",
		SPANK_SYSTICKS_PER_SECOND, SPANK_MS_TO_SYSTICKS(1));
    shell_print(shell, "Max ticks  : 0x%llx", (uint64_t)SPANK_TIMER_MAX_SYSTICKS);

    uint8_t tx_phr, tx_sd;    
    spank_tx_power(&tx_phr, &tx_sd);
    shell_print(shell, "TX power   : PHR=%.1f dB, SHR+DATA=%.1f dB"
#if defined(CONFIG_SPANK_DRIVER_UWB_TX_POWER)
		" (asked: %.1f dB)"
#else
		" (asked: default)"
#endif
		,
		(float)tx_phr / 2, (float)tx_sd / 2
#if defined(CONFIG_SPANK_DRIVER_UWB_TX_POWER)
		, ((float)CONFIG_SPANK_DRIVER_UWB_TX_POWER) / 2
#endif
		);
    
    char b_flags[5] = { 0 };
    uint8_t behaviour = spank_my_behaviour();
    b_flags[0] = (behaviour & SPANK_NODE_BEHAVIOUR_RANGING  ) ? 'R' : '-';
    b_flags[1] = (behaviour & SPANK_NODE_BEHAVIOUR_ELECTABLE) ? 'E' : '-';
    b_flags[2] = (behaviour & SPANK_NODE_BEHAVIOUR_PINNED   ) ? 'P' : '-';
    b_flags[3] = (behaviour & SPANK_NODE_BEHAVIOUR_STATIC   ) ? 'S' : '-';
    shell_print(shell, "Behaviour  : %s [Ranging|Electable|Pinned|Static]",
		b_flags);

    shell_print(shell, "Size packed: <position:%d>, <velocity:%d>, <stddev_xyz:%d>, <posture:%d>", sizeof(spank_packed_position_t), sizeof(spank_packed_velocity_t),sizeof(spank_packed_stddev_xyz_t),sizeof(spank_packed_posture_t));

#if defined(CONFIG_SPANK_IO_SNIFFER_FRAME ) ||	\
    defined(CONFIG_SPANK_IO_SNIFFER_PACKET)
    char  sniffers[32] = "";
    char *sniffer_type;
    char *sniffer_mode;
    
#if defined(CONFIG_SPANK_IO_SNIFFER_FRAME )
    sniffer_type = ", frame=";
    if (spank_io_sniffer_frame == NULL) {
	sniffer_mode = "n/a";
    } else {
	sniffer_mode = spank_sniffer(SPANK_SNIFFER_FRAME, SPANK_SNIFFER_STATUS)
	             ? "on" : "off";
    }
    strncat(sniffers, sniffer_type, sizeof(sniffers)-1);
    strncat(sniffers, sniffer_mode, sizeof(sniffers)-1);
#endif

#if defined(CONFIG_SPANK_IO_SNIFFER_PACKET)
    sniffer_type = ", packet=";
    if (spank_io_sniffer_packet == NULL) {
	sniffer_mode = "n/a";
    } else {
	sniffer_mode = spank_sniffer(SPANK_SNIFFER_PACKET, SPANK_SNIFFER_STATUS)
	             ? "on" : "off";
    }
    strncat(sniffers, sniffer_type, sizeof(sniffers)-1);
    strncat(sniffers, sniffer_mode, sizeof(sniffers)-1);
#endif
	
    shell_print(shell, "Sniffer    : %s", &sniffers[2]);
#else
    shell_print(shell, "Sniffer    : n/a");
#endif
    
    return 0;
}


#if defined(CONFIG_REDSKIN_EXTIO)
static int
cmd_info_extio(const struct shell *shell, size_t argc, char **argv)
{
    ARG_UNUSED(argc);
    ARG_UNUSED(argv);

    extio_shell_info(shell);
    return 0;
}
#endif


static int
cmd_info(const struct shell *shell, size_t argc, char **argv)
{
    ARG_UNUSED(argc);
    ARG_UNUSED(argv);

    shell_print(shell, "--< Operating system >--");
    info_str_cmd_os(shell, 0, NULL);
    shell_print(shell, "");
    shell_print(shell, "--< Build >-------------");
    info_str_cmd_build(shell, 0, NULL);
    shell_print(shell, "");
    shell_print(shell, "--< MCU >---------------");
    info_str_cmd_mcu(shell, 0, NULL);
    shell_print(shell, "");
    shell_print(shell, "--< SPANK >-------------");
    cmd_info_spank(shell, 0, NULL);
#if defined(CONFIG_REDSKIN_EXTIO)
    shell_print(shell, "");
    shell_print(shell, "--< ExtIO >-------------");
    cmd_info_extio(shell, 0, NULL);
#endif

    return 0;
}



SHELL_STATIC_SUBCMD_SET_CREATE(
  sub_info,
  SHELL_CMD(os,    NULL, "Show operating system information.",  info_str_cmd_os),
  SHELL_CMD(build, NULL, "Show build information.",          info_str_cmd_build),
  SHELL_CMD(mcu,   NULL, "Show micro-controller information.", info_str_cmd_mcu),
  SHELL_CMD(spank, NULL, "Show SPANK information.",          cmd_info_spank),
#if defined(CONFIG_REDSKIN_EXTIO)
  SHELL_CMD(extio, NULL, "Show ExtIO information.",          cmd_info_extio),
#endif
  SHELL_SUBCMD_SET_END /* Array terminated. */
);


SHELL_CMD_ARG_REGISTER(
  info, &sub_info, "Various system infomation", cmd_info, 1, 0
);
