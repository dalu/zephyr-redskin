/*
 * Copyright (c) 2019
 * Stephane D'Alu, Inria Chroma / Inria Agora, INSA Lyon, CITI Lab.
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#include <stdlib.h>
#include <limits.h>

#include <zephyr/kernel.h>
#include <zephyr/shell/shell.h>

#include "../accelerometer.h"



/*== Start =============================================================*/

static int
cmd_accelerometer_start(const struct shell *shell, size_t argc, char **argv)
{
    ARG_UNUSED(argc);
    ARG_UNUSED(argv);

    accelerometer_trigger(accelerometer_trigger_handler);

    return 0;
}


/*== Stop ==============================================================*/

static int
cmd_accelerometer_stop(const struct shell *shell, size_t argc, char **argv)
{
    ARG_UNUSED(argc);
    ARG_UNUSED(argv);

    accelerometer_trigger(NULL);

    return 0;
}



/*== Config ============================================================*/

static int
cmd_accelerometer_config(const struct shell *shell, size_t argc, char **argv)
{
    uint16_t frequency = 0;
    uint16_t fullscale = 0;
    char *   argptr    = NULL;

    if ((argc <= 1) || (argc >= 4)) {
        shell_help(shell);
        shell_print(shell, "Usage: config <frequency> <fullscale>");
        shell_print(shell, " <frequency> = 1, 10, 25, 50, 100, 200, 400, "
                                          "1620, 1344, 5376");
        shell_print(shell, " <fullscale> = 2 4 8 16");
        return 1;
    }

    if (argc >= 2) {
        argptr               = argv[1];
        char *        endptr = NULL;
        unsigned long v      = strtoul(argptr, &endptr, 10);
        if ((*argptr == '\0') || (*endptr != '\0')) {
            goto expected_number;
        }
        frequency = (uint16_t)v;
    }

    if (argc >= 3) {
        argptr               = argv[2];
        char *        endptr = NULL;
        unsigned long v      = strtoul(argptr, &endptr, 10);
        if ((*argptr == '\0') || (*endptr != '\0')) {
            goto expected_number;
        }
        fullscale = (uint16_t)v;
    }

    if (accelerometer_config_sampling(frequency, fullscale) < 0) {
        shell_error(shell, "unable to apply configuration (wrong value)");
        return -EINVAL;
    }

    return 0;

expected_number:
    shell_error(shell, "number expected, got '%s'", argptr);
    return -EINVAL;
}



/*======================================================================*/



SHELL_STATIC_SUBCMD_SET_CREATE(
  sub_accelerometer,
  SHELL_CMD(start,  NULL, "Start acceleromter",      cmd_accelerometer_start ),
  SHELL_CMD(stop,   NULL, "Stop accelerometer",      cmd_accelerometer_stop  ),
  SHELL_CMD(config, NULL, "Configure accelerometer", cmd_accelerometer_config),
  SHELL_SUBCMD_SET_END);

SHELL_CMD_REGISTER(accelerometer, &sub_accelerometer, "Accelerometer", NULL);
