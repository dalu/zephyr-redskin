/*
 * Copyright (c) 2020
 * Stephane D'Alu, Inria Chroma / Inria Agora, INSA Lyon, CITI Lab.
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#include <zephyr/kernel.h>
#include <zephyr/shell/shell.h>
#include <zephyr/sys/reboot.h>

static int
cmd_sys(const struct shell *shell, size_t argc, char **argv)
{
    ARG_UNUSED(argc);
    ARG_UNUSED(argv);

    return 0;
}


static int
cmd_sys_reboot(const struct shell *shell, size_t argc, char **argv)
{
    ARG_UNUSED(argc);
    ARG_UNUSED(argv);
    sys_reboot(SYS_REBOOT_COLD);

    return 0;
}

SHELL_STATIC_SUBCMD_SET_CREATE(sub_sys,
    SHELL_CMD(reboot, NULL, "Cold reboot",              cmd_sys_reboot),
    SHELL_SUBCMD_SET_END
);

SHELL_CMD_ARG_REGISTER(sys, &sub_sys, "System", cmd_sys, 1, 0);
