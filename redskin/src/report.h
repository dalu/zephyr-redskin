/*
 * Copyright (c) 2020
 * Stephane D'Alu, Inria Chroma / Inria Agora, INSA Lyon, CITI Lab.
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#ifndef __REPORT__H
#define __REPORT__H

#include <stdint.h>

#define REPORT_FILTER_STATE		0x0001
#define REPORT_FILTER_TWR_DISTANCE	0x0002

#define REPORT_FILTER_ALL		0xffff 
#define REPORT_FILTER_NONE		0x0000


/**
 * Select the filter used for reported data
 *
 * @param filter	or-ed list of REPORT_DUMP_*
 */
void report_set_filter(uint16_t filter);


/**
 * Get the filter currently applied for reported data
 *
 * @return or-ed list of REPORT_DUMP_*
 */
uint16_t report_get_filter(void);

#endif
