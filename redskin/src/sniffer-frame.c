/*
 * Copyright (c) 2022
 * Stephane D'Alu, Inria Chroma / Inria Agora, INSA Lyon, CITI Lab.
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#include <string.h>

#include <zephyr/sys/printk.h>

#include <spank/io.h>
#include <spank/extio/types.h>
#include <spank/extio/usb.h>
#include <zephyr/usb/usb_device.h>
#include "sniffer.h"

/* == Frame ============================================================= */

#if defined(CONFIG_REDSKIN_EXTIO_USB)

#define SNIFFER_FRAME_ALLOCATED_SIZE		SPANK_DRIVER_FRAME_MAXSIZE
#define SNIFFER_FRAME_SUBMIT_RETRY_MAX		3
#define SNIFFER_FRAME_SUBMIT_RETRY_DELAY	K_MSEC(5)

struct sniffer_frame_item {
    void *queue_reserved;		// first word reserved for use by queue
    union {                             // dumped frame
	struct spank_extio_dumped_frame dumped_frame;
	uint8_t raw[SPANK_EXTIO_DUMPED_FRAME_MAXSIZE(
					     SNIFFER_FRAME_ALLOCATED_SIZE)];
    };
} __attribute__((aligned(sizeof(void *))));

static struct sniffer_frame_item sniffer_frame_pool[CONFIG_REDSKIN_SNIFFER_FRAME_BACKLOG];

static K_QUEUE_DEFINE(sniffer_frame_free_queue);
static K_QUEUE_DEFINE(sniffer_frame_backlog_queue);


struct sniffer_frame_transmitter {
    struct k_work_delayable    work_delayable;
    struct sniffer_frame_item *pending_item;
    int                        retries;
};

static struct sniffer_frame_transmitter sniffer_frame_transmitter = {
    .pending_item = NULL,
};

    
static void
sniffer_frame_usb_transfer_release(uint8_t ep, int tsize, void *cb_data)
{
    struct spank_extio_dumped_frame  *dumped_frame = cb_data;
    struct sniffer_frame_item        *item         =
	CONTAINER_OF(dumped_frame,
		     struct sniffer_frame_item, dumped_frame);

    k_queue_prepend(&sniffer_frame_free_queue, item);

    k_work_reschedule(&sniffer_frame_transmitter.work_delayable, K_NO_WAIT);
}

static void
sniffer_frame_submitter_handler(struct k_work *work) {
    
    struct k_work_delayable          *dwork  = k_work_delayable_from_work(work);
    struct sniffer_frame_transmitter *sft     =
	CONTAINER_OF(dwork, struct sniffer_frame_transmitter, work_delayable);
    
    if (sft->pending_item == NULL) {
	sft->pending_item = k_queue_get(&sniffer_frame_backlog_queue,K_NO_WAIT);
	sft->retries      = SNIFFER_FRAME_SUBMIT_RETRY_MAX;
    }
    if (sft->pending_item == NULL)
	return;

    struct spank_extio_dumped_frame *dumped_frame =
	&sft->pending_item->dumped_frame;

    if (usb_transfer(SPANK_USB_BULK_EP_PACKETS | USB_CONTROL_EP_IN,
		     (uint8_t *)dumped_frame,
		     SPANK_EXTIO_DUMPED_FRAME_SIZE(dumped_frame),
		     USB_TRANS_WRITE,
		     sniffer_frame_usb_transfer_release,
		     dumped_frame) < 0) {
	if (sft->retries-- > 0) {
	    k_work_reschedule(&sniffer_frame_transmitter.work_delayable,
			      SNIFFER_FRAME_SUBMIT_RETRY_DELAY);
	}
    }
    sft->pending_item = NULL;
}



static int
sniffer_frame_init(const struct device *arg)
{
    for (int i = 0 ; i < ARRAY_SIZE(sniffer_frame_pool) ; i++) {
	k_queue_append(&sniffer_frame_free_queue,
		       &sniffer_frame_pool[i]);

    }

    k_work_init_delayable(&sniffer_frame_transmitter.work_delayable,
			  sniffer_frame_submitter_handler);

    return 0;
}
SYS_INIT(sniffer_frame_init,
	 APPLICATION, CONFIG_APPLICATION_INIT_PRIORITY);






static inline void
sniffer_frame_to_extio_usb(uint8_t *data, size_t captured_size,
			   spank_io_frame_info_t *info, uint64_t ticks) {

    // Retrieve free buffer (drop if none left)
    struct sniffer_frame_item *item =
	k_queue_get(&sniffer_frame_free_queue, K_NO_WAIT);
    if (item == NULL)
	return;

    // Shortcut to header
    spank_extio_dumped_frame_header_t *header = &item->dumped_frame.header;
	
    // Adjust captured size if necessary
    if (captured_size > SNIFFER_FRAME_ALLOCATED_SIZE)
	captured_size = SNIFFER_FRAME_ALLOCATED_SIZE;

    // Fill header
    header->captured_length = captured_size;
    header->frame_length    = info->length;

    memcpy(&header->os_ticks,
	   &ticks,
	   sizeof(uint64_t));
    memcpy(&header->timestamp,
	   &info->timestamp,
	   sizeof(uint64_t));
    memcpy(&header->power.signal,
	   &info->properties.power.signal,
	   sizeof(spank_rssi_t));
    memcpy(&header->power.firstpath,
	   &info->properties.power.firstpath,
	   sizeof(spank_rssi_t));
    memcpy(&header->clock_tracking.offset,
	   &info->properties.clock_tracking.offset,
	   sizeof(int32_t));
    memcpy(&header->clock_tracking.interval,
	   &info->properties.clock_tracking.interval,
	   sizeof(int32_t));

    // Fill data
    memcpy(item->dumped_frame.data, data, captured_size);

    // Add to backlog queue
    k_queue_append(&sniffer_frame_backlog_queue, item); 

    // Schedule transmit
    k_work_reschedule(&sniffer_frame_transmitter.work_delayable, K_NO_WAIT);
}
#endif


static inline void
sniffer_frame_to_console(uint8_t *data, size_t captured_size,
			 spank_io_frame_info_t *info, uint64_t ticks)
{
    printk("Frame o size          = %zu\n"
	   "      | timestamp     = %llu\n"
	   "      | os ticks      = %llu\n"
	   "      | power         = %0.2f (%0.2f)\n"
	   "      o time-tracking = %d/%d\n",
	   info->length,
	   info->timestamp,
	   ticks,
	   info->properties.power.firstpath / 100.0,
	   info->properties.power.signal    / 100.0,
	   info->properties.clock_tracking.offset,
	   info->properties.clock_tracking.interval
	   );
}



static uint16_t sniffer_frame_output =
#if defined(CONFIG_REDSKIN_EXTIO_USB)
    SNIFFER_FRAME_OUTPUT_EXTIO_USB;
#else
    SNIFFER_FRAME_OUTPUT_CONSOLE;
#endif


void sniffer_frame_set_output(uint16_t output) {
    sniffer_frame_output = output;
}

uint16_t sniffer_frame_get_output(void) {
    return sniffer_frame_output;
}

    
/*
 * Implement the weak symbol (spank_io_sniffer_frame) to report 
 * on incoming frames.
 * NOTE: SPANK need to be compiled with SPANK_IO_SNIFFER_FRAME_SUPPORT enabled
 */
void
spank_io_sniffer_frame(uint8_t *data, size_t captured_size,
		       spank_io_frame_info_t *info) {
    uint64_t ticks = k_uptime_get();    
    if (sniffer_frame_output & SNIFFER_FRAME_OUTPUT_CONSOLE)
	sniffer_frame_to_console  (data, captured_size, info, ticks);
#if defined(CONFIG_REDSKIN_EXTIO_USB)
    if (sniffer_frame_output & SNIFFER_FRAME_OUTPUT_EXTIO_USB)
	sniffer_frame_to_extio_usb(data, captured_size, info, ticks);
#endif
}


