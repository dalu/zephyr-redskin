/*
 * Copyright (c) 2020
 * Stephane D'Alu, Inria Chroma / Inria Agora, INSA Lyon, CITI Lab.
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#ifndef __EXTIO__H
#define __EXTIO__H

#if defined(CONFIG_REDSKIN_SHELL)
#include <zephyr/shell/shell.h>
#endif


/* List of supported ExtIO flavors
 */
#define EXTIO_FLAVOR_I2C		0
#define EXTIO_FLAVOR_USB		1


/**
 * Callback definition
 */
typedef void (*extio_callback_t)(void *args);


/**
 * ExtIO flavor
 */
struct extio_flavor {
    char             *name;
    extio_callback_t  distance_ready_cb;
#if defined(CONFIG_REDSKIN_SHELL)
    struct {
	char         *name;
	void        (*info)(const struct shell *shell);
	void        (*cmd)(const struct shell *shell, size_t argc, char **argv);
    } shell;
#endif
};


/**
 * Initialize ExtIO
 */
void extio_init(void);


/**
 * Register callback run when a new distance is available
 */
void extio_register_flavor(int idx, struct extio_flavor *flavor);

/**
 * Display shell info
 */
#if defined(CONFIG_REDSKIN_SHELL) || defined(__DOXYGEN__)
void extio_shell_info(const struct shell *shell);
#endif


#endif
