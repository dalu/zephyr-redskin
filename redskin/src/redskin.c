/*
 * Copyright (c) 2019
 * Stephane D'Alu, Inria Chroma / Inria Agora, INSA Lyon, CITI Lab.
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#include <zephyr/kernel.h>
#include <zephyr/device.h>
#include <zephyr/drivers/gpio.h>
#include <zephyr/drivers/spi.h>
#include <zephyr/drivers/i2c.h>
#include <zephyr/sys/printk.h>
#include <zephyr/logging/log.h>

#include <stdlib.h>



#define SPANK_SYSLOG_PREFIX "MAIN  "
#include "dw1000/osal.h"
#include "dw1000/dw1000.h"
#include "spank/osal.h"
#include "spank/syslog.h"
#include "spank/driver.h"
#include "spank/uav.h"
#include "spank/spank.h"


LOG_MODULE_REGISTER(redskin);


/* UM §9.3: air utilisation must be < 18% to have 97% transmission
 *
 * UM §9.5: node density and air utilisation:
 * Data rate | Preamble length | Payload | Transmission | TX per second
 *           |   (symbols)     | (bytes) |  time        | 18% air-utilisation
 * ----------+-----------------+---------+-------------+---------------------
 *  110 kbps |            2048 |      12 | 3.042 ms     | 59.2
 *  850 kbps |             256 |      12 | 380.3 μs     | 473.4
 *  6.8 Mbps |              64 |      12 | 103.3 μs     | 1742
 */


/*======================================================================*/
/* Device tree information                                              */
/*======================================================================*/

#define DW1000_NODE		DT_INST(0, decawave_dw1000)
#define DW1000			DT_LABEL(DW1000_NODE)
#define DW1000_SPI_SLAVE_ID	DT_REG_ADDR_BY_IDX(DW1000_NODE, 0)
#define DW1000_SPI_MAX_FREQ	DT_PROP (DW1000_NODE, spi_max_frequency)

#define DW1000_SPI_NODE		DT_BUS  (DW1000_NODE)

#define DW1000_CS		DT_GPIO_CTLR(DW1000_SPI_NODE, cs_gpios)
#define DW1000_CS_PIN		DT_GPIO_PIN  (DW1000_SPI_NODE, cs_gpios)
#define DW1000_CS_FLAGS		DT_GPIO_FLAGS(DW1000_SPI_NODE, cs_gpios)

#define DW1000_RESET		DT_GPIO_CTLR(DW1000_NODE, reset_gpios)
#define DW1000_RESET_PIN	DT_GPIO_PIN  (DW1000_NODE, reset_gpios)
#define DW1000_RESET_FLAGS	DT_GPIO_FLAGS(DW1000_NODE, reset_gpios)

#define DW1000_INT		DT_GPIO_CTLR(DW1000_NODE, int_gpios)
#define DW1000_INT_PIN		DT_GPIO_PIN  (DW1000_NODE, int_gpios)
#define DW1000_INT_FLAGS	DT_GPIO_FLAGS(DW1000_NODE, int_gpios)



/*======================================================================*/
/* Forward declaration                                                  */
/*======================================================================*/

static void
_tx_done(spank_driver_t *drv, uint32_t status);
static void
_rx_ok(spank_driver_t *drv, uint32_t status, size_t length, bool ranging);
static void
_rx_timeout(spank_driver_t *drv, uint32_t status);
static void
_rx_error(spank_driver_t *drv, uint32_t status);



/*======================================================================*/
/* Local variables                                                      */
/*======================================================================*/

/* DW1000 irq/task processing context
 */
typedef struct dw1000_irq_msg { // Keep everything aligned to uint32_t
    dw1000_t *dw;
} __attribute__((aligned(sizeof(uint32_t)))) dw1000_irq_msg_t;
K_MSGQ_DEFINE(dw1000_irq_msgq, sizeof(dw1000_irq_msg_t), 1, sizeof(uint32_t));


static struct {
    k_tid_t         tid;
    struct k_thread thread;
    K_THREAD_STACK_MEMBER(thread_stack,
                          CONFIG_DW1000_TASK_STACK_SIZE * sizeof(int));
} DW;


/* DWM1000 driver
 */
static dw1000_t DW0;


/* DW1000 SPI driver configuration (from the MCU point of view)
 * Partialy initialized with baud rate, MSB first, MODE0 (CPOL=0/CPHA=0),
 * and 8bit word.
 *
 * The low speed is forced to a 2MHz maximal SPI frequency required
 * for correct operations during configuration of the DW1000 chip.
 *
 * note: SPI pin assignement (MISO/MOSI/CLK) has already been
 *       performed by the DTS.
 */
#define DW1000_SPI_CS_CONTROL  				\
    {							\
	.gpio.port     = DEVICE_DT_GET(DW1000_CS),	\
	.gpio.pin      = DW1000_CS_PIN,			\
	.gpio.dt_flags = DW1000_CS_FLAGS,		\
	.delay         = 0,				\
    }

static struct spi_config     dw1000_spi_config_low_speed = {
    .frequency = MIN(2000000, DW1000_SPI_MAX_FREQ),
    .operation = SPI_OP_MODE_MASTER | SPI_LINES_SINGLE | SPI_TRANSFER_MSB |
                 SPI_WORD_SET(8),
    .slave     = DW1000_SPI_SLAVE_ID,
    .cs        = DW1000_SPI_CS_CONTROL
};
static struct spi_config dw1000_spi_config_high_speed = {
    .frequency = DW1000_SPI_MAX_FREQ,
    .operation = SPI_OP_MODE_MASTER | SPI_LINES_SINGLE | SPI_TRANSFER_MSB |
                 SPI_WORD_SET(8),
    .slave     = DW1000_SPI_SLAVE_ID,
    .cs        = DW1000_SPI_CS_CONTROL
};
static dw1000_spi_driver_t DW0_spi = {
    .dev               = DEVICE_DT_GET(DW1000_SPI_NODE),
    .config_low_speed  = &dw1000_spi_config_low_speed,
    .config_high_speed = &dw1000_spi_config_high_speed,
};


/* DW1000 Configuration
 * (SPI, IRQ, Reset, callbacks, ....)
 */
static struct dw1000_ioline  dw1000_ioline_reset = {
    .gpio_dev = DEVICE_DT_GET(DW1000_RESET),
    .gpio_pin = DW1000_RESET_PIN,
};
static struct dw1000_ioline  dw1000_ioline_irq   = {
    .gpio_dev = DEVICE_DT_GET(DW1000_INT),
    .gpio_pin = DW1000_INT_PIN,
};
static const dw1000_config_t DW0_config          = {
    .spi              = &DW0_spi,
    .irq              = &dw1000_ioline_irq,
    .reset            = &dw1000_ioline_reset,
    .leds             = DW1000_LED_ALL,
    .leds_blink_time  = 3,
    .lde_loading      = 1, // Loading of LDE microcode
    .rxauto           = 1, // Automatically re-enable receiver
    .tx_antenna_delay = SPANK_ANTENNA_DELAY_METER_TO_CLOCK(154.6) / 2,
    .rx_antenna_delay = SPANK_ANTENNA_DELAY_METER_TO_CLOCK(154.6) / 2,
    .cb.tx_done       = _tx_done,
    .cb.rx_timeout    = _rx_timeout,
    .cb.rx_error      = _rx_error,
    .cb.rx_ok         = _rx_ok,
};


/* SPANK IO driver
 */
static spank_io_t _spank_io;

/* SPANK task
 */
static k_tid_t         spank_thread_id;
static struct k_thread spank_thread;
static K_THREAD_STACK_DEFINE(spank_thread_stack,
                             CONFIG_SPANK_TASK_STACK_SIZE * sizeof(int));



/*======================================================================*/
/* Local functions                                                      */
/*======================================================================*/

static void
_tx_done(spank_driver_t *drv, uint32_t status)
{
    ARG_UNUSED(drv);
    ARG_UNUSED(status);

    spank_io_on_event(&_spank_io, SPANK_IO_EVT_TX_DONE);
}

static void
_rx_ok(spank_driver_t *drv, uint32_t status, size_t length, bool ranging)
{
    ARG_UNUSED(drv);
    ARG_UNUSED(status);
    ARG_UNUSED(length);
    ARG_UNUSED(ranging);

    spank_io_on_event(&_spank_io, SPANK_IO_EVT_RX_DONE);
}

static void
_rx_timeout(spank_driver_t *drv, uint32_t status)
{
    ARG_UNUSED(drv);
    ARG_UNUSED(status);

    spank_io_on_event(&_spank_io, SPANK_IO_EVT_RX_TIMEOUT);
}

static void
_rx_error(spank_driver_t *drv, uint32_t status)
{
    ARG_UNUSED(drv);
    ARG_UNUSED(status);

    spank_io_on_event(&_spank_io, SPANK_IO_EVT_RX_FAILED);
}


static void __attribute__((noreturn))
spank_task(void *arg1, void *arg2, void *arg3)

{
    ARG_UNUSED(arg1);
    ARG_UNUSED(arg2);
    ARG_UNUSED(arg3);

    spank_loop();
    __builtin_unreachable();
}

static void __attribute__((noreturn))
dw1000_processing_task(void *arg1, void *arg2, void *arg3)
{
    ARG_UNUSED(arg1);
    ARG_UNUSED(arg2);
    ARG_UNUSED(arg3);

    dw1000_irq_msg_t msg;
    while (1) {
        if (k_msgq_get(&dw1000_irq_msgq, &msg, K_FOREVER) < 0)
            continue;

        dw1000_process_events(msg.dw);
    }
    __builtin_unreachable();
}

static struct gpio_callback gpio_cb_dw1000_irq;
static void
dw1000_irq_handler(const struct device *port, struct gpio_callback *cb, uint32_t pins)
{
    ARG_UNUSED(port);
    ARG_UNUSED(cb);
    ARG_UNUSED(pins);

    dw1000_irq_msg_t msg = {
      .dw = &DW0,
    };
    k_msgq_put(&dw1000_irq_msgq, &msg, K_NO_WAIT);
}



/*======================================================================*/
/* Exported functions                                                   */
/*======================================================================*/

int
redskin_init(void)
{
    /* Sanity check
     */
    if (CONFIG_SYS_CLOCK_TICKS_PER_SEC < 1000) {
        SPANK_FATAL("OS ticks/sec is too low (%d)"
                    ", please configure kernel SYS_CLOCK_TICKS_PER_SEC",
                    CONFIG_SYS_CLOCK_TICKS_PER_SEC);
    }


    /* Found/Generate our MAC address using the MCU serial number
     */
    uint64_t mac_addr_64 = (((uint64_t)NRF_FICR->DEVICEID[1] << 32) |
			    ((uint64_t)NRF_FICR->DEVICEID[0] << 0));


    /* Initialise IRQ signaling
     */

    /* MCU initialisation
     */    
    // Reset pin
    gpio_pin_configure(dw1000_ioline_reset.gpio_dev,
                       dw1000_ioline_reset.gpio_pin,
                       DW1000_RESET_FLAGS | GPIO_OUTPUT | GPIO_OUTPUT_LOW);

    
    // IRQ input (on rising edge)
    gpio_pin_configure(dw1000_ioline_irq.gpio_dev, dw1000_ioline_irq.gpio_pin,
                       DW1000_INT_FLAGS | GPIO_INPUT | GPIO_PULL_UP);

    gpio_pin_interrupt_configure(dw1000_ioline_irq.gpio_dev,
                                 dw1000_ioline_irq.gpio_pin, GPIO_INT_DISABLE);

    gpio_init_callback(&gpio_cb_dw1000_irq, dw1000_irq_handler,
                       BIT(dw1000_ioline_irq.gpio_pin));
    gpio_add_callback(dw1000_ioline_irq.gpio_dev, &gpio_cb_dw1000_irq);


    /* Initialise I/O through DW1000 device
     */
    // Sanity check (necessary?)
    if (! device_is_ready(DW0_spi.dev)) {
	LOG_ERR("Device " DEVICE_DT_NAME(DW1000_SPI_NODE) " is not ready");
        return -1;
    }

    // DW1000 low level device initialisation
    //  (the whole configuration is handled by the spank_driver wrapper)
    if (!spank_driver_init(&DW0, &DW0_config)) {
        LOG_ERR("Failed to initialize SPANK driver");
        return -1;
    }

    // Check delay estimation
    uint32_t delay = spank_driver_delayed_send_estimator(
					 &DW0, SPANK_DRIVER_FRAME_MAXSIZE);
    LOG_INF("Embedding timestamp delay (runtime): %0.2f ms (%" PRIu32 ")",
	    (float)delay / SPANK_DRIVER_TIME_CLOCK * 1000, delay);
    LOG_INF("Embedding timestamp delay (compile time): %0.2f ms (%" PRIu32 ")",
	    (float)SPANK_DRIVER_TX_DELAYED_DEFAULT_DELAY / SPANK_DRIVER_TIME_CLOCK * 1000, SPANK_DRIVER_TX_DELAYED_DEFAULT_DELAY);

#if SPANK_DRIVER_TX_DELAYED_DEFAULT_DELAY == 0
#error "Embedding timestamp default delay is required at compile time"
#endif

    if (delay == 0) {
	LOG_ERR("Unable to estimate embedding timestamp delay");
    } else if (delay > SPANK_DRIVER_TX_DELAYED_DEFAULT_DELAY) {
	LOG_ERR("Embedding timestamp delay is too low"
		" (compiled time = %" PRIu32 " << runtime = %" PRIu32 ")",
		SPANK_DRIVER_TX_DELAYED_DEFAULT_DELAY, delay);
    }

    // Initialize IO
    spank_io_t *io     = &_spank_io;
    io->dev            = &DW0;
    io->pan_id         = 0xbccf;
    io->src_addr       = mac_addr_64; // possible truncating
    io->time_msk       = ((uint64_t)1 << SPANK_DRIVER_TIME_BITS) - 1;
    io->metre_per_tick = SPANK_SPEED_OF_LIGHT / SPANK_DRIVER_TIME_CLOCK;
    spank_io_init(io);


    /* Initialize UAV interfacing
     */
    spank_uav_init();


    /* Enable interrupt processing of DW1000
     */
    gpio_pin_interrupt_configure(dw1000_ioline_irq.gpio_dev,
                                 dw1000_ioline_irq.gpio_pin,
                                 GPIO_INT_EDGE_RISING);

    /* Initialize SPANK
     */
    spank_init(&_spank_io);


    /* Create Tasks
     */
    DW.tid = k_thread_create(
      &DW.thread, DW.thread_stack, K_THREAD_STACK_SIZEOF(DW.thread_stack),
      dw1000_processing_task, NULL, NULL, NULL, CONFIG_DW1000_TASK_PRIORITY,
      K_FP_REGS, K_NO_WAIT);
    k_thread_name_set(DW.tid, "dw1000");

    spank_thread_id = k_thread_create(
      &spank_thread, spank_thread_stack,
      K_THREAD_STACK_SIZEOF(spank_thread_stack), spank_task, NULL, NULL, NULL,
      CONFIG_SPANK_TASK_PRIORITY, K_FP_REGS, K_NO_WAIT);
    k_thread_name_set(spank_thread_id, "spank");


    /* Start SPANK
     */
#if defined(CONFIG_REDSKIN_DEBUGGER_SPLIT_PERSONALITY)
    if (!(CoreDebug->DHCSR & CoreDebug_DHCSR_C_DEBUGEN_Msk))
#endif
    {
        spank_start();
    }

    return 0;
}
