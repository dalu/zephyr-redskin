/*
 * Copyright (c) 2020
 * Stephane D'Alu, Inria Chroma / Inria Agora, INSA Lyon, CITI Lab.
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#ifndef __EXTIO_HELPERS_H
#define __EXTIO_HELPERS_H

#include <string.h>

#include <spank/types.h>
#include <spank/extio/types.h>


/*
 * For now implementation between spank types and spank_extio types
 * are a direct mapping
 */


/*======================================================================*
 * ExtIO -> Native                                                      *
 *======================================================================*/

static inline void
spank_position_from_extio_to_native(
	spank_extio_position_t *extio,
	spank_position_t       *native)
{
    _Static_assert(sizeof(*extio) == sizeof(*native),
		   "extio/native position structure sizes differ");
    memcpy(native, extio, sizeof(*extio));
}

static inline void
spank_velocity_from_extio_to_native(
	spank_extio_velocity_t *extio,
	spank_velocity_t       *native)
{
    _Static_assert(sizeof(*extio) == sizeof(*native),
		   "extio/native velocity structure sizes differ");
    memcpy(native, extio, sizeof(*extio));
}


static inline void
spank_stddev_xyz_from_extio_to_native(
	spank_extio_stddev_xyz_t *extio,
	spank_stddev_xyz_t       *native)
{
    _Static_assert(sizeof(*extio) == sizeof(*native),
		   "extio/native stddev_xyz structure sizes differ");
    memcpy(native, extio, sizeof(*extio));
}

static inline void
spank_posture_from_extio_to_native(
	spank_extio_posture_t *extio,
	spank_posture_t       *native)
{
    _Static_assert(sizeof(*extio) == sizeof(*native),
		   "extio/native posture structure sizes differ");
    memcpy(native, extio, sizeof(*extio));

#if 0
    spank_position_from_extio_to_native(
				&extio ->position.value,
				&native->position.value);
    spank_stddev_xyz_from_extio_to_native(
				&extio ->position.stddev,
				&native->position.stddev);
    spank_velocity_from_extio_to_native(
				&extio ->velocity.value,
				&native->velocity.value);
    spank_stddev_xyz_from_extio_to_native(
				&extio ->velocity.stddev,
				&native->velocity.stddev);
#endif
}



/*======================================================================*
 * Native -> ExtIO                                                      *
 *======================================================================*/

static inline void
spank_position_from_native_to_extio(
	spank_position_t       *native,
	spank_extio_position_t *extio)
{
    _Static_assert(sizeof(*extio) == sizeof(*native),
		   "extio/native position structure sizes differ");
    memcpy(extio, native, sizeof(*extio));
}

static inline void
spank_velocity_from_native_to_extio(
	spank_velocity_t       *native,
	spank_extio_velocity_t *extio)
{
    _Static_assert(sizeof(*extio) == sizeof(*native),
		   "extio/native velocity structure sizes differ");
    memcpy(extio, native, sizeof(*extio));
}

static inline void
spank_stddev_xyz_from_native_to_extio(
	spank_stddev_xyz_t       *native,
	spank_extio_stddev_xyz_t *extio)
{
    _Static_assert(sizeof(*extio) == sizeof(*native),
		   "extio/native sddev_xyz structure sizes differ");
    memcpy(extio, native, sizeof(*extio));
}

static inline void
spank_posture_from_native_to_extio(
	spank_posture_t       *native,
	spank_extio_posture_t *extio)
{
    _Static_assert(sizeof(*extio) == sizeof(*native),
		   "extio/native posture structure sizes differ");
    memcpy(extio, native, sizeof(*extio));

#if 0
    spank_position_from_native_to_extio(
				&native->position.value,
				&extio ->position.value);
    spank_stddev_xyz_from_native_to_extio(
				&native->position.stddev,
				&extio ->position.stddev);
    spank_velocity_from_native_to_extio(
				&native->velocity.value,
				&extio ->velocity.value);
    spank_stddev_xyz_from_native_to_extio(
				&native->velocity.stddev,
				&extio ->velocity.stddev);
#endif
}






static inline void
spank_distance_measurement_from_native_to_extio(
	spank_uav_distance_measurement_t   *native,
	spank_extio_distance_measurement_t *extio)
{
    extio->id        = native->id;
    extio->distance  = native->distance;
    extio->stddev    = native->stddev;
    extio->freshness = (((spank_systime() - native->time) * 1000) +
			(SPANK_SYSTICKS_PER_SECOND - 1)
			) / SPANK_SYSTICKS_PER_SECOND;
    spank_posture_from_native_to_extio(&native->posture, &extio->posture);
}

static inline void
spank_distance_measurement_extra_from_native_to_extio(
	spank_uav_distance_measurement_extra_t   *native,
	spank_extio_distance_measurement_extra_t *extio)
{
    memcpy(&extio->power.firstpath,         &native->power.firstpath,
	   sizeof(double));
    memcpy(&extio->power.signal,            &native->power.signal,
	   sizeof(double));
    memcpy(&extio->clock_tracking.offset,   &native->clock_tracking.offset,
	   sizeof(int32_t));
    memcpy(&extio->clock_tracking.interval, &native->clock_tracking.interval,
	   sizeof(uint32_t));
    memcpy(&extio->chip_info.temp,          &native->chip_info.temp,
	   sizeof(uint16_t));
    memcpy(&extio->chip_info.vbat,          &native->chip_info.vbat,
	   sizeof(uint16_t));
}

#endif
