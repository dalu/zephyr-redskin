/*
 * Copyright (c) 2021
 * Stephane D'Alu, Inria Chroma / Inria Agora, INSA Lyon, CITI Lab.
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#ifndef __EXTIO__USB__H
#define __EXTIO__USB__H

/**
 * Initialize USB part
 */
int extio_usb_init(void);

#endif
