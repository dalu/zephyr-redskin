/*
 * Copyright (c) 2021-2022
 * Stephane D'Alu, Inria Chroma / Inria Agora, INSA Lyon, CITI Lab.
 *
 * SPDX-License-Identifier: Apache-2.0
 */

/* SEE:
 *  - https://www.beyondlogic.org/usbnutshell/usb4.shtml
 *
 * NOTES:
 *  - endpoint numbering is interface independent
 *  - doesn't seems possible to printf/LOG_ from extio_usb_vendor_handler
 *  - NRF52840 is USB 12Mb/s full speed
 *      - 14 bulk/interrupt (7 IN, 7 OUT)   packet maxsize   64 bytes
 *      -  2 isochronous    (1 IN, 1 OUT)   packet maxsize 1023 bytes
 *      -  1 frame = 1 ms
 */


#include <zephyr/kernel.h>
#include <zephyr/logging/log.h>
LOG_MODULE_DECLARE(extio, CONFIG_REDSKIN_EXTIO_LOG_LEVEL);
#if defined(CONFIG_REDSKIN_SHELL)
#include <zephyr/shell/shell.h>
#endif

#include <stdbool.h>
#include <zephyr/sys/byteorder.h>
#include <zephyr/usb/usb_device.h>
#include <zephyr/usb/usb_ch9.h>

#include <info-str/common.h>

#include <spank/osal.h>
#include <spank/types.h>
#include <spank/spank.h>
#include <spank/uav.h>
#include <spank/uav-puppet.h>
#include <spank/version.h>
#include <spank/extio/types.h>
#include <spank/extio/usb.h>

#include "../extio.h"
#include "../extio-helpers.h"
#include "extio.h"
#include "../sniffer.h"


#if (SPANK_USB_FLG_CONFIG_REPORT_DATA_READY & SPANK_EXTIO_EVENT_ALL)
#error The SPANK_USB_FLG_CONFIG_REPORT_DATA_READY should not be part of SPANK_EXTIO_EVENT_ALL
#endif


/*======================================================================*/
/* Forward declaration                                                  */
/*======================================================================*/

struct extio_usb_ctx;



/*======================================================================*/
/* Macros                                                               */
/*======================================================================*/

#define EXTIO_USB_INT_EP_DATAREADY_ADDR					\
    (SPANK_USB_INT_EP_DATAREADY | USB_CONTROL_EP_IN)
#define EXTIO_USB_INT_EP_EVENTS_ADDR					\
    (SPANK_USB_INT_EP_EVENTS    | USB_CONTROL_EP_IN)
#define EXTIO_USB_BULK_EP_PACKETS_ADDR					\
    (SPANK_USB_BULK_EP_PACKETS  | USB_CONTROL_EP_IN)

#define EXTIO_USB_INT_EP_DATAREADY_MAXSIZE	1
#define EXTIO_USB_INT_EP_EVENTS_MAXSIZE		64

#define EXTIO_USB_IS_ACTIVE(ctx) ((ctx)->configured && !(ctx)->suspended)

#define EXTIO_USB_NEED_INTR_DATA_READY(ctx, flags)			\
    (((ctx)->config.report & SPANK_USB_FLG_CONFIG_REPORT_DATA_READY) &&	\
     ((ctx)->config.report & (flags)))

#define EXTIO_USB_NEED_INTR_EVENT(ctx, flags)				\
    (!((ctx)->config.report & SPANK_USB_FLG_CONFIG_REPORT_DATA_READY) &&\
      ((ctx)->config.report & (flags)))


/*======================================================================*/
/* Local types                                                          */
/*======================================================================*/


/* ExtIO USB Context
 */
struct extio_usb_ctx {
    bool configured;			// USB device configured
    bool suspended;                     // USB device suspended
    
    // USB transfer
    struct {
	atomic_t state;			// Transaction state
#define EXTIO_USB_TRX_STATE_FREE   0
#define EXTIO_USB_TRX_STATE_INUSE  1
	struct {			// Temporary memory for RX/TX
	    uint32_t type;
	    union { 
		struct {
		    spank_extio_distance_measurement_t dm;
		    spank_extio_info_t info;
		} __attribute__((packed)) dm_info;
	    };
	} __attribute__((packed)) pkt_buffer;
    } trx;

    // Data Ready 
    struct {				// Keep track of pending data
	uint8_t flags;
#define EXTIO_USB_FLG_DATA_READY_DISTANCE_MEASUREMENT			\
	SPANK_USB_FLG_DATA_READY_DISTANCE_MEASUREMENT
#define EXTIO_USB_FLG_DATA_READY_CONTROL				\
	SPANK_USB_FLG_DATA_READY_CONTROL
    } data_ready;

    // Configuration
    struct {
	uint8_t report;			// Mask for data-ready interrupt
    } config;
};




/*======================================================================*/
/* Local variables                                                      */
/*======================================================================*/

static struct extio_usb_ctx extio_usb_ctx = {
    .configured = false,
    .suspended  = false,
};


static struct usb_ep_cfg_data extio_usb_ep_data[] = {
    {
	.ep_cb   = usb_transfer_ep_callback,
	.ep_addr = EXTIO_USB_INT_EP_DATAREADY_ADDR,
    },
    {
	.ep_cb   = usb_transfer_ep_callback,
	.ep_addr = EXTIO_USB_INT_EP_EVENTS_ADDR,
    },
#if defined(CONFIG_SPANK_IO_SNIFFER_FRAME)
    {
	.ep_cb   = usb_transfer_ep_callback,
	.ep_addr = EXTIO_USB_BULK_EP_PACKETS_ADDR,
    },
#endif
};

#if 0 // XXX FIXME
#define EXTIO_USB_IF0_NAME "ExtIO"
USBD_STRING_DESCR_USER_DEFINE(primary) struct extio_usb_if0_name {
    uint8_t bLength;
    uint8_t bDescriptorType;
    uint8_t bString[USB_BSTRING_LENGTH(EXTIO_USB_IF0_NAME)];
} __packed extio_usb_if0_name = {
    .bLength         = USB_STRING_DESCRIPTOR_LENGTH(EXTIO_USB_IF0_NAME),
    .bDescriptorType = USB_DESC_STRING,
    .bString         = EXTIO_USB_IF0_NAME,
};
#endif

USBD_CLASS_DESCR_DEFINE(primary, 0) struct {
    struct usb_if_descriptor if0;
    struct usb_ep_descriptor if0_int_in_ep_dataready;
    struct usb_ep_descriptor if0_int_in_ep_events;
#if defined(CONFIG_SPANK_IO_SNIFFER_FRAME)
    struct usb_ep_descriptor if0_bulk_in_ep_packets;
#endif
} __packed extio_usb_desc = {
    /* Interface descriptor 0 */
    .if0 = {
	.bLength            = sizeof(struct usb_if_descriptor),
	.bDescriptorType    = USB_DESC_INTERFACE,
	.bInterfaceNumber   = 0,
	.bAlternateSetting  = 0,
	.bNumEndpoints      = 2
	                  + (IS_ENABLED(CONFIG_SPANK_IO_SNIFFER_FRAME) ? 1 : 0),
	.bInterfaceClass    = USB_BCC_VENDOR,
	.bInterfaceSubClass = 0x01,
	.bInterfaceProtocol = 0,
	.iInterface         = 0,
    },
    .if0_int_in_ep_dataready = {
	.bLength            = sizeof(struct usb_ep_descriptor),
	.bDescriptorType    = USB_DESC_ENDPOINT,
	.bEndpointAddress   = EXTIO_USB_INT_EP_DATAREADY_ADDR,
	.bmAttributes       = USB_DC_EP_INTERRUPT,
	.wMaxPacketSize     = sys_cpu_to_le16(EXTIO_USB_INT_EP_DATAREADY_MAXSIZE),
	.bInterval          = 10,
    },
    .if0_int_in_ep_events = {
	.bLength            = sizeof(struct usb_ep_descriptor),
	.bDescriptorType    = USB_DESC_ENDPOINT,
	.bEndpointAddress   = EXTIO_USB_INT_EP_EVENTS_ADDR,
	.bmAttributes       = USB_DC_EP_INTERRUPT,
	.wMaxPacketSize     = sys_cpu_to_le16(EXTIO_USB_INT_EP_EVENTS_MAXSIZE),
	.bInterval          = 10,
    },
#if defined(CONFIG_SPANK_IO_SNIFFER_FRAME)
    .if0_bulk_in_ep_packets = {
	.bLength            = sizeof(struct usb_ep_descriptor),
	.bDescriptorType    = USB_DESC_ENDPOINT,
	.bEndpointAddress   = EXTIO_USB_BULK_EP_PACKETS_ADDR,
	.bmAttributes       = USB_DC_EP_BULK,
	.wMaxPacketSize     = sys_cpu_to_le16(64),
    },
#endif
};


BUILD_ASSERT(EXTIO_USB_INT_EP_DATAREADY_MAXSIZE >=
	     sizeof(extio_usb_ctx.data_ready.flags),
	     "wMaxPacketSize for Data Ready EP is too small");
BUILD_ASSERT(EXTIO_USB_INT_EP_EVENTS_MAXSIZE >=
	     sizeof(extio_usb_ctx.trx.pkt_buffer),
	     "wMaxPacketSize for Events EP is too small");


/*======================================================================*/
/* Local functions                                                      */
/*======================================================================*/

#if defined(CONFIG_REDSKIN_SHELL)
static void
extio_usb_shell_info(const struct shell *shell)
{
    shell_print(shell, "USB buffers: %d x %d",
		CONFIG_USB_MAX_NUM_TRANSFERS, CONFIG_USB_REQUEST_BUFFER_SIZE);
    shell_print(shell, "USB Intr EP: data-ready=%d (%d), events=%d (%d)",
		sizeof(extio_usb_ctx.data_ready.flags),
		EXTIO_USB_INT_EP_DATAREADY_MAXSIZE,
		sizeof(extio_usb_ctx.trx.pkt_buffer),
		EXTIO_USB_INT_EP_EVENTS_MAXSIZE);
}
#endif


static void
extio_usb_release_noop(uint8_t ep, int tsize, void *priv) {
}

static void
extio_usb_release_trx_pkt_buffer(uint8_t ep, int tsize, void *priv) {
    struct extio_usb_ctx *ctx = &extio_usb_ctx;

    atomic_set(&ctx->trx.state, EXTIO_USB_TRX_STATE_FREE);
}


/* Set data ready line (if necessary).
 */
static inline void
extio_usb_set_data_ready(struct extio_usb_ctx *ctx, int flags)
{
    ctx->data_ready.flags |= flags;

    if ((ctx->config.report & SPANK_USB_FLG_CONFIG_REPORT_DATA_READY) &&
	(ctx->config.report & flags)) {
	// Initiate USB transfer
	usb_transfer(EXTIO_USB_INT_EP_DATAREADY_ADDR,
		     &ctx->data_ready.flags,
		     sizeof(ctx->data_ready.flags),
		     USB_TRANS_WRITE | USB_TRANS_NO_ZLP,
		     extio_usb_release_noop, NULL);
    }
}



/* Clear data ready line (if necessary)
 */
static inline void
extio_usb_clear_data_ready(struct extio_usb_ctx *ctx, int flags)
{
    ctx->data_ready.flags &= ~flags;
}



/* Get the UAV behaviour
 */
static inline int
extio_usb_req_get_config_behaviour(int32_t *len, uint8_t **data)
{
    struct extio_usb_ctx *ctx = &extio_usb_ctx;
    uint8_t *extio_behaviour  = (void *)(*data);

    // Sanity check
    BUILD_ASSERT(CONFIG_USB_REQUEST_BUFFER_SIZE >= sizeof(*extio_behaviour),
		 "CONFIG_USB_REQUEST_BUFFER_SIZE is too small");

    // Retrieve drop behaviour
    switch (spank_uav_get_drop_behaviour()) {
    case SPANK_UAV_DROP_BEHAVIOUR_DEFAULT:
	*extio_behaviour = SPANK_USB_VAL_CONFIG_BEHAVIOUR_DROP_DEFAULT;
	break;
    case SPANK_UAV_DROP_BEHAVIOUR_OLDEST:
	*extio_behaviour = SPANK_USB_VAL_CONFIG_BEHAVIOUR_DROP_OLDEST;
	break;
    case SPANK_UAV_DROP_BEHAVIOUR_RECENT:
	*extio_behaviour = SPANK_USB_VAL_CONFIG_BEHAVIOUR_DROP_RECENT;
	break;
    case SPANK_UAV_DROP_BEHAVIOUR_RANDOM:
	*extio_behaviour = SPANK_USB_VAL_CONFIG_BEHAVIOUR_DROP_RANDOM;
	break;
    default:
	__ASSERT(0, "unhandled uav drop behaviour case");
    }

    // Job's done
    *len  = sizeof(*extio_behaviour);
    return 0;
}



/* Set the UAV behaviour
 */
static inline int
extio_usb_req_set_config_behaviour(int32_t *len, uint8_t **data)
{
    uint8_t extio_behaviour;
    if (*len != sizeof(extio_behaviour))
	return -EINVAL;
    extio_behaviour = (*data)[0];

    int uav_behaviour;
    switch(extio_behaviour) {
    case SPANK_USB_VAL_CONFIG_BEHAVIOUR_DROP_DEFAULT:
	uav_behaviour = SPANK_UAV_DROP_BEHAVIOUR_DEFAULT;
	break;
    case SPANK_USB_VAL_CONFIG_BEHAVIOUR_DROP_OLDEST:
	uav_behaviour = SPANK_UAV_DROP_BEHAVIOUR_OLDEST;
	break;
    case SPANK_USB_VAL_CONFIG_BEHAVIOUR_DROP_RECENT:
	uav_behaviour = SPANK_UAV_DROP_BEHAVIOUR_RECENT;
	break;
    case SPANK_USB_VAL_CONFIG_BEHAVIOUR_DROP_RANDOM:
	uav_behaviour = SPANK_UAV_DROP_BEHAVIOUR_RANDOM;
	break;
    default:
	LOG_WRN("unknown behaviour requested (%d)", extio_behaviour);
	return -EINVAL;
    }
    
    spank_uav_set_drop_behaviour(uav_behaviour);
    return 0;
}

static inline int
extio_usb_req_get_config_report(int32_t *len, uint8_t **data)
{
    struct extio_usb_ctx *ctx = &extio_usb_ctx;

    *len  = sizeof(ctx->config.report);
    *data = &ctx->config.report;
    return 0;
}

static inline int
extio_usb_req_set_config_report(int32_t *len, uint8_t **data)
{
    struct extio_usb_ctx *ctx = &extio_usb_ctx;

    if (*len != sizeof(ctx->config.report))
	return -EINVAL;
    
    ctx->config.report = (*data)[0];
    return 0;
}

static inline int
extio_usb_req_get_data_ready(int32_t *len, uint8_t **data)
{
    struct extio_usb_ctx *ctx = &extio_usb_ctx;

    *len  = sizeof(ctx->data_ready.flags);
    *data = &ctx->data_ready.flags;
    return 0;
}



/* Get UAV distance measurement (and perform format conversion)
 */
static inline int
extio_usb_req_get_distance_measurement(int32_t *len, uint8_t **data,
				       uint16_t with)
{
    struct extio_usb_ctx                     *ctx        = &extio_usb_ctx;
    char                                     *ptr        = (char *)(*data);
    spank_extio_distance_measurement_t       *extio_dm   = (void *)ptr;
    
    // Sanity check
    BUILD_ASSERT(CONFIG_USB_REQUEST_BUFFER_SIZE >=
	     (sizeof(spank_extio_distance_measurement_t      ) +
	      sizeof(spank_extio_distance_measurement_extra_t) +
	      sizeof(spank_extio_info_t                      )),
		 "CONFIG_USB_REQUEST_BUFFER_SIZE is too small");

    // Retrieve distance measure from UAV (uav-puppet specific)
    spank_uav_metainfo_t uav_info;
    spank_uav_distance_measurement_t *uav_dm =
	spank_uav_get_distance_measurement(&uav_info);

    // If no distance available, returns an error.
    // (we differ here from the I2C flavor which will return 0xff-filled data)
    if (uav_dm == NULL) {
	return -EINVAL;
    }
    
    // Perform conversion
    spank_distance_measurement_from_native_to_extio(uav_dm, extio_dm);
    ptr += sizeof(*extio_dm);
    
    // If no more data, clear the data ready flag
    if (! uav_dm->has_more) {
	extio_usb_clear_data_ready(ctx,
			   EXTIO_USB_FLG_DATA_READY_DISTANCE_MEASUREMENT);
    }

    // If user requested extra information
    if (with & SPANK_USB_OPT_DISTANCE_MEASUREMENT_EXTRA) {
	spank_extio_distance_measurement_extra_t *extio_extra = (void *)ptr;
	ptr += sizeof(*extio_extra);
	spank_distance_measurement_extra_from_native_to_extio(
					      &uav_dm->extra, extio_extra);
    }
    // If user requested piggy-back information
    if (with & SPANK_USB_OPT_DISTANCE_MEASUREMENT_INFO) {
	spank_extio_info_t *extio_info = (void *) ptr;
	ptr += sizeof(*extio_info);
	extio_info->pending = uav_info.pending;
	extio_info->lost    = uav_info.lost;
    }
	
    // Release distance 
    spank_uav_release_distance_measurement(uav_dm);
	
    // Job's done
    *len = (char*)ptr - (char*)(*data);

    return 0;
}



/* Get the UAV posture (and perform format conversion)
 */
static inline int
extio_usb_req_get_posture(int32_t *len, uint8_t **data)
{
    spank_posture_t        posture;
    spank_extio_posture_t *extio_posture = (void *)(*data);

    // Sanity check
    BUILD_ASSERT(CONFIG_USB_REQUEST_BUFFER_SIZE >= sizeof(*extio_posture),
		 "CONFIG_USB_REQUEST_BUFFER_SIZE is too small");

    // Retrieve posture
    spank_uav_get_posture(&posture);
    spank_posture_from_native_to_extio(&posture, extio_posture);

    // Job's done
    *len = sizeof(*extio_posture);
    return 0;
}



/* Set the UAV posture (and perform format conversion)
 */
static inline int
extio_usb_req_set_posture(int32_t *len, uint8_t **data)
{
    if (*len != sizeof(spank_extio_posture_t))
	return -EINVAL;
    spank_posture_t posture = { 0 };
    spank_extio_posture_t *extio_posture = (spank_extio_posture_t *)*data;
    spank_posture_from_extio_to_native(extio_posture, &posture);
    spank_uav_set_posture(&posture);
    return 0;
}


#if defined(CONFIG_SPANK_IO_SNIFFER_FRAME)
/* Get frame sniffer
 */
static inline int
extio_usb_req_get_frame_sniffer(int32_t *len, uint8_t **data)
{
    uint8_t *sniffer  = (void *)(*data);
    
    *sniffer = (spank_sniffer(SPANK_SNIFFER_FRAME, SPANK_SNIFFER_STATUS) ==
		SPANK_SNIFFER_ENABLE)
	     ? SPANK_USB_VAL_FRAME_SNIFFER_ENABLED
	     : SPANK_USB_VAL_FRAME_SNIFFER_DISABLED;
    *len     = sizeof(*sniffer);
    return 0;
}


/* Get frame sniffer
 */
static inline int
extio_usb_req_set_frame_sniffer(int32_t *len, uint8_t **data)
{
    uint8_t extio_sniffer;
    if (*len != sizeof(extio_sniffer))
	return -EINVAL;
    extio_sniffer = (*data)[0];

    switch(extio_sniffer) {
    case SPANK_USB_VAL_FRAME_SNIFFER_DISABLED:
	// Disable sniffer
	spank_sniffer(SPANK_SNIFFER_FRAME, SPANK_SNIFFER_DISABLE);
	break;
	
    case SPANK_USB_VAL_FRAME_SNIFFER_ENABLED:
	// Ensure USB output mode is selected
	{
	  uint16_t output = sniffer_frame_get_output();
	  sniffer_frame_set_output(output | SNIFFER_FRAME_OUTPUT_EXTIO_USB);
	}

	// Enable sniffer
	spank_sniffer(SPANK_SNIFFER_FRAME, SPANK_SNIFFER_ENABLE);
	break;
	
    default:
	LOG_WRN("unknown sniffer setting (%d)", extio_sniffer);
	return -EINVAL;
    }

    return 0;
}
#endif


/* Get app version
 */
static inline int
extio_usb_req_get_version(int32_t *len, uint8_t **data)
{
    static char version[] = INFO_STR_APP_VERSION;
    *len  = sizeof(version);
    *data = version;
    return 0;
}



/* Get spank version
 */
static inline int
extio_usb_req_get_spank_version(int32_t *len, uint8_t **data)
{
    static char version[] = SPANK_VERSION;
    *len  = sizeof(version);
    *data = version;
    return 0;
}




/**
 * @brief Handler called for vendor specific commands.
 *
 * @param pSetup    Information about the request to execute.
 * @param len       Size of the buffer.
 * @param data      Buffer containing the request result.
 *
 * @return  0 on success, negative errno code on fail.
 */
static int
extio_usb_vendor_handler(struct usb_setup_packet *setup,
			    int32_t *len, uint8_t **data)
{
    // This handler is called for every interface having registered it,
    // the first returning 0 stop the lookup.
    
    // Only process request for our interface
    if ((USB_REQTYPE_GET_RECIPIENT(setup->bmRequestType) !=
	 USB_REQTYPE_RECIPIENT_INTERFACE) ||
        (extio_usb_desc.if0.bInterfaceNumber  != (setup->wIndex & 0xFF)))
        return -ENOTSUP;
    
    // Dispatch set/get requests
    switch(USB_REQTYPE_GET_DIR(setup->bmRequestType)) {
    case USB_REQTYPE_DIR_TO_HOST:
	switch(setup->bRequest) {
	case SPANK_USB_REQ_CONFIG_BEHAVIOUR:
	    return extio_usb_req_get_config_behaviour(len, data);

	case SPANK_USB_REQ_CONFIG_REPORT:
	    return extio_usb_req_get_config_report(len, data);
	    
	case SPANK_USB_REQ_DATA_READY:
	    return extio_usb_req_get_data_ready(len, data);
	    
	case SPANK_USB_REQ_POSTURE:
	    return extio_usb_req_get_posture(len, data);

	case SPANK_USB_REQ_DISTANCE_MEASUREMENT: {
	    return extio_usb_req_get_distance_measurement(len, data,
							  setup->wValue);
	}

#if defined(CONFIG_SPANK_IO_SNIFFER_FRAME)
	case SPANK_USB_REQ_FRAME_SNIFFER: 
	    return extio_usb_req_get_frame_sniffer(len, data);
#endif

	case SPANK_USB_REQ_VERSION: 
	    return extio_usb_req_get_version(len, data);

	case SPANK_USB_REQ_SPANK_VERSION:
	    return extio_usb_req_get_spank_version(len, data);

	}
	break;

    case USB_REQTYPE_DIR_TO_DEVICE:
	switch(setup->bRequest) {
	case SPANK_USB_REQ_CONFIG_REPORT:
	    return extio_usb_req_set_config_report(len, data);

	case SPANK_USB_REQ_CONFIG_BEHAVIOUR:
	    return extio_usb_req_set_config_behaviour(len, data);

	case SPANK_USB_REQ_POSTURE:
	    return extio_usb_req_set_posture(len, data);

#if defined(CONFIG_SPANK_IO_SNIFFER_FRAME)
	case SPANK_USB_REQ_FRAME_SNIFFER:
	    return extio_usb_req_set_frame_sniffer(len, data);
#endif
	}
	break;
    }

    // Request not supported
    return -ENOTSUP;
}






static void
extio_usb_interface_config(struct usb_desc_header *head,
			   uint8_t bInterfaceNumber)
{
	ARG_UNUSED(head);
#if 0  // XXX FIXME
	extio_usb_desc.if0.iInterface =
	    usb_get_str_descriptor_idx(&extio_usb_if0_name);
#endif
	
	extio_usb_desc.if0.bInterfaceNumber = bInterfaceNumber;
}




/**
 * @brief Callback used to know the USB connection status
 *
 * @param status USB device status code.
 *
 * @return  N/A.
 */
static void
extio_usb_dev_status_cb(struct usb_cfg_data *cfg,
			  enum usb_dc_status_code status,
			  const uint8_t *param)
{
    ARG_UNUSED(param);
    ARG_UNUSED(cfg);

    struct extio_usb_ctx *ctx = &extio_usb_ctx;

    /* Check the USB status and do needed action if required */
    switch (status) {
    // USB error reported by the controller
    case USB_DC_ERROR:
	LOG_DBG("USB device error");
	break;
	
    // USB reset
    case USB_DC_RESET:
	LOG_DBG("USB device reset detected");
	ctx->configured = false;
	ctx->suspended  = false;
	break;

    // USB connection established, hardware enumeration is completed
    case USB_DC_CONNECTED:
	LOG_DBG("USB device connected");
	break;

    // USB configuration done
    case USB_DC_CONFIGURED:
	LOG_DBG("USB device configured");
	if (! ctx->configured) {
	    ctx->configured = true;
	    LOG_INF("USB ExtIO is now active");
	    // start
	}
	break;

    // USB connection lost
    case USB_DC_DISCONNECTED:
	LOG_DBG("USB device disconnected");
	for (int i = 0 ; i < ARRAY_SIZE(extio_usb_ep_data) ; i++) {
	    usb_cancel_transfer(extio_usb_ep_data[i].ep_addr);
	}
	ctx->configured = false;
	ctx->suspended  = false;
	break;

    // USB connection suspended by the HOST
    case USB_DC_SUSPEND:
	LOG_DBG("USB device suspended");
	ctx->suspended  = true;
	break;

    // USB connection resumed by the HOST
    case USB_DC_RESUME:
	if (ctx->suspended) {
	    ctx->suspended = false;
	    LOG_DBG("USB device resumed");
	    if (ctx->configured) {
		// start
	    }
	} else {
	    LOG_DBG("USB spurious resume event");
	}
	break;

    // USB interface selected
    case USB_DC_INTERFACE:
	LOG_DBG("USB interface selected");
	break;
	
    // Set Feature ENDPOINT_HALT received 
    case USB_DC_SET_HALT:
	break;

    // Clear Feature ENDPOINT_HALT received
    case USB_DC_CLEAR_HALT:
	break;
	    
    // Start of Frame received
    case USB_DC_SOF:
	break;
	
    // Initial USB connection status
    case USB_DC_UNKNOWN:
	LOG_DBG("USB unknown state");
	break;
	
    default:
	LOG_DBG("USB unknown state");
	break;
    }
}


USBD_DEFINE_CFG_DATA(extio_usb_config) = {
    .interface_config         = extio_usb_interface_config,
    .interface_descriptor     = &extio_usb_desc.if0,
    .cb_usb_status            = extio_usb_dev_status_cb,
    .interface.vendor_handler = extio_usb_vendor_handler,
    .num_endpoints            = ARRAY_SIZE(extio_usb_ep_data),
    .endpoint                 = extio_usb_ep_data,
};



static void
extio_usb_distance_ready_cb(void *args)
{
    struct extio_usb_ctx *ctx = &extio_usb_ctx;

    // Ensure USB is active
    if (! EXTIO_USB_IS_ACTIVE(ctx)) {
	return;
    }

    // In case of generated event interrupt, the data-ready
    // register is not set as the event would be immediately consumed.
    if (! EXTIO_USB_NEED_INTR_EVENT(ctx,
			  EXTIO_USB_FLG_DATA_READY_DISTANCE_MEASUREMENT)) {
	extio_usb_set_data_ready(ctx,
				 EXTIO_USB_FLG_DATA_READY_DISTANCE_MEASUREMENT);
	return;
    }
	   
    // Retrieve distance measure from UAV (uav-puppet specific)
    for (bool has_more = true ; has_more ; ) {
	spank_uav_metainfo_t uav_info;
	spank_uav_distance_measurement_t *uav_dm =
	    spank_uav_get_distance_measurement(&uav_info);

	// In case another ExtIO flavor stole the data and none are left
	if (uav_dm == NULL)
	    break;

	// More data available?
	has_more = uav_dm->has_more;

	// If a transfer packet buffer is available, send it!
	if (atomic_cas(&ctx->trx.state,
		       EXTIO_USB_TRX_STATE_FREE, EXTIO_USB_TRX_STATE_INUSE)) {
	    // Packet type
	    ctx->trx.pkt_buffer.type = SPANK_EXTIO_EVENT_DISTANCE_MEASUREMENT;

	    // Perform conversion
	    spank_distance_measurement_from_native_to_extio(
				    uav_dm, &ctx->trx.pkt_buffer.dm_info.dm);
	    ctx->trx.pkt_buffer.dm_info.info.lost    = uav_info.lost;
	    ctx->trx.pkt_buffer.dm_info.info.pending = uav_info.pending;

	    // Release distance 
	    spank_uav_release_distance_measurement(uav_dm);

	    // Initiate USB transfer
	    int rc = usb_transfer(EXTIO_USB_INT_EP_EVENTS_ADDR,
				  (uint8_t *)&ctx->trx.pkt_buffer.dm_info,
				  sizeof(ctx->trx.pkt_buffer.dm_info),
				  USB_TRANS_WRITE | USB_TRANS_NO_ZLP,
				  extio_usb_release_trx_pkt_buffer, NULL);
	    if (rc < 0) {
		atomic_set(&ctx->trx.state, EXTIO_USB_TRX_STATE_FREE);
	    }
	}
	
        // Otherwise discard data
	else {
	    // Release distance 
	    spank_uav_release_distance_measurement(uav_dm);
	    // Warn
	    LOG_WRN("trx.pkt_buffer unavailable, interrupt transfer skipped");
	}
    }

    // Remove data ready flag for distance measurement
    // (in case reporting has been switched after data ready)
    extio_usb_clear_data_ready(ctx,
			       EXTIO_USB_FLG_DATA_READY_DISTANCE_MEASUREMENT);
}




/*======================================================================*/
/* Flavor definition                                                    */
/*======================================================================*/

static struct extio_flavor extio_usb_flavor = {
    .name              = "USB",
    .distance_ready_cb = extio_usb_distance_ready_cb,
#if defined(CONFIG_REDSKIN_SHELL)
    .shell.name        = "usb",
    .shell.info        = extio_usb_shell_info,
    .shell.cmd         = NULL,
#endif
};




/*======================================================================*/
/* Exported functions                                                   */
/*======================================================================*/

/**
 * Initialize the USB implementation for ExtIO.
 */
int
extio_usb_init(void)
{
    // Not much to do, as USB is automatically initialized
    // from USBD_* macros
    
    // Register
    extio_register_flavor(EXTIO_FLAVOR_USB, &extio_usb_flavor);
    
    // Job's done
    return 0;
}
