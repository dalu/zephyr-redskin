/*
 * Copyright (c) 2019-2020
 * Stephane D'Alu, Inria Chroma / Inria Agora, INSA Lyon, CITI Lab.
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#include <zephyr/kernel.h>
#include <zephyr/logging/log.h>
#include <stdlib.h>

#include "redskin.h"

#if defined(CONFIG_USB_DEVICE_STACK)
#include <zephyr/usb/usb_device.h>
#endif

#if defined(CONFIG_REDSKIN_EXTIO)
#include "extio.h"
#if defined(CONFIG_REDSKIN_EXTIO_I2C)
#include "i2c-slave/extio.h"
#endif
#if defined(CONFIG_REDSKIN_EXTIO_USB)
#include "usb/extio.h"
#endif
#endif

#if defined(CONFIG_REDSKIN_ACCELEROMETER)
#include "accelerometer.h"
#endif

#if defined(CONFIG_REDSKIN_UI_MINIMAL)
#include "ui.h"
#endif
#if defined(CONFIG_REDSKIN_UI_GRAPHICAL)
#include "gui/init.h"
#endif

LOG_MODULE_REGISTER(main);


int
main(void)
{
    /* Try to guard against stack overflow due to no optimizations */
#if defined(CONFIG_NO_OPTIMIZATIONS)
    _Static_assert(CONFIG_SYSTEM_WORKQUEUE_STACK_SIZE >= 2048);
#endif

    /* Start USB stack */
#if defined(CONFIG_USB_DEVICE_STACK)
    int usb_is_enabled = 1;
    if (usb_enable(NULL) < 0) {
	usb_is_enabled = 0;
	LOG_ERR("Failed to enable USB stack");
    }
#endif

    /* User interface */
#if defined(CONFIG_REDSKIN_UI_MINIMAL)
    if (ui_init() < 0) {
        LOG_ERR("failed to start ui");
    }
#endif

#if defined(CONFIG_REDSKIN_UI_GRAPHICAL)
    if (gui_init() < 0) {
        LOG_ERR("failed to start gui");
    }
#endif
    
    /* Redksin / SPANK */
    if (redskin_init() < 0) {
        LOG_ERR("failed to start redskin");
    }

    /* ExtIO support */
#if defined(CONFIG_REDSKIN_EXTIO)
    extio_init();
#if defined(CONFIG_REDSKIN_EXTIO_I2C)
    if (extio_i2c_init() < 0) {
        LOG_ERR("failed to initialize ExtIO I2C");
    }
#endif
#if defined(CONFIG_REDSKIN_EXTIO_USB)
    if (usb_is_enabled) {
	if (extio_usb_init() < 0) {
	    LOG_ERR("failed to initialize ExtIO USB");
	}
    } else {
	LOG_WRN("ExtIO USB not enabled as USB stack failed");
    }
#endif
#endif

    /* Switch main thread to low priority */
    k_thread_priority_set(k_current_get(), CONFIG_NUM_PREEMPT_PRIORITIES - 1);

    /* Accelerometer */
#if defined(CONFIG_REDSKIN_ACCELEROMETER)
    accelerometer_init();
    accelerometer_config_sampling(1, 0);
    accelerometer_config_slope(10, 10);
#endif

    return 0;
}
