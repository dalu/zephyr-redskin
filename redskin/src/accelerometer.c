/*
 * Copyright (c) 2019-2020,2023
 * Stephane D'Alu, Inria Chroma / Inria Agora, INSA Lyon, CITI Lab.
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#include <stdlib.h>
#include <errno.h>
#include <math.h>
#include <stdio.h>

#include <zephyr/logging/log.h>
#include <zephyr/device.h>
#include <zephyr/drivers/sensor.h>

#if defined(CONFIG_REDSKIN_UI_MINIMAL)
#include "ui.h"
#endif

LOG_MODULE_DECLARE(main);


/*
 * Retrieve device tree information.
 */
#define ACCELEROMETER DT_CHOSEN(spank_accelerometer)


#if DT_HAS_CHOSEN(spank_accelerometer)
static const struct device *accel = DEVICE_DT_GET(ACCELEROMETER);
#endif

int
accelerometer_init(void)
{
#if DT_HAS_CHOSEN(spank_accelerometer)
    // Get sensor device
    if (accel == NULL) {
	LOG_ERR("Device " DEVICE_DT_NAME(ACCELEROMETER) " not found");
	return -ENODEV;
    }
    if (! device_is_ready(accel)) {
	LOG_ERR("Device " DEVICE_DT_NAME(ACCELEROMETER) " is not ready");
        return -ENODEV;
    }

    // Done
    return 0;
#else
    return -ENODEV;
#endif
}


/*
 * LIS2DH
 * Supported sampling : 1, 10, 25, 50, 100, 200, 400, 1620, 1344, 5376
 * Supported fullscale: 2 4 8 16
 */
int
accelerometer_config_sampling(uint16_t frequency, uint16_t fullscale)
{
#if DT_HAS_CHOSEN(spank_accelerometer)
    /* Sanity check */
    switch(frequency) {
    case    0:
    case    1: case   10: case   25: case   50: case  100:
    case  200: case  400: case 1620: case 1344: case 5376:
	break;
    default:
	return -EINVAL;
    }

    switch (fullscale) {
    case  0:
    case  2: case  4: case  8: case 16:
	break;
    default:
	return -EINVAL;
    }

    /* Adjust frequency */
    if (frequency > 0) {
	struct sensor_value odr_attr = { .val1 = frequency, .val2 = 0 };
	if (sensor_attr_set(accel, SENSOR_CHAN_ACCEL_XYZ,
			    SENSOR_ATTR_SAMPLING_FREQUENCY, &odr_attr) < 0) {
	    LOG_WRN("Can't set sampling frequency to %dHZ for %s",
		    frequency, accel->name);
	}
    }

    /* Adjuste fullscale */
    if (fullscale > 0) {
	struct sensor_value fs_attr;
	sensor_g_to_ms2(fullscale, &fs_attr);
	if (sensor_attr_set(accel, SENSOR_CHAN_ACCEL_XYZ,
			    SENSOR_ATTR_FULL_SCALE, &fs_attr) < 0) {
            LOG_WRN("Can't set fullscale to %dg for %s",
		    fullscale, accel->name);
        }
    }

    /* Done */
    return 0;
#else
    return -ENODEV;
#endif
}


int
accelerometer_config_slope(uint32_t threshold, uint32_t duration)
{
#if DT_HAS_CHOSEN(spank_accelerometer)
    /* Configure slope threshold */
    if (threshold > 0) {
	struct sensor_value th_attr = { .val1 = threshold, .val2 = 0 };
	if (sensor_attr_set(accel,
			    SENSOR_CHAN_ACCEL_XYZ,
			    SENSOR_ATTR_SLOPE_TH,
			    &th_attr) < 0) {
	    LOG_WRN("Can't set slope threshold for %s", accel->name);
	}
    }

    /* Configure slope duration */
    if (duration > 0) {
	struct sensor_value dur_attr = { .val1 = duration, .val2 = 0 };
	if (sensor_attr_set(accel,
			    SENSOR_CHAN_ACCEL_XYZ,
			    SENSOR_ATTR_SLOPE_DUR,
			    &dur_attr) < 0) {
	    LOG_WRN("Can't set duration threshold for %s", accel->name);
	}
    }
    
    /* Done */
    return 0;
#else
    return -ENODEV;
#endif
}


int
accelerometer_trigger(sensor_trigger_handler_t handler)
{
#if DT_HAS_CHOSEN(spank_accelerometer)
    /* Configure sensor trigger */
    struct sensor_trigger trig = {
        .type = SENSOR_TRIG_DATA_READY,
	.chan = SENSOR_CHAN_ACCEL_XYZ,
    };    
    if (sensor_trigger_set(accel, &trig, handler)) {
	LOG_ERR("Could not set trigger for %s", accel->name);
	return -1;
    }

    return 0;
#else
    return -ENODEV;
#endif
}


void
accelerometer_trigger_handler(const struct device *dev,
			      struct sensor_trigger *trig)
{
#if DT_HAS_CHOSEN(spank_accelerometer)
    struct sensor_value val[3];
    int32_t ret = 0;

    ret = sensor_sample_fetch(dev);
    if (ret < 0 && ret != -EBADMSG) {
	// data not updated yet
	printf("Sensor sample update error\n");
	return;
    }
    
    
    ret = sensor_channel_get(dev, SENSOR_CHAN_ACCEL_XYZ, val);
    if (ret < 0) {
	printf("Can't read sensor channels\n");
	return;
    }

#if defined(CONFIG_REDSKIN_UI_MINIMAL)
    ui_blink(UI_LED_0, BLINK_ONCE(1));
#endif

    double x = sensor_value_to_double(&val[0]);
    double y = sensor_value_to_double(&val[1]);
    double z = sensor_value_to_double(&val[2]);
    
    printf("|n| = %f\n", sqrt(x*x + y*y + z*z) );    
#endif
}

