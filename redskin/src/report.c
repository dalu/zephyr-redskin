/*
 * Copyright (c) 2019-2020
 * Stephane D'Alu, Inria Chroma / Inria Agora, INSA Lyon, CITI Lab.
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#define SPANK_SYSLOG_PREFIX "SPANK "

#include <spank/spank.h>
#include <spank/syslog.h>
#include <spank/report.h>

#include "report.h"

#if defined(CONFIG_REDSKIN_UI_MINIMAL)
#include "ui.h"
#endif

static volatile uint16_t report_filter = REPORT_FILTER_NONE;


void
report_set_filter(uint16_t filter)
{
    report_filter = filter;
}


uint16_t
report_get_filter(void)
{
    return report_filter;
}


#if defined(CONFIG_REDSKIN_UI_MINIMAL)
/* Helper for led blinking */
static inline void
_report_blink(blink_code_t code)
{
    ui_blink(UI_LED_1, code);
}
#endif


/*
 * Implement the weak symbol (spank_report) used for reporting on
 * internal spank protocol behaviour
 * NOTE: SPANK need to be compiled with SPANK_DEBUG enabled
 */
void
spank_report(int type, void *args)
{
    switch (type) {
    case SPANK_REPORT_TWR_DISTANCE: {
	if (! (report_filter & REPORT_FILTER_TWR_DISTANCE))
	    break;
	spank_report_twr_distance_t *report =
	    (spank_report_twr_distance_t *)args;

	printk("Distance = %4d"
	       " [" SPANK_FMTaddr " -> " SPANK_FMTaddr "]"
	       " <%4d, %4d, %4d>"
	       "\n",
	       report->distance,
	       SPANK_FMT_ADDR(report->from),
	       SPANK_FMT_ADDR(spank_whoami()),
	       report->posture->position.value.x,
	       report->posture->position.value.y,
	       report->posture->position.value.z
	       );
	break;
    }

    case SPANK_REPORT_STATE: {
	spank_report_state_t report = (spank_report_state_t)args;
	
	// Visual reporting is not subject to filtering
#if defined(CONFIG_REDSKIN_UI_MINIMAL)
        switch (report) {
        case SPANK_STOPPED       : _report_blink(BLINK_OFF    ); break;
        case SPANK_LISTENING     : _report_blink(BLINK_ON     ); break;
        case SPANK_ELECTING      : _report_blink(BLINK_ONCE(3)); break;
        case SPANK_RANGE_QUERYING: _report_blink(BLINK_ONCE(1)); break;
        }
#endif

	if (! (report_filter & REPORT_FILTER_STATE))
	    break;

	char *state = "Unknown";
        switch (report) {
        case SPANK_STOPPED       : state = "STOPPED"  ; break;
        case SPANK_LISTENING     : state = "LISTENING"; break;
        case SPANK_ELECTING      : state = "LISTENING"; break;
        case SPANK_RANGE_QUERYING: state = "QUERYING" ; break;
        }
	printk("State = %s\n", state);
        break;
    }
    }
}
